/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.todoclaro.miclaroapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import org.apache.cordova.CordovaActivity;

import java.util.Objects;

public class MainActivity extends CordovaActivity {

    private final String TAG = "MainActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getBoolean("cdvStartInBackground", false)) {
            moveTaskToBack(true);
        }
        loadUrl(launchUrl);
        new Handler().postDelayed(() -> evaluateIntent(getIntent()), 5000);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        evaluateIntent(intent);
    }

    /**
     * Evaluate intent from redirects
     */
    private void evaluateIntent(Intent intent) {
        if (intent != null) {
            try {
                Uri uri = intent.getData();
                if (uri != null) {
                    Log.d(TAG,
                        uri.getScheme() + " : " + uri.getHost() + " : " + uri.getPath());
                    String appScheme = getString(R.string.app_scheme);
                    if (Objects.equals(uri.getScheme(), appScheme)) {
                        processWidgetIntent(intent);
                    } else {
//                        processExternalIntent(intent);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Map intent from widget redirect
     */
    private void processWidgetIntent(Intent intent) {
        Uri uri = intent.getData();
        if (uri != null) {
            String action = uri.getHost();
            openHostInIonic(action);
        }
    }

    /**
     * Map intent from external redirect
     */
    private void processExternalIntent(Intent intent) {
        Uri uriReceived = intent.getData();
        if (uriReceived != null) {
            String path = uriReceived.toString().replace(getString(R.string.mi_claro_web), "");
            openHostInIonic(path);
        }
    }

    private void openHostInIonic(String action) {
        if (action != null) {
            loadUrl("javascript:handleOpenURL('" + action + "');");
        }
    }
}

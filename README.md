# miclaro3-ionic-version

Project creation guide on android and IOS.

For android use the console command: ionic cordova run Android. (then open the project with android Studio).

For IOS it is recommended: ionic cordova prepare iOS (then open the project in XCode)

---------------------------------------------
Note: if you are going to generate the IOS project, you must remember that the firebase files for development and production are different.
---------------------------------------------
Touch id ios fix plugin:
the following code must be replaced in the ios touch id plugin (TouchID.m): 

```
- (void) isAvailable:(CDVInvokedUrlCommand*)command {
  
  if (NSClassFromString(@"LAContext") == NULL) {
      [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR] callbackId:command.callbackId];
      return;
  }
  
  [self.commandDelegate runInBackground:^{
      NSError *error = nil;
      LAContext *laContext = [[LAContext alloc] init];
      if ([laContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
          NSString *biometryType = @"touch";
          if (@available(iOS 11.0, *)) {
              if (laContext.biometryType == LABiometryTypeFaceID) {
                  biometryType = @"face";
              }
          }
          [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:biometryType]
                                      callbackId:command.callbackId];
      } else {
          NSArray *errorKeys = @[@"code", @"localizedDescription"];
          [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:[error dictionaryWithValuesForKeys:errorKeys]]
                                      callbackId:command.callbackId];
      }
  }];
}
```

---------------------------------------------
## Preferences plugin (should be used local)

https://github.com/yornel13/me.apla.cordova.app-preferences-cordova

download and then install

## Url from file to dynamic link
https://miclaro.claropr.com/.well-known/assetlinks.json

import { Injectable } from '@angular/core';
import { UtilsService } from './utils.service';
import { IntentProvider } from './intent.provider';
import { ServicesProvider } from './services.provider';
import { BrowserProvider } from './browser.provider';
import { pages } from '../utils/const/pages';
import { AlertProvider } from './alert.provider';
import { RedirectPathEnum } from '../utils/const/redirect-path.enum';
import { APP } from '../utils/const/appConstants';
import { Utils } from '../utils/utils';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class RedirectProvider {

    constructor(
        public router: Router,
        private alertProvider: AlertProvider,
        private utilsService: UtilsService,
        private userStorage: IntentProvider,
        private modelsServices: ServicesProvider,
        private browserProvider: BrowserProvider) {
    }

    /**
     * This method check if a redirect is waiting and then sends user to that module
     */
    checkIfRedirectIsWaiting() {
        if (this.userStorage.redirectData) {
            this.redirect(this.userStorage.redirectData.url);
        }
    }

    redirect(url: string, fromChat = false) {
        const host = url.replace(APP.SCHEME, '');
        console.log(`postponing redirect to: ${host}`);
        this.userStorage.redirectData = {url};
        if (this.userStorage.isAppReady) {
            if (url.includes('recover-password?')) {
                this.recoverProcess(url);
            } else {
                this.manageRedirects(host, fromChat);
            }
        }
    }

    manageRedirects(path: string, fromChat = false) {
        if (path === RedirectPathEnum.LOGIN) {
            // GO TO LOGIN
            this.userStorage.redirectData = undefined;
        } else if (path === RedirectPathEnum.CHAT || path === RedirectPathEnum.WIDGET_CHAT) {
            this.redirectToChat();
        } else if (path === RedirectPathEnum.HOME) {
            // GO HOME
            if (fromChat) {
                this.redirectToHomeFromChat();
            } else {
                this.redirectToHome();
            }
            this.userStorage.redirectData = undefined;
        } else if (path === RedirectPathEnum.INVOICE_SUMMARY || path === RedirectPathEnum.WIDGET_PAY || path === RedirectPathEnum.INVOICE_SUMMARY_BILLS) {
            this.redirectToInvoiceSummary();
        } else if (path === RedirectPathEnum.INVOICE_DOWNLOAD) {
            // Descarga tu Factura / postpago - fijo
            this.redirectToInvoiceDownload();
        } else if (path === RedirectPathEnum.PAYMENT_HISTORY) {
            // Historial de pagos / postpago - fijo
            this.redirectToInvoiceHistory();
        } else if (path === RedirectPathEnum.ELECTRONIC_BILL) {
            // Factura electronica / postpago - fijo
            this.redirectToElectronicBill();
        } else if (path === RedirectPathEnum.DIRECT_DEBIT) {
            // Debito Directo / postpago
            this.redirectToDirectDebit();
        } else if (path === RedirectPathEnum.CONSUMPTION) {
            // Mi consumo
            this.redirectToConsumption();
        } else if (path === RedirectPathEnum.DEVICE) {
            // Mis Equipos y Servicios / postpago
            this.redirectToServicesAndEquipment();
        } else if (path === RedirectPathEnum.CHANGE_PLAN) {
            // Cambio de plan
            this.redirectToChangePlan();
        } else if (path === RedirectPathEnum.NETFLIX) {
            // Netflix / postpago - Fijo (Android)
            this.redirectToNetflix();
        } else if (path === RedirectPathEnum.REFERRED) {
            // Programa Refiere y Gana / postpago - Fijo
            this.redirectToReferrer();
        } else if (path === RedirectPathEnum.CLUB) {
            // Removing claro club
            // claro Club / postpago
            // this.redirectToClub();
        } else if (path === RedirectPathEnum.PURCHASES) {
            // Compras / postpago
            this.redirectToPurchases();
        } else if (path === RedirectPathEnum.DATA_PLAN) {
            // Paquetes Adicionales de Datos / postpago
            this.redirectToDataPlan();
        } else if (path === RedirectPathEnum.SHOPPING_HISTORY) {
            // Historial de Compras / postpago
            this.redirectToShoppingHistory();
        } else if (path === RedirectPathEnum.GIFT_ONE_GB) {
            // Regala 1 GB a un Pospago / postpago
            this.redirectToGiftOneGB();
        } else if (path === RedirectPathEnum.GIFT_RECHARGE) {
            // Regala una Recarga Prepago / postpago
            this.redirectToGiftRecharge();
        } else if (path === RedirectPathEnum.NOTIFICATIONS) {
            // Notifications
            this.redirectToNotifications();
        } else if (path === RedirectPathEnum.PROFILE_1 || path === RedirectPathEnum.WIDGET_MY_ACCOUNT) {
            // Mi Perfil - Información Personal
            this.redirectToProfile(1);
        } else if (path === RedirectPathEnum.PROFILE_2) {
            // Mi Perfil - Actualiza Tu Correo
            this.redirectToProfile(2);
        } else if (path === RedirectPathEnum.PROFILE_3) {
            // Mi Perfil - Cambiar Usuario y Contraseña
            this.redirectToProfile(3);
        } else if (path === RedirectPathEnum.PROFILE_4) {
            // Mi Perfil - Dirección Postal
            this.redirectToProfile(4);
        } else if (path === RedirectPathEnum.PROFILE_5) {
            // Mi Perfil - Preguntas de Seguridad
            this.redirectToProfile(5);
        } else if (path === RedirectPathEnum.MY_ACCOUNTS) {
            // Mis cuentas
            this.redirectToAccountsManage();
        } else if (path === RedirectPathEnum.ADD_ACCOUNT) {
            // Mis cuentas
            this.redirectToAccountsManage(1);
        } else if (path === RedirectPathEnum.RECHARGE) {
            // Recarga / prepago
            this.redirectToPrepaidRecharge();
        } else if (path === RedirectPathEnum.TRANSACTIONS) {
            // Transacciones / prepago
            this.redirectToPrepaidTransactions();
        } else if (path === RedirectPathEnum.FAULT_REPORT) {
            // Reporte de Averias / Fijo
            this.redirectToFaultReport();
        } else if (path === RedirectPathEnum.SUPPORT) {
            // SUPPORT
            this.redirectToSupport();
        } else if (path === RedirectPathEnum.FORGOT_PASSWORD) {
            // SUPPORT
            this.redirectToRecoverPassword();
        } else if (path === RedirectPathEnum.REGISTER) {
            // SUPPORT
            this.redirectToRegister();
        } else if (path.includes('transit')) {
            this.redirectTransit(path);
        } else if (path === RedirectPathEnum.FREQUENT_QUESTIONS) {
            this.redirectFaq();
        }
    }

    redirectFromChat(url: string) {
        const host = url.replace(APP.MI_CLARO_WEB, '').trim();
        // IF contains web link and any extra path
        if (url.includes(APP.MI_CLARO_WEB) && host.length > 0) {
            this.redirect(host, true);
        } else { // IF contains web link or any other link
            this.browserProvider.openExternalBrowser(url);
        }
    }

    recoverProcess(url) {
        this.showProgress();
        this.browserProvider.openRecoveryBrowser(`${environment.WEB_URL}${url}`).then(
            (done) => {
                if (done) {
                    this.utilsService.clearStore().then(
                        () => {
                            this.utilsService.deleteDataForWidgets();
                            this.userStorage.logged = false;
                            this.dismissProgress();
                            this.utilsService.goTo('login');
                        }
                    );
                } else {
                    this.dismissProgress();
                }
            }
        ).catch(err => {
            this.dismissProgress();
            this.alertProvider.showError(err).then();
        });
        this.userStorage.redirectData = undefined;
    }

    private redirectTransit(path) {
        let url = path.replace('transit?url=', '');
        if (this.userStorage.logged) {
            url = `${url}?token=${this.userStorage.tokenSession}`;
        }
        this.browserProvider.openExternalBrowser(url);
        this.userStorage.redirectData = undefined;
    }

    private redirectToHome() {
        if (this.userStorage.logged) {
            this.utilsService.goTo(pages.HOME);
            this.userStorage.redirectData = undefined;
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToHomeFromChat() {
        this.alertProvider.showAlertGoWebOrKeepInApp(() => {
            this.browserProvider.openExternalBrowser(APP.MI_CLARO_WEB);
            this.userStorage.redirectData = undefined;
        }, () => {
            if (this.userStorage.logged) {
                this.utilsService.goTo(pages.HOME);
                this.userStorage.redirectData = undefined;
            } else {
                this.alertPleaseLoginOrRegister();
            }
        });
    }

    private redirectToInvoiceSummary() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                this.utilsService.goTo(pages.INVOICE_SUMMARY);
            } else {
                this.alertNotValidForPrepaid();
            }
            this.userStorage.redirectData = undefined;
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToInvoiceDownload() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.INVOICE_DOWNLOAD);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertNotValidForPrepaid();
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToInvoiceHistory() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.INVOICE_HISTORY);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertNotValidForPrepaid();
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToElectronicBill() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.ELECTRONIC_BILL);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertNotValidForPrepaid();
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToDirectDebit() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.DIRECT_DEBIT);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertValidOnlyForPostpaid();
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToConsumption() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPrepaidAccount) {
                this.utilsService.goTo(pages.CONSUMPTION_PREPAID);
            } else {
                this.utilsService.goTo(pages.CONSUMPTION);
            }
            this.userStorage.redirectData = undefined;
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToServicesAndEquipment() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.SERVICES_AND_EQUIPMENT);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertNotValidForPrepaid();
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToChangePlan() {
        if (this.userStorage.logged) {
            if (this.userStorage.isGuest) {
                this.alertAccessLimited();
            } else {
                this.utilsService.goTo(pages.CHANGE_PLAN);
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToNetflix() {
        if (Utils.getPlatformInfo().ios) {
            this.alertProvider.showAlertOnlyOnWeb(url => {
                this.browserProvider.openExternalBrowser(url);
            });
        } else {
            if (this.userStorage.logged) {
                if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                    if (this.userStorage.isGuest) {
                        this.alertAccessLimited();
                    } else {
                        this.utilsService.goTo(pages.NETFLIX);
                        this.userStorage.redirectData = undefined;
                    }
                } else {
                    this.alertNotValidForPrepaid();
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertPleaseLoginOrRegister();
            }
        }
    }

    private redirectToReferrer() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.REFERRER);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertNotValidForPrepaid();
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToClub() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.CLUB);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertValidOnlyForPostpaid();
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToPurchases() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.PURCHASES);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertValidOnlyForPostpaid();
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToDataPlan() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.DATA_PLAN);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertNotValidForPrepaid();
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToShoppingHistory() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.SHOPPING_HISTORY);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertValidOnlyForPostpaid();
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToGiftOneGB() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.GIFT_ONE_GB);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertValidOnlyForPostpaid();
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToGiftRecharge() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.GIFT_RECHARGE);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertValidOnlyForPostpaid();
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToNotifications() {
        if (this.userStorage.logged) {
            this.utilsService.goTo(pages.NOTIFICATIONS);
            this.userStorage.redirectData = undefined;
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToProfile(index?: number) {
        if (this.userStorage.logged) {
            if (this.userStorage.isGuest) {
                this.alertAccessLimited();
            } else {
                switch (index) {
                    case 1:
                        this.utilsService.goTo(pages.PROFILE_NAME);
                        break;
                    case 2:
                        this.utilsService.goTo(pages.PROFILE_EMAIL);
                        break;
                    case 3:
                        this.utilsService.goTo(pages.PROFILE_PASSWORD);
                        break;
                    case 4:
                        this.utilsService.goTo(pages.PROFILE_POSTAL);
                        break;
                    case 5:
                        this.utilsService.goTo(pages.PROFILE_QUESTIONS);
                        break;
                    default:
                        this.utilsService.goTo(pages.PROFILE);
                        break;
                }
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToAccountsManage(index?: number) {
        if (this.userStorage.logged) {
            if (this.userStorage.isGuest) {
                this.alertAccessLimited();
            } else {
                if (index === 1) {
                    this.utilsService.goTo(pages.ADD_ACCOUNTS);
                } else {
                    this.utilsService.goTo(pages.ACCOUNTS_MANAGE);
                }
                this.userStorage.redirectData = undefined;
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToPrepaidRecharge() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPrepaidAccount) {
                this.utilsService.goTo(pages.RECHARGE);
                this.userStorage.redirectData = undefined;
            } else {
                this.alertValidOnlyForPrepaid();
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToPrepaidTransactions() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPrepaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.TRANSACTIONS);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertValidOnlyForPrepaid();
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToFaultReport() {
        if (this.userStorage.logged) {
            if (this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                } else {
                    this.utilsService.goTo(pages.FAULT_REPORT);
                    this.userStorage.redirectData = undefined;
                }
            } else {
                this.alertValidOnlyForTelephony();
            }
        } else {
            this.alertPleaseLoginOrRegister();
        }
    }

    private redirectToSupport() {
        this.utilsService.goTo(pages.SUPPORT);
        this.userStorage.redirectData = undefined;
    }

    private redirectToRecoverPassword() {
        if (this.userStorage.logged) {
            this.confirmCloseSession();
        } else {
            this.utilsService.goTo(pages.RECOVER_PASSWORD);
            this.userStorage.redirectData = undefined;
        }
    }

    private redirectToRegister() {
        if (this.userStorage.logged) {
            this.confirmCloseSession();
        } else {
            this.utilsService.goTo(pages.REGISTER);
            this.userStorage.redirectData = undefined;
        }
    }

    private redirectFaq() {
        this.utilsService.goTo(pages.FAQ);
        this.userStorage.redirectData = undefined;
    }

    private redirectToChat() {
        this.openChat();
        this.userStorage.redirectData = undefined;
    }

    private showProgress(message?: string) {
        this.utilsService.showLoader.emit({show: true, message});
    }

    private dismissProgress() {
        this.utilsService.showLoader.emit({show: false, message: undefined});
    }

    private alertNotValidForPrepaid() {
        this.alertProvider.showAlert('Estimado cliente, este servicio no se encuentra disponible para clientes prepago');
    }

    private alertValidOnlyForPostpaid() {
        this.alertProvider.showAlert('Estimado cliente, este servicio se encuentra disponible solo para clientes postpago');
    }

    private alertValidOnlyForPrepaid() {
        this.alertProvider.showAlert('Estimado cliente, este servicio se encuentra disponible solo para clientes prepago');
    }

    private alertValidOnlyForTelephony() {
        this.alertProvider.showAlert('Estimado cliente, este servicio se encuentra disponible solo para clientes de telefonia fija');
    }

    private alertPleaseLoginOrRegister() {
        if (this.router.isActive('/login', false)) {
            this.alertProvider.showAlert('Estimado cliente, para acceder a este modulo debe ingresar con su usuario y contraseña, si no tiene presione en la opción <b>&#191;No tienes cuenta&#63; Reg&iacute;strate</b> para crear sus datos de acceso.');
        } else {
            this.utilsService.goTo(pages.LOGIN);
        }
    }

    private confirmCloseSession() {
        this.alertProvider.alertShouldCloseSession(() => {
            this.utilsService.confirmCloseSession(
                () => {
                    // NOTHING TO DO HERE
                },
                () => {
                    this.userStorage.redirectData = undefined;
                });
        }, () => {
            this.userStorage.redirectData = undefined;
        });
    }

    private alertAccessLimited() {
        this.alertProvider.showAlertAccessLimited(
            () => {
                this.utilsService.goTo(pages.REGISTER);
            }, () => {
                this.utilsService.goTo(pages.LOGIN);
            });
    }

    openChat() {
        let tokenSession = '';
        let tokenSSO = '';
        if (this.userStorage.logged) {
            tokenSession = this.userStorage.loginData.token;
            tokenSSO = this.userStorage.loginData.SSOAccessToken;
        }
        this.showProgress();
        this.modelsServices.chat(
            tokenSession,
            tokenSSO)
            .then(response => {
                this.utilsService.registerScreen('chat');
                this.browserProvider.openChatBrowser(response.url).then((redirectLink: string) => {
                    this.dismissProgress();
                    if (redirectLink) {
                        this.alertProvider.showConfirm('Estas siendo rediregido a:', redirectLink, () => { // TODO, only for testing
                            this.redirectFromChat(redirectLink);
                        });
                    }
                }).catch(err => {
                    this.dismissProgress();
                    this.alertProvider.showError(err);
                });
            }, error => {
                this.dismissProgress();
                this.alertProvider.showAlert(error.message);
            });
    }
}

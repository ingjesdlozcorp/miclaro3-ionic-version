import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { keys } from '../utils/const/keys';

@Injectable({
    providedIn: 'root'
})
export class StorageProvider {

    constructor(private storage: Storage) { }

    async isLogged() {
        const isLogged = await this.storage.get(keys.LOGIN.IS_LOGGED).then();
        return !!isLogged;
    }

    async setLogged(logged: boolean) {
        await this.storage.set(keys.LOGIN.IS_LOGGED, logged).then();
    }

    async isGuest() {
        const isLogged = await this.storage.get(keys.LOGIN.IS_GUEST).then();
        return !!isLogged;
    }

    async setGuest(guest: boolean) {
        await this.storage.set(keys.LOGIN.IS_GUEST, guest).then();
    }

    async isRated() {
        const isRated = await this.storage.get(keys.APP.IS_RATED).then();
        return !!isRated;
    }

    async setRated(isRated: boolean) {
        await this.storage.set(keys.APP.IS_RATED, isRated).then();
    }

    async getOpenAppTimes() {
        const times = await this.storage.get(keys.APP.OPEN_APP_TIMES).then();
        // tslint:disable-next-line:no-bitwise
        return times | 0;
    }

    async setOpenAppTimes(times: number) {
        await this.storage.set(keys.APP.OPEN_APP_TIMES, times).then();
    }

    async increaseOpenAppTimes() {
        const times = await this.storage.get(keys.APP.OPEN_APP_TIMES).then();
        // tslint:disable-next-line:no-bitwise
        let count: number = times | 0;
        count++;
        await this.setOpenAppTimes(count);
    }

    async setPushGranted(isGranted: boolean) {
        await this.storage.set(keys.APP.PUSH_GRANTED, isGranted).then();
    }

    async isPushGranted() {
        const isGranted = await this.storage.get(keys.APP.PUSH_GRANTED).then();
        return !!isGranted;
    }

    async setNotificationPushToken(token: string) {
        await this.storage.set(keys.APP.PUSH_TOKEN, token).then();
    }

    async getNotificationPushToken() {
        return await this.storage.get(keys.APP.PUSH_TOKEN).then();
    }

    async isAccessAccepted(): Promise<{ isAccepted: boolean, version: string }> {
        return await this.storage.get(keys.APP.ACCEPTED_ACCESS).then();
    }

    async setAccessAccepted(access: {isAccepted: boolean, version: string}) {
        await this.storage.set(keys.APP.ACCEPTED_ACCESS, access).then();
    }
}

import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class AlertProvider {

    constructor(
        private alertController: AlertController
    ) { }

    async showError(message, onDismiss?) {
        const alert = await this.alertController.create({
            header: 'Aviso',
            message,
            buttons: [{
                text: 'Ok',
                handler: () => {
                    if (onDismiss) {
                        onDismiss();
                    }
                }
            }],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showAlert(message, onDismiss?, okText?) {
        const alert = await this.alertController.create({
            message,
            buttons: [{
                text: okText === undefined ? 'Ok' : okText,
                handler: () => {
                    if (onDismiss) {
                        onDismiss();
                    }
                }
            }],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showConfirm(header, message, onAccept?, onCancel?) {
        const alert = await this.alertController.create({
            header,
            message,
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: () => {
                        if (onCancel) {
                            onCancel();
                        }
                    }
                },
                {
                    text: 'Aceptar',
                    handler: () => {
                        if (onAccept) {
                            onAccept();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showConfirmCustom(header, message, acceptButton, cancelButton, onAccept?, onCancel?) {
        const alert = await this.alertController.create({
            header,
            message,
            buttons: [
                {
                    text: cancelButton,
                    role: 'cancel',
                    handler: () => {
                        if (onCancel) {
                            onCancel();
                        }
                    }
                },
                {
                    text: acceptButton,
                    handler: () => {
                        if (onAccept) {
                            onAccept();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showChat(message, onChat, onDismiss?) {
        const alert = await this.alertController.create({
            cssClass: 'chat-alert',
            message:
                '        <div class="basicrow f-black f-mildt roboto-m">\n' +
                '            <img style="width: 40px; height: 40px;" src="assets/images/miclaro-icon.png" alt="">\n' +
                '            Bienvenido a Mi <b>Claro Asistencia</b>\n' +
                '        </div>\n' +
                '\n' +
                '        <div class="basicrow m-top-i">\n' +
                '            <div class="logline full"></div>\n' +
                '        </div>\n' +
                '\n' +
                '        <div class="basicrow f-reg f-black roboto-r m-top-i text-justify" id="chat-error-text">\n' +
                '            <b>Incidencia:</b> ' + message + '\n' +
                '        </div>\n' +
                '\n' +
                '        <div class="basicrow m-top-i">\n' +
                '            <div class="logline full"></div>\n' +
                '        </div>\n' +
                '\n' +
                '        <div class="basicrow f-reg f-black roboto-r m-top-ii text-justify">\n' +
                '            ¿Desea que uno de nuestros agentes de servicio al cliente te asista en línea a través de nuestro Chat?\n' +
                '        </div>',
            buttons: [
                {
                    text: 'Conectar Via Chat',
                    handler: () => {
                        if (onChat) {
                            onChat();
                        }
                    }
                },
                {
                    text: 'No, Gracias',
                    role: 'cancel',
                    handler: () => {
                        if (onDismiss) {
                            onDismiss();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showAlertSessionExpired(onDismiss?) {
        const alert = await this.alertController.create({
            header: 'Sesión Expirada',
            message: 'Estimado cliente su sesión ha expirado.',
            buttons: [{
                text: 'Ok',
                handler: () => {
                    if (onDismiss) {
                        onDismiss();
                    }
                    location.reload();
                }
            }],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showAlertPleaseLoginOrRegister(onOpenRegisterPage, onCancel?) {
        const self = this;
        const alert = await this.alertController.create({
            header: 'Acceso Restringido',
            message: 'Estimado cliente, para acceder a este modulo debe ingresar con su usuario y contraseña, si no tiene presione en la opción <b>&#191;No tienes cuenta&#63; Reg&iacute;strate</b> para crear sus datos de acceso.',
            buttons: [
                {
                    text: 'Registrarse',
                    handler: () => {
                        onOpenRegisterPage();
                    }
                },
                {
                    text: 'Cerrar',
                    role: 'cancel',
                    handler: () => {
                        if (onCancel) {
                            onCancel();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showAlertOnlyOnWeb(onOpenWeb, onCancel?) {
        const self = this;
        const alert = await this.alertController.create({
            header: 'Acceso Restringido',
            message: 'Estimado cliente, este servicio solo se encuentra disponible en  <b>Mi Claro Web</b>.',
            buttons: [
                {
                    text: 'Ir a Mi Claro Web',
                    handler: () => {
                        onOpenWeb('https://miclaro.claropr.com/');
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: () => {
                        if (onCancel) {
                            onCancel();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showAlertAccessLimited(onOpenRegisterPage, onOpenLoginPage, onDismiss?) {
        const alert = await this.alertController.create({
            message: 'Actualmente, estas en modo de usuario invitado. Si eres el dueño de la cuenta, ' +
                'debes autenticarte y/o registrarte para esta y otras secciones transacciones solo ' +
                'disponibles para el administrador.',
            buttons: [
                {
                    text: 'Ir a registro',
                    handler: () => {
                        onOpenRegisterPage();
                    }
                },
                {
                    text: 'Autenticarse',
                    handler: () => {
                        onOpenLoginPage();
                    }
                },
                {
                    text: 'Continuar como invitado',
                    role: 'cancel',
                    handler: () => {
                        if (onDismiss) {
                            onDismiss();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showAlertGoWebOrKeepInApp(openInExternalBrowser, openInApp?) {
        const self = this;
        const alert = await this.alertController.create({
            message: '¿Desea abrir este enlace en su navegador web o mantener en la app?.',
            buttons: [
                {
                    text: 'Abrir en el NAVEGADOR',
                    handler: () => {
                        openInExternalBrowser();
                    }
                },
                {
                    text: 'Mantener en el APP',
                    role: 'cancel',
                    handler: () => {
                        if (openInApp) {
                            openInApp();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    alertShouldCloseSession(onAccept, onCancel) {
        this.showConfirmCustom('',
            'Estimado cliente, para acceder a esta opcion debe cerrar su sesión.',
            'Cerra Sesión',
            'Cancelar', onAccept, onCancel);
    }
}

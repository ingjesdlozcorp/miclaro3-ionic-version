import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {environment} from '../../environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

const BannersCollection = 'banners';
const ConfigCollection = 'config';
const IptvDoc = 'iptv';

@Injectable({
    providedIn: 'root'
})
export class FirestoreService {

    constructor(private firestore: AngularFirestore) {
    }

    private getConfig(doc: string): Observable<any> {
        return this.firestore.collection(ConfigCollection).doc(doc).get()
            .pipe(map(res => res.data()));
    }

    public getConfigIPTV(): Observable<any> {
        return this.getConfig(IptvDoc);
    }

    public getAllBanners(): Observable<any> {
        return this.firestore.collection(BannersCollection, ref => {
            let query = ref.where('enabled', '==', true);
            if (environment.SHOW_ONLY_BANNERS_FOR_PRODUCTION) {
                query = query.where('prod', '==', true);
            }
            return query;
        }).snapshotChanges();
    }

    checkBannerVisibility(typeBan, banner): boolean {
        if (typeBan.postpaid) {
            return !!banner.postpaid;
        }
        if (typeBan.prepaid) {
            return !!banner.prepaid;
        }
        if (typeBan.telephony) {
            return !!banner.telephony;
        }
    }
}

export interface BannerData {
    action: string;
    actionType: string;
    enabled: boolean;
    isClickable: boolean;
    postpaid: boolean;
    prepaid: boolean;
    telephony: boolean;
    url: string;
    prod: boolean;
}

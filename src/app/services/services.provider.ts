import { Injectable } from '@angular/core';
import { UtilsService } from './utils.service';
import { Device } from '@ionic-native/device/ngx';
import { Observable } from 'rxjs';
import { GoogleMapOptions, GoogleMaps, GoogleMapsEvent } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AccessFilter } from '../models/access.filter';
import { IntentProvider } from './intent.provider';
import { Utils } from '../utils/utils';
import { environment } from '../../environments/environment';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Address, PaymentCard } from '../pages/module/equipment-and-services/iptv/iptv-buy/popup-payment-shipping/popup-payment-shipping.component';
import { map } from 'rxjs/operators';
import { Channel } from '../models/iptv';

@Injectable({
    providedIn: 'root'
})
export class ServicesProvider {

    public SO: string;
    public soVersion: string;
    public version: string;
    public token: any;
    public UUID: string;
    public model: string;
    public mapElement: any;
    public map: any;
    public location = {
        latitude: '',
        longitude: ''
    };
    public data;
    public temporaryStorage = {
        productTypeField: '',
        ticket: '',
        selectedSubscriber: '',
        faultSelectedType: 1,
        faultSelectedLine: 1,
        contact1: '',
        contact2: '',
        description: '',
        success_report_title: '',
        store: {},
        storeList: [],
        storeType: 0,
        coords: {},
        referrerMember: {
            CountAvialable: 0,
            CountExpired: 0,
            CountPending: 0,
            CountRedeems: 0,
            CountReferrs: 0,
            TotalAvailable: 0,
            TotalCredits: 0,
            TotalRedeem: 0,
            TotalReferer: 0,
            account: '',
            accountName: '',
            memberID: 0
        },
        referData: {
            account: '',
            balance: '',
            paperless: true,
            registerUpdated: true,
            solvent: true,
            subscriber: ''
        }
    };
    private firstGetAccountDetailsLoad = true;
    private accountDetailsIsUpdated = false;

    constructor(private appVersion: AppVersion,
                private util: UtilsService,
                private userStorage: IntentProvider,
                public device: Device,
                private googleMaps: GoogleMaps,
                private geolocation: Geolocation) {
        this.SO = device.platform;
        this.soVersion = device.version;
        this.UUID = device.uuid;
        this.model = device.model;

        if (Utils.getPlatformInfo().desktop) {
            /***** Only For Web Test *****/
            this.UUID = 'test-uuid-greenSize';
            this.model = 'test-ionic';
            this.version = 'test';
            /***** Only For Web Test *****/
        } else {
            this.appVersion.getVersionNumber().then(
                (result) => {
                    this.version = result;
                }
            );
        }
    }

    isAccountDetailsUpdated(): boolean {
        return this.accountDetailsIsUpdated;
    }

    setAccountDetailsUpdate() {
        this.accountDetailsIsUpdated = true;
    }

    myPosition() {
        return new Promise(async (resolve, reject) => {
            try {
                const resp = await this.geolocation.getCurrentPosition();
                this.location.latitude = resp.coords.latitude.toString();
                this.location.longitude = resp.coords.longitude.toString();
                console.log(this.location);
                resolve(this.location);
            } catch (error) {
                console.log('Error getting location', error);
                reject(error);
            }
        });
    }

    // todo: validate if the method is used
    loadMap(coords) {
        this.mapElement = document.getElementById('store-map');
        const mapOption: GoogleMapOptions = {
            camera: {
                target: {
                    lat: coords.latitude,
                    Ing: coords.longitude

                },
                zoom: 18,
                tilt: 30
            }
        };
        this.map = this.googleMaps.create(this.mapElement, mapOption);

        this.map.one(GoogleMapsEvent.MAP_READY).then(
            () => {
                console.log('Mapa Cargado');
            }
        );
    }

    getToken() {
        const token = localStorage.getItem('token');
        this.token = (token !== 'undefined') ? token : null;
        return this.token;
    }

    login(username, password, pushToken) {
        const method = 'authenticate';
        const parameters: any = {
            Username: btoa(username),
            Password: btoa(password),
            source: Utils.getPlatformOS(),
            method
        };
        if (pushToken) {
            parameters.deviceToken = pushToken;
        }

        return this.util.serverListener(JSON.stringify(parameters)).toPromise();
    }

    loadAccount(account: string, subscriber: string, token: string, isGuest: boolean): Promise<any> {
        return new Observable(observer => {
            this.getAccountDetailsBestOption(account, subscriber, token).then(
                (details: any) => {
                    details.AccounInfo.defaultSubscriberField = subscriber;
                    details.AccounInfo.defaultSubscriberObject =
                        Utils.getSubscriberByNumber(subscriber, details.SubscriberInfo);
                    this.data = details;
                    if (!this.firstGetAccountDetailsLoad) {
                        this.setAccountDetailsUpdate();
                    }
                    this.firstGetAccountDetailsLoad = false;
                    this.getAccess(account, subscriber, token).then(
                        (access: any) => {
                            this.userStorage.accountDetails = details;
                            this.userStorage.access = AccessFilter.do(
                                access.Sections,
                                isGuest,
                                this.userStorage.accountInfo.accountTypeField,
                                this.userStorage.accountInfo.accountSubtypeField,
                                this.userStorage.accountInfo.defaultSubscriberObject ? this.userStorage.accountInfo.defaultSubscriberObject.productTypeField : null
                            );
                            this.userStorage.logged = true;
                            observer.next(details);
                            observer.complete();
                        }, error => {
                            observer.error(error);
                            observer.complete();
                        }
                    );
                }, error => {
                    observer.error(error);
                    observer.complete();
                }
            );
        }).toPromise();
    }

    getAccountDetailsBestOption(account, subscriber, token): Promise<any> {
        if (this.firstGetAccountDetailsLoad) {
            return this.getAccountDetailsHome(account, subscriber, token);
        }
        return this.getAccountDetails(account, subscriber, token);
    }

    getAccountDetails(account, subscriber, token) {

        const method = 'getaccountdetails';
        const parameters = JSON.stringify({
            subscriber: btoa(subscriber),
            account: btoa(account),
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getAccountDetailsHome(account, subscriber, token) {

        const method = 'getaccountdetailshome';
        const parameters = JSON.stringify({
            subscriber,
            account,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getAccess(account, subscriber, token) {

        const method = 'getaccess';
        const parameters = JSON.stringify({
            subscriber,
            account,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getChallengeQuestions(subscriber) {

        const method = 'getChallengeQuestions';
        const parameters = JSON.stringify({
            subscriber,
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getChallengeQuestions2(token) {

        const method = 'getChallengeQuestions2';

        const parameters = JSON.stringify({
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    answerSecurityQuestions(subscriber, questions) {

        const method = 'getpasswordrecovery';
        const parameters = JSON.stringify({
            subscriber,
            ResponseList: questions,
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    recoveryPasswordBySubscriber(subscriber, sms, email) {

        const method = 'getPasswordRecoveryBySubscriber';
        const parameters = JSON.stringify({
            subscriber,
            sms,
            email,
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    passwordUpdate(currentPassword, newPassword, token) {
        const method = 'setPasswordUpdate';

        const parameters = JSON.stringify({
            currentPassword,
            newPassword,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    validateSubscriber(subscriber) {
        const method = 'validateSubscriber';

        const parameters = JSON.stringify({
            subscriber,
            source: Utils.getPlatformOS(),
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    validateSSNAndEmail(subscriber, code, ssn, email) {
        const method = 'registerUserValidateSSNAndEmail';

        const parameters = JSON.stringify({
            Subscriber: subscriber,
            SSN: ssn,
            Code: code,
            Email: email,
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    validatePassword(subscriber, code, ssn, email, password) {
        const method = 'registerUser';

        const parameters = JSON.stringify({
            Subscriber: subscriber,
            SSN: ssn,
            Code: code,
            Email: email,
            Password: password,
            source: 'mobile',
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    loginGuest(subscriber, pushToken) {
        const method = 'authenticateAsGuest';

        const parameters: any = {
            subscriber,
            device: this.model,
            deviceToken: this.UUID,
            method
        };
        if (pushToken) {
            parameters.deviceToken = pushToken;
        }

        return this.util.serverListener(JSON.stringify(parameters)).toPromise();
    }

    validateUser(subscriber) {
        const method = 'validateUser';

        const parameters = JSON.stringify({
            subscriber,
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    registerGuest(subscriber, pushToken) {
        const method = 'registerGuestUser';

        const parameters = {
            subscriber,
            device: this.model,
            deviceToken: this.UUID,
            method
        };
        if (pushToken) {
            parameters.deviceToken = pushToken;
        }

        return this.util.serverListener(JSON.stringify(parameters)).toPromise();
    }

    validateGuest(subscriber, code, token, pushToken) {
        const method = 'validateGuestAccount';

        const parameters = {
            subscriber,
            code,
            device: this.model,
            deviceToken: this.UUID,
            method,
            token
        };
        if (pushToken) {
            parameters.deviceToken = pushToken;
        }

        return this.util.serverListener(JSON.stringify(parameters)).toPromise();
    }

    resendGuestCode(subscriber) {
        const method = 'updateUserGetCode';

        const parameters = JSON.stringify({
            subscriber,
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updateGuest(subscriber, code, pushToken) {
        const method = 'updateUserSetToken';

        const parameters = {
            subscriber,
            code,
            device: this.model,
            deviceToken: this.UUID,
            method
        };
        if (pushToken) {
            parameters.deviceToken = pushToken;
        }

        return this.util.serverListener(JSON.stringify(parameters)).toPromise();
    }

    validateSubscriberUpdate(subscriber, token) {
        const method = 'validateSubscriberUpdate';

        const parameters = JSON.stringify({
            subscriber,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updateValidateAccount(subscriber, code, ssn, token) {
        const method = 'updAccountValidateCode';

        const parameters = JSON.stringify({
            subscriber: subscriber + '',
            code,
            ssn,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updateUsername(email, token) {
        const method = 'changeUser';

        const parameters = JSON.stringify({
            email,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updatePassword(newPassword, oldPassword, account, token) {
        const method = 'changePassword';

        const parameters = JSON.stringify({
            ban: account,
            newPassword,
            password: oldPassword,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }


    getSecurityQuestions(token) {
        const method = 'getquestions';

        const parameters = JSON.stringify({
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    setChallengeQuestions(questionId, response, token) {
        const method = 'setchallengequestions';

        const parameters = JSON.stringify({
            questionID: questionId,
            response,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getMessage(code, token) {

        const method = 'getMessage';

        const parameters = JSON.stringify({
            method,
            code,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    deactivateUser(token) {

        const method = 'deactivateUser';

        const parameters = JSON.stringify({
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getPlans(creditClass, customerSubType, customerType, price, soc, technology, token) {
        const method = 'getPlanes2';

        const parameters = JSON.stringify({
            creditClass,
            customerSubType,
            customerType,
            price,
            soc,
            tecnology: technology,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getPlansPrepaid(account, subscriber, customerType, technology, token) {

        const method = 'getChangePlanPrepago';

        const parameters = JSON.stringify({
            account,
            suscriber: subscriber,
            accountType: customerType,
            tech: technology,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getPlansPrepaid2(
        creditClass,
        productType,
        customerSubType,
        customerType,
        price,
        soc,
        tecnology,
        token) {

        const method = 'getPlans';

        const parameters = JSON.stringify({
            creditClass,
            productType,
            customerSubType,
            customerType,
            price,
            soc,
            tecnology,
            source: 'MICLARO',
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getPlansDSL(subscriber, token) {
        const method = 'DSLCatalog';

        const parameters = JSON.stringify({
            phoneNumber: subscriber,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updateSubscriberDSLPlan(productId, oldSocPrice, contract, alphaCodeContract, productType, subscriber, account, token) {
        const method = 'adaDslPackageChange';

        const parameters = JSON.stringify({
            ProductType: productType,
            alphaCodeContract,
            contract,
            dslBan: account,
            dslPhoneNumber: subscriber,
            oldSocPrice,
            productId,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updateSubscriberPlan(newSoc, oldSoc, productType, subscriber, token) {
        const method = 'updateSubscriberPricePlanSocs';

        const parameters = JSON.stringify({
            NewSocCode: newSoc,
            OldSocCode: oldSoc,
            mProductType: productType,
            mSubscriberNo: subscriber,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updateSubscriberPlanNextCycle(newSoc, oldSoc, productType, subscriber, account, token) {
        const method = 'updateSubscriberPricePlanSocsNextCicle';

        const parameters = JSON.stringify({
            BAN: account,
            NewSocCode: newSoc,
            OldSocCode: oldSoc,
            mProductType: productType,
            mSubscriberNo: subscriber,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updatePrepaidSubscriberPlan(amount, planName, rechargeMinutes, newSoc, currentSoc, accountType,
                                accountSubType, account, subscriber, token) {
        const method = 'changePrepaidAccount';

        const parameters = JSON.stringify({
            suscriber: subscriber,
            amount,
            ban: account,
            currentAccountSubType: accountSubType,
            currentAccountType: accountType,
            currentSoc,
            newAccountSubType: accountSubType,
            newAccountType: accountType,
            newSoc,
            planName,
            rechargeMinutes,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updatePrepaidSubscriberPlan2(
        accountSubType,
        accountType,
        productType,
        effectiveDate,
        oldSocCode,
        socCode,
        socId,
        subscriberId,
        token) {
        const method = 'changePrepaidPlan';

        const parameters = JSON.stringify({
            accountSubType,
            accountType,
            effectiveDate,
            oldSocCode,
            socCode,
            productType,
            socId,
            subscriberId,
            source: this.getPaymentId(),
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getEvaluatePlanPrepaid(token, subscriber, socId) {
        const method = 'getEvaluatePlan';
        const parameters = JSON.stringify({
            token,
            subscriber,
            socId,
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }

    returnPayment(paymentToken) {
        const method = 'voidPayment';
        const parameters = JSON.stringify({
            appKey: 'MICLAROWEB',
            authToken: 'aA@d39Yugle5yc2ko!',
            paymentToken,
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }

    getDataPackets(groupID, transactionId, subscriber, token, offerID?) {
        const method = 'GetOffersToSubscriber';

        const parameters: any = {
            OfferGroup: '',
            SubscriberId: subscriber,
            TransactionId: transactionId,
            method,
            token
        };
        if (offerID) {
            parameters.offerID = offerID;
        }

        return this.util.serverListener(JSON.stringify(parameters)).toPromise();
    }

    getReadSubscriber(subscriber, transactionId, token) {
        const method = 'GetReadSubscriber';

        const parameters = JSON.stringify({
            IdSubscriber: subscriber,
            TransactionId: transactionId,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    validateCreditLimit(account, accountTotalRent, productPrice, token) {
        const method = 'ValidateCreditLimit';

        const parameters = JSON.stringify({
            Ban: btoa(account),
            ProductPrice: btoa(productPrice),
            AccountTotalRent: btoa(accountTotalRent),
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    addOfferToSubscriber(transactionId, subscriberId, offerId, charge, cycle, paymentId, username, token) {
        const method = 'AddOffersToSubscriber';

        const parameters = JSON.stringify({
            TransactionId: transactionId,
            SubscriberId: subscriberId,
            OfferId: offerId,
            Charge: charge,
            Cicle: cycle,
            paymentID: paymentId,
            UserID: username,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    addPackageTen(dto) {
        const parameters = JSON.stringify({
            token: dto.token,
            OldSocCode: dto.oldSocCode,
            mSocCode: dto.socCode,
            mProductType: dto.productType,
            mSubscriberNo: dto.subscriber,
            BAN: dto.ban,
            CommitmentStartDate: dto.commitmentStartDate,
            ExpirationDate: dto.expirationDate,
            EffectiveDate: dto.effectiveDate,
            RelatedSocCode: dto.relatedSocCode,
            method: dto.method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getUserProfile(account, token) {
        const method = 'GetPersonalData';

        const parameters = JSON.stringify({
            BAN: account,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updatePersonalData(account, email, phone, phoneF, token) {
        const method = 'UpdatePersonalData';

        const parameters = JSON.stringify({
            BAN: btoa(account),
            Email: btoa(email),
            PhoneNumber: btoa(phone),
            PhoneNumber2: btoa(phoneF),
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updatePersonalAddress(account, address1, address2, city, zipCode, token) {
        const method = 'UpdatePersonalDir';

        const parameters = JSON.stringify({
            BAN: account,
            AddressDet: address1,
            AddressDet2: address2,
            City: city,
            zip: zipCode,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    // Account Models (7):
    addAccount(accountNumber, ssn, defaultAccount, token) {

        const method = 'addAccounts';

        const parameters = JSON.stringify({
            account: accountNumber,
            ssn,
            setAsDefaultAccount: defaultAccount,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    setDefaultAccount(accountNumber, subscriber, accountType, accountSubType, productType, token) {
        const method = 'SetAsDefaultAccount';

        const parameters = JSON.stringify({
            account: accountNumber,
            subscriber,
            accountType,
            accountSubType,
            productType,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getAccounts(token) {
        const method = 'getAcounts';

        const parameters = JSON.stringify({
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    deleteAccount(accountNumber, token) {
        const method = 'deleteAccount2';

        const parameters = JSON.stringify({
            account: accountNumber,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getHistoryOrders(account, token) {
        // const token = token
        const method = 'accountPackagesInfo';

        const parameters = JSON.stringify({
            Ban: account,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    subscribeNetflix(account, subscriber, ip, token) {
        const tokenSession = token;
        const method = 'suscribirNeflix';

        const parameters = JSON.stringify({
            account,
            subscriber,
            customerType: '',
            mdeviceSerialNumber: '',
            moperatorUrlError: '',
            mpromotionId: '',
            msalesChannel: '',
            productId: '',
            subProductId: '',
            method,
            ip,
            userAgent: navigator.userAgent,
            token: tokenSession,
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getInstallments(account, subscriber, productType, query, value, token) {

        const method = 'getInstallments';
        const parameters = JSON.stringify({
            userId: '',
            appKey: this.getPaymentId(),
            authToken: this.getPaymentToken(),
            appId: this.getPaymentId(),
            accountNumber: account,
            subscriberNumber: subscriber,
            queryType: query,
            valueRequested: value,
            productType,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    resendCode() {
        const tokenSession = this.getToken();
        const method = 'resendCode';

        const parameters = JSON.stringify({
            method,
            token: tokenSession,
        });

        return this.util.serverListener(parameters).toPromise();
    }

    // Customer model (15):
    validateAccount(account, accountType, accountSubType, subscriber) {
        const tokenSession = this.getToken();
        const method = 'validateAccount';

        const parameters = JSON.stringify({
            suscriber: subscriber,
            ban: account,
            currentAccountSubType: accountSubType,
            currentAccountType: accountType,
            newAccountSubType: accountSubType,
            newAccountType: accountType,
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    accountDetails(subscriber, account) {
        const tokenSession = this.getToken();
        const method = 'getaccountdetails';
        const parameters = JSON.stringify({
            subscriber: btoa(subscriber),
            account: btoa(account),
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    userAccess(subscriber, account) {
        const tokenSession = this.getToken();
        const method = 'getaccess';
        const parameters = JSON.stringify({
            subscriber,
            account,
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updateToken(account, subscriber, token) {

        const method = 'updateToken';
        const parameters = JSON.stringify({
            subscriber,
            account,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    /*
    passwordUpdate(currentPassword, newPassword, successCB, errorCB){

        const tokenSession = app.utils.Storage.getSessionItem('token');

        const method = 'setPasswordUpdate';

        const parameters = JSON.stringify({
            currentPassword: currentPassword,
            newPassword: btoa(newPassword),
            method: method,
            token: tokenSession
        });

          return this.util.serverListener(parameters).toPromise();
    } */

    getBan(account) {
        const tokenSession = this.getToken();

        const parameters = JSON.stringify({
            BAN: account,
            method: 'getBan',
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updateBillParameters(account, token) {

        const parameters = JSON.stringify({
            Ban: String(account),
            method: 'updateBillParameters',
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getSubscriber(subscriber, token) {

        const parameters = JSON.stringify({
            Subscriber: subscriber,
            method: 'GetSubscriber',
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getMember(account, token) {

        const parameters = JSON.stringify({
            account,
            method: 'getMember',
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    addMember(data, token) {

        data.token = token;
        data.method = 'addmember';
        const parameters = JSON.stringify(data);

        return this.util.serverListener(parameters).toPromise();
    }

    sendGift1GB(data, token) {
        const tokenSession = token;
        const method = 'AddGift1GB';

        data.token = tokenSession; // ????
        data.method = method; // ??????

        const parameters = JSON.stringify(data);

        return this.util.serverListener(parameters).toPromise();
    }

    sendGiftRecharge(data, token) {
        const tokenSession = token;
        const method = 'giftRecharge';

        data.token = tokenSession; // ?????
        data.method = method;  // ????

        const parameters = JSON.stringify(data);

        return this.util.serverListener(parameters).toPromise();
    }

    getGift1GBSend(account, token) {
        const parameters = JSON.stringify({
            BAN: account,
            method: 'GetGift1GBSend',
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getGift1GBByGUI(account, gui, token) {

        const parameters = JSON.stringify({
            BAN: account,
            GUI: gui,
            method: 'GetGift1GBByGUI',
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    // payment model(13):
    doPayment(account, amount, token) {
        const tokenSession = token;
        const method = 'DoPayment';

        const parameters = JSON.stringify({
            Account: account,
            Amount: amount,
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    paymentHistory(account, year, token) {
        const tokenSession = token;
        const method = 'PaymentHistory';

        const parameters = JSON.stringify({
            Ban: account,
            year,
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getInvoiceHistory(account, year, token) {
        const method = 'GetHistoricoFacturas2';
        const format = 'pdf';

        const parameters = JSON.stringify({
            Ban: account,
            year,
            format,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    prepaidHistory(account, subscriber, idCustomerCard, index, year, month, status, type, token) {
        const method = 'refillHistory';

        const parameters = JSON.stringify({
            account,
            suscriber: subscriber,
            idCustomerCard,
            index,
            year,
            month,
            status,
            type,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getPrepaidTransactions(subscriber, token) {

        const method = 'getPrepaidInfo';

        const parameters = JSON.stringify({
            subscriberId: subscriber,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getDirectDebitInfo(account, token) {
        const tokenSession = token;
        const method = 'getDirectDebitInfo2';

        const parameters = JSON.stringify({
            accountNumber: account,
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updateDirectDebit(data, token) {
        const tokenSession = token;
        const method = 'updateDirectDebit2';

        data.token = tokenSession;
        data.method = method;

        const parameters = JSON.stringify(data);

        return this.util.serverListener(parameters).toPromise();
    }

    makePayment(data, token) {

        const method = 'makePayment';

        data.token = token;
        data.method = method;

        const parameters = JSON.stringify(data);

        return this.util.serverListener(parameters).toPromise();
    }

    makePaymentRecharge(data, subscriber) {
        const tokenSession = this.getToken();
        const method = 'prepaidPayment';

        data.suscriber = subscriber;
        data.token = tokenSession;
        data.method = method;

        const parameters = JSON.stringify(data);

        return this.util.serverListener(parameters).toPromise();
    }

    makePaymentRechargeAth(data, subscriber) {
        const tokenSession = this.getToken();
        const method = 'prepaidPaymentATH';

        data.suscriber = subscriber;
        data.token = tokenSession;
        data.method = method;

        const parameters = JSON.stringify(data);

        return this.util.serverListener(parameters).toPromise();
    }

    doRecharge(data, subscriber) {
        const tokenSession = this.getToken();
        const method = 'minutesRecharge';


        data.suscriber = subscriber;
        data.token = tokenSession;
        data.method = method;

        const parameters = JSON.stringify(data);

        return this.util.serverListener(parameters).toPromise();
    }

    listProductService(subscriber, idProduct, token) {

        const method = 'productserviceList';

        const parameters = JSON.stringify({
            suscriber: subscriber,
            idProductType: idProduct,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    listPrepaidAddress(subscriber, token) {

        const method = 'prepaidAddress';

        const parameters = JSON.stringify({
            suscriber: subscriber,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    listPrepaidPaymentsType(subscriber, token) {

        const method = 'listTypesPayments';

        const parameters = JSON.stringify({
            suscriber: subscriber,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    // Referrer model (7):
    getHobbies(account, token) {

        const tokenSession = token;

        const method = 'gethobbies';
        const parameters = JSON.stringify({
            account,
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getValidateReferrer(account, subscriber, token) {

        const tokenSession = token;

        const method = 'getValidateReferrer';

        const parameters = JSON.stringify({
            account,
            subscriber,
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getSharingMediaByUser(memberID, token) {

        const tokenSession = token;

        const method = 'getSharingMediaByUser';

        const parameters = JSON.stringify({
            method,
            token: tokenSession,
            memberID,
            campaignID: 1,
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getCredits(account, token) {

        const tokenSession = token;

        const method = 'getCreditsByAccount';

        const parameters = JSON.stringify({
            Account: account,
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
        // $.getJSON("js/pojo/get-credits.json", function(json) {
        //     var data = json;
        //     successCB(data);
        // });
    }

    applyCredits(account, subscriber, amount, token) {

        const method = 'RedeemCuponsByAccount';

        const parameters = JSON.stringify({
            account,
            subscriber,
            total: amount,
            applyDiscountID: 1,
            strcomentario: 'dashboard',
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getReferrerAccountsAllStatus(account, token) {
        const tokenSession = token;
        const method = 'getReferrAccountsAllStatus';

        const parameters = JSON.stringify({
            account,
            subscriber: '',
            campaignID: 1,
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    sharedCoupons(memberID, account, subscriber, emails, userLink, token) {

        const tokenSession = token;

        const method = 'sharedCupons';

        const parameters = JSON.stringify({
            account: account + '',
            subscriber: subscriber + '',
            memberID: memberID + '',
            email: emails,
            link: userLink + '',
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getStores(storeType, userLocation, distanceMeasure, url) {

        const parameters: any = {
            type: storeType
        };

        if (distanceMeasure !== '') {
            parameters.latitude = userLocation.latitude;
            parameters.longitude = userLocation.longitude;
            parameters.measureUnit = distanceMeasure;
        }

        return this.util.helpListener(parameters, url).toPromise();
    }

    // Model User (14):
    getVerifyEmail(email, token) {

        const method = 'getVerifyEmail';

        const parameters = JSON.stringify({
            email,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getPersonalAlertsStatus(accountNumber, subscriber, token) {
        const tokenSession = token;
        const method = 'GetPersonalAlertsAndNTCStatus';

        const parameters = JSON.stringify({
            BAN: accountNumber,
            Subscriber: subscriber,
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updateNotToCall(accountNumber, subscriber, action, token) {
        const tokenSession = token;
        const method = 'NotToCall';

        const parameters = JSON.stringify({
            BAN: accountNumber,
            Subscriber: subscriber,
            Action: action,
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updateAlerts(accountNumber, subscriber, alerts, token) {
        const tokenSession = token;
        const method = 'UpdateAlerts';

        const parameters = JSON.stringify({
            BAN: accountNumber,
            Subscriber: subscriber,
            alertList: alerts,
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updateNotification(idMessage, accountNumber, token) {
        const tokenSession = token;
        const method = 'updMessageStatus';

        const parameters = JSON.stringify({
            id_message: idMessage,
            account: accountNumber,
            method,
            token: tokenSession
        });

        return this.util.serverListener(parameters).toPromise();
    }

    // help:
    sendHelpMessage(userEmail, message) {
        const parameters = {
            userMail: userEmail,
            method: 'registerHelpFail',
            info: {
                userAgent: this.SO,
                versionCode: null,
                versionName: null,
                soVersion: this.soVersion,
                userId: this.UUID
            },
            message
        };
        return this.util.serverListener(parameters).toPromise();
    }

    subscriptionAutomaticRenewalAdd(data, token) {

        const method = 'subscriptionAutomaticRenewalAdd';
        const parameters = {
            SubscriberId: data.subcriberId,
            OfferID: data.OfferId,
            BaseOfferId: data.BaseOfferID,
            method,
            token
        };

        return this.util.serverListener(parameters).toPromise();
    }

    subscriptionAutomaticRenewalRemove(data, token) {

        const method = 'subscriptionAutomaticRenewalRemove';
        const parameters = {
            SubscriberId: data.subcriberId,
            OfferID: data.OfferId,
            BaseOfferId: data.BaseOfferID,
            method,
            token
        };
        return this.util.serverListener(parameters).toPromise();
    }

    checkWarrantyStatus(subscriberId, token) {

        const method = 'warrantyStatus';

        const parameters = {
            subscriber: subscriberId,
            method,
            // token
        };

        // TODO, para caso de prueba
        // return new Promise((resolve, reject) => {
        //     setTimeout(() => {
        //         const object = JSON.parse('{"statusDescription":"Equipo en tránsito al taller del Centro de Atención.","warrantyReclamationDate":1583158271000}');
        //         resolve(object);
        //     }, 3000);
        // });
        return this.util.serverListener(parameters).toPromise();
    }

    checkIfTicketExists(subscriber) {

        const method = 'checkIfTicketExistsReport';

        const parameters = JSON.stringify({
            subscriber,
            method,
        });

        return this.util.serverListener(parameters).toPromise();
    }

    setRaiseIssueTicket(data: any) {

        const method = 'raiseIssueTicket';
        const {subscriberNumber, ban, contactNumber1, contactNumber2, outageType, operatorID, customerReport, remark, productType} = data;

        const parameters = JSON.stringify({
            method,
            subscriberNumber,
            ban,
            contactNumber1,
            contactNumber2,
            outageType,
            operatorID,
            customerReport,
            remark,
            productType
        });

        return this.util.serverListener(parameters).toPromise();

        // return new Promise((resolve, reject) => {
        //     setTimeout(() => {
        //         const object = JSON.parse('{"commitmenrDate":"20181213","createTicket":"Y",' +
        //             '"errorCode":"0","errorMessage":"Success","memoSIF":"SIF Memo sended","q_id":"27","tt_id":"1234567"}');
        //         resolve(object);
        //     }, 3000);
        // });
    }

    checkAppVersion(so, appVersion) {
        const url = `${environment.API_URL}app/getVersion`;

        const parameters = JSON.stringify({
            appId: environment.ID,
            version: appVersion,
            osType: so,
        });

        return this.util.helpListener(parameters, url).toPromise();
    }

    // services for new payments method
    getPaymentOptions(productId: string, merchantCodeId?: string) {

        const method = 'getPaymentOptions';

        const parameters = {
            appId: this.getPaymentId(),
            productId,
            merchantCodeId: merchantCodeId ? merchantCodeId : '',
            appKey: this.getPaymentId(),
            authToken: this.getPaymentToken(),
            method
        };

        return this.util.serverListener(parameters).toPromise();
    }

    initiatePaymentProcess(productId: string, paymentOptionId: string, paymentAmount: number,
                           customerEmail: string, subscriberNumber: string, subscriberAccountNumber: string,
                           subscriberFullName: string, transactionDescription: string,
                           merchantCodeId?: string, locationId?: string, invoiceNumber?: string) {

        const method = 'initiatePaymentProcess';

        const parameters: any = {
            appId: this.getPaymentId(),
            productId,
            paymentOptionId,
            paymentAmount,
            customerEmail,
            subscriberNumber,
            subscriberAccountNumber,
            subscriberFullName,
            subscriberAddress1: '',
            subscriberAddress2: '',
            subscriberCity: '',
            subscriberState: '',
            subscriberZipCode: '',
            transactionDescription,
            appKey: this.getPaymentId(),
            authToken: this.getPaymentToken(),
            method
        };

        if (merchantCodeId) {
            parameters.merchantCodeId = merchantCodeId;
        }

        if (locationId) {
            parameters.locationId = locationId;
        }

        if (invoiceNumber) {
            parameters.invoiceNumber = invoiceNumber;
        }

        return this.util.serverListener(parameters).toPromise();
    }

    verifyPayment(paymentToken: string) {

        const method = 'verifyPaymentStatus';

        const parameters = {
            paymentToken,
            appKey: this.getPaymentId(),
            authToken: this.getPaymentToken(),
            method
        };

        return this.util.serverListener(parameters).toPromise();
    }

    registerUserDevice(userName, subscriber, pnToken, successCB, errorCB) {

        const method = 'device/register';

        const parameters = {
            userName,
            suscriber: subscriber,
            type: this.SO,
            pnToken,
            uuid: this.UUID
        };

        return this.util.serverListener(parameters).toPromise();
    }

    chat(tokenSession = '', tokenSSO = '', account = '', name = '', email = '') {
        const method = 'chatbot';

        const parameters = {
            tokenSession,
            tokenSSO,
            region: '',
            category: '',
            id: '',
            origin: '',
            customerName: name,
            accountNumber: account,
            email,
            appType: Utils.getPlatformInfo().ios ? 'App iOS' : 'App Android',
            cedula: '',
            method
        };

        return this.util.serverListener(parameters).toPromise();
    }

    getNetflixCodeOptions(subscriber, token) {
        const method = 'GetValidationCodeOptions';

        const parameters = JSON.stringify({
            subscriber,
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    sendNetflixValidationCode(subscriber, email, token) {
        const method = 'GetValidationCode';

        const parameters = JSON.stringify({
            subscriber,
            method,
            email,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    validateNetflixCode(code, token) {
        const method = 'validateCode';

        const parameters = JSON.stringify({
            method,
            code,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getMyIp() {
        return this.util.getIpClient();
    }

    private getPaymentId() {
        if (Utils.getPlatformInfo().ios) {
            return environment.PAYMENT_ID_IOS;
        } else {
            return environment.PAYMENT_ID_ANDROID;
        }
    }

    private getPaymentToken() {
        if (Utils.getPlatformInfo().ios) {
            return environment.PAYMENT_TOKEN_IOS;
        } else {
            return environment.PAYMENT_TOKEN_ANDROID;
        }
    }

    /**
     * IPTV Services
     */
    getByUrl(url): Promise<any> {
        return this.util.getHtmlFromLocalTemplate(url).toPromise();
    }


    getTokenIptv(): Promise<any> {
        const method = 'getTokenIPTV';
        const parameters = JSON.stringify({
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    setTokenIptvSSO(token: string, ssoToken: string): Promise<any> {
        const method = 'loginTokenSSOIPTV';
        const parameters = JSON.stringify({
            method,
            token,
            ssoToken,
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getPlansIPTV(token): Promise<any> {
        const method = 'listCatalogueIPTV';
        const parameters = JSON.stringify({
            method,
            token,
            catalogId: 11,
            pageNo: 1,
            pageItems: 100,
            creditClass: 'C',
            orderBy: 'ASC',
            categoryID: '0',
            new: '0',
            offers: '0',
            brand: ''
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getChannelsIPTV(token, idPlan, idAddOn = 0, featured = false): Observable<any> {
        const method = 'channelsListIPTV';
        const parameters = JSON.stringify({
            method,
            token,
            idPlan,
            idAddOn,
            top: featured ? 8 : 200
        });

        return this.util.serverListener(parameters)
            .pipe(map(result => this.buildPathChannel(result)));
    }

    getChannelsFullIPTV(token): Observable<any> {
        const method = 'channelsFullIPTV';
        const parameters = JSON.stringify({
            method,
            token
        });

        return this.util.serverListener(parameters)
            .pipe(map(result => this.buildPathChannel(result)));
    }

    // To build routes dynamically. It should be removed soon when the service is adjusted
    private buildPathChannel(channels: Channel[]) {
        channels.map(channel => {
            const newPath = channel.url.replace('img', 'images');
            channel.url = `${environment.WEB_URL}${newPath}`;
            return channel;
        });
        return channels;
    }

    addItemsToCart(token: string, cartItems: any): Promise<any> {
        const method = 'addToCart';

        const json = {
            method,
            token,
            cartItems,
        };
        const parameters = JSON.stringify(json);

        return this.util.serverListener(parameters).toPromise();
    }

    addStreamingToCart(token: string, tokenShop: string, json: any): Promise<any> {
        json.method = 'addToCartStreaming';
        json.token = token;
        json.tokenShop = tokenShop;
        json.flowId = 8;

        return this.util.serverListener(json).toPromise();
    }

    deleteItemFromCart(token: string, tokenShop: string, cartId: number, productId?: number) {
        const method = 'deleteItemIPTV';
        const parameters = JSON.stringify({
            method,
            token,
            tokenShop,
            cartId,
            productId: 0,
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getCartIptv(token: string, tokenShop: string, promoCode = '') {
        const method = 'getCartIPTV';
        const parameters = JSON.stringify({
            method,
            token,
            tokenShop,
            promoCode,
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getPlansSVA(token: string, productId: number): Promise<any> {
        const method = 'plansIPTV';
        const parameters = JSON.stringify({
            method,
            token,
            productId,
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getOfferPlansSVA(token: string, tokenShop: string, productId: number): Promise<any> {
        const method = 'addOnsPromoIPTV';
        const parameters = JSON.stringify({
            method,
            token,
            tokenShop,
            productId,
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getStreamingDevice(token: string, tokenShop: string): Promise<any> {
        const method = 'productsIPTV';
        const parameters = JSON.stringify({
            method,
            token,
            tokenShop
        });

        return this.util.serverListener(parameters).toPromise();
    }

    validateAccountIptv(token: string, tokenShop: string, email: string): Promise<any> {
        const method = 'validateIptv2AccountIPTV';
        const parameters = JSON.stringify({
            method,
            token,
            tokenShop,
            email
        });

        return this.util.serverListener(parameters).toPromise();
    }

    createAddressIptv(token: string, tokenShop: string, address: Address): Promise<any> {
        const method = 'createAddressIPTV';
        const parameters = JSON.stringify({
            method,
            token,
            tokenShop,
            name: address.name,
            email: address.email,
            phone: address.phone,
            address1: address.address1,
            address2: address.address2,
            city: address.city,
            state: address.state,
            zip: address.zip,
            notes: address.notes,
            intUserId: '1',
        });

        return this.util.serverListener(parameters).toPromise();
    }

    payOrderIptv(token: string, tokenShop: string, card: PaymentCard, cart: any, account): Promise<any> {
        const method = 'paymentIPTV';
        const parameters = JSON.stringify({
            method,
            token,
            tokenShop,
            amount: cart.totalPrice,
            deposit: cart.depositAmount,
            name: card.name,
            numberCard: card.numberCard,
            cvv: card.cvv,
            expMonth: card.expMonth,
            expYear: card.expYear,
            zipCode: card.zipCode,
            email: account.email,
            typeCard: card.typeCard,
            flowId: '8',
            account: account.ban
        });

        return this.util.serverListener(parameters).toPromise();
    }

    processOrderIptv(token: string, tokenShop: string): Promise<any> {
        const method = 'proccessOrderIPTV';
        const parameters = JSON.stringify({
            method,
            token,
            tokenShop
        });

        return this.util.serverListener(parameters).toPromise();
    }

    updateMessageStatus(dto) {
        const parameters = JSON.stringify(dto);
        console.log(parameters);
        return this.util.serverListener(parameters).toPromise();
    }

    updateDeviceToken(dto: any): Promise<any> {
        const method = 'UpdateDeviceToken';
        const parameters = JSON.stringify({
            method,
            bearerToken: dto.bearerToken,
            deviceToken: dto.deviceToken,
            userAgent: dto.userAgent,
            deviceBrand: dto.deviceBrand,
            deviceModel: dto.deviceModel
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getMFAOptions(userId: any): Promise<any> {
        const method = 'GetMFAOptions';
        const parameters = JSON.stringify({
            method,
            id: userId
        });

        return this.util.serverListener(parameters).toPromise();
    }

    processMFAOption(dto: any): Promise<any> {
        const method = 'ProcessMFAOption';
        const parameters = JSON.stringify({
            method,
            userId: dto.userId,
            code: dto.code
        });

        return this.util.serverListener(parameters).toPromise();
    }

    validateOtpCode(dto: any): Promise<any> {
        const method = 'ValidateOtpCode';
        const parameters = JSON.stringify({
            method,
            userId: dto.userId,
            subscriber: dto.subscriber,
            code: dto.code
        });

        return this.util.serverListener(parameters).toPromise();
    }

    validateNewDevice(dto: any): Promise<any> {
        const method = 'ValidateNewDevice';
        const parameters = JSON.stringify({
            method,
            userId: dto.userId,
            userAgent: dto.userAgent,
            brand: dto.brand,
            model: dto.model,
            deviceToken: dto.deviceToken,
            ip: dto.ip
        });

        return this.util.serverListener(parameters).toPromise();
    }

    verifyMFAStatus(dto: any): Promise<any> {
        const method = 'VerifyMFAStatus';
        const parameters = JSON.stringify({
                method,
                token: dto.token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    registerMFAUserDevice(dto: any): Promise<any> {
        const method = 'RegisterUserDevice';
        const parameters = JSON.stringify({
            method,
            token: dto.token,
            bearerToken: dto.bearerToken
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getConfirmationData(token: any): Promise<any> {
        const method = 'GetConfirmationData';
        const parameters = JSON.stringify({
            method,
            token
        });

        return this.util.serverListener(parameters).toPromise();
    }

    sendConfirmationAnswer(dto: any): Promise<any> {
        const method = 'SendConfirmationAnswer';
        const parameters = JSON.stringify({
            method,
            token: dto.token,
            answer: dto.answer
        });

        return this.util.serverListener(parameters).toPromise();
    }
}

import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError, mergeMap, timeout } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { NavController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { IntentProvider } from './intent.provider';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import * as CryptoJS from 'crypto-js';
import { Utils } from '../utils/utils';
import * as moment from 'moment';
import { keys } from '../utils/const/keys';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { BrowserProvider } from './browser.provider';

const PREF_URL = 'api-url';
const PREF_TOKEN = 'login';
const PREF_PRODUCT = 'productType';

@Injectable({
    providedIn: 'root'
})
export class UtilsService {

    static errorGeneral = 'Disculpe, actualmente estamos presentando inconveniente, por favor intente mas tarde.';

    static error404 = 'Disculpe, no fue posible establecer la comunicación.';

    public showLoader: EventEmitter<{ show: boolean, message: string }> = new EventEmitter<{ show: boolean, message: string }>();

    public popupBase: EventEmitter<PopupBase> = new EventEmitter<PopupBase>();

    public resetTimer: EventEmitter<boolean> = new EventEmitter<boolean>();

    public openMenu: EventEmitter<boolean> = new EventEmitter<boolean>();
    public isMenuOpen = false;

    public help = environment.HELP_URL;

    public chat = environment.CHAT_URL;

    public sailed: string[] = ['login'];

    public moment = moment;

    public biometricOptions = {
        available: false,
        ios: Utils.getPlatformInfo().ios,
        android: Utils.getPlatformInfo().android,
        type: 'touch',
        activated: false,
        password: 'none'
    };

    public mfaCanceled = false;

    constructor(
        public storage: Storage,
        private userStorage: IntentProvider,
        private httpClient: HttpClient,
        public navCtrl: NavController,
        private alertController: AlertController,
        public firebase: FirebaseX,
        public keyboard: Keyboard,
        private appPreferences: AppPreferences,
        public browserProvider: BrowserProvider) {
        this.openMenu.subscribe(open => {
            this.isMenuOpen = open;
        });
    }

    /**
     * Save page name on firebase for analytics only if is mobile
     */
    registerScreen(pageName, data = {}) {
        if (!Utils.getPlatformInfo().desktop) {
            this.firebase.setScreenName(pageName).then();
            this.firebase.logEvent(pageName, data).then();
        }
    }

    hideKeyboard() {
        if (!Utils.getPlatformInfo().desktop) {
            this.keyboard.hide();
        }
    }

    goTo(page: string) {
        const currentPage = this.sailed[this.sailed.length - 1];
        if (currentPage.includes('profile') && page.includes('profile')) {
            this.sailed.pop();
        }
        if (page === 'login') {
            this.navCtrl.navigateRoot('login').then(() => {
                this.sailed = ['login'];
            });
        } else if (page === 'home/dashboard' && this.sailed[this.sailed.length - 1] === 'home/dashboard') {
            this.navCtrl.navigateForward('dummy', { replaceUrl: true, animated: false })
                .then(() => {
                    this.showPage(page, false);
                });
        } else {
            this.showPage(page, false);
        }
    }

    back() {
        const currentPage = this.sailed[this.sailed.length - 1];
        if (currentPage === 'login' || currentPage === 'update-app') {
            this.exitApp();
        } else if (currentPage === 'home/dashboard') {
            this.confirmExitApp();
        } else if (currentPage === 'update/username') {
            this.confirmCloseSession();
        } else {
            this.sailed.pop();
            const page = this.sailed.pop();
            // page where can't navigate
            if (page === 'module/change-plan/confirm' ||
                page === 'module/change-plan/success') {
                this.sailed.push(page);
                this.back();
                return;
            }
            this.showPage(page, true);
        }
    }

    async confirmExitApp() {
        const alert = await this.alertController.create({
            header: 'Confirmación',
            message: '¿Esta seguro que desea salir de la aplicación?',
            buttons: [{
                text: 'Cancelar',
                role: 'cancel',
                cssClass: 'secondary'
            }, {
                text: 'Si, Salir',
                handler: () => {
                    this.exitApp();
                }
            }]
        });

        await alert.present();
    }

    async confirmCloseSession(onClose?, onCancel?) {
        const alert = await this.alertController.create({
            header: 'Salir',
            message: '¿Esta seguro que desea cerrar la sesión?',
            buttons: [{
                text: 'No',
                role: 'cancel',
                cssClass: 'secondary',
                handler: () => {
                    if (onCancel) {
                        onCancel();
                    }
                }
            }, {
                text: 'Si',
                handler: () => {
                    if (onClose) {
                        onClose();
                    }
                    this.closeSession();
                }
            }]
        });
        await alert.present();
    }

    public closeSession() {
        this.firebase.setScreenName('logout button');
        this.clearStore().then(() => {
            this.userStorage.logged = false;
            this.deleteDataForWidgets();
            this.goTo('login');
        });
    }

    public clearStore(): Promise<any> {
        return new Promise(resolve => {
            this.storage.get(keys.APP.PUSH_TOKEN).then(pushToken => {
                this.storage.clear().then(async () => {
                    const access = await this.storage.get(keys.APP.ACCEPTED_ACCESS);
                    await this.storage.set(keys.APP.ACCEPTED_ACCESS, access);
                    if (pushToken) {
                        this.storage.set(keys.APP.PUSH_TOKEN, pushToken).then(() => {
                            resolve();
                        });
                    } else {
                        resolve();
                    }
                });
            });
        });
    }

    private showPage(page: string, back: boolean) {
        this.navCtrl.navigateForward(page, { replaceUrl: true, animationDirection: back ? 'back' : 'forward' })
            .then(() => {
                if (page === 'home/dashboard') {
                    this.sailed = ['home/dashboard'];
                } else {
                    if (page !== this.sailed[this.sailed.length - 1]) { // only if don't contain same page
                        this.sailed.push(page);
                    }
                }
            });
    }

    serverListener(data: any, noError?: boolean) {
        return this.httpClient.post(environment.PROCESS_URL, data,
            {
                headers: this.getHeaders()
            }).pipe(
            timeout(environment.TIMEOUT),
            catchError(this.handleError),
            mergeMap(res => {
                return this.handlerResponse(res);
            }),
        );
    }

    handlerResponse(response: any, noError?: boolean): Observable<any> {
        return new Observable(observer => {
            if ((response.hasError || response.HasError || response.hasErrorField) && !noError) {
                let message = UtilsService.errorGeneral;
                if (response.errorDisplay) {
                    message = response.errorDisplay;
                } else {
                    if (response.errorDesc) {
                        message = response.errorDesc;
                    } else {
                        if (response.ErrorDesc) {
                            message = response.ErrorDesc;
                        }
                    }
                }
                if (message === 'Display Messsage 7') {
                    message = response.message;
                }
                observer.error({
                    message,
                    num: response.errorNum,
                    desc: response.errorDesc,
                    withStatus200: true,
                    token: response.token,
                    account: response.account,
                    accountType: response.accountType,
                    code: response.code
                });
            } else {
                observer.next(response);
                observer.complete();
            }
        });
    }

    handleError(error): Observable<any> {
        let errorMessage: string;
        if (error.status === 404 || error.status === 0) {
            errorMessage = UtilsService.error404;
        } else {
            errorMessage = UtilsService.errorGeneral;
        }
        return throwError({
            message: errorMessage,
            withStatus200: false,
            num: 0
        });
    }

    helpListener(data: any, url: any) {
        return this.httpClient.post(url, data,
            {
                headers: this.getHelpHeaders()
            })
            .pipe(
                timeout(environment.TIMEOUT),
                catchError(this.handleError),
                mergeMap(res => {
                    return this.handlerResponse(res);
                })
            );
    }

    getHelpHeaders(): HttpHeaders {
        let header = new HttpHeaders();
        header = header.append('api-key', '6af3982a-ce65-41a0-93d9-52bd172685cd'); // pones la key esta en las const
        header = header.append('content-type', 'application/json; charset=utf-8');
        return header;
    }

    getHeaders(): HttpHeaders {
        let header = new HttpHeaders();
        header = header.append('content-type', 'application/json; charset=utf-8');
        return header;
    }

    getHeadersWithToken(token): HttpHeaders {
        let header = new HttpHeaders();
        header = header.append('Authorization', `Bearer ${token}`);
        return header;
    }

    faultReportServerListener(data: FormData, url) {
        return this.httpClient.post(url, data,
            {}).pipe(
            timeout(environment.TIMEOUT),
            catchError(this.handleError),
            mergeMap(res => {
                return this.handlerResponse(res);
            }),
        );
    }

    getHtmlFromLocalTemplate(url) {
        const headers = new HttpHeaders({
            'Content-Type': 'text/plain',
        });
        return this.httpClient.get(url, {
            headers,
            responseType: 'text'
        });
    }

    iptvtServerListener(url, data: any) {
        return this.httpClient.post(url, data,
            {}).pipe(
            timeout(environment.TIMEOUT),
            catchError(this.handleError),
            mergeMap(res => {
                return this.handlerResponse(res);
            }),
        );
    }

    /**
     * only call on android or ios
     */
    storeDataForWidgets(account, subscriber, productType) {
        if (!Utils.getPlatformInfo().desktop) {
            const token = account + '||' + subscriber + '||' + this.moment().format('DD-MM-YYYY HH:mm:ss');
            const ENCRYPTION_KEY = 'qcPQK9012G3G7DCt';
            const ENCRYPTION_IV = '4W4NtvbLf85vUTZ3';
            const cipherText = CryptoJS.AES.encrypt(token, CryptoJS.enc.Utf8.parse(ENCRYPTION_KEY), {
                iv: CryptoJS.enc.Utf8.parse(ENCRYPTION_IV),
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });
            const tokenEncrypted = cipherText.toString();
            const url = environment.API_URL + 'widgets/getInvoiceInfo';

            const pref = Utils.getPlatformInfo().ios ? this.appPreferences.suite(environment.GROUP_ID) : this.appPreferences;
            console.log('GROUP_ID: ' + environment.GROUP_ID);
            const storeUrl = pref.store(PREF_URL, url).then(
                () => {
                    console.log('Shared: ' + PREF_URL + ' ' + url);
                }
            );
            const storeToken = pref.store(PREF_TOKEN, tokenEncrypted).then(
                () => {
                    console.log('Shared: ' + PREF_TOKEN + ' ' + tokenEncrypted);
                }
            );
            const storeType = pref.store(PREF_PRODUCT, productType).then(
                () => {
                    console.log('Shared: ' + PREF_PRODUCT + ' ' + productType);
                }
            );
            Promise.all([storeUrl, storeToken, storeType]).then(
                () => {
                    console.log('Shared preferences saved');
                }
            );
        }
    }

    deleteDataForWidgets() {
        if (!Utils.getPlatformInfo().desktop) {
            const pref = Utils.getPlatformInfo().ios ? this.appPreferences.suite(environment.GROUP_ID) : this.appPreferences;
            const storeUrl = pref.remove(PREF_URL).then(
                () => {
                    console.log('Removed: ' + PREF_URL);
                }
            );
            const storeToken = pref.remove(PREF_TOKEN).then(
                () => {
                    console.log('Removed: ' + PREF_TOKEN);
                }
            );
            const storeType = pref.remove(PREF_PRODUCT).then(
                () => {
                    console.log('Removed: ' + PREF_PRODUCT);
                }
            );
            Promise.all([storeUrl, storeToken, storeType]).then(
                () => {
                    console.log('Shared preferences removed');
                }
            );
        }
    }

    getIpClient(): Promise<string> {
        return new Promise((resolve, reject) => {
            this.httpClient.get('https://geolocation-db.com/json/')
                .subscribe((result: any) => {
                    resolve(result.IPv4);
                }, () => {
                    reject(UtilsService.errorGeneral);
                });
        });
    }

    exitApp() {
        const app = 'app';
        navigator[app].exitApp();
    }
}

export interface PopupBase {
    open: boolean;
    title: string;
    body: BodyPopupBase;
    extraClasses?: string;
    zIndex?: number;
}

export interface BodyPopupBase {
    name: string;
    [property: string]: any;
}

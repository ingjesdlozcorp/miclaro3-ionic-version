import { Injectable } from '@angular/core';
import { Utils } from '../utils/utils';
import { InAppBrowser, InAppBrowserObject, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

@Injectable({
    providedIn: 'root'
})
export class BrowserProvider {

    constructor(private iab: InAppBrowser) {
    }

    /**
     * openExternalBrowser:
     *   to open navigation web in a external browser like safari or chrome
     *   this method is called immediately
     */
    openExternalBrowser(url) {
        this.iab.create(url, '_system', {shouldPauseOnSuspend: 'yes'});
    }

    /**
     * openChatBrowser:
     *   to open navigation web in app, promise is called after 'in app web' is closed
     *   this is called in async task
     */
    openChatBrowser(url): Promise<any> {
        let isOpen = false;
        return new Promise((resolve, reject) => {
            const options: InAppBrowserOptions = {
                location: 'no',
                // @ts-ignore
                fullscreen: 'no',
                hidden: 'yes',
                toolbar: 'no',
                shouldPauseOnSuspend: 'yes'
            };

            const browser: InAppBrowserObject = this.iab.create(url, '_blank', options);
            if (!Utils.getPlatformInfo().desktop) {
                let redirectLink;
                browser.on('loaderror').subscribe(
                    (res) => {
                        console.log(res);
                        reject('En este momento no podemos acceder a la pagina solicitada.');
                        browser.close();
                    }
                );
                browser.on('loadstart').subscribe(
                    (res) => {
                        if (isOpen) {
                            if (res.url.trim() === url.trim()) {
                                redirectLink = undefined;
                                browser.close();
                            } else {
                                redirectLink = res.url;
                                browser.close();
                            }
                        }
                    }
                );
                browser.on('loadstop').subscribe(
                    (res) => {
                        console.log(res);
                        if (!isOpen) {
                            browser.show();
                        } else {
                            if (res.url.includes('.pdf')) {
                                redirectLink = res.url;
                                browser.close();
                            }
                        }
                        isOpen = true;
                    }
                );
                browser.on('exit').subscribe(
                    (res) => {
                        console.log(res);
                        resolve(redirectLink);
                    }
                );
            } else {
                browser.show();
                resolve();
            }
        });
    }

    /**
     * openPaymentBrowser:
     *   to open navigation web in app, promise is called after 'in app web' is closed
     *   this is called in async task
     */
    openPaymentBrowser(url): Promise<boolean> {
        let paymentCompleted = false;
        let paymentDone = false;
        let isOpen = false;
        return new Promise((resolve, reject) => {
            if (!Utils.getPlatformInfo().android) {
                BROWSER_GENERIC_THEME_OPTION.location = 'no';
                BROWSER_GENERIC_THEME_OPTION.lefttoright = 'yes';
            }
            const browser: InAppBrowserObject = this.iab.create(url, '_blank', BROWSER_GENERIC_THEME_OPTION);
            if (!Utils.getPlatformInfo().desktop) {
                browser.on('loaderror').subscribe(
                    (res) => {
                        console.log(res);
                        if (!paymentCompleted) {
                            paymentCompleted = true;
                            reject('En este momento no podemos acceder a la pagina solicitada.');
                            browser.close();
                        }
                    }
                );
                browser.on('loadstart').subscribe(
                    (res) => {
                        console.log(res);
                        if (res.url === BROWSER_URL_LIST.checkoutPaymentUrl ||
                            res.url === BROWSER_URL_LIST.referPaymentUrl ||
                            (res.url === BROWSER_URL_LIST.prodWebUrl)) {
                            paymentCompleted = true;
                            paymentDone = true;
                            browser.close();
                        }
                        if (res.url.includes('Payment/Confirmation')) {
                            paymentDone = true;
                        }
                    }
                );
                browser.on('loadstop').subscribe(
                    (res) => {
                        console.log(res);
                        if (!paymentCompleted) {
                            browser.show();
                        }
                        isOpen = true;
                    }
                );
                browser.on('exit').subscribe(
                    (res) => {
                        console.log(res);
                        resolve(paymentDone);
                    }
                );
            } else {
                browser.show();
                resolve();
            }
        });
    }

    /**
     * openRecoveryBrowser:
     *   to open navigation web in app, promise is called after 'in app web' is closed
     *   this is called in async task
     */
    openRecoveryBrowser(url) {
        let changeCompleted = false;
        return new Promise((resolve, reject) => {
            if (!Utils.getPlatformInfo().android) {
                BROWSER_GENERIC_THEME_OPTION.location = 'no';
                BROWSER_GENERIC_THEME_OPTION.lefttoright = 'yes';
            }
            const browser: InAppBrowserObject = this.iab.create(url, '_blank', BROWSER_GENERIC_THEME_OPTION);
            if (!Utils.getPlatformInfo().desktop) {
                browser.on('loaderror').subscribe(
                    (res) => {
                        if (!changeCompleted) {
                            changeCompleted = true;
                            reject('En este momento no podemos acceder a la pagina solicitada.');
                            browser.close();
                        }
                    }
                );
                browser.on('loadstart').subscribe(
                    (res) => {
                        if (this.checkIfRecoverPasswordProcessIsDone(res)) {
                            changeCompleted = true;
                            browser.close();
                        }
                    }
                );
                browser.on('loadstop').subscribe(
                    (res) => {
                        if (!changeCompleted) {
                            browser.show();
                        }
                        if (this.checkIfRecoverPasswordProcessIsDone(res)) {
                            changeCompleted = true;
                            browser.close();
                        }
                    }
                );
                browser.on('exit').subscribe(
                    (res) => {
                        resolve(changeCompleted);
                    }
                );
            } else {
                browser.show();
                resolve();
            }
        });
    }

    checkIfRecoverPasswordProcessIsDone(data): boolean {
        return (data.url === BROWSER_URL_LIST.loginProd || data.url === BROWSER_URL_LIST.loginUat);
    }
}

export const BROWSER_GENERIC_THEME_OPTION: InAppBrowserOptions = {
    location: 'yes',
    hidden: 'yes',
    fullscreen: 'no',
    toolbarcolor: '#ef3829',
    zoom: 'no',
    closebuttoncolor: '#ffffff',
    closebuttoncaption: 'CERRAR',
    // Only IOS
    presentationstyle: 'fullscreen',
    toolbarposition: 'top',
    hideurlbar: 'yes',
    toolbar: 'yes',
    hidenavigationbuttons: 'yes',
    shouldPauseOnSuspend: 'yes'
};

const BROWSER_URL_LIST = {
    loginProd: 'https://miclaro.claropr.com/login',
    loginUat: 'https://miclarouat.claropr.com/login',
    checkoutPaymentUrl: 'https://checkout.evertecinc.com/Close.aspx',
    referPaymentUrl: 'http://miclaroreferals.claroinfo.com/Close-Payments/mmpClosingPyamnet.aspx',
    prodWebUrl: 'https://miclaro.claropr.com/'
};

import { Injectable } from '@angular/core';
import {
    AccountDetails,
    AccountInfo,
    MessagesList,
    Qualification,
    SubscriberInfo
} from '../models/account.details';
import { Account, Authenticate } from '../models/authenticate';
import { Page, Section } from '../models/account.access';
import { Utils } from '../utils/utils';
import { Offer } from '../pages/module/equipment-and-services/change-plan/change-plan.component';
import { OrderInfo } from '../pages/module/equipment-and-services/change-plan/change-plan-confirm/change-plan-confirm.component';
import { Packet } from '../pages/module/purchases/data-plan/data-plan.component';
import { Payment } from '../models/payment';
import { Plan } from '../models/iptv';

@Injectable({
    providedIn: 'root'
})
export class IntentProvider {

    // tslint:disable-next-line:variable-name
    private _isAppReady: boolean;
    // tslint:disable-next-line:variable-name
    private _loginData: Authenticate;
    // tslint:disable-next-line:variable-name
    private _accountDetails: AccountDetails;
    // tslint:disable-next-line:variable-name
    private _access: Section[];
    // tslint:disable-next-line:variable-name
    private _tab: number; // 0: Postpaid, 1: Prepaid, 2 Telephony
    // tslint:disable-next-line:variable-name
    private _logged: boolean;
    // tslint:disable-next-line:variable-name
    private _selectedSubscriber: SubscriberInfo;
    // tslint:disable-next-line:variable-name
    private _selectedPlan: Offer;
    // tslint:disable-next-line:variable-name
    private _selectedPacket: Packet;
    // tslint:disable-next-line:variable-name
    private _orderInfo: OrderInfo;
    // tslint:disable-next-line:variable-name
    private _payment: Payment;
    // tslint:disable-next-line:variable-name
    private _prepaidCustomerId: number;
    // tslint:disable-next-line:variable-name
    private _prepaidSelectedSubscriber: string;
    // tslint:disable-next-line:variable-name
    private _PaymentProcess: {
        subscriber?: string;
        payments?: any[];
        products?: any[];
        address?: any[];
        selectedProduct?: any;
        amount?: string;
        ivu?: string;
        totalAmount?: string;
        transactionResult?: {
            status: any;
            transactionId: any;
            rechargeId: any;
            date: any;
        };
        paymentToken?: string;
    };
    // tslint:disable-next-line:variable-name
    private _hasConfirmDue: boolean;
    // tslint:disable-next-line:variable-name
    private _accountListRefer: any;
    // tslint:disable-next-line:variable-name
    private _redirectData: {
        url: string
    };
    // tslint:disable-next-line:variable-name
    private _flashPayment?: {
        flashData?: any,
        newPaymentRequest?: any,
        doPaymentRequest?: any,
        operationAmount?: any,
        paymentId?: string,
    } = {};

    // tslint:disable-next-line:variable-name
    private _processIptvFrom: string;
    // tslint:disable-next-line:variable-name
    private _planIptvSelected: Plan;
    // tslint:disable-next-line:variable-name
    private _tokenIptv: string;
    // tslint:disable-next-line:variable-name
    private _accountSSO: any;
    // tslint:disable-next-line:variable-name
    private _orderIptvProcessed: { order: any, deviceStreaming, addons };
    // tslint:disable-next-line:variable-name
    private _firstTimeDashboardTelephony: boolean;

    get isAppReady(): boolean {
        return this._isAppReady;
    }

    set isAppReady(value: boolean) {
        this._isAppReady = value;
    }

    /* Authenticate Details */
    get loginData() {
        return this._loginData;
    }

    set loginData(value) {
        this._loginData = value;
    }

    get accountList(): Account[] {
        if (this.loginData) {
            if (this.isGuest) {
                const accounts: Account[] = [];
                accounts.push(this.getGuestAccount());
                return accounts;
            } else {
                return this._loginData.accounts.AccountList;
            }
        } else {
            console.log('No authenticated data found');
            return [];
        }
    }

    get tokenSession(): string {
        if (this._loginData) {
            return this._loginData.token;
        } else {
            console.log('No authenticated data found');
            return '';
        }
    }

    get SSOAccessToken(): string {
        if (this._loginData) {
            return this._loginData.SSOAccessToken;
        } else {
            console.log('No authenticated data found');
            return '';
        }
    }

    get isGuest(): boolean {
        if (this._loginData) {
            return this._loginData.enterAsGuest;
        } else {
            console.log('No authenticated data found');
            return null;
        }
    }

    get postpaidAccounts(): Account[] {
        if (this._loginData) {
            const accounts = [];
            const allAccounts = this.accountList;
            allAccounts.forEach((account: Account) => {
                if (Utils.isPostpaid(account.accountType, account.accountSubType, account.productType)) {
                    accounts.push(account);
                }
            });
            return accounts;
        } else {
            console.log('No authenticated data found');
            return null;
        }
    }

    get prepaidAccounts(): Account[] {
        if (this._loginData) {
            const accounts = [];
            const allAccounts = this.accountList;
            allAccounts.forEach((account: Account) => {
                if (Utils.isPrepaid(account.accountType, account.accountSubType)) {
                    accounts.push(account);
                }
            });
            return accounts;
        } else {
            console.log('No authenticated data found');
            return null;
        }
    }

    get telephonyAccounts(): Account[] {
        if (this._loginData) {
            const accounts = [];
            const allAccounts = this.accountList;
            allAccounts.forEach((account: Account) => {
                if (Utils.isTelephony(account.accountType, account.accountSubType, account.productType)) {
                    accounts.push(account);
                }
            });
            return accounts;
        } else {
            console.log('No authenticated data found');
            return null;
        }
    }

    getAccountByBan(ban: string): Account {
        if (this._loginData) {
            if (this.isGuest) {
                return this.getGuestAccount();
            } else {
                return this.accountList.find(x => x.account === ban);
            }
        } else {
            console.log('No authenticated data found');
            return null;
        }
    }

    getGuestAccount(): Account {
        return {
            account: this._loginData.account,
            accountType: this._loginData.accountType,
            accountSubType: this._loginData.accountSubType,
            subsriberByDefault: this._loginData.subscriber,
            productType: this._loginData.productType
        };
    }


    /* Account details vars */
    set accountDetails(value) {
        this._accountDetails = value;
    }

    get accountDetails(): AccountDetails {
        return this._accountDetails;
    }

    get accountInfo(): AccountInfo {
        if (this._accountDetails) {
            return this._accountDetails.AccounInfo;
        } else {
            return null;
        }
    }

    get notifications(): MessagesList[] {
        return this._accountDetails.Messages.MessagesList;
    }

    get countPendingNotifications(): number {
        return this._accountDetails.Messages.newMessageCounter;
    }

    get subscriberList(): SubscriberInfo[] {
        return this._accountDetails.SubscriberInfo;
    }

    get getQualification(): Qualification {
        return this._accountDetails.qualification;
    }

    get isPostpaidAccount(): boolean {
        if (!this.accountInfo) {
            return false;
        }
        const account: Account = this
            .getAccountByBan(String(this.accountInfo.bANField));
        return Utils.isPostpaid(account.accountType, account.accountSubType, account.productType);
    }

    get isPrepaidAccount(): boolean {
        if (!this.accountInfo) {
            return false;
        }
        const account: Account = this
            .getAccountByBan(String(this.accountInfo.bANField));
        return Utils.isPrepaid(account.accountType, account.accountSubType);
    }

    get isTelephonyAccount(): boolean {
        if (!this.accountInfo) {
            return false;
        }
        const account: Account = this
            .getAccountByBan(String(this.accountInfo.bANField));
        return Utils.isTelephony(account.accountType, account.accountSubType, account.productType);
    }

    get isByop(): boolean {
        if (!this.accountInfo) {
            return false;
        }
        const account: Account = this
            .getAccountByBan(String(this.accountInfo.bANField));
        return Utils.isByop(account.accountType, account.accountSubType, account.productType);
    }


    /* User access vars */
    set access(value) {
        this._access = value;
    }

    get access(): Section[] {
        return this._access;
    }

    getAccessPageByID(id: number) {
        let aPage = null;
        this.access.forEach((section: Section) => {
            section.Pages.forEach((page: Page) => {
                if (page.accessID === id) {
                    aPage = page;
                    return aPage;
                }
            });
        });
        return aPage;
    }


    get tab(): number {
        return this._tab;
    }

    set tab(value: number) {
        this._tab = value;
    }

    get logged(): boolean {
        if (this._logged === undefined) {
            return false;
        }
        return this._logged;
    }

    set logged(value: boolean) {
        this._logged = value;
    }

    get selectedSubscriber(): SubscriberInfo {
        return this._selectedSubscriber;
    }

    set selectedSubscriber(value: SubscriberInfo) {
        this._selectedSubscriber = value;
    }

    get selectedPlan(): Offer {
        return this._selectedPlan;
    }

    set selectedPlan(value: Offer) {
        this._selectedPlan = value;
    }

    get selectedPacket(): Packet {
        return this._selectedPacket;
    }

    set selectedPacket(value: Packet) {
        this._selectedPacket = value;
    }

    get orderInfo(): OrderInfo {
        return this._orderInfo;
    }

    set orderInfo(value: OrderInfo) {
        this._orderInfo = value;
    }

    get payment(): Payment {
        return this._payment;
    }

    set payment(value: Payment) {
        this._payment = value;
    }

    get prepaidCustomerId(): number {
        return this._prepaidCustomerId;
    }

    set prepaidCustomerId(value: number) {
        this._prepaidCustomerId = value;
    }


    get prepaidSelectedSubscriber(): string {
        return this._prepaidSelectedSubscriber;
    }

    set prepaidSelectedSubscriber(value: string) {
        this._prepaidSelectedSubscriber = value;
    }

    getFullCurrentSelectedSubscriber(): SubscriberInfo {
        if (this._loginData) {
            return this.subscriberList.find(x => x.subscriberNumberField === this.prepaidSelectedSubscriber);
        } else {
            console.log('No authenticated data found');
            return null;
        }
    }

    get PaymentProcess() {
        return this._PaymentProcess;
    }

    set PaymentProcess(value) {
        this._PaymentProcess = value;
    }

    get hasConfirmDue(): boolean {
        return this._hasConfirmDue;
    }

    set hasConfirmDue(value: boolean) {
        this._hasConfirmDue = value;
    }

    get accountListRefer(): any {
        return this._accountListRefer;
    }

    set accountListRefer(value: any) {
        this._accountListRefer = value;
    }

    containsSubscriber(subscriber: string): boolean {
        if (this._loginData) {
            const object: SubscriberInfo = this.accountDetails.SubscriberInfo
                .find(x => x.subscriberNumberField === subscriber);
            return !!object;
        } else {
            console.log('No authenticated data found');
            return false;
        }
    }

    isBusiness(): boolean {
        if (this._loginData) {
            return Utils.isBusinessAccount(this._loginData.accountType);
        } else {
            console.log('No authenticated data found');
            return false;
        }
    }

    get redirectData(): { url: string } {
        return this._redirectData;
    }

    set redirectData(value: { url: string }) {
        this._redirectData = value;
    }

    // FLASH PAYMENT

    get flashPayment() {
        return this._flashPayment;
    }

    set flashPayment(value: any) {
        if (!this._flashPayment) {
            this._flashPayment = {};
        }
        this._flashPayment = value;
    }

    // IPTV

    get tokenIptv(): string {
        return this._tokenIptv;
    }

    set tokenIptv(value: string) {
        this._tokenIptv = value;
    }

    get accountSSO(): any {
        return this._accountSSO;
    }

    set accountSSO(value: any) {
        this._accountSSO = value;
    }

    get planIptvSelected(): Plan {
        return this._planIptvSelected;
    }

    set planIptvSelected(value: Plan) {
        this._planIptvSelected = value;
    }

    get orderIptvProcessed(): any {
        return this._orderIptvProcessed;
    }

    set orderIptvProcessed(value: any) {
        this._orderIptvProcessed = value;
    }

    get firstTimeDashboardTelephony(): boolean {
        return this._firstTimeDashboardTelephony;
    }

    set firstTimeDashboardTelephony(value: boolean) {
        this._firstTimeDashboardTelephony = value;
    }

    get processIptvFrom(): string {
        return this._processIptvFrom;
    }

    set processIptvFrom(value: string) {
        this._processIptvFrom = value;
    }
}

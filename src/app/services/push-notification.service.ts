import { Injectable } from '@angular/core';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { AngularFirestore } from '@angular/fire/firestore';
import { Utils } from '../utils/utils';
import { StorageProvider } from './storage.provider';
import {BrowserProvider} from './browser.provider';

@Injectable({
    providedIn: 'root'
})
export class PushNotificationService {

    constructor(
        private firebase: FirebaseX,
        private firestore: AngularFirestore,
        private storageProvider: StorageProvider,
        private browserProvider: BrowserProvider
        ) { }

    saveFirebaseToken() {
        this.storageProvider.isPushGranted().then(
            (granted: boolean) => {
                if (granted) {
                    this.saveTokenLocal();
                } else {
                    try {
                        this.firebase.grantPermission().then(
                            async hasPermission => {
                                if (hasPermission) {
                                    await this.storageProvider.setPushGranted(true);
                                    this.saveTokenLocal();
                                }
                            });
                    } catch (e) {
                        console.error('FirebasePlugin grantPermission error', e);
                    }
                }
            }
        );
        this.onTokenRefresh();
    }

    private saveTokenLocal() {
        this.firebase.getToken().then(
            token => {
                console.log('fmc token', token);
                this.storageProvider.setNotificationPushToken(token);
            },
            error => {
                console.error('get fmc token');
                console.log(error);
            }
        );
    }

    onTokenRefresh() {
        // We use FCM only for Android. For iOS - we use plain APNS
        if (!Utils.getPlatformInfo().ios) {
            this.firebase.onTokenRefresh().subscribe(
                token => {
                    console.log(token);
                    this.storageProvider.setNotificationPushToken(token);
                },
                error => {
                    console.error(error);
                }
            );
        } else {
            // iOS device token
            this.firebase.onApnsTokenReceived().subscribe(
                token => {
                    console.log(token);
                    this.storageProvider.setNotificationPushToken(token);
                },
                error => {
                    console.error(error);
                }
            );
        }

    }

    onMessageReceived() {
        this.firebase.onMessageReceived().subscribe(
            (data: any) => {
                console.log(data);
                const url = data.click_action;
                this.browserProvider.openExternalBrowser(url);
            },
            error => {
                console.error(error);
            }
        );
    }
}

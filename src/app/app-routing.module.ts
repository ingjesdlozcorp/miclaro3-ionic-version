import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './pages/login/login/login.component';
import {GuestComponent} from './pages/login/guest/guest.component';
import {DummyComponent} from './pages/dummy/dummy.component';
import {TouchComponent} from './pages/login/touch/touch.component';
import { UpdateAppComponent } from './pages/update-app/update-app.component';
import { MfaComponent } from './pages/login/mfa/mfa.component';
import { PushConfirmationComponent } from './pages/login/push-confirmation/push-confirmation.component';

export const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'mfa', component: MfaComponent },
    { path: 'authentication-callback', component: PushConfirmationComponent },
    { path: 'update-app', component: UpdateAppComponent },
    { path: 'guest', component: GuestComponent },
    { path: 'touch', component: TouchComponent },
    { path: 'dummy', component: DummyComponent },
    { path: 'register', loadChildren: './pages/register/register.module#RegisterModule' },
    { path: 'recover', loadChildren: './pages/recover/recover.module#RecoverModule' },
    { path: 'update', loadChildren: './pages/update/update.module#UpdateModule' },
    { path: 'home', loadChildren: './pages/home/home.module#HomeModule' },
    { path: 'help', loadChildren: './pages/help/help.module#HelpModule' },
    { path: 'module', loadChildren: './pages/module/module.module#ModuleModule' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        scrollPositionRestoration: 'enabled'
    })],
    exports: [RouterModule],
    providers: []
})

export class AppRoutingModule { }

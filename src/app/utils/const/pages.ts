export const pages = {
    DUMMY: 'dummy',
    GUEST: 'guest',
    LOGIN: 'login',
    HOME: 'home/dashboard',
    REGISTER: 'register/step1',
    UPDATE_APP: 'update-app',
    INVOICE_SUMMARY: 'module/invoice-summary',
    INVOICE_DOWNLOAD: 'module/invoice-download',
    INVOICE_HISTORY: 'module/invoice-history',
    ELECTRONIC_BILL: 'module/electronic-bill',
    DIRECT_DEBIT: 'module/direct-debit',
    CONSUMPTION: 'module/consumption',
    CONSUMPTION_PREPAID: 'module/consumption-prepaid',
    SERVICES_AND_EQUIPMENT: 'module/device',
    CHANGE_PLAN: 'module/change-plan',
    NETFLIX: 'module/netflix',
    REFERRER: 'module/refer/home',
    CLUB: 'module/club/home',
    PURCHASES: 'module/purchases',
    DATA_PLAN: 'module/data-plan',
    SHOPPING_HISTORY: 'module/shopping-history',
    GIFT_ONE_GB: 'module/gift/one-gb',
    GIFT_RECHARGE: 'module/gift/recharge',
    NOTIFICATIONS: 'notifications',
    PROFILE: 'module/profile',
    PROFILE_NAME: 'module/profile/name',
    PROFILE_EMAIL: 'module/profile/email',
    PROFILE_PASSWORD: 'module/profile/password',
    PROFILE_POSTAL: 'module/profile/postal',
    PROFILE_QUESTIONS: 'module/profile/questions',
    ACCOUNTS_MANAGE: 'module/add-account',
    ADD_ACCOUNTS: 'module/add-account/1',
    RECHARGE: 'module/recharge',
    TRANSACTIONS: 'module/recharge/history',
    FAULT_REPORT: 'module/fault/step1',
    SUPPORT: 'help/home',
    FAQ: 'help/faq',
    RECOVER_PASSWORD: 'recover/step1'
};


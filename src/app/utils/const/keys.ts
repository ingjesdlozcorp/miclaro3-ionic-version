export const keys = {
    SESSION_TOKEN: 'session-token',
    SSO_ACCESS_TOKEN: 'SSOAccessToken',
    RECOVER_PASSWORD: {
        IS_TELEPHONY: 'recover-password-is-telephony',
        QUESTION_LIST: 'recover-password-question-list',
        SUBSCRIBER: 'recover-password-subscriber',
        EMAIL: 'recover-password-email',
        TEMPORARY_PASSWORD: 'recover-password-new-password',
        TOKEN: 'recover-password-session-token'
    },
    REGISTER: {
        IS_POSTPAID: 'register-is-postpaid',
        IS_PREPAID: 'register-is-prepaid',
        SUBSCRIBER: 'register-subscriber',
        SSN: 'register-ssn',
        CODE: 'register-code',
        EMAIL: 'register-email',
        TOKEN: 'register-session-token',
        IS_GUEST_UPDATE: 'register-is-guest-update'
    },
    UPDATE_REGISTRATION: {
        USERNAME_USED: 'update-registration-username-used',
        PASSWORD_USED: 'update-registration-password-used',
        KEEP_AUTH: 'update-registration-keep',
        SUBSCRIBER: 'update-registration-subscriber',
        PRODUCT_TYPE_G: 'update-registration-is-type-G',
        IS_TELEPHONY: 'update-registration-is-telephony',
        SSN: 'update-registration-ssn',
        CODE: 'update-registration-code',
        TOKEN: 'update-registration-session-token',
    },
    LOGIN: {
        DATA: 'login-full-data',
        TRY_IN: 'login-try-in-now',
        USERNAME: 'login-username',
        SUBSCRIBER: 'login-subscriber',
        PASSWORD: 'login-password',
        KEEP: 'login-keep-authenticated',
        BIOMETRIC: 'login-biometric-authenticated',
        IS_LOGGED: 'login-is-logged',
        IS_GUEST: 'login-is-guest'
    },
    LIST: {
        STORES: 'stores',
        STORE_TYPE: 'active-store-type',
        STORE: 'store',
        SELECTED_STORE: 'selected-store'
    },
    /**
     *  Necesitaba estas constantes en el help sectión
     *  despues se coordina  la verdadera estructura del
     * ACCOUNT_DETAILS:
     *
     */
    ACCOUNT_DETAILS: {
        SELECTED_ACCOUNT: 'selected-account',
        ACCOUNT_INFO: 'account-info',
        ACCOUNT_LIST_SUBSCRIBER: 'accounts-list-subscribers',
        ACCOUNT_LIST_SUBSCRIBER_IS_LOADED: 'accounts-subscribers-is-loaded'
    },
    HISTORY: {
        VISITED: 'visited'
    },
    DEBIT_DIRECT: {
        PAY_DIRECT_DEBIT: 'pay-debit-direct'
    },
    NOTIFICATION: {
        NOTIFICATION_LIST: 'notifications'
    },
    ORDERS: {
       IS_LOADED: 'account-orders-is-loaded',
       LIST: 'account-list-orders'
    },
    NETFLIX: {
        SUBSCRIBER_LOADED: 'netflix-accounts-subscribers-is-loaded',
        NETFLIX_SUBSCRIBER: 'netflix-accounts-list-subscribers',
        ACCOUNT: 'netflix-subscription-account-number',
        SUBSCRIBER_NUMBER: 'netflix-subscription-subscriber-number'
    },
    REFER: {
        ACCOUNT_LIST_REFER: 'accounts-list-refer',
        ACCOUNT_REFER_IS_LOADED: 'accounts-refer-is-loaded',
        REFER_DATA: 'referrer-data',
        REFERRER_MEMBER: 'referrer-member',
        HOBIES_LIST: 'hobbiesList',
        REFERRER_VALID_MEMBER: 'referred-valid-member-id'
    },
    PAYMENT: {
        PAYMENT_DETAILS: 'payment-data_details',
        PAYMENT_AMOUNT: 'payment-data_amount',
        PAYMENT_SUBSCRIBER: 'payment-data_subscriber',
        PAYMENT_SUBSCRIBER_TYPE: 'payment-data_subscriber_type',
        PAYMENT_ACCOUNT: 'payment-data_account',
        PAYMENT_EMAIL: 'payment-data_email',
        PAYMENT_DESCRIPTION: 'payment-data_description',
        PAYMENT_TYPE: 'payment-data_type',
        PAYMENT_GIFT_1GB: 'payment-data_gift-1GB-data',
        PAYMENT_GIFT_RECHARGE: 'payment-data_gift-recharge-data',
        GIFT_SENT_TEXT: 'gift-send-text',
        PAYMENT_VERSION: 'payment-user-version',
        PAYMENT_FLASH: {
            FLASH_DATA: 'flash-data'
        }
    },
    GIFT: {
        DATA_GIFT: 'data-gift'
    },
    APP: {
        OUTDATED_APP: 'outdated-app',
        IS_RATED: 'app-is-done-rated',
        OPEN_APP_TIMES: 'open_app_times',
        PUSH_GRANTED: 'push-granted',
        PUSH_TOKEN: 'push-token',
        ACCEPTED_ACCESS: 'accepted-access',
    },
    USER_ID:'user-id',
    MFA_TOKEN:'mfa-token'
};


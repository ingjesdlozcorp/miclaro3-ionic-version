export enum Popups {
    IPTV_CHANNELS = 'popup-iptv-channels',
    TERMS_OFFER = 'popup-terms-offer',
    ACCOUNT_MESSAGE = 'account-client-message',
}

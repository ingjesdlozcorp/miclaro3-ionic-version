import { Injectable } from '@angular/core';
import { SubscriberInfo } from '../models/account.details';
import { formatDate } from '@angular/common';

/* this class should be used only for static methods */
@Injectable()
export class Utils {

    static validateEmail(email): boolean {
        const expr = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return expr.test(String(email).toLowerCase());
    }

    static isPostpaid(accountType, accountSubType, productType) {
        return (accountType === 'I2' && accountSubType === '4') ||
            (accountType === 'I3' && accountSubType === '4') ||
            (accountType === 'I' && accountSubType === 'R') ||
            (accountType === 'I' && accountSubType === '4') ||
            (accountType === 'I' && accountSubType === 'E') ||
            (accountType === 'I' && accountSubType === 'S' && productType === 'G');
    }

    static isPrepaid(accountType, accountSubType) {
        return (accountType === 'I' && accountSubType === 'P') ||
            (accountType === 'I3' && accountSubType === 'P');
    }

    static isTelephony(accountType, accountSubType, productType) {
        return (accountType === 'I' && accountSubType === 'W') ||
            (accountType === 'I' && accountSubType === 'S' && productType === 'O') ||
            (accountType === 'I' && accountSubType === 'S' && productType === 'V') ||
            (accountType === 'F' && accountSubType === '4');
    }

    static isByop(accountType, accountSubType, productType) {
        return (accountType === 'I3' && accountSubType === '4') ||
            (accountType === 'I3' && accountSubType === 'p');
    }

    static isBusinessAccount(accountType): boolean {
        return accountType === 'B' || accountType === 'G';
    }

    static objectTypeOfSubscriber(accountType, accountSubType, productType): any {
        return {
            postpaid: Utils.isPostpaid(
                accountType,
                accountSubType,
                productType
            ),
            prepaid: Utils.isPrepaid(
                accountType,
                accountSubType
            ),
            telephony: Utils.isTelephony(
                accountType,
                accountSubType,
                productType
            ),
            byop: Utils.isByop(
                accountType,
                accountSubType,
                productType
            )
        };
    }

    static objectTypeOfAccount(accountType, accountSubType, productType): any {
        return this.objectTypeOfSubscriber(accountType, accountSubType, productType);
    }

    /*
     * G or C = WLS, Others = WRL
     */
    static subscriberIsMobile(productType) {
        return (productType === 'C' || productType === 'G');
    }

    // Use only in validations to iptv
    static isTelephonyOnlyLine(accountType, accountSubType, productType) {
        return (accountType === 'I' && accountSubType === 'W' && productType === 'O') || (accountType === 'I' && accountSubType === 'S' && productType === 'G');
    }

    static isDLS(accountSubtype, accountType) {
        return (accountSubtype === 'W' && accountType === 'I');
    }

    static formatAmount(amount) {
        if (amount === undefined || amount === null || amount === '' || amount === 'not_set') {
            amount = '0.00';
        }
        const newAmount = parseFloat(String(amount).replace('$', ''));
        const formatter = new Intl.NumberFormat('en-US', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
        });
        amount = formatter.format(newAmount);
        return amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

    static typeOfTelephony(productType: string): string {
        // O = wireline - fijo, I = IPTV - fijo, V = VOIP - fijo, N = ISP -fijo, J = DTH - fijo, S = DISH nuevo - fijo
        // C = cellular - móvil, G = GSM - móvil, P & K móvil if business

        let type;
        if (productType === 'O' || productType === 'I' || productType === 'V' || productType === 'N' || productType === 'J' || productType === 'S') {
            type = 'Fijo';
        } else if (productType === 'C' || productType === 'G' || productType === 'P' || productType === 'K') {
            type = 'Móvil';
        }
        return type;
    }

    static convertCase(str) {
        const lower = String(str).toLowerCase();
        return lower.replace(/(^| )(\w)/g, (x) => x.toUpperCase());
    }

    /*
    dateForTimePassword() {
        var now = new Date();
        now.setTime(now.getTime() + (1000 * 60 * app.sessionPasswordTime));
        return now;
    }
    */

    // CARD VALIDATE
    static getTypeCard(numberCard: string): string | boolean {
        if (this.isCardVisa(numberCard)) {
            return 'VS';
        } else if (this.isCardMaster(numberCard)) {
            return 'MC';
        } else if (this.isCardAmerican(numberCard)) {
            return 'AM';
        } else {
            return false;
        }
    }

    static isCardVisa(numberCard) {
        const expVisa = /^4\d{15}(\d{3})?$/;
        return expVisa.test(numberCard);
    }

    static isCardMaster(numberCard) {
        const expMaster = /^4\d{15}(\d{3})?$/;
        return expMaster.test(numberCard);
    }

    static isCardAmerican(numberCard) {
        const expAmerican = /^4\d{15}(\d{3})?$/;
        return expAmerican.test(numberCard);
    }

    // **************************************************

    static transformAvailable(remaining) { // receive in kilobytes

        const remainingMB = (remaining / 1024) / 1024;
        const remainingGB = ((remaining / 1024) / 1024) / 1024;

        let text = remainingMB.toFixed(2) + ' MB';
        if (remainingMB > 2048) {
            text = remainingGB.toFixed(2) + ' GB';
        }
        return text;
    }

    static formatSubscriber(subscriber) { // for subscriber
        let newNumber = '(' + splice(subscriber + '', 3, 0, ') ');
        newNumber = splice(newNumber, 9, 0, '-');
        return newNumber;
    }

    static parseDate(dateString?: string): string {
        let date: Date;
        if (dateString) {
            date = new Date(dateString);
        } else {
            date = new Date();
        }
        const month = date.getMonth();
        const dayOfMonth = date.getDate();
        const dayOfWeek = date.getDay();
        const year = date.getFullYear();
        const daysList = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'];
        const monthList = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        return daysList[dayOfWeek] + ', ' + dayOfMonth + ' de ' + monthList[month] + ' de ' + year;
    }

    static getSubscriberByNumber(iNumber: string, subscribers: SubscriberInfo[]): SubscriberInfo {
        let subscriber = null;
        subscribers.forEach((object: SubscriberInfo) => {
            if (iNumber === object.subscriberNumberField) {
                subscriber = object;
            }
        });
        return subscriber;
    }

    static getCardMonths(): any[] {
        // Months
        const months = [];
        for (let j = 1; j <= 12; j++) {
            months.push((j < 10) ? '0' + j : j);
        }
        return months;
    }

    static getCardYears(): any[] {
        // Years
        const years = [];
        const today = formatDate(new Date(), 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
        let now = Number(today.substring(6, 10));
        for (let i = 0; i < 10; i++) {
            years.push(now);
            now++;
        }
        return years;
    }

    static getLast10Years(): any[] {
        // Years
        const years = [];
        const today = formatDate(new Date(), 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
        let now = Number(today.substring(6, 10));
        for (let i = 0; i < 10; i++) {
            years.push(now);
            now--;
        }
        return years;
    }

    static getCurrentMonth(): string {
        const today = formatDate(new Date(), 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
        let month = today.substring(3, 5);
        if (month.length === 1) {
            month = '0' + month;
        }
        console.log(month);
        return month;
    }

    static getCurrentYear(): string {
        const today = formatDate(new Date(), 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
        return today.substring(6, 10);
    }

    static getPlatformInfo(): PlatformInfo {
        return {
            android: (navigator.userAgent.toLowerCase().indexOf('android') > -1),
            iphone: (navigator.userAgent.toLowerCase().indexOf('iphone') > -1),
            ipad: (navigator.userAgent.toLowerCase().indexOf('ipad') > -1),
            ios: (navigator.userAgent.toLowerCase().indexOf('iphone') > -1)
                || (navigator.userAgent.toLowerCase().indexOf('ipad') > -1),
            desktop: !window.hasOwnProperty('cordova')
        };
    }

    static getPlatformOS(): PlatformOS {
        const {ios, ipad, iphone} = Utils.getPlatformInfo();
        return ios || ipad || iphone ? PlatformOS.IOS : PlatformOS.ANDROID;
    }

    static formatStringDate(stringDate) {
        return new Date(stringDate).toLocaleString();
    }

    static formatStringDateForSIG(stringDate) {
        return formatDate(new Date(stringDate), 'yyyy-MM-dd hh:mm', 'en-US');
    }

    static formatMMDDYYYYToYYYY_MM_DD(stringDate) {
        const text = stringDate.split('/');
        return `${text[2]}-${text[0]}-${text[1]}`;
    }

    static formatBasicCartItem(token): CartItem {
        return {
            token,
            promoCode: '',
            installments: 24,
            decDeposit: 0,
            decDownPayment: 0,
            Qty: 1,
            flowId: 1,
            ssoToken: '',
            userID: 0,
            parentProductId: 0,
            parentCartId: 0,
            creditClass: 'C',
            downgradeAllowed: false,
            pendingAccelerated: 0,
            acceletartedAmount: 0,
            pastDueAmount: 0,
            delicuency: false
        };
    }

    static convertBase64ToBlob(b64Data, contentType): Blob {
        contentType = contentType || '';
        const sliceSize = 512;
        b64Data = b64Data.replace(/^[^,]+,/, '');
        b64Data = b64Data.replace(/\s/g, '');
        const byteCharacters = window.atob(b64Data);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);
            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        return new Blob(byteArrays, {type: contentType});
    }
}

function splice(text, start, delCount, newSubStr) {
    return text.slice(0, start) + newSubStr + text.slice(start + Math.abs(delCount));
}

export interface PlatformInfo {
    android: boolean;
    iphone: boolean;
    ipad: boolean;
    ios: boolean;
    desktop: boolean;
}

export interface CartItem {
    token: string;
    productId?: number;
    promoCode: string;
    installments: number;
    decPrice?: number;
    decDeposit: number;
    decDownPayment: number;
    decTotalPrice?: number;
    Qty: number;
    flowId: number;
    ssoToken: string;
    userID: number;
    parentProductId: number;
    parentCartId: number;
    creditClass: string;
    downgradeAllowed: boolean;
    pendingAccelerated: number;
    acceletartedAmount: number;
    pastDueAmount: number;
    delicuency: boolean;
}

export enum PlatformOS {
    IOS = 'IOS',
    ANDROID = 'ANDROIDOS'
}

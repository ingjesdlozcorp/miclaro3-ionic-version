import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { IonicStorageModule } from '@ionic/storage';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Device } from '@ionic-native/device/ngx';
import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { Network } from '@ionic-native/network/ngx';
import { FirebaseDynamicLinks } from '@ionic-native/firebase-dynamic-links/ngx';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServicesProvider } from './services/services.provider';
import { UtilsService } from './services/utils.service';
import { ResponseInterceptor } from './utils/_helpers/response.interceptor';
import { SharedModule } from './pages/_shared/shared.module';
import { Utils } from './utils/utils';
import { IntentProvider } from './services/intent.provider';
import { GuestComponent } from './pages/login/guest/guest.component';
import { LoginComponent } from './pages/login/login/login.component';
import { UpdateAppComponent } from './pages/update-app/update-app.component';
import { DummyComponent } from './pages/dummy/dummy.component';
import { NgxPopperModule } from 'ngx-popper';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';

import { KeychainTouchId } from '@ionic-native/keychain-touch-id/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { TouchComponent } from './pages/login/touch/touch.component';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { MfaComponent } from './pages/login/mfa/mfa.component';
import { MfaStepOneComponent } from './pages/login/mfa/components/mfa-step-one/mfa-step-one.component';
import { MfaStepTwoComponent } from './pages/login/mfa/components/mfa-step-two/mfa-step-two.component';
import { PushConfirmationComponent } from './pages/login/push-confirmation/push-confirmation.component';
const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 'auto'
};
@NgModule({
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        IonicStorageModule.forRoot(),
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        SharedModule,
        SwiperModule,
        AngularFirestoreModule,
        NgxPopperModule.forRoot({placement: 'top', styles: {'background-color': 'white'}}),
        AngularFireModule.initializeApp({
            apiKey: 'AIzaSyDHwiMXCvO2KyV3FF63izZ3zM7WhcrUfxw',
            authDomain: 'miclaroapp-9af23.firebaseapp.com',
            databaseURL: 'https://miclaroapp-9af23.firebaseio.com',
            projectId: 'miclaroapp-9af23',
            storageBucket: 'miclaroapp-9af23.appspot.com',
            messagingSenderId: '13982630197',
            appId: '1:13982630197:web:21a466d91c3933f8130c7c',
            measurementId: 'G-W6F8FKG775'
        })
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        GuestComponent,
        DummyComponent,
        TouchComponent,
        UpdateAppComponent,
        MfaComponent,
        MfaStepOneComponent,
        MfaStepTwoComponent,
        PushConfirmationComponent
    ],
    exports: [
        RouterModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        ServicesProvider,
        UtilsService,
        Device,
        Utils,
        GoogleMaps,
        Geolocation,
        IntentProvider,
        LaunchNavigator,
        SocialSharing,
        AppVersion,
        FirebaseX,
        FirebaseDynamicLinks,
        Network,
        FileOpener,
        File,
        KeychainTouchId,
        Keyboard,
        Clipboard,
        AppPreferences,
        InAppBrowser,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        { provide: HTTP_INTERCEPTORS, useClass: ResponseInterceptor, multi: true },
        { provide: SWIPER_CONFIG, useValue: DEFAULT_SWIPER_CONFIG }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}

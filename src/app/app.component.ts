import { Component, NgZone, SystemJsNgModuleLoader } from '@angular/core';

import { AlertController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { UtilsService } from './services/utils.service';
import { keys } from './utils/const/keys';
import { BasePage } from './pages/base.page';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from './services/services.provider';
import { IntentProvider } from './services/intent.provider';
import { KeychainTouchId } from '@ionic-native/keychain-touch-id/ngx';
import { Utils } from './utils/utils';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { RedirectProvider } from './services/redirect.provider';
import { StorageProvider } from './services/storage.provider';
import { pages } from './utils/const/pages';
import { PushNotificationService } from './services/push-notification.service';
import { ModalController } from '@ionic/angular';
import { ModalAccessibilityComponent } from './pages/_shared/components/modal-accessibility/modal-accessibility.component';
import { FirebaseDynamicLinks } from '@ionic-native/firebase-dynamic-links/ngx';
import { environment } from '../environments/environment';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
    providers: [SystemJsNgModuleLoader]
})
export class AppComponent extends BasePage {

    TIME_IN_MS = 1000 * 60 * 10;
    timer;

    constructor(
        private redirectProvider: RedirectProvider,
        private storageProvider: StorageProvider,
        private loader: SystemJsNgModuleLoader,
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private biometric: KeychainTouchId,
        private pushNotificationService: PushNotificationService,
        private appVersion: AppVersion,
        private network: Network,
        private modalController: ModalController,
        private firebaseDynamicLinks: FirebaseDynamicLinks,
        public keyboard: Keyboard,
        router: Router,
        storage: Storage,
        modelsServices: ServicesProvider,
        alertController: AlertController,
        utilsService: UtilsService,
        userStorage: IntentProvider,
        private ngZone: NgZone) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.initializeApp();
    }

    initializeApp() {
        const self = this;
        this.appReset();
        this.goPage(pages.DUMMY); // To reset current page
        this.platform.ready().then(() => {
            self.splashScreen.show();
            setTimeout(() => self.loadBiometricOptions(), 5000);
        });
        this.platform.backButton.subscribeWithPriority(0, () => {
            this.goBack();
        });
        this.bindEvents();
        this.utils.resetTimer.subscribe(() => {
            this.resetTimer();
        });
        this.activeActionsFromRedirect();
    }

    activeActionsFromRedirect() {
        (window as any).handleOpenURL = (url) => {
            setTimeout(() => {
                this.ngZone.run(() => {
                    this.redirectProvider.redirect(url);
                });
            }, 0);
        };

        (window as any).handlePasswordRecover = (url) => {
            setTimeout(() => {
                this.ngZone.run(() => {
                    // this.redirectProvider.redirect(url);
                    this.openExternalBrowser(url);
                });
            }, 0);
        };
    }

    loadBiometricOptions() {
        if (!Utils.getPlatformInfo().desktop) {
            this.biometric.isAvailable()
                .then((res: any) => {
                    console.log('biometric is available: ' + res);
                    this.utils.biometricOptions.available = true;
                    if (Utils.getPlatformInfo().ios) {
                        this.utils.biometricOptions.type = res === 'face' ? 'face' : 'touch';
                    } else {
                        this.utils.biometricOptions.type = 'touch';
                    }
                })
                .catch((error: any) => {
                    console.error('biometric is unavailable: ' + error);
                    this.utils.biometricOptions.available = false;
                })
                .finally(() => {
                    this.loadModules().then();
                });
        } else {
            this.loadModules().then();
        }
    }

    async loadModules() {
        await this.loader.load('./pages/register/register.module#RegisterModule');
        await this.loader.load('./pages/recover/recover.module#RecoverModule');
        await this.loader.load('./pages/update/update.module#UpdateModule');
        await this.loader.load('./pages/home/home.module#HomeModule');
        await this.loader.load('./pages/help/help.module#HelpModule');
        await this.loader.load('./pages/module/module.module#ModuleModule');
        await this.loadVersion();
    }

    async loadVersion() {
        // direct load app if testing
        if (Utils.getPlatformInfo().desktop) {
            this.loadApp();
            return;
        }

        this.pushNotificationService.saveFirebaseToken();

        if (this.network.type === this.network.Connection.NONE || this.network.type === this.network.Connection.UNKNOWN) {
            this.statusBar.styleLightContent();
            this.splashScreen.hide();
            this.showConfirmCustom(
                'Sin conexión',
                'Por favor compruebe su conexión a internet.',
                'Volver a Intentar',
                'Salir',
                () => {
                    location.reload();
                },
                () => {
                    this.utils.exitApp();
                });
        } else {
            const so = Utils.getPlatformInfo().ios ? 'IOS' : 'ANDROID';
            const version = await this.appVersion.getVersionNumber();
            console.log('version', version);
            this.services.checkAppVersion(so, version).then((response: any) => {
                if (response && response.object && response.object.enabled === 'Y') {
                    this.loadApp();
                } else {
                    this.goUpdate();
                }
            }).catch(error => {
                if (error.withStatus200 === true) {
                    this.goUpdate();
                } else {
                    this.fetch(keys.APP.OUTDATED_APP).then(value => {
                        if (value) {
                            this.goUpdate();
                        } else {
                            this.loadApp();
                        }
                    });
                }
            });
        }
    }

    goUpdate() {
        this.store(keys.APP.OUTDATED_APP, true);
        this.goPage(pages.UPDATE_APP);
        this.statusBar.styleLightContent();
        this.splashScreen.hide();
    }

    loadApp() {
        this.cacheStorage().isAppReady = true;
        this.store(keys.APP.OUTDATED_APP, false);
        this.statusBar.styleLightContent();
        this.splashScreen.hide();
        this.subscribeToDynamicLink();
        if (Utils.getPlatformInfo().ios) {
            this.keyboard.hideFormAccessoryBar(false);
        }
        // if (this.cacheStorage().redirectData) {
        //     this.redirectProvider.redirect(this.cacheStorage().redirectData.url);
        // } else {
        this.validateIsFirstTime();
        this.openLogin();
        // }
    }

    async openLogin() {
        const isLogged = await this.storageProvider.isLogged();
        const isGuest = await this.storageProvider.isGuest();
        this.goLoginPage(isLogged && isGuest);
    }

    private subscribeToDynamicLink() {
        this.firebaseDynamicLinks.onDynamicLink()
            .subscribe((res: any) => {
                if (res && res.deepLink) {
                    setTimeout(() => {
                        this.ngZone.run(() => {
                            const url = res.deepLink.replace(environment.WEB_URL, '');
                            this.redirectProvider.redirect(url);
                        });
                    }, 0);
                }
            }, (error: any) => console.log('onDynamicLink', error));
    }

    bindEvents() {
        addEventListener('mousemove', this.resetTimer.bind(this), false);
        addEventListener('mousedown', this.resetTimer.bind(this), false);
        addEventListener('keypress', this.resetTimer.bind(this), false);
        addEventListener('DOMMouseScroll', this.resetTimer.bind(this), false);
        addEventListener('mousewheel', this.resetTimer.bind(this), false);
        addEventListener('touchmove', this.resetTimer.bind(this), false);
        addEventListener('MSPointerMove', this.resetTimer.bind(this), false);
    }

    startTimer() {
        this.timer = setTimeout(this.evaluateSession.bind(this), this.TIME_IN_MS);
    }

    evaluateSession() {
        this.fetch(keys.LOGIN.IS_LOGGED).then(
            isLogged => {
                if (isLogged && this.cacheStorage().logged) {
                    const keep = this.fetch(keys.LOGIN.KEEP);
                    if (keep) {
                        console.warn('automatic re-login full');
                        location.reload();
                    } else {
                        clearTimeout(this.timer);
                        this.showAlertSessionExpired();
                    }
                } else {
                    this.resetTimer();
                }
            }
        );
    }

    resetTimer() {
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.startTimer();
    }

    private async validateIsFirstTime() {
        if (Utils.getPlatformInfo().android && !Utils.getPlatformInfo().desktop) {
            const access = await this.storageProvider.isAccessAccepted();
            if (access) {
                const version = await this.appVersion.getVersionNumber();
                if (!access.isAccepted || !access.version || access.version !== version) {
                    this.showDialogAccessibility();
                }
            } else {
                this.showDialogAccessibility();
            }
        }
    }

    private async showDialogAccessibility() {
        const modal = await this.modalController.create({
            component: ModalAccessibilityComponent,
            backdropDismiss: false,
            cssClass: 'modal-accessibility'
        });
        await modal.present();
    }
}

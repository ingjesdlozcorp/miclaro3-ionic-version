export interface Offer {
    intOfferId: number;
    intProductId: number;
    decPrice: number;
    decDiscount: number;
    decSalePrice: number;
    chvOfferDetails: string;
    chvOfferTems: string;
}

export interface Plan {
    installments: number;
    update_price: number;
    productId: number;
    productName: string;
    imgUrl: string;
    promoImg: string;
    brand: string;
    regular_price: number;
    salePrice: number;
    downPayment: number;
    shortDescription: string;
    bitPromotion: boolean;
    offer: Offer;
    // develop
    channelsFeatured?: Channel[];
}

export interface Channel {
    name: string;
    url: string;
}

export interface OfferAddOn {
    bitChild: boolean;
    bitDiscount: boolean;
    decPrice: number;
    decSalePrice: number;
    planDesc: string;
    planId: number;
    planName: string;
    planSoc: string;
}

export interface PlanAddOn {
    hasChannels: boolean;
    planId: number;
    planName: string;
    planSoc: string;
    planDesc: string;
    decPrice: number;
    bitEnable: boolean;
    bitInclude: boolean;
    productsExlude: string;
    imgUrl: string;
    // develop
    selected?: boolean;
    offer?: OfferAddOn;
}

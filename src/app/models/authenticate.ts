export interface Authenticate {
    subscriber?: string;
    username?: string;
    account?: string;
    email?: string;
    requiredAccountUpdate?: boolean;
    requiredPaperless?: boolean;
    requiredQuestions?: boolean;
    requiredEmailUpdate?: boolean;
    createdDate?: string;
    locked?: boolean;
    lastfailDate?: string;
    failTimeCounter?: number;
    validCode?: boolean;
    registerUser?: boolean;
    lastLogin?: string;
    maxDevicesCount?: number;
    guest?: boolean;
    accountType?: string;
    accountSubType?: string;
    productType?: string;
    token?: string;
    SSOID?: string;
    SSOAccessToken?: string;
    requiredPasswordReset?: boolean;
    hasError?: boolean;
    errorDisplay?: string;
    errorDesc?: string;
    errorNum?: number;
    userID?: number;
    roleID?: number;
    password?: string;
    accounts?: AuthAccount;
    enterAsGuest?: boolean;
}

export interface AuthAccount {
    AccountList: Account[];
    hasError: false;
    errorDisplay: '';
    errorDesc: '';
    errorNum: 0;
    errorSubject: '';
}

export interface Account {
    account?: string;
    accountID?: number;
    accountSubType?: string;
    accountType?: string;
    active?: boolean;
    productType?: string;
    registerDate?: string;
    userID?: number;
    subsriberByDefault?: string;
    userName?: string;
}

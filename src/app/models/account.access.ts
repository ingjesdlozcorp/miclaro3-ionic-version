export interface Section {
    sectionName: string;
    index: number;
    Pages: Page[];
}

export interface Page {
    userID: number;
    pageName: string;
    allowAsGuest: boolean;
    accessID: number;
    extraClass: string;
}


export interface Payment {
    details: any;
    amount: string;
    subscriber: string;
    subscriberType: string;
    account: string;
    email: string;
    description: string;
    type: string;
    data: any;
}

export const PAY = {
    ADDITIONAL_DATA: 'pay_additional_data',
    SEND_1GB: 'pay_send_1gb',
    SEND_RECHARGE: 'pay_send_recharge',
    INSTALLMENT: 'pay_installment'
};


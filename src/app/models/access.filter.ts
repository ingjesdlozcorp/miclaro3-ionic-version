import {Page, Section} from './account.access';
import {Utils} from '../utils/utils';

export class AccessFilter {

    static routesToGo = [
        { pageName: 'TUS NOTIFICACIONES', accessID: 1, path: 'notifications', needSubscribers: false },
        { pageName: 'MIS CUENTAS', accessID: 3, path: 'add-account', needSubscribers: true },
        { pageName: 'MI PERFIL', accessID: 2, path: 'profile', needSubscribers: false },
        { pageName: 'CAMBIAR CONTRASENA', accessID: 4, path: 'profile/password', needSubscribers: false },
        { pageName: 'ACTUALIZA TU CORREO ELECTRONICO', accessID: 5, path: 'profile/email', needSubscribers: false },
        { pageName: 'PREFERENCIA DE NOTIFICACIONES', accessID: 6, path: 'notifications-preference', needSubscribers: false },
        { pageName: 'SOPORTE', accessID: 7, path: 'support', needSubscribers: false },
        { pageName: 'CERRAR SESION', accessID: 8, path: '', needSubscribers: false },
        { pageName: 'RESUMEN DE CUENTA', accessID: 77, path: 'prepaid-summary', needSubscribers: false },
        { pageName: 'RESUMEN DE FACTURA', accessID: 9, path: 'invoice-summary', needSubscribers: false },
        { pageName: 'DESCARGA TU FACTURA', accessID: 10, path: 'invoice-download', needSubscribers: false },
        { pageName: 'DETALLE DE FACTURA', accessID: 11,  path: 'invoice-detail', needSubscribers: false },
        { pageName: 'HISTORIAL DE PAGOS', accessID: 12, path: 'payment-history', needSubscribers: false },
        { pageName: 'FACTURA ELECTRONICA', accessID: 13, path: 'electronic-bill', needSubscribers: false },
        { pageName: 'DEBITO DIRECTO', accessID: 32, path: 'direct-debit', needSubscribers: false },
        { pageName: 'DETALLE DE CONSUMOS', accessID: 14,  path: 'consumption', needSubscribers: true },
        { pageName: 'MI CONSUMOS', accessID: 28,  path: 'consumption/prepaid', needSubscribers: true },
        { pageName: 'DETALLE DE LLAMADAS', accessID: 15, path: 'call-detail', needSubscribers: true },
        { pageName: 'DETALLE DE TU EQUIPO Y PLAN', accessID: 16, path: 'device', needSubscribers: true },
        { pageName: 'DETALLE DE TU PLAN', accessID: 34, path: 'device', needSubscribers: true },
        { pageName: 'CAMBIO DE PLAN', accessID: 17, path: 'change-plan', needSubscribers: true },
        { pageName: 'CAMBIO DE PLAN', accessID: 35, path: 'change-plan', needSubscribers: true },
        { pageName: 'COMPRA DE DATA ADICIONAL', accessID: 21, path: 'data-plan', needSubscribers: false },
        { pageName: 'NETFLIX', accessID: 18, path: 'netflix', needSubscribers: false },
        { pageName: 'NETFLIX', accessID: 36, path: 'netflix', needSubscribers: false },
        { pageName: 'PROGRAMA DE REFERIDOS', accessID: 19, path: 'refer/home', needSubscribers: false },
        { pageName: 'PROGRAMA DE REFERIDOS', accessID: 31, path: 'refer/home', needSubscribers: false },
        { pageName: 'PROGRAMA DE REFERIDOS', accessID: 37, path: 'refer/home', needSubscribers: false },
        { pageName: 'CLARO CLUB', accessID: 321, path: 'club/home', needSubscribers: false },
        { pageName: 'TIENDA', accessID: 20, path: '', needSubscribers: false },
        { pageName: 'COMPRAS', accessID: 38, path: 'purchases', needSubscribers: false },
        { pageName: 'COMPRA DE SERVICIOS DE VALOR AGREGADO', accessID: 22, path: '', needSubscribers: false },
        { pageName: 'HISTORIAL DE COMPRAS', accessID: 23, path: 'shopping-history', needSubscribers: false },
        { pageName: 'REGALA 1GB', accessID: 24,  path: 'gift/one-gb', needSubscribers: false },
        { pageName: 'REGALA 1 RECARGA', accessID: 25, path: 'gift/recharge', needSubscribers: false },
        { pageName: 'RECARGA', accessID: 26, path: 'recharge', needSubscribers: false },
        { pageName: 'TRANSACCIONES', accessID: 30, path: 'recharge/history', needSubscribers: false },
        { pageName: 'REPORTE DE AVERIA', accessID: 33, path: 'fault/step1', needSubscribers: false },
        { pageName: 'CLAROTV+', accessID: 40, path: 'iptv', needSubscribers: false }
    ];

    public static isRouteActive(id: number, currentUrl: string): boolean {
        let isActive = false;
        if (currentUrl.includes('/module/')) {
            currentUrl = currentUrl.replace('/module/', '');
        }
        AccessFilter.routesToGo.forEach(route => {
            if (route.path !== '' && currentUrl === route.path && id === route.accessID) {
                isActive = true;
            }
        });
        return isActive;
    }

    public static do(value: Section[], isGuest, accountType, accountSubType, productType) {
        value.push(value.shift());
        const newList = [];
        /**
         * si es business no chequea los accesos y le coloca solo cerrar session
         */
        if (!Utils.isBusinessAccount(accountType)) {
            value.forEach((section: Section, index) => {
                const newPages = [];
                section.index = index;
                if (section.sectionName === 'MENU') {
                    if (Utils.isTelephony(accountType, accountSubType, productType)) {
                        const sectionPurchase = {
                            sectionName: 'COMPRAS',
                            Pages: [
                                {
                                    userID: 0,
                                    pageName: 'HISTORIAL DE COMPRAS',
                                    allowAsGuest: false,
                                    accessID: 23
                                }
                            ]
                        };
                        newList.push(sectionPurchase);
                    }
                    section.sectionName = 'MI CUENTA';
                }
                // if (section.sectionName === 'MIS EQUIPOS Y SERVICIOS' || section.sectionName === 'MIS SERVICIOS') {
                //     if (Utils.isPostpaid(accountType, accountSubType, productType) || Utils.isTelephony(accountType, accountSubType, productType)) {
                //         const sectionPurchase = {
                //             userID: 0,
                //             pageName: 'CLAROTV+',
                //             allowAsGuest: false,
                //             accessID: 555,
                //             extraClass: ''
                //         };
                //         section.Pages.push(sectionPurchase);
                //     }
                // }
                // section.sectionName = AccessFilter.capitalize(section.sectionName);
                section.Pages.forEach((page: Page) => {
                    // page.pageName = AccessFilter.capitalize(page.pageName);
                    if (!page.allowAsGuest && isGuest) {
                        page.extraClass = 'offnouse';
                    } else {
                        page.extraClass = '';
                    }
                    if (page.accessID !== 11  // TODO, modulo detalles de facturar
                        && page.accessID !== 15 // TODO, modulo detalles de llamadas
                        && page.accessID !== 22 // TODO, modulo SVA (Compra de servicios de Valor Agregado)
                        && !(page.accessID === 24 && Utils.isTelephony(accountType, accountSubType, productType)) // TODO, modulo regala 1 gb en fijo
                        && !(page.accessID === 25 && Utils.isTelephony(accountType, accountSubType, productType)) // TODO, modulo regala 1 recarga en fijo
                        && page.accessID !== 27 // TODO, modulo de historico de recargas (prepago)
                        && page.accessID !== 29 // TODO, modulo de transaferencias (prepago)
                        && !(page.accessID === 19 && Utils.isPrepaid(accountType, accountSubType)) // TODO, modulo de referidos en prepago
                        && !(page.accessID === 31 && Utils.isPrepaid(accountType, accountSubType)) // TODO, modulo de referidos en prepago
                        && !(page.accessID === 37 && Utils.isPrepaid(accountType, accountSubType)) // TODO, modulo de referidos en prepago
                        && !(page.accessID === 18 && Utils.getPlatformInfo().ios) // TODO, modulo de netflix en IOS
                        && !(page.accessID === 36 && Utils.getPlatformInfo().ios) // TODO, modulo de netflix en IOS
                        && !(page.accessID === 40 && Utils.isPrepaid(accountType, accountSubType)) // TODO, modulo de clarotv+ (prepago)
                    ) {
                        newPages.push(page);
                    }
                });
                if (section.sectionName.toLowerCase() === 'MI CONSUMO'.toLowerCase()) {
                    if (Utils.isPrepaid(accountType, accountSubType)) {
                        newList.push({
                            sectionName: 'FACTURA Y PAGO',
                            Pages: [
                                {
                                    userID: 0,
                                    pageName: 'RESUMEN DE CUENTA',
                                    allowAsGuest: false,
                                    accessID: 77
                                }
                            ]
                        });
                    }
                }
                if (newPages.length > 0) {
                    section.Pages = newPages;
                    newList.push(section);
                }
            });
        }

        if (newList.length === 0) {
            const section = {
                    sectionName: 'MENU',
                    Pages: [
                        {
                            userID: 0,
                            pageName: 'CERRAR SESION',
                            allowAsGuest: false,
                            accessID: 8
                        }
                    ]
                };
            newList.push(section);
        }

        return newList;
    }

    private static capitalize(str) {
        return str.replace(/\w\S*/g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
    }
}

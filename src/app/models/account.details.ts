export interface AccountDetails {
    qualification: Qualification;
    Messages: Messages;
    AccounInfo: AccountInfo;
    SubscriberInfo: SubscriberInfo[];
    HasError: boolean;
    ErrorDesc?: any;
    ErrorNum: number;
}

export interface AccountInfo {
    addressLinesField: AddressLinesField;
    invoicesField: any[];
    bANField: number;
    dOBField: string;
    sSNField: string;
    emailField: string;
    contractMonthsField: string;
    accountTypeField: string;
    accountSubtypeField: string;
    banStatusField: string;
    banStatusDateField: Date;
    billCycleField: number;
    billDateField: string;
    billDueDateField: string;
    billBalanceField: string;
    pastDueAmountField: number;
    lastPaymentDateField: string;
    lastPaymentAmountField: number;
    cycleStartDateField: string;
    cycleEndDateField: string;
    cycleDateField: string;
    cycleDaysLeftField: string;
    creditClassField: string;
    creditBalanceField: string;
    firstNameField: string;
    lastNameField: string;
    addressField: string;
    cityField: string;
    stateField: string;
    countryField: string;
    zIPField: string;
    invoiceURLField?: any;
    creditInfoField: CreditInfoField;
    paperlessField: boolean;
    hasErrorField: boolean;
    errorDescField?: any;
    errorNumField: number;
    PropertyChanged?: any;
    defaultSubscriberField?: string;
    defaultSubscriberObject?: SubscriberInfo;
}

export interface RefererResponse {
    registerUpdated: boolean;
    paperless: boolean;
    solvent: boolean;
}

export interface NetflixResponse {
    solvent: boolean;
    Account_Status: boolean;
    CreditLimit: boolean;
}

export interface Qualification {
    RedeemProgram: boolean;
    Netflix: boolean;
    RefererResponse: RefererResponse;
    NetflixResponse: NetflixResponse;
}

export interface MessagesList {
    id_message: number;
    subject: string;
    message: string;
    status: number;
    account: string;
    subscriber: string;
    alert_type: number;
    message_date: string;
}

export interface Messages {
    newMessageCounter: number;
    MessagesList: MessagesList[];
    hasError: boolean;
    errorDisplay: string;
    errorDesc: string;
    errorNum: number;
    errorSubject: string;
}

export interface AddressLinesField {
    line1Field: string;
    line2Field: string;
    PropertyChanged?: any;
}

export interface CreditInfoField {
    mUsedLimitField?: any;
    mCreditBalanceField?: any;
    mAllowedLimitField?: any;
    hasErrorField: boolean;
    errorDescField: string;
    errorNumField: number;
    PropertyChanged?: any;
}

export interface EquipmentInfoField {
    techField: string;
    itemIdField: string;
    itemNameField?: any;
    itemImageField: string;
    itemDescriptionField: string;
    pricecodeField: string;
    itemBrandField: string;
    installmentsField: string;
    installmentValueField: string;
    PropertyChanged?: any;
    mInvoiceStoreIdField?: string;
    mInvoiceOIdField?: string;
}

export interface PlanInfoField {
    sOCInfoField: string;
    sOCDescriptionField: string;
    sOCDescriptionInfoField: string;
    relatedSocCodeField: string;
    socRateField: number;
    totalRateField: number;
    commitmentStartDateField: string;
    mCommitmentOrigNoMonthField: string;
    commitmentEndDateField: string;
    effectiveDateField: string;
    endDateField?: any;
    PropertyChanged?: any;
}

export interface BalancesField {
    subscriberIdField: string;
    offerIdField: string;
    balanceIdField: string;
    balanceAmountField: string;
    balanceAmountUnitField: string;
    effectiveDateField?: Date;
    expiryDateField?: Date;
    quotaRolloverField: string;
    quotaRolloverTextField: string;
    PropertyChanged?: any;
}

export interface DataOffersField {
    balancesField: BalancesField;
    subscriberIdField: string;
    quotaField: string;
    quotaTextField: string;
    displayNameField: string;
    nameField: string;
    offerIdField: string;
    usedField: string;
    usedTextField: string;
    offerGroupField: string;
    groupIDField?: any;
    endDateField?: any;
    PropertyChanged?: any;
}

export interface UsageInfoField {
    dataOffersField: DataOffersField[];
    localBalanceField: string;
    localTopField: string;
    localDescriptionField: string;
    localPUJField: string;
    pUJTOPField: string;
    roamBalanceField: string;
    roamTopField: string;
    minutesUsageField: string;
    sMSUSageField: string;
    mMSUsageField: string;
    sMSPremiunUsageField: string;
    roamingUsageField: string;
    lDUsageField: string;
    lDIUsageField: string;
    notificationField: string;
    PropertyChanged?: any;
}

export interface AdditionalpackagesField {
    localPackagesField: any[];
    roamingPackagesField: any[];
    PropertyChanged?: any;
}

export interface ServEquipmentSerialField {
    itemIdField: string;
    equipmentTypeField: string;
    eSNField: string;
    PropertyChanged?: any;
}

export interface ServEquipmentSerialsField {
    servEquipmentSerialField: ServEquipmentSerialField[];
    PropertyChanged?: any;
}

export interface ServSVAsField {
    sOCInfoField: string;
    sOCDescField: string;
    relatedSocCodeField: string;
    effectiveDateField: string;
    socRateField: number;
    PropertyChanged?: any;
}

export interface ServSVAInfoField {
    servSVAsField: ServSVAsField[];
    PropertyChanged?: any;
}

export interface SubscriberInfo {
    eWalletBalanceField?: any;
    equipmentInfoField?: EquipmentInfoField;
    planInfoField?: PlanInfoField;
    usageInfoField?: UsageInfoField;
    additionalpackagesField?: AdditionalpackagesField;
    servEquipmentSerialsField?: ServEquipmentSerialsField;
    servSVAInfoField?: ServSVAInfoField;
    netFlixBlackListedField?: string;
    redeemProgramField?: any;
    subscriberNumberField?: string;
    productTypeField?: string;
    subscriberStatusField?: string;
    prepaidBalanceField?: any;
    lastPrepaidReplenishmentDateField?: any;
    lastPrepaidReplenishmentField?: any;
    localBalanceField?: any;
    reasonCodeField?: string;
    groupIDField?: any;
    hasErrorField?: boolean;
    errorDescField?: any;
    errorNumField?: number;
    PropertyChanged?: any;
}



import { Component, OnInit } from '@angular/core';
import { BasePage } from '../base.page';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../services/utils.service';
import { IntentProvider } from '../../services/intent.provider';
import { environment } from '../../../environments/environment';
import { Utils } from '../../utils/utils';

@Component({
    selector: 'app-update',
    templateUrl: './update-app.component.html'
})
export class UpdateAppComponent extends BasePage implements OnInit {

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {

    }

    openAppStore() {
        if (Utils.getPlatformInfo().android) {
            this.openExternalBrowser(`market://details?id=${environment.PACKET_NAME}`);
        } else if (Utils.getPlatformInfo().ios) {
            this.openExternalBrowser('https://apps.apple.com/us/app/mi-claro-pr/id775322054');
        } else {
            this.openExternalBrowser('https://play.google.com/store/apps/details?id=com.todoclaro.miclaroapp');
        }
    }
}

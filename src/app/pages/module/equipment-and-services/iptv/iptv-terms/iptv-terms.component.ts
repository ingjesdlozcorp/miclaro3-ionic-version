import {Component, OnInit} from '@angular/core';
import {BasePage} from 'src/app/pages/base.page';
import {ServicesProvider} from 'src/app/services/services.provider';
import {Router} from '@angular/router';
import {AlertController} from '@ionic/angular';
import {UtilsService} from 'src/app/services/utils.service';
import {IntentProvider} from 'src/app/services/intent.provider';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-module-iptv-terms',
    templateUrl: './iptv-terms.component.html',
    styleUrls: ['./iptv-terms.component.scss'],
})
export class IptvTermsComponent extends BasePage implements OnInit {

    public accounts;
    public selectedTab;
    public selectedAccount;
    public accountSections;
    public data;
    public accountInfo;
    public open = true;
    public contentShow = new Array(23);
    public theOpenGroup = [];


    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.data = this.cacheStorage();
        this.accounts = this.data.accountList;
        this.accountInfo = this.data.accountInfo;
        this.selectedAccount = this.cacheStorage().loginData.account;
    }

    ngOnInit() {
    }

    searchDelete(param: number) {
        for (let i = 0; i < this.theOpenGroup.length; i++) {
            if (param === this.theOpenGroup[i]) {
                this.theOpenGroup.splice(i, 1);
            }
        }
    }

    showingMenu(param: number) {
        let close = false;

        if (this.theOpenGroup.length > 0) {
            for (const group of this.theOpenGroup) {
               if (param === group) {
                    close = true;
                }
            }
        }
        // para mostrar el contenido:
        if (close === false) {
            for (let i = 0; i < this.contentShow.length; i++) {
                if (param === i) {
                    this.contentShow[i] = true;
                    this.theOpenGroup.push(param);
                }
            }

        } else {
            // para cerrar el contenido;
            this.searchDelete(param);
            for (let i = 0; i < this.contentShow.length; i++) {
                if (param === i) {
                    this.contentShow[i] = false;
                }
            }
        }
    }

}

import { Component, OnInit, } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import { BasePage } from '../../../../base.page';
import { DeviceStreaming } from '../iptv-buy/popup-device-streaming/popup-device-streaming.component';


@Component({
    selector: 'app-module-recharge-success',
    templateUrl: './iptv-success.component.html',
    styleUrls: ['./iptv-success.component.scss'],
})
export class IptvSuccessComponent extends BasePage implements OnInit {

    orderProcessed: any;
    deviceStreaming: DeviceStreaming;
    addons: any;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.orderProcessed = this.cacheStorage().orderIptvProcessed.order;
        this.deviceStreaming = this.cacheStorage().orderIptvProcessed.deviceStreaming;
        this.addons = this.cacheStorage().orderIptvProcessed.addons;
    }
}

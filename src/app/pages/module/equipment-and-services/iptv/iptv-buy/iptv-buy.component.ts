import { Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { ServicesProvider } from 'src/app/services/services.provider';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import { Plan, PlanAddOn } from '../../../../../models/iptv';
import { DeviceStreaming } from './popup-device-streaming/popup-device-streaming.component';
import { Address, PaymentCard } from './popup-payment-shipping/popup-payment-shipping.component';

const STEP = {
    PLAN: 0,
    ADD_ONS: 1,
    STREAMING: 2,
    ACCOUNT: 3,
    PAYMENT: 4,
    SHIPPING: 5
};

@Component({
    selector: 'app-module-iptv-buy',
    templateUrl: './iptv-buy.component.html',
    styleUrls: ['./iptv-buy.component.scss'],
})
export class IptvBuyComponent extends BasePage implements OnInit {

    token: string;
    tokenIptv: string;
    STEP = STEP;
    currentStep: Step;
    currentSubstep: Step;
    steps: Step[] = [
        {name: 'Plan', number: STEP.PLAN, complete: true},
        {name: 'Servicios Adicionales', number: STEP.ADD_ONS, complete: false},
        {
            name: 'Escoge tu dispositivo de Streaming', number: STEP.STREAMING, complete: false, steps: [
                {number: 1, complete: false},
                {number: 2, complete: false},
            ]
        },
        {name: 'Crear cuenta Clarotv+', number: STEP.ACCOUNT, complete: false},
        {name: 'Información de Pago', number: STEP.PAYMENT, complete: false},
        {name: 'Información de Envío', number: STEP.SHIPPING, complete: false},
    ];
    additionalServices: PlanAddOn[];
    popupController = {
        popupAdditionalServices: false,
        popupCreateAccount: false,
        popupDeviceStreaming: false,
        popupPaymentShipping: false,
        popupType: 1,
        popupData: ''
    };

    cart: any;
    plan: Plan;
    addons: { selected: PlanAddOn[], payToday: number, payByMonth: number };
    account: { email: string };
    deviceStreaming: DeviceStreaming;
    paymentCard: PaymentCard;
    addressShipping: Address;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.steps[0].complete = true;
        this.plan = this.cacheStorage().planIptvSelected;
        this.token = this.cacheStorage().tokenSession;
        this.tokenIptv = this.cacheStorage().tokenIptv;
        this.loadData();
        this.changeStep(this.steps[STEP.ADD_ONS]);
    }

    private loadData() {
        this.showProgress('Cargando...');
        this.getCart();
    }

    // *******************************************************
    changeStep(step: Step) {
        if (this.currentStep && this.currentStep.number === step.number) {
            this.currentStep = null;
        } else {
            this.currentStep = step;
            switch (this.currentStep.number) {
                case STEP.PLAN:
                    this.selectAnotherPlan();
                    break;
                case STEP.ADD_ONS:
                    if (step.complete) {
                        this.openPopupAddServices();
                    } else {
                        this.getAdditionalServices();
                    }
                    break;
                case STEP.ACCOUNT:
                    this.openPopupCreateAccount();
                    break;
                case STEP.PAYMENT:
                    this.openPopupPaymentShipping(1);
                    break;
                case STEP.SHIPPING:
                    this.openPopupPaymentShipping(2);
                    break;
            }
        }
        this.currentSubstep = null;
    }

    changeSubStep(step: Step, subStep: Step) {
        this.currentStep = step;
        this.currentSubstep = subStep;
        if (this.currentStep.number === STEP.STREAMING) {
            if (subStep.number === 2 && !this.currentStep.steps[0].complete) {
                this.showAlert('Debe seleccionar primero el tipo de contrato');
                return;
            }
            if (subStep.complete && this.deviceStreaming) {
                this.openPopupDeviceStreaming(subStep);
            } else {
                this.getStreamingDevice();
            }
        }
    }

    private getCart() {
        this.services.getCartIptv(this.token, this.tokenIptv).then((success: any) => {
            this.dismissProgress();
            if (!success.hasError) {
                this.cart = success;
            } else {
                this.showError(UtilsService.errorGeneral);
            }
        }, e => {
            this.showError(UtilsService.errorGeneral);
            this.dismissProgress();
        });
    }

    private getAdditionalServices() {
        this.showProgress('Cargando los servicios, por favor espere...');
        const token = this.cacheStorage().tokenIptv;
        this.services.getPlansSVA(token, this.plan.productId).then((success: any) => {
            if (success.plans) {
                this.additionalServices = success.plans;
                this.getOfferOfPlanSva();
            } else {
                this.showError(UtilsService.errorGeneral);
                this.dismissProgress();
            }
        }, e => {
            this.showError(UtilsService.errorGeneral);
            this.dismissProgress();
        });
    }

    private getOfferOfPlanSva() {
        const token = this.cacheStorage().tokenSession;
        const tokenIptv = this.cacheStorage().tokenIptv;
        const promises = [];
        for (const planSva of this.additionalServices) {
            promises.push(this.services.getOfferPlansSVA(token, tokenIptv, planSva.planId));
        }

        Promise.all(promises).then((response: any[]) => {
            this.dismissProgress();
            if (response && response.length) {
                response.map((success: any) => {
                    if (success.offers && success.offers.length) {
                        const planSva = this.additionalServices.filter(plan => plan.planId === success.productId);
                        if (planSva.length) {
                            planSva[0].offer = success.offers[0];
                        }
                    }
                });
                this.openPopupAddServices();
            }
        }, e => {
            this.showError(UtilsService.errorGeneral);
            this.dismissProgress();
        });
    }

    private getStreamingDevice() {
        this.showProgress('Cargando, por favor espere...');
        const token = this.cacheStorage().tokenSession;
        const tokenIptv = this.cacheStorage().tokenIptv;
        this.services.getStreamingDevice(token, tokenIptv).then((success: any) => {
            this.dismissProgress();
            if (!success.hasError) {
                this.popupController.popupData = success;
                this.openPopupDeviceStreaming(this.currentSubstep);
            } else {
                this.showError(success.errorDesc);
            }
        }, e => {
            this.showError(e.message);
            this.dismissProgress();
        });
    }

    /**
     * Open modal add additional services
     */
    openPopupAddServices() {
        this.popupController.popupAdditionalServices = true;
    }

    closePopupAddServices() {
        if (!this.currentStep.complete) {
            this.changeSubStep(this.steps[STEP.STREAMING], this.steps[STEP.STREAMING].steps[0]);
        }
        this.steps[STEP.ADD_ONS].complete = true;
        this.popupController.popupAdditionalServices = false;
        this.getCart();
    }

    withoutServices(response) {
        this.addons = response ? response : null;
        this.closePopupAddServices();
    }

    addServices(addons: { selected: PlanAddOn[], payToday: number, payByMonth: number }) {
        this.addons = addons;
        this.closePopupAddServices();
    }

    /**
     * Open modal create Account
     */
    openPopupCreateAccount() {
        this.popupController.popupCreateAccount = true;
    }

    createAccount(event: any) {
        this.account = {email: event};
        if (!this.currentStep.complete) {
            this.changeStep(this.steps[STEP.PAYMENT]);
        }
        this.steps[STEP.ACCOUNT].complete = true;
        this.closePopupCreateAccount();
    }

    closePopupCreateAccount() {
        this.popupController.popupCreateAccount = false;
    }

    /**
     * Open modal device Streaming
     * @param subStep: Step
     */
    openPopupDeviceStreaming(subStep: Step) {
        this.popupController.popupType = subStep.number;
        this.popupController.popupDeviceStreaming = true;
    }

    closePopupDeviceStreaming() {
        this.popupController.popupDeviceStreaming = false;
    }

    saveDeviceStreaming(event: any) {
        this.deviceStreaming = event;
        this.currentSubstep.complete = true;
        if (this.popupController.popupType === 1) {
            this.currentStep.steps[1].complete = false;
            this.currentStep.complete = false;
            this.changeSubStep(this.steps[STEP.STREAMING], this.steps[STEP.STREAMING].steps[1]);
        }
        if (this.popupController.popupType === 2) {
            if (!this.currentStep.complete) {
                this.currentStep.complete = true;
                this.getCart();
                this.changeStep(this.steps[STEP.ACCOUNT]);
            }
        }
        if (this.currentStep.complete) {
            this.getCart();
        }
        this.closePopupDeviceStreaming();
    }

    /**
     * Open modal payment and shipping
     * @param subStep: Step
     */
    openPopupPaymentShipping(subStep: number) {
        this.popupController.popupType = subStep;
        this.popupController.popupPaymentShipping = true;
    }

    savePaymentShipping(event: any) {
        this.closePopupPaymentShipping();
        if (this.popupController.popupType === 1) {
            this.paymentCard = event;
            if (!this.currentStep.complete) {
                this.currentStep.complete = true;
                this.changeStep(this.steps[STEP.SHIPPING]);
            }
        } else {
            this.addressShipping = event;
            this.currentStep.complete = true;
        }
    }

    closePopupPaymentShipping() {
        this.popupController.popupPaymentShipping = false;
    }

    // *******************************************************
    calculateDiscountPlan(): number {
        if (this.plan.offer) {
            const discount = this.plan.salePrice * this.plan.offer.decDiscount;
            return this.plan.salePrice - discount;
        } else {
            return this.plan.salePrice;
        }
    }

    validateProcessOrder() {
        const stepsMissing = this.steps.filter(step => !step.complete);
        if (stepsMissing.length >= 1) {
            let stepMissing: string;
            stepMissing = stepsMissing[0].name;
            this.showAlert(`Estimado cliente, el paso "${stepMissing.toUpperCase()}" esta incompleto, todos los paso son necesarios para procesar su orden`);
            return;
        }
        this.payOrder();
    }

    private payOrder() {
        this.showProgress('Procesando...');
        const token = this.cacheStorage().tokenSession;
        const tokenIptv = this.cacheStorage().tokenIptv;
        const account = this.cacheStorage().accountSSO;
        this.services.payOrderIptv(token, tokenIptv, this.paymentCard, this.cart, account).then((success: any) => {
            if (!success.hasError) {
                this.processOrder(token, tokenIptv);
            } else {
                this.showError(success.errorDisplay);
                this.dismissProgress();
            }
        }, e => {
            this.showError(e.message);
            this.dismissProgress();
        });
    }

    private processOrder(token, tokenIptv) {
        this.services.processOrderIptv(token, tokenIptv).then((success: any) => {
            if (!success.hasError) {
                this.cacheStorage().orderIptvProcessed = {
                    order: success,
                    addons: this.addons,
                    deviceStreaming: this.deviceStreaming,
                };
                this.goPage('module/iptv/success');
                this.dismissProgress();
            } else {
                this.showError(success.errorDisplay);
                this.dismissProgress();
            }
        }, e => {
            this.showError(e.message);
            this.dismissProgress();
        });
    }

    private selectAnotherPlan() {
        this.showProgress('Removiendo plan actual...');

        const token = this.cacheStorage().tokenSession;
        const tokenShop = this.cacheStorage().tokenIptv;

        const promises = [];
        for (const plan of this.cart.products) {
            promises.push(this.services.deleteItemFromCart(token, tokenShop, 0));
        }

        Promise.all(promises).then((success: any[]) => {
            this.dismissProgress();
            if (!success[0].hasError) {
                this.goPage('module/iptv/select-plan');
            } else {
                this.showError(UtilsService.errorGeneral);
            }
        }, e => {
            this.showError(e.message);
            this.dismissProgress();
        });
    }
}

interface Step {
    name?: string;
    number: number;
    complete: boolean;
    steps?: Step[];
}

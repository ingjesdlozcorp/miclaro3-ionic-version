import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { APP } from '../../../../../../utils/const/appConstants';
import { ServicesProvider } from '../../../../../../services/services.provider';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-popup-iptv-terms-on-popup',
    templateUrl: './popup-terms-on-popup.component.html',
    styleUrls: ['./popup-terms-on-popup.component.scss'],
})
export class PopupTermsOnPopupComponent implements OnInit {

    @Input() title: any = 'Términos y condiciones <app-claro-tv></app-claro-tv>';
    @Input() template: string;
    @Output() close = new EventEmitter();
    terms: any;
    currentYear: number;
    private services: ServicesProvider;

    constructor(private modelsServices: ServicesProvider,
                private sanitizer: DomSanitizer) {
        this.services = modelsServices;
        this.currentYear = new Date().getFullYear();
    }

    ngOnInit(): void {
        this.getTerms();
    }

    private getTerms() {
        this.services.getByUrl(APP.URL_LOCAL_TEMPLATES + this.template)
            .then(res => {
                this.terms = this.sanitizer.bypassSecurityTrustHtml(res);
            })
            .catch(e => console);
    }

    closeEmit() {
        this.close.emit();
    }
}


import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../../../../services/utils.service';
import { IntentProvider } from '../../../../../../services/intent.provider';
import { BasePage } from '../../../../../base.page';

@Component({
    selector: 'app-popup-device-streaming',
    templateUrl: './popup-device-streaming.component.html',
    styleUrls: ['./popup-device-streaming.component.scss'],
})
export class PopupDeviceStreamingComponent extends BasePage implements OnInit {

    @Input() currentCart: any;
    @Input() type: number;
    @Input() data: any;
    @Input() dataPrevious: any;
    @Output() close = new EventEmitter();
    @Output() save = new EventEmitter<any>();
    @Output() withoutDeviceStreaming = new EventEmitter<any>();

    contracts: any[];
    product: {prices: Deadline[], imgUrl: string, productName: string};

    popupController = {
        popupTerms: false,
        popupTemplate: 'terms-streaming.html',
    };
    terms: boolean;

    withContract = true;
    contractSelected: Contract;
    totalDevices = 1;
    deadlineSelected = 12;
    wayToPay = 2;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit(): void {
        if (this.type === 1) {
            this.contracts = this.data.iptvContracts.contracts;
            this.contracts = this.contracts.reverse();
            this.contractSelected = this.contracts[0];
        }
        if (this.type === 2) {
            this.product = this.data.colors[0].storages[0].products[0];
            const defaultPrice = this.product.prices.filter(price => price.defaultPrice)[0];
            this.deadlineSelected = defaultPrice.intMonths;
        }
        if (this.dataPrevious) {
            this.withContract = this.dataPrevious.withContract;
            this.contractSelected = this.dataPrevious.contractSelected;
            if (this.dataPrevious.totalDevices) {
                this.totalDevices = this.dataPrevious.totalDevices;
                this.deadlineSelected = this.dataPrevious.deadlineSelected;
                this.wayToPay = this.dataPrevious.wayToPay;
                this.terms = true;
            }
        }
    }

    closeEmit() {
        this.close.emit();
    }

    validate() {
        if (this.type === 2 && !this.terms) {
            this.openTerms();
            return;
        }
        if (this.type === 1) {
            const contract: DeviceStreaming = {
                contractSelected: this.contractSelected,
                withContract: this.withContract,
            };
            this.save.emit(contract);
        }

        if (this.type === 2) {
            const contract: DeviceStreaming = {
                contractSelected: this.contractSelected,
                withContract: this.withContract,
                totalDevices: this.totalDevices,
                deadlineSelected: this.deadlineSelected,
                wayToPay: this.wayToPay,
                payByMonth: this.wayToPay === 1 ? this.getDeadlinePayment() : 0,
                payToday: this.wayToPay === 2 ? this.getRegularPayment() : this.getDeadlinePayment()
            };
            this.saveStreaming(contract);
        }
    }

    private saveStreaming(contract: DeviceStreaming) {
        this.showProgress('Guardando...');
        const token = this.cacheStorage().tokenSession;
        const tokenShop = this.cacheStorage().tokenIptv;
        const json = {
            bitContract: String(contract.withContract),
            intInstallments: contract.wayToPay === 1 ? String(contract.deadlineSelected) : '0',
            intContractTermsInMonths: contract.withContract ? String(contract.contractSelected.contractInMoths) : '0',
            qty: String(contract.totalDevices)
        };

        this.services.addStreamingToCart(token, tokenShop, json).then(success => {
            if (!success.hasError) {
                this.save.emit(contract);
            } else {
                this.showError(UtilsService.errorGeneral);
            }
            this.dismissProgress();
        }, e => {
            this.showError(UtilsService.errorGeneral);
            this.dismissProgress();
        });
    }

    increaseDevice() {
        if (this.totalDevices < 4) {
            this.totalDevices++;
        }
    }

    // ************************************************************
    decreaseDevice() {
        if (this.totalDevices > 1) {
            this.totalDevices--;
        }
    }

    getDeadlinePayment(): number {
        const {regularPrice, salePrice} = this.contractSelected;
        if (this.totalDevices === 1) {
            return salePrice / this.deadlineSelected;
        }

        return (salePrice + regularPrice * (this.totalDevices - 1)) / this.deadlineSelected;

    }

    getRegularPayment() {
        const {regularPrice, salePrice} = this.contractSelected;

        if (this.totalDevices === 1) {
            return salePrice;
        }

        return salePrice + regularPrice * (this.totalDevices - 1);
    }

    /**
     * Open popup of terms and conditions
     */
    openTerms() {
        this.popupController.popupTerms = true;
    }

    closePopupTerms() {
        this.popupController.popupTerms = false;
        this.terms = true;
    }
}

export interface Contract {
    contract: string;
    contractInMoths: number;
    firtsProductDiscount: string;
    regularPrice: number;
    salePrice: number;
}

export interface Deadline {
    claro_update: number;
    months: string;
    defaultPrice: boolean;
    intMonths: number;
}

export interface DeviceStreaming {
    contractSelected: Contract;
    withContract: boolean;
    totalDevices?: number;
    deadlineSelected?: number;
    wayToPay?: number;
    payToday?: number;
    payByMonth?: number;
}

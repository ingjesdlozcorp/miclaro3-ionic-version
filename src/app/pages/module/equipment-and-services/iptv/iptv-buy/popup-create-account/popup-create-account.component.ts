import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BasePage } from '../../../../../base.page';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../../../../services/utils.service';
import { IntentProvider } from '../../../../../../services/intent.provider';
import { Utils } from '../../../../../../utils/utils';

@Component({
    selector: 'app-popup-create-account',
    templateUrl: './popup-create-account.component.html',
    styleUrls: ['./popup-create-account.component.scss'],
})
export class PopupCreateAccountComponent extends BasePage implements OnInit {

    @Input() email: string;
    @Output() close = new EventEmitter();
    @Output() create = new EventEmitter<any>();
    accountValidating = true;
    accountValidated = false;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        if (this.email) {
            this.accountValidating = false;
            this.accountValidated = true;
        }
    }

    closeEmit() {
        if (!this.accountValidating && this.accountValidated) {
            this.saveEmit();
        } else {
            this.close.emit();
        }
    }

    saveEmit() {
        this.create.emit(this.email);
    }

    validate() {
        if (!this.email || !this.email.trim()) {
            this.showAlert('Es necesario que agregue su correo electrónico');
            return;
        }
        if (!Utils.validateEmail(this.email)) {
            this.showAlert('Es necesario que agregue un correo electrónico válido');
            return;
        }
        this.validateAccount();
    }

    private validateAccount() {
        this.showProgress('Espere mientras validamos su cuenta...');
        const token = this.cacheStorage().tokenSession;
        const tokenIptv = this.cacheStorage().tokenIptv;

        this.services.validateAccountIptv(token, tokenIptv, this.email).then((success: any) => {
            this.dismissProgress();
            this.accountValidated = !success.hasError && !Boolean(success.accountExist);
            this.accountValidating = false;
        }, e => {
            this.showError(e.message);
            this.dismissProgress();
        });
    }

    retry() {
        this.accountValidating = true;
    }
}


import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { formatDate } from '@angular/common';
import { BasePage } from '../../../../../base.page';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../../../../services/utils.service';
import { IntentProvider } from '../../../../../../services/intent.provider';
import { DomSanitizer } from '@angular/platform-browser';
import { Utils } from '../../../../../../utils/utils';

@Component({
    selector: 'app-popup-payment-shipping',
    templateUrl: './popup-payment-shipping.component.html',
    styleUrls: ['./popup-payment-shipping.component.scss'],
})
export class PopupPaymentShippingComponent extends BasePage implements OnInit {

    @Input() type: number;
    @Input() dataPrevious: PaymentCard | Address;
    @Output() close = new EventEmitter();
    @Output() save = new EventEmitter<any>();

    listVillages = listVillages;
    jsToday = '';
    months: any = [];
    years: any = [];

    creditCard: PaymentCard = {expMonth: '01'};
    address: Address = {state: 'PR'};

    popupController = {
        popupReturnPolicies: false,
        popupTitle: 'Políticas de devolución',
        popupTemplate: 'return-policy.html',
    };
    returnPolicies: boolean;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider,
                private sanitizer: DomSanitizer) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);

        this.jsToday = formatDate(new Date(), 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
        let now = Number(this.jsToday.substring(6, 10));
        this.creditCard.expYear = now;
        for (let j = 1; j <= 12; j++) {
            this.months.push((j < 10) ? '0' + j : j);
        }
        for (let i = 0; i < 10; i++) {
            this.years.push(now);
            now = now + 1;
        }
    }

    ngOnInit(): void {
        if (this.dataPrevious) {
            if (this.type === 1) {
                this.creditCard = this.dataPrevious;
            } else {
                this.address = this.dataPrevious;
            }
        }

    }

    closeEmit() {
        this.close.emit();
    }

    saveEmit() {
        if (this.type === 1 && this.validatePaymentMethod()) {
            this.save.emit(this.creditCard);
        }
        if (this.type === 2 && this.validateShippingAddress()) {
            if (!this.returnPolicies) {
                this.openReturnPolicy();
                return;
            }
            this.saveAddress();
        }
    }

    private validatePaymentMethod(): boolean {
        if (!this.creditCard.name || !this.creditCard.name.trim()) {
            this.showAlert('Es necesario que agregue el nombre del titular de la tarjeta');
            return false;
        }
        if (!this.creditCard.numberCard) {
            this.showAlert('Es necesario que agregue el número de la tarjeta');
            return false;
        }
        if (!Utils.getTypeCard(this.creditCard.numberCard.trim())) {
            this.showAlert('El número de tarjeta no es válido');
            return false;
        }
        if (!this.creditCard.cvv) {
            this.showAlert('Es necesario que agregue el cvv de la tarjeta');
            return false;
        }
        if (!this.creditCard.zipCode || !this.creditCard.zipCode.trim()) {
            this.showAlert('Es necesario que agregue el código postal');
            return false;
        }
        return true;
    }

    private validateShippingAddress(): boolean {
        if ((!this.address.firstname || !this.address.firstname.trim()) || (!this.address.lastname || !this.address.lastname.trim())) {
            this.showAlert('Es necesario que agregue su nombre y apellidos');
            return false;
        }
        if (!this.address.phone || !this.address.phone.trim()) {
            this.showAlert('Es necesario que agregue su telefóno de contácto');
            return false;
        }
        if (!this.address.email || !this.address.email.trim()) {
            this.showAlert('Es necesario que agregue su correo electrónico');
            return false;
        }
        if (!Utils.validateEmail(this.address.email.trim())) {
            this.showAlert('Es necesario que agregue un correo electrónico válido');
            return false;
        }
        if (!this.address.address1 || !this.address.address1.trim()) {
            this.showAlert('Es necesario que agregue su dirección');
            return false;
        }
        if (!this.address.zip || !this.address.zip.trim()) {
            this.showAlert('Es necesario que seleccione el pueblo');
            return false;
        }
        if (!this.zipCodeValidator(this.address.zip.trim())) {
            this.showAlert('El pueblo ingresado no pertenece a ningun lugar de Puerto Rico');
            return false;
        }
        if (!this.address.notes || !this.address.notes.trim()) {
            this.address.notes = '';
            // this.showAlert('Es necesario que agregue una nota');
            // return false;
        }
        this.address.name = this.address.firstname + ' ' + this.address.lastname;
        return true;
    }

    zipCodeValidator(codeEnter: any): boolean {
        let townFound = false;
        listVillages.map(village => {
            if (village.value === codeEnter) {
                townFound = true;
                this.address.city = village.label;
            }
        });
        return townFound;
    }

    openReturnPolicy() {
        this.popupController.popupReturnPolicies = true;
    }

    closePopupPolicies() {
        this.popupController.popupReturnPolicies = false;
        this.returnPolicies = true;
    }

    private saveAddress() {
        this.showProgress('Espere mientras guardamos su direccion...');
        const token = this.cacheStorage().tokenSession;
        const tokenIptv = this.cacheStorage().tokenIptv;
        this.services.createAddressIptv(token, tokenIptv, this.address).then((success: any) => {
            this.dismissProgress();
            this.save.emit(this.address);
        }, e => {
            this.showError(e.message);
            this.dismissProgress();
        });
    }

    checkIsValid(numberCard: string) {
        const cardType = Utils.getTypeCard(numberCard);
        if (cardType) {
            this.creditCard.typeCard = cardType;
        }
    }
}

export interface PaymentCard {
    name?: string;
    numberCard?: string;
    cvv?: number;
    typeCard?: any;
    expMonth?: number | string;
    expYear?: number;
    account?: number;
    subscriber?: any;
    email?: string;
    zipCode?: string;
}

export interface Address {
    name?: string;
    email?: string;
    address1?: string;
    address2?: string;
    city?: string;
    state?: string;
    zip?: string;
    notes?: string;
    phone?: string;
    intUserId?: string;
    firstname?: string;
    lastname?: string;
}

const listVillages = [
    {label: 'Adjuntas', value: '00601'},
    {label: 'Adjuntas', value: '00631'},
    {label: 'Aguada', value: '00602'},
    {label: 'Aguadilla', value: '00603'},
    {label: 'Aguadilla', value: '00604'},
    {label: 'Aguadilla', value: '00605'},
    {label: 'Aguadilla', value: '00690'},
    {label: 'Aguas Buenas', value: '00703'},
    {label: 'Aibonito', value: '00705'},
    {label: 'La Plata', value: '00786'},
    {label: 'Añasco', value: '00610'},
    {label: 'Angeles', value: '00611'},
    {label: 'Arecibo', value: '00612'},
    {label: 'Arecibo', value: '00613'},
    {label: 'Arecibo', value: '00614'},
    {label: 'Arecibo', value: '00616'},
    {label: 'Arecibo', value: '00652'},
    {label: 'Arecibo', value: '00688'},
    {label: 'Arroyo', value: '00714'},
    {label: 'Barceloneta', value: '00617'},
    {label: 'Barranquitas', value: '00794'},
    {label: 'Bayamón', value: '00956'},
    {label: 'Bayamón', value: '00957'},
    {label: 'Bayamón', value: '00959'},
    {label: 'Bayamón', value: '00961'},
    {label: 'Bayamón', value: '009606'},
    {label: 'Bayamón', value: '00958'},
    {label: 'Cabo Rojo', value: '00623'},
    {label: 'Boquerón', value: '00622'},
    {label: 'Caguas', value: '00725'},
    {label: 'Caguas', value: '00727'},
    {label: 'Caguas', value: '00726'},
    {label: 'Camuy', value: '00627'},
    {label: 'Canóvanas', value: '00729'},
    {label: 'Canóvanas', value: '00745'},
    {label: 'Carolina', value: '00979'},
    {label: 'Carolina', value: '00982'},
    {label: 'Carolina', value: '00983'},
    {label: 'Carolina', value: '00985'},
    {label: 'Carolina', value: '00987'},
    {label: 'Carolina', value: '00981'},
    {label: 'Carolina', value: '00984'},
    {label: 'Carolina', value: '00986'},
    {label: 'Carolina', value: '00988'},
    {label: 'Cataño', value: '00962'},
    {label: 'Cataño', value: '00963'},
    {label: 'Cayey', value: '00736'},
    {label: 'Cayey', value: '00737'},
    {label: 'Ceiba', value: '00735'},
    {label: 'Ceiba', value: '00742'},
    {label: 'Ciales', value: '00638'},
    {label: 'Cidra', value: '00739'},
    {label: 'Coamo', value: '00769'},
    {label: 'Comerio', value: '00782'},
    {label: 'Corozal', value: '00783'},
    {label: 'Culebra', value: '00775'},
    {label: 'Dorado', value: '00646'},
    {label: 'Fajardo', value: '00738'},
    {label: 'Fajardo', value: '00740'},
    {label: 'Florida', value: '00650'},
    {label: 'Guánica', value: '00653'},
    {label: 'Guánica', value: '00647'},
    {label: 'Guayama', value: '00784'},
    {label: 'Guayama', value: '00704'},
    {label: 'Guayama', value: '00785'},
    {label: 'Guayanilla', value: '00656'},
    {label: 'Guaynabo', value: '00965'},
    {label: 'Guaynabo', value: '00966'},
    {label: 'Guaynabo', value: '00968'},
    {label: 'Guaynabo', value: '00971'},
    {label: 'Guaynabo', value: '00970'},
    {label: 'Guaynabo', value: '00969'},
    {label: 'Gurabo', value: '00778'},
    {label: 'Hatillo', value: '00659'},
    {label: 'Hormigueros', value: '00660'},
    {label: 'Humacao', value: '00791'},
    {label: 'Humacao', value: '00792'},
    {label: 'Humacao', value: '00741'},
    {label: 'Isabela', value: '00662'},
    {label: 'Jayuya', value: '00664'},
    {label: 'Juana Díaz', value: '00795'},
    {label: 'Juncos', value: '00777'},
    {label: 'Lajas', value: '00667'},
    {label: 'Lares', value: '00669'},
    {label: 'Las Marías', value: '00670'},
    {label: 'Las Piedras', value: '00771'},
    {label: 'Loíza', value: '00772'},
    {label: 'Luquillo', value: '00773'},
    {label: 'Manatí', value: '00674'},
    {label: 'Maricao', value: '00606'},
    {label: 'Maunabo', value: '00707'},
    {label: 'Mayaguez', value: '00680'},
    {label: 'Mayaguez', value: '00682'},
    {label: 'Mayaguez', value: '00681'},
    {label: 'Moca', value: '00676'},
    {label: 'Morovis', value: '00687'},
    {label: 'Naguabo', value: '00718'},
    {label: 'Rio Blanco', value: '00744'},
    {label: 'Naranjito', value: '00719'},
    {label: 'Orocovis', value: '00720'},
    {label: 'Patillas', value: '00723'},
    {label: 'Peñuelas', value: '00624'},
    {label: 'Ponce', value: '00716'},
    {label: 'Ponce', value: '00717'},
    {label: 'Ponce', value: '00728'},
    {label: 'Ponce', value: '00730'},
    {label: 'Ponce', value: '00731'},
    {label: 'Ponce', value: '00733'},
    {label: 'Ponce', value: '00780'},
    {label: 'Ponce', value: '00715'},
    {label: 'Ponce', value: '00732'},
    {label: 'Ponce', value: '00734'},
    {label: 'Quebradillas', value: '00678'},
    {label: 'Rincón', value: '00677'},
    {label: 'Río Grande', value: '00721'},
    {label: 'Río Grande', value: '00745'},
    {label: 'Sabana Grande', value: '00637'},
    {label: 'Salinas', value: '00751'},
    {label: 'San Germán', value: '00683'},
    {label: 'San Germán', value: '00636'},
    {label: 'San Juan', value: '00921'},
    {label: 'San Juan', value: '00923'},
    {label: 'San Juan', value: '00924'},
    {label: 'San Juan', value: '00929'},
    {label: 'San Juan', value: '00915'},
    {label: 'San Juan', value: '00916'},
    {label: 'San Juan', value: '00920'},
    {label: 'San Juan', value: '00909'},
    {label: 'San Juan', value: '00910'},
    {label: 'Fort Buchanan', value: '00934'},
    {label: 'San Juan', value: '00936'},
    {label: 'San Juan', value: '00917'},
    {label: 'San Juan', value: '00919'},
    {label: 'San Juan', value: '00911'},
    {label: 'San Juan', value: '00912'},
    {label: 'San Juan', value: '00913'},
    {label: 'San Juan', value: '00914'},
    {label: 'San Juan', value: '00940'},
    {label: 'San Juan', value: '00901'},
    {label: 'San Juan', value: '00902'},
    {label: 'San Juan', value: '00906'},
    {label: 'San Juan', value: '00925'},
    {label: 'San Juan', value: '00926'},
    {label: 'San Juan', value: '00927'},
    {label: 'San Juan', value: '00928'},
    {label: 'San Juan', value: '00930'},
    {label: 'San Juan', value: '00907'},
    {label: 'San Juan', value: '00908'},
    {label: 'San Juan', value: '00931'},
    {label: 'San Juan', value: '00933'},
    {label: 'San Lorenzo', value: '00754'},
    {label: 'San Sebastián', value: '00685'},
    {label: 'Santa Isabel', value: '00757'},
    {label: 'Toa Alta', value: '00953'},
    {label: 'Toa Alta', value: '00954'},
    {label: 'Toa Baja', value: '00949'},
    {label: 'Toa Baja', value: '00950'},
    {label: 'Toa Baja', value: '00951'},
    {label: 'Toa Baja', value: '00952'},
    {label: 'Trujillo Alto', value: '00976'},
    {label: 'Trujillo Alto', value: '00977'},
    {label: 'Saint Just', value: '00978'},
    {label: 'Utuado', value: '00641'},
    {label: 'Utuado', value: '00611'},
    {label: 'Vega Alta', value: '00692'},
    {label: 'Vega Baja', value: '00693'},
    {label: 'Vega Baja', value: '00694'},
    {label: 'Vieques', value: '00765'},
    {label: 'Villalba', value: '00766'},
    {label: 'Yabucoa', value: '00767'},
    {label: 'Yauco', value: '00698'}
];

import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AlertController, IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Plan, PlanAddOn } from '../../../../../../../models/iptv';
import { BasePage } from '../../../../../../base.page';
import { ServicesProvider } from '../../../../../../../services/services.provider';
import { UtilsService } from '../../../../../../../services/utils.service';
import { IntentProvider } from '../../../../../../../services/intent.provider';
import { Popups } from '../../../../../../../utils/const/popups';

@Component({
    selector: 'app-additional-service',
    templateUrl: './additional-service.component.html',
    styleUrls: ['./additional-service.component.scss'],
})
export class AdditionalServiceComponent extends BasePage implements OnInit {

    @ViewChild('slides', {static: false}) slides: IonSlides;
    @Input() planSelected: Plan;
    @Input() addOns: PlanAddOn[];
    @Output() select = new EventEmitter<PlanAddOn>();
    @Output() unselect = new EventEmitter<PlanAddOn>();
    @Output() showTermsOffer = new EventEmitter<PlanAddOn>();

    private addOnSelected: PlanAddOn;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit(): void {
        this.addOns.map(item => {
            if (item.bitInclude) {
                item.selected = true;
                this.select.emit(item);
            }
        });
    }

    changeStatusService(item: PlanAddOn) {
        if (!item.bitEnable || item.bitInclude) {
            return;
        }
        if (!item.selected) {
            item.selected = true;
            this.validateCinemax(item);
            this.validatePlansExclude(item);
            this.select.emit(item);
        } else {
            this.unselect.emit(item);
        }
    }

    private validateCinemax(item: PlanAddOn) {
        const {planSoc} = item;
        if (planSoc === socList.HBO || planSoc === socList.CINEMAX || planSoc === socList.COMBO_HBO_CINEMAX) {
            if (planSoc === socList.HBO || planSoc === socList.CINEMAX) {
                if (this.getPlan(socList.COMBO_HBO_CINEMAX).selected) {
                    const plan = this.getPlan(socList.COMBO_HBO_CINEMAX);
                    plan.selected = false;
                    this.unselect.emit(plan);
                }
                if (item.planSoc === socList.CINEMAX) {
                    const plan = this.getPlan(socList.HBO);
                    if (plan.selected) {
                        plan.selected = false;
                        item.selected = false;
                        this.unselect.emit(plan);
                        this.getPlan(socList.COMBO_HBO_CINEMAX).selected = true;
                        this.moveSlideTo(3);
                    }
                }
                if (planSoc === socList.HBO) {
                    const plan = this.getPlan(socList.CINEMAX);
                    if (this.getPlan(socList.CINEMAX).selected) {
                        plan.selected = false;
                        item.selected = false;
                        this.unselect.emit(plan);
                        this.getPlan(socList.COMBO_HBO_CINEMAX).selected = true;
                        this.moveSlideTo(3);
                    }
                }
            }
        }
    }

    private validatePlansExclude(item: PlanAddOn) {
        const {productsExlude} = item;
        if (productsExlude) {
            const productsToExclude = productsExlude.split(',');
            productsToExclude.map(prod => {
                const plan = this.getPlan(prod, true);
                if (plan.selected) {
                    plan.selected = false;
                    this.unselect.emit(plan);
                }
            });
        }
    }

    private getPlan(param: string, byId = false): PlanAddOn {
        const find = this.addOns.filter(sva => byId ? +sva.planId === +param : sva.planSoc === param);
        return find[0];
    }

    async moveSlideTo(index: any) {
        await this.slides.slideTo(index);
    }

    getTextButton(item: PlanAddOn): string {
        if (item.bitInclude) {
            return 'Incluido';
        }
        if (!item.bitEnable) {
            return 'No Disponible';
        }
        if (!item.selected) {
            return 'Elegir servicio';
        }
        return 'Remover';
    }

    viewChannels(plan: PlanAddOn) {
        this.addOnSelected = plan;
        this.showProgress('Espere mientras cargamos los canales...');
        this.getChannels();
    }

    private async getChannels() {
        const token = this.cacheStorage().tokenIptv;
        this.services.getChannelsIPTV(token, this.planSelected.productId, this.addOnSelected.planId).subscribe(success => {
            if (success) {
                this.showPopup(`Guía de Canales: ${this.addOnSelected.planName}`, {name: Popups.IPTV_CHANNELS, channels: success});
            }
            this.dismissProgress();
        }, e => {
            this.dismissProgress();
        });
    }

    viewTermsOffer(plan: PlanAddOn) {
        this.showPopup(plan.offer.planName, {name: Popups.TERMS_OFFER, terms: plan.offer.planDesc});
    }
}

const socList = {
    HBO: 'HBO202',
    CINEMAX: 'CINEMA',
    COMBO_HBO_CINEMAX: 'COMBO'
};

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { Plan, PlanAddOn } from '../../../../../../models/iptv';
import { UtilsService } from '../../../../../../services/utils.service';
import { Utils } from '../../../../../../utils/utils';
import { ServicesProvider } from '../../../../../../services/services.provider';
import { IntentProvider } from '../../../../../../services/intent.provider';
import { BasePage } from '../../../../../base.page';

@Component({
    selector: 'app-popup-additional-services',
    templateUrl: './popup-additional-services.component.html',
    styleUrls: ['./popup-additional-services.component.scss'],
})
export class PopupAdditionalServicesComponent extends BasePage implements OnInit {

    @Input() currentCart: any;
    @Input() plan: Plan;
    @Input() addOns: PlanAddOn[];
    @Input() totalServices = 0;
    @Output() close = new EventEmitter();
    @Output() withoutSva = new EventEmitter();
    @Output() save = new EventEmitter<any>();

    popupController = {
        popupTerms: false,
        popupTemplate: 'terms-sva.html',
    };
    terms: boolean;
    previousData: boolean;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit(): void {
        this.terms = this.totalServices > 0;
        this.previousData = this.totalServices > 0;
    }

    closeEmit() {
        this.close.emit();
    }

    async withoutServices() {
        if (this.previousData) {
            this.showProgress('Removiendo servicios adicionales...');
            const response = await this.removeAllAdditionalServices();
            if (response) {
                this.dismissProgress();
            }
            this.withoutSva.emit(response);
            return;
        }
        if (this.hasInclude().length) {
            this.saveAddOns();
        } else {
            this.withoutSva.emit();
        }
    }

    private removeAllAdditionalServices(): Promise<AddOnsMapped> {
        return new Promise<AddOnsMapped>(resolve => {
            let currentAddOns = this.currentCart.products.filter(prod => prod.addOns);
            const includes = this.hasInclude();
            const addOns = this.mapAddOns(true);
            if (includes.length) {
                currentAddOns = currentAddOns.filter(addOn => {
                    let noInclude = true;
                    includes.map(inc => {
                        if (inc.planId === addOn.productId) {
                            noInclude = false;
                        }
                    });
                    return noInclude;
                });
            }

            const promises = [];
            const token = this.cacheStorage().tokenSession;
            const tokenShop = this.cacheStorage().tokenIptv;
            for (const plan of currentAddOns) {
                promises.push(this.services.deleteItemFromCart(token, tokenShop, plan.cartId));
            }

            if (promises.length) {
                Promise.all(promises).then((success: any[]) => {
                    resolve(addOns);
                }, e => {
                    this.showError(e.message);
                    this.dismissProgress();
                    resolve(null);
                });
            } else {
                resolve(addOns);
            }
        });
    }

    addService(item: PlanAddOn) {
        this.totalServices++;
    }

    removeService(item: PlanAddOn) {
        if (this.previousData) {
            const currentSVA = this.currentCart.products.filter(sva => sva.productId === item.planId);
            if (currentSVA.length) {
                this.removeServiceFromCart(currentSVA[0]);
                return;
            }
        }
        this.addOns.map(plan => {
            if (plan.planId === item.planId) {
                plan.selected = false;
            }
        });
        this.totalServices--;
    }

    removeServiceFromCart(sva) {
        this.showProgress('Removiendo servicio...');

        const token = this.cacheStorage().tokenSession;
        const tokenShop = this.cacheStorage().tokenIptv;

        this.services.deleteItemFromCart(token, tokenShop, sva.cartId).then((success: any) => {
            this.addOns.map(plan => {
                if (plan.planId === sva.productId) {
                    plan.selected = false;
                }
            });
            this.totalServices--;
            this.showAlert(`El servicio ${sva.productName} ha sido removido con éxito`);
            this.dismissProgress();
        }, e => {
            this.showError(e.message);
            this.dismissProgress();
        });
    }

    async validateAddOns() {
        if (!this.terms) {
            this.openTerms();
            return;
        }
        this.saveAddOns();
    }

    mapAddOns(onlyIncludes = false): AddOnsMapped {
        const addonsSelected = this.addOns.filter(plan => {
            if (onlyIncludes) {
                plan.selected = false;
                return plan.bitInclude;
            } else {
                return plan.selected;
            }
        });

        let payToday = 0;
        let payByMonth = 0;
        if (!onlyIncludes) {
            addonsSelected.map(addon => {
                if (!addon.bitInclude) {
                    if (!addon.offer) {
                        payToday += addon.decPrice;
                    } else {
                        payToday += addon.offer.decSalePrice;
                    }
                    payByMonth += addon.decPrice;
                }
            });
        }

        return {
            selected: addonsSelected,
            payToday,
            payByMonth,
        };
    }

    getAddOnsToAdd(addonsSelected) {
        let addonsToAdd = addonsSelected;
        if (this.previousData) {
            const response = this.currentCart.products.filter(prod => prod.addOns);
            addonsToAdd = addonsSelected.filter(addon => {
                let noExist = true;
                response.map(prod => {
                    if (prod.productId === addon.planId) {
                        noExist = false;
                    }
                });
                return noExist;
            });
        }
        return addonsToAdd;
    }

    saveAddOns() {
        this.showProgress('Agregando servicios...');

        const addOns = this.mapAddOns();
        const addOnsToAdd = this.getAddOnsToAdd(addOns.selected);
        const token = this.cacheStorage().tokenIptv;
        const cartItems = [];

        addOnsToAdd.map(addOn => {
            const cartItem = Utils.formatBasicCartItem(token);
            cartItem.flowId = 8;
            cartItem.productId = addOn.planId;
            cartItem.parentProductId = this.plan.productId;
            cartItem.decPrice = addOn.decPrice;
            cartItem.decTotalPrice = addOn.offer ? addOn.offer.decSalePrice : addOn.decPrice;
            cartItems.push(cartItem);
        });

        this.services.addItemsToCart(token, JSON.stringify(cartItems)).then(success => {
            this.save.emit(addOns);
            this.dismissProgress();
        }, e => {
            this.addOns.map(plan => {
                addOnsToAdd.map(addOn => {
                    if (plan.planId === addOn.planId) {
                        plan.selected = false;
                    }
                });
            });
            this.showError(UtilsService.errorGeneral);
            this.dismissProgress();
        });
    }

    private hasInclude(): PlanAddOn[] {
        return this.addOns.filter(item => item.bitInclude);
    }

    /**
     * Open popup of terms and conditions
     */
    openTerms() {
        this.popupController.popupTerms = true;
    }

    closePopupTerms() {
        this.popupController.popupTerms = false;
        this.terms = true;
    }
}


export interface AddOnsMapped {
    selected: PlanAddOn[];
    payToday: number;
    payByMonth: number;
}

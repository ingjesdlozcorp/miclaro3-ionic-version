import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-module-iptv-faq',
    templateUrl: './iptv-faq.component.html',
    styleUrls: ['./iptv-faq.component.scss'],
})
export class IptvFaqComponent implements OnInit {
    public open = true;
    public contentShow = [
        false, false, false, false,
        false, false, false, false,
        false, false, false, false, false
    ];
    public theOpenGroup = [];

    constructor() {
    }

    ngOnInit() {
    }

    searchDelete(param: number) {
        for (let i = 0; i < this.theOpenGroup.length; i++) {
            if (param === this.theOpenGroup[i]) {
                this.theOpenGroup.splice(i, 1);
            }
        }
    }

    showingMenu(param: number) {
        let close = false;

        if (this.theOpenGroup.length > 0) {
            for (const group of this.theOpenGroup) {
                if (param === group) {
                    close = true;
                }
            }
        }
        // para mostrar el contenido:
        if (close === false) {
            for (let i = 0; i < this.contentShow.length; i++) {
                if (param === i) {
                    this.contentShow[i] = true;
                    this.theOpenGroup.push(param);
                }
            }

        } else {
            // para cerrar el contenido;
            this.searchDelete(param);
            for (let i = 0; i < this.contentShow.length; i++) {
                if (param === i) {
                    this.contentShow[i] = false;
                }
            }
        }
    }

}

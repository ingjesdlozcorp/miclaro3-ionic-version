import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { BasePage } from 'src/app/pages/base.page';
import { Storage } from '@ionic/storage';
import { FirestoreService } from '../../../../../services/firestore.service';

@Component({
    selector: 'app-module-iptv-support',
    templateUrl: './iptv-support.component.html',
    styleUrls: ['./iptv-support.component.scss'],
})
export class IptvSupportComponent extends BasePage implements OnInit {

    public data;
    private urlGuide: any;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider,
                private firestore: FirestoreService) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.data = this.cacheStorage();
    }

    ngOnInit() {
        this.firestore.getConfigIPTV().subscribe((res: any) => {
            this.urlGuide = res.linkGuide;
        });
    }

    iptvGuide() {
        if (this.urlGuide) {
            this.openExternalBrowser(this.urlGuide);
        }
    }
}

import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { UtilsService } from '../../../../services/utils.service';
import { BasePage } from '../../../base.page';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../services/services.provider';
import { AlertController, IonSlides } from '@ionic/angular';
import { IntentProvider } from '../../../../services/intent.provider';
import { Channel, Plan } from '../../../../models/iptv';

@Component({
    selector: 'app-module-iptv',
    templateUrl: './iptv.component.html',
    styleUrls: ['./iptv.component.scss'],
})
export class IptvComponent extends BasePage implements OnInit {

    @ViewChildren('slides') slides: QueryList<IonSlides>;

    utils: UtilsService;
    banner = 'assets/images/iptv/banner_app_iptv-750px.jpg';
    plans: any[] = [0, 0, 0, 0];
    errorMessage = '';
    hasToken: boolean;
    channels: Channel[] = [];
    channelsCarousel: Channel[][] = [];

    constructor(router: Router,
                storage: Storage,
                modelsServices: ServicesProvider,
                alertController: AlertController,
                utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.services = modelsServices;
    }

    ngOnInit() {
        this.getTokenIptV();
    }

    showTerms() {
        this.goPage('terms');
    }

    faq() {
        this.goPage('faq');
    }

    support() {
        this.goPage('support');
    }

    selectPlan(plan: Plan) {
        this.cacheStorage().planIptvSelected = plan;
        this.goPage('buy');
    }

    goPage(page: string) {
        const baseUrl = 'module/iptv/';
        this.utils.goTo(`${baseUrl}${page}`);
    }

    private getTokenIptV() {
        if (this.cacheStorage().tokenIptv) {
            this.hasToken = true;
            this.getChannelsFull();
            return;
        }
        this.showProgress();
        // tslint:disable-next-line:no-shadowed-variable
        this.services.getTokenIptv().then((success: any) => {
            this.loginTokenSSO(success.token);
            this.getChannelsFull();
        }, e => {
            this.dismissProgress();
            this.showError(e.message, () => {
                this.goHomePage();
            });
        });
    }

    private loginTokenSSO(tokenIptv) {
        const ssoToken = this.cacheStorage().SSOAccessToken;
        const data = this.cacheStorage();
        this.services.setTokenIptvSSO(tokenIptv, ssoToken).then(success => {
            const account = success.response.customerInfo.accountList.filter(acc => acc.ban = data.accountInfo.bANField)[0];
            if (account.delinquencyStatus) {
                this.showError(`Su cuenta ${account.ban}, tiene un balance pendiente de $${account.totalBalance}. Por favor realice su pago para continuar.`, this.goHomePage());
            } else {
                this.cacheStorage().tokenIptv = tokenIptv;
                this.cacheStorage().accountSSO = account;
                this.hasToken = true;
            }
            this.dismissProgress();
        }, e => {
            this.dismissProgress();
            this.showError(e.message, () => {
                this.goHomePage();
            });
        });
    }

    private getChannelsFull() {
        this.showProgress();
        const token = this.cacheStorage().tokenSession;

        this.services.getChannelsFullIPTV(token).subscribe(success => {
            if (success.length) {
                const channels = [...success];
                for (const channelsKey of channels) {
                    const arrayThreeItems = channels.splice(0, 4);
                    this.channelsCarousel.push(arrayThreeItems);
                }
                this.channels = success;
            }
            this.dismissProgress();
        }, e => {
            this.dismissProgress();
        });
    }
}

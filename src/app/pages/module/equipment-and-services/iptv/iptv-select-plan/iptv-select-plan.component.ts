import {Component, OnInit} from '@angular/core';
import {BasePage} from 'src/app/pages/base.page';
import {ServicesProvider} from 'src/app/services/services.provider';
import {Router} from '@angular/router';
import {AlertController} from '@ionic/angular';
import {UtilsService} from 'src/app/services/utils.service';
import {IntentProvider} from 'src/app/services/intent.provider';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-module-iptv-select-plan',
    templateUrl: './iptv-select-plan.component.html',
    styleUrls: ['./iptv-select-plan.component.scss'],
})
export class IptvSelectPlanComponent extends BasePage implements OnInit {

    utils: UtilsService;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.services = modelsServices;
    }

    ngOnInit() {
    }

    goPage(page: string) {
        this.utils.goTo(page);
    }

    selectPlan(plan: any) {
        this.cacheStorage().planIptvSelected = plan;
        this.goPage('module/iptv/buy');
    }
}

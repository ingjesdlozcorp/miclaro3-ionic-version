import { Component, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { ServicesProvider } from 'src/app/services/services.provider';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import { Channel } from '../../../../../models/iptv';
import { Popups } from '../../../../../utils/const/popups';

@Component({
    selector: 'app-module-iptv-channels-guide',
    templateUrl: './iptv-channels-guide.component.html',
    styleUrls: ['./iptv-channels-guide.component.scss'],
})
export class IptvChannelsGuideComponent extends BasePage implements OnInit {

    @Input() text = 'Completa';
    @Input() channels: Channel[];

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.services = modelsServices;
    }

    ngOnInit() {
    }

    openPopupGuideChannels() {
        this.showPopup('Guía de Canales', {name: Popups.IPTV_CHANNELS, channels: this.channels});
    }
}

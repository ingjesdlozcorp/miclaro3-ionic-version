import { Component, OnInit } from '@angular/core';
import { Utils } from '../../../../../utils/utils';
import { SubscriberInfo } from '../../../../../models/account.details';
import { BasePage } from '../../../../base.page';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../../../services/utils.service';
import { IntentProvider } from '../../../../../services/intent.provider';
import { Offer } from '../change-plan.component';
import { APP } from '../../../../../utils/const/appConstants';
import { BrowserProvider } from '../../../../../services/browser.provider';

export interface OrderInfo {
    id: string;
    message: string;
    isPrepaid?: boolean;
    ivuPlan?: number;
}

@Component({
    selector: 'app-module-change-plan-confirm',
    templateUrl: './change-plan-confirm.component.html',
    styleUrls: ['./change-plan-confirm.component.scss'],
})
export class ChangePlanConfirmComponent extends BasePage implements OnInit {

    formatSubscriber = Utils.formatSubscriber;
    typeOfTelephony = Utils.typeOfTelephony;
    subscriber: SubscriberInfo;
    selectedPlan: Offer;
    dslMonths;
    months12 = false;
    current = true;
    terms = false;
    typeSubscriber = {
        postpaid: false,
        prepaid: false,
        telephony: false
    };
    currentDate = '';
    nextDate = '';
    selectAmount = false;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.subscriber = this.cacheStorage().selectedSubscriber;
        const data = this.cacheStorage();
        this.typeSubscriber = Utils.objectTypeOfSubscriber(
            data.accountInfo.accountTypeField,
            data.accountInfo.accountSubtypeField,
            this.subscriber.productTypeField);
        this.selectedPlan = this.cacheStorage().selectedPlan;
        this.dslMonths = this.subscriber.planInfoField.mCommitmentOrigNoMonthField;


        this.currentDate = Utils.parseDate();
        this.nextDate = Utils.parseDate(this.cacheStorage().accountInfo.cycleEndDateField);

        if (this.typeSubscriber.prepaid && !this.hasBalance()) {
            this.selectAmount = true;
        }
    }

    getPlanName(): string {
        const plan = this.subscriber.planInfoField;
        return plan.sOCDescriptionField ? plan.sOCDescriptionField : 'No posee';
    }

    getPlanAmount(): string {
        const plan = this.subscriber.planInfoField;
        let planAmount = 0;
        if (plan.sOCDescriptionField != null) {
            planAmount = plan.socRateField;
        }
        return Utils.formatAmount(planAmount);
    }

    confirm() {
        if (!this.terms) {
            this.showAlert('Estimado Cliente, debe aceptar los términos y condiciones para continuar.');
            return;
        }

        if (this.typeSubscriber.postpaid) {
            this.changePlanPostpaid();
        } else if (this.typeSubscriber.prepaid) {
            this.changePlanPrepaid();
        } else if (this.typeSubscriber.telephony) {
            this.changePlanTelephony();
        }
    }

    goHomePage() {
        if (this.cacheStorage().isBusiness()) {
            this.clearStore();
            this.utils.goTo('module/consumption');
        } else {
            this.utils.goTo('home/dashboard');
        }
    }

    getCurrentAmount(): number {
        const data = this.cacheStorage();
        return parseFloat(data.getFullCurrentSelectedSubscriber().prepaidBalanceField);
    }

    hasBalance(): boolean {
        return parseFloat(this.selectedPlan.price) <= this.getCurrentAmount();
    }

    changePlanTelephony() {
        this.showProgress();
        const data = this.cacheStorage();
        this.services.updateSubscriberDSLPlan(
            this.selectedPlan.soc,
            String(this.subscriber.planInfoField.socRateField),
            'Y',
            this.months12 ? '12months' : '24months',
            this.subscriber.productTypeField,
            this.subscriber.subscriberNumberField,
            String(data.accountInfo.bANField),
            data.tokenSession)
            .then(
                (success: any) => {
                    this.dismissProgress();
                    data.orderInfo = {
                        id: success.orderNumber,
                        message: success.ErrorDesc
                    };
                    this.goPage('module/change-plan/success');
                },
                error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
    }

    changePlanPostpaid() {
        this.showProgress();
        const data = this.cacheStorage();
        if (this.current) {
            this.services.updateSubscriberPlan(
                this.selectedPlan.soc,
                this.subscriber.planInfoField.sOCInfoField,
                this.subscriber.productTypeField,
                this.subscriber.subscriberNumberField,
                data.tokenSession)
                .then(
                    (success: any) => {
                        this.dismissProgress();
                        data.orderInfo = {
                            id: success.orderNumber,
                            message: success.ErrorDesc,
                        };
                        this.goPage('module/change-plan/success');
                    },
                    error => {
                        this.dismissProgress();
                        this.showError(error.message);
                    }
                );
        } else {
            this.services.updateSubscriberPlanNextCycle(
                this.selectedPlan.soc,
                this.subscriber.planInfoField.sOCInfoField,
                this.subscriber.productTypeField,
                this.subscriber.subscriberNumberField,
                String(data.accountInfo.bANField),
                data.tokenSession)
                .then(
                    (success: any) => {
                        this.dismissProgress();
                        data.orderInfo = {
                            id: success.orderNumber,
                            message: success.ErrorDesc
                        };
                        this.goPage('module/change-plan/success');
                    },
                    error => {
                        this.dismissProgress();
                        this.showError(error.message);
                    }
                );
        }
    }

    changePlanPrepaid() {
        const data = this.cacheStorage();
        this.showProgress();
        this.services.updatePrepaidSubscriberPlan2(
            data.accountInfo.accountSubtypeField,
            data.accountInfo.accountTypeField,
            this.subscriber.productTypeField,
            Utils.formatMMDDYYYYToYYYY_MM_DD(data.accountInfo.cycleEndDateField),
            this.subscriber.planInfoField.sOCInfoField,
            this.selectedPlan.soc,
            this.selectedPlan.id,
            this.subscriber.subscriberNumberField,
            data.tokenSession)
            .then(
                (success: any) => {
                    this.dismissProgress();
                    data.orderInfo = {
                        id: success.idTransaction,
                        message: success.message, // TODO, unknonw real name
                    };
                    if (!this.hasBalance()) {
                        this.goPage('module/change-plan/recharge');
                    } else {
                        this.evaluatePlan();
                    }
                },
                error => {
                    this.dismissProgress();
                    this.verifyErrorCodeOnRecharge(error.message);
                }
            );
    }

    private evaluatePlan() {
        this.showProgress();
        const data = this.cacheStorage();
        this.services.getEvaluatePlanPrepaid(
            data.tokenSession,
            this.subscriber.subscriberNumberField,
            this.selectedPlan.soc)
            .then(success => {
                this.dismissProgress();
                this.goPage('module/change-plan/success');
            }, error => {
                this.dismissProgress();
                data.orderInfo = {id: 'error', message: 'sinRecarga'};
                this.goPage('module/change-plan/success');
            });
    }

    verifyErrorCodeOnRecharge(error: any) {
        const data = this.cacheStorage();
        switch (error.code) {
            case '001':
                this.showError('Estimado cliente nuestro sistema indica que ha llegado al límite de <b>Cambio de Plan</b> en un mes.', () => {
                    this.goHomePage();
                });
                break;
            case '002':
                this.showError('Estimado Cliente al momento estamos presentando problemas para activacion de su nuevo plan.', () => {
                    this.goHomePage();
                });
                break;
            case '003':
                this.showError('Actualmente el sistema está presentando problemas de comunicación, por favor contacte a Soporte Operacional o intente nuevamente en los próximos minutos.', () => {
                    this.goHomePage();
                });
                break;
            default:
                this.showConfirmCustom('Un error ha ocurrido', error.message, 'Volver a intertar', 'Cancelar',
                    () => {
                        this.changePlanPrepaid();
                    }, () => {
                    });
                break;
        }
    }

    async showTerms(onDismiss?) {
        const textTerms = this.typeSubscriber.telephony ? this.termsDLS() : this.termsGSM();
        const alert = await this.getAlert().create({
            header: 'Términos y Condiciones',
            message:
                '<div class="termr-t">\n' +
                textTerms +
                '</div>',
            buttons: [
                {
                    text: 'Cerrar',
                    role: 'cancel',
                    handler: () => {
                        if (onDismiss) {
                            onDismiss();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    termsDLS(): string {
        return '1. Los planes sugeridos están considerando la velocidad máxima de internet y disponibilidad de\n' +
            '\t\t\t\t\t\t\tmayores velocidades en tu área.\n' +
            '\t\t\t\t\t\t\t2. No se permiten Down Grade de planes.\n' +
            '\t\t\t\t\t\t\t3. Se Renueva contrato a 12 o 24 meses, según aplique.\n' +
            '\t\t\t\t\t\t\t4. Cliente existente no se le hará verificación de crédito.\n' +
            '\t\t\t\t\t\t\t5. La promoción estará sujeta a las velocidades disponibles y aceptadas para el cambio de plan.\n' +
            '\t\t\t\t\t\t\t6. La cancelación temprana del servicio de internet, conlleva penalidad de $150 y devolución del\n' +
            '\t\t\t\t\t\t\tmodem. Si no devuelve el modem aplicará penalidad de $100 adicionales o si lo devuelve\n' +
            '\t\t\t\t\t\t\tincompleto aplicará cargo de $35 adicionales.\n' +
            '\t\t\t\t\t\t\t7. Precios incluidos no incluyen IVU, ni cargos federales del FUSC y PRUSF, los cuales pueden\n' +
            '\t\t\t\t\t\t\tvariar del tiempo en tiempo por disposición gubernamental.';
    }

    termsGSM(): string {
        return '<b>PLANES SIN FRONTERAS:</b>\n' +
            '\n' +
            '\t\t\t\t\t\t\tEstos planes están sólo disponibles para residentes de Puerto Rico. No aplican a residentes de Puerto Rico estudiando o viviendo en EE.UU., México, República Dominicana, Canadá, Argentina, Uruguay, Paraguay, Panamá, Guatemala, Nicaragua, Honduras, Costa Rica, Perú, Colombia, Chile, El Salvador, Brasil y Ecuador. No aplica cuando el cliente está fuera de PR haciendo "roaming" a destinos fuera de EE.UU., Canadá, México, República Dominicana, Argentina, Uruguay, Paraguay, Panamá, Guatemala, Nicaragua, Honduras, Costa Rica, Perú, Colombia, Chile, El Salvador, Brasil y Ecuador salvo las llamadas entre esos países y desde esos países a Puerto Rico. No cubre llamadas a números que puedan incurrir en cargos adicionales o que conlleven costos. Claro no es responsable de la cobertura y calidad de servicios en las redes de sus socios de roaming en estos países.? Esta oferta no puede ser combinada con ningún otro paquete o plan de servicio internacional.\n' +
            '\n' +
            '\t\t\t\t\t\t\tLos minutos de larga distancia a EE.UU., México, República Dominicana, Canadá, Argentina, Uruguay, Paraguay, Panamá, Guatemala, Nicaragua, Honduras, Costa Rica, Perú, Colombia, Chile, El Salvador, Brasil y Ecuador incluidos son solo cuando el cliente realiza la llamada desde PR. El "roaming" de este plan aplica solo a llamadas dentro de los países incluidos en el plan, entre los países incluidos en el plan y de los países incluidos en el plan hacia Puerto Rico. No aplica a llamadas realizadas a otros países.\n' +
            '\n' +
            '\n' +
            '\t\t\t\t\t\t\tLos planes con data ilimitada tienen un paquete de data asignado de acuerdo a su mensualidad usando la velocidad dentro de la cobertura de la red 2G, 3G y 4G LTE.? Una vez alcanzada esa velocidad el cliente continuará navegando a 128kbps por el resto de su ciclo (30 días) de facturación, lo cual podría impactar la funcionalidad de algunas aplicaciones de data, tales como la transmisión continua (?streaming?) de audio o video y/o la navegación en el internet. Al iniciar el siguiente ciclo, se tendrá nuevamente acceso a los recursos disponibles de la red hasta un máximo de 1.5mpbs. Servicio de Internet sujeto al área de cobertura. No puede ser utilizado como módem. El Internet Móvil incluido es para uso en PR a velocidades 3G, 4G o LTE. En EE.UU., República Dominicana, México y Canadá, Argentina, Uruguay, Paraguay, Panamá, Guatemala, Nicaragua, Honduras, Costa Rica, Perú, Colombia, Chile, El Salvador, Brasil y Ecuador a velocidades 3G. Red LTE no está disponible en todo PR\n' +
            '\t\t\t\t\t\t\t.\n' +
            '\t\t\t\t\t\t\tEn el plan de $80 la data ilimitada sin reducción de velocidad es solo en PR. En EE.UU., República Dominicana, México y Canadá, Argentina, Uruguay, Paraguay, Panamá, Guatemala, Nicaragua, Honduras, Costa Rica, Perú, Colombia, Chile, El Salvador, Brasil y Ecuador el cliente solo tendrá 2GB y luego reduce velocidad hasta un máximo de 128KBPS lo cual podría impactar la funcionalidad de algunas aplicaciones de data, tales como la transmisión continua (?streaming?) de audio o video y/o la navegación en el internet.? El servicio de Tethering es hasta 5GB.\n' +
            '\n' +
            '\t\t\t\t\t\t\tLos mensajes de texto (SMS) o mensajes multimedios (MMS) ilimitados son solo en PR, cuando los envían desde PR a números en EE.UU., México, Canadá, Republica Dominicana, Argentina, Uruguay, Paraguay, Panamá, Guatemala, Nicaragua, Honduras, Costa Rica, Perú, Colombia, Chile, Brasil, Ecuador y El Salvador; o cuando el cliente está en EE.UU., México, Canadá, República Dominicana, Argentina, Uruguay, Paraguay, Panamá, Guatemala, Nicaragua, Honduras, Costa Rica, Perú, Colombia, Chile, Brasil, Ecuador y El Salvador y los envía a Puerto Rico o cuando son enviados entre los países incluidos en el plan. También aplica cuando los envían desde PR a destinos internacionales especificados en la página claropr.com. Ningún plan de llamadas de Claro incluye mensajes de texto o mensajes multimedios para participar en concursos en PR o fuera de PR.\n' +
            '\n' +
            '\n' +
            '\t\t\t\t\t\t\tIMPORTANTE: todos los planes y servicios que ofrece Claro son únicamente para uso personal según contratado y para el diálogo y/o comunicación entre personas. El Cliente reconoce y acepta que todo uso comercial, fraudulento, comunitario o la reventa del servicio se consideran conducta de abuso y/o mal uso del servicio, tales como, pero no limitado a servicios de monitoreo, uso de su equipo como módem para su servicio de Internet, transmisión o difusión de materiales grabados, telemercadeo, llamadas auto-marcadas, y transferencias de llamadas u otras conexiones similares. El cliente reconoce y acepta que en caso de que Claro detecte que el uso del servicio se aparta de las normas de buen uso de dicho servicio, Claro podrá, a su sola discreción y sin previo aviso al cliente; (i) suspender el servicio; (ii) terminar el servicio, (iii) modificar su servicio, o (iv) cobrarle la tarifa a granel aplicable.\n' +
            '\t\t\t\t\t\t\tPodrían aplicar cargos recurrentes o gubernamentales como 9-1-1, servicio universal local y federal, el programa de administración de números, portabilidad local de números e impuestos, los cuales están sujetos a cambio sin previo aviso.? ? 2015, Claro Puerto Rico. Productos ofrecidos por Claro Puerto Rico.\n' +
            '\n' +
            '\t\t\t\t\t\t\tUso de Internet Móvil a Granel ("On Demand"):\n' +
            '\t\t\t\t\t\t\tTodo cliente de Claro en un plan que no tiene un paquete de datos, tiene la habilidad de navegar el Internet en su celular, hasta alcanzar un consumo máximo de $50.00 mientras se encuentre en Puerto Rico. Una vez alcance esta cantidad será bloqueado su servicio de datos. El cliente puede permanecer bloqueado hasta que comience su próximo ciclo de facturación mensual o llamar a Servicio al Cliente discando *611 y acogerse a un paquete de Internet Móvil desde $9.99. Si el cliente se encuentra fuera de Puerto Rico, y utiliza el servicio de Internet Móvil podrá navegar en su celular hasta alcanzar un consumo máximo de 20MB, luego su servicio de datos será bloqueado. Si su equipos tiene la funcionalidad de WiFi podrá siempre ser utilizada, pero deberá deshabilitar su conexión a la red de datos de Claro, para no incurrir en cargos por el servicio de datos. Para mayor información consulte el manual del usuario de su equipo celular. Si el cliente no desea tener la habilidad de conexión al Internet podría solicitar la eliminación del servicio llamando a Servicio al Cliente o visitando cualquiera de nuestros Centros de Atención al Cliente alrededor de la Isla.\n' +
            '\t\t\t\t\t\t\tSi usted agota el contenido de datos de su plan, usted puede comprar un paquete adicional booster para aumentar nuevamente la velocidad de navegación de Internet desde $4.99.\n' +
            '\n' +
            '\n' +
            '\t\t\t\t\t\t\t<b>PLANES UPDATE</b>\n' +
            '\n' +
            '\t\t\t\t\t\t\tLos términos y condiciones de este programa sólo aplican a aquellos modelos de celulares que de tiempo en tiempo Claro seleccione.\n' +
            '\n' +
            '\t\t\t\t\t\t\tAl acogerse a este programa el Cliente reconoce y acepta que el precio del equipo que seleccione, de entre aquellos que cualifiquen, será dividido en veinte (20) o veinticuatro (24) plazos mensuales iguales, a la elección del Cliente, y sin cargos de financiamiento. Requiere verificación y aprobación de crédito de conformidad con las políticas crediticias de Claro. Máximo de (6) líneas por cliente. Clientes mayores de 21 años; residentes de Puerto Rico. En algunos casos podría ser requerida la prestación de un depósito, en cuyo caso el Cliente se obliga a prestarlo.\n' +
            '\n' +
            '\t\t\t\t\t\t\tEl precio del equipo no incluye el IVU ni ningún otro impuesto aplicable, los cuales deberán ser pagados por el Cliente al momento de la compra del equipo.\n' +
            '\n' +
            '\t\t\t\t\t\t\tLa edad mínima requerida para participar de este programa es 21 años.\n' +
            '\n' +
            '\t\t\t\t\t\t\tEste programa sólo aplica a Clientes nuevos y Clientes existentes con y sin contrato. Para Clientes existentes con contrato vigente, aplicará siempre y cuando al momento de acogerse a este programa hayan transcurrido completamente doce (12) meses o más del término de su contrato vigente. El plan de llamadas seleccionado por el cliente existente debe ser uno de un costo igual o mayor que el que tenía bajo el contrato existente.\n' +
            '\n' +
            '\t\t\t\t\t\t\tLos Clientes que se acojan a este programa cualificarán para planes de llamadas específicamente diseñados para este programa. Una vez un cliente se acoge al programa, de querer cancelar su servicio, deberá pagar previamente la totalidad del balance adeudado por el pago del equipo adquirido, esto es, el número de meses restantes al contrato por la cantidad mensual pagada.\n' +
            '\n' +
            '\t\t\t\t\t\t\tDurante la totalidad del término de pago aplazado del equipo, el Cliente se obliga a mantener activado un plan de voz y datos de Claro que sea elegible. Una vez expirado el plazo de veinte (20) o veinticuatro (24) meses, el Cliente podrá seleccionar el plan que desee.\n' +
            '\n' +
            '\t\t\t\t\t\t\tDurante la totalidad del término de pago aplazado el Cliente asume el riesgo de pérdida o daño del equipo. El Cliente es responsable del pago del precio total del equipo o de su reparación si éste se pierde o se daña y la pérdida o reparación que está cubierta por un seguro o por la garantía del manufacturero.\n' +
            '\n' +
            '\t\t\t\t\t\t\tEn caso de que el Cliente termine su servicio con Claro, voluntaria o involuntariamente, antes de que cumpla con el pago total del equipo, el balance pendiente de pago se acelera y el Cliente se obliga a pagar la totalidad de las mensualidades que resten, así como los cargos corrientes pendientes de pago al momento de la cancelación.\n' +
            '\n' +
            '\t\t\t\t\t\t\tSi un cliente existente que haya optado por acogerse a este programa el cual ha sido liberado del cumplimiento del término restante de un contrato previo y le haya sido eliminada la penalidad por terminación temprana, si el Cliente desea terminar su obligación bajo este programa durante los primeros siete (7) días de haberse acogido al mismo, entonces aplicará una de las siguientes opciones: (a) el Cliente será reactivado con el equipo adquirido bajo este programa en el plan de llamadas bajo contrato que tenía o, de éste no estar disponible deberá seleccionar uno de igual o mayor precio, con un término de contrato a 24 meses, así como será reestablecida la obligación de pagar penalidad por terminación temprana de cancelar el mismo antes de su expiración. Si el equipo recibido hubiera conllevado un costo de haber sido activado desde un principio en el plan seleccionado por el Cliente, éste deberá también pagar dicho costo como condición para acogerse a esta opción. o (b) de no desear permanecer como cliente de Claro, el Cliente deberá devolver el equipo recibido bajo este programa con todos sus accesorios siempre y cuando esté en buenas condiciones físicas y funcione adecuadamente, y deberá pagar la penalidad por terminación temprana condonada para acogerse a este programa.\n' +
            '\n' +
            '\t\t\t\t\t\t\tEl Cliente tendrá la opción de acelerar el pago del equipo hasta llegar a un 50% del precio del mismo. El cliente podrá hacer un pago por la cantidad que le falte para llegar al 50%.\n' +
            '\n' +
            '\t\t\t\t\t\t\t12. Una vez el cliente haya pagado el 50% del precio del equipo escogido, podrá cambiar el mismo, seleccionar un nuevo equipo y aplazar el pago del nuevo equipo. De querer seleccionar un nuevo equipo el Cliente tendrá las siguientes opciones: (a) de querer adquirir el equipo que posee, deberá pagar la totalidad de los pagos aplazados que resten; o (b) de no querer el equipo y querer uno nuevo, deberá entregar el equipo usado con todos sus accesorios, siempre y cuando éste esté en buenas condiciones físicas y funcione adecuadamente. Se entenderá, pero sin limitarse, que un equipo funciona adecuadamente y está en buenas condiciones físicas cuando: (a) enciende y apaga; (b) no tiene rayasos; (c) no está roto; (d) la pantalla no está rota o fundida; (e) no evidencia humedad (sello de humedad); (f) está completo, no le faltan piezas o accesorios. De ser así, el balance será cancelado por Claro y el Cliente acepta que un nuevo término de pago aplazado de veinte (20) o veinticuatro (24) meses comenzará a correr bajo los términos y condiciones que estén vigentes en ese momento. Cuando el Cliente entregue su equipo, deberá asegurarse que ha borrado toda información personal, suya o de terceros, fotos, videos o imágenes suyas o de terceros. Claro no se hace responsable por información personal, fotos, videos o imágenes dejadas por el Cliente en el teléfono entregado a Claro. El cliente reconoce y acepta que Claro, o un tercero autorizado por Claro, podrán borrar todo contenido dejado por el Cliente en el teléfono. El Cliente libera de responsabilidad y mantendrá indemne a Claro, incluyendo el pago de honorarios de abogado, por cualquier reclamación que sea presentada por él o por cualquier tercero como consecuencia de esto.\n' +
            '\t\t\t\t\t\t\t13. Con excepción de lo dispuesto en la Sección 9, la terminación de este acuerdo no conlleva pago de penalidades por terminación pero sí requiere el pago del precio total del equipo y de cualquier balance de cargos corrientes pendientes de pago.\n' +
            '\n' +
            '\t\t\t\t\t\t\t14. El plan ilimitado tendrá un paquete de data asignado en el cual el cliente podrá navegar dentro de la cobertura de la red 3G y 4G LTE. Una vez alcanzada la velocidad incluida, el cliente continuará navegando a 128kbps por el resto de su ciclo (30 días) de facturación, lo cual podría impactar la funcionalidad de algunas aplicaciones de data, tales como la transmisión continua ("streaming") de audio o video y/o la navegación en el Internet. Al iniciar el siguiente ciclo, se tendrá nuevamente acceso a los recursos disponibles de la red hasta un máximo de 1.5mpbs. Servicio de Internet sujeto al área de cobertura. No puede ser utilizado como módem. El Internet Móvil incluido en este plan es para uso en PR y en EE.UU. Red LTE no está disponible en todo PR.\n' +
            '\n' +
            '\t\t\t\t\t\t\t15. En el caso de los planes con data ilimitada sin reducción de velocidad. La data sin reducción de velocidad es solo para uso en PR, para data en Estados Unidos el cliente tendrá solo 2GB y luego reduce velocidad hasta un máximo de 128Kbps lo cual podría impactar la funcionalidad de algunas aplicaciones de data, tales como la transmisión continua ("streaming") de audio o video y/o la navegación en el internet. El servicio de Tethering es hasta 5GB.\n' +
            '\n' +
            '\t\t\t\t\t\t\t16. Los planes nacionales incluyen 100 minutos de llamadas de larga distancia internacional que aplican a 230 países. Costo por minuto de larga distancia internacional adicional a los 100 según el destino al que llame. Los minutos de larga distancia a EE.UU. incluidos en el plan son solo cuando el cliente realiza la llamada desde PR. No incluye larga distancia Internacional.\n' +
            '\n' +
            '\t\t\t\t\t\t\t17. Además de los términos y condiciones aquí consignados, también son de aplicación los términos y condiciones del contrato del que este Addendum forma parte; disponiéndose, sin embargo, que de haber conflicto entre ambos términos y condiciones, prevalecerán los términos y condiciones de este Addendum.\n' +
            '\t\t\t\t\t\t\tSalvo que otra cosa se disponga en una oferta o promoción de Claro, este programa no es combinable con otras ofertas, programas o descuentos.\n' +
            '\t\t\t\t\t\t\tSi usted agota el contenido de datos de su plan, usted puede comprar un paquete adicional booster para aumentar nuevamente la velocidad de navegación de Internet desde $4.99.\n' +
            '\n' +
            '\n' +
            '\t\t\t\t\t\t\t<b>PLANES BYOP CREDIT LIMIT</b>\n' +
            '\n' +
            '\t\t\t\t\t\t\tRequerirá identificación con foto y firma de acuerdo de servicio del programa BYOP para sus versiones sin contrato. Los planes BYOP no requieren contrato, no tienen cargos por cancelación y no requieren verificación de crédito. No pueden ser compartidos. Máximo cinco (5) líneas por cliente. Requiere que el cliente pague el primer mes de renta del plan al momento de la activación. Una vez salga la factura, el cliente verá reflejado en la misma el pago realizado y el cobro del próximo mes. El cliente tendrá un límite de consumo mensual de $500 aplicable a todos aquellos servicios que no están incluidos en el plan y generan un cargo. Excepto por el pago de una penalidad por la terminación de contrato y los términos que estén en contradicción con los antes expuestos, todos los otros términos y condiciones del servicio celular pospago aplican para BYOP credit limit. Este servicio tampoco aplica para menores de 18 años. Requiere que el cliente traiga un equipo GSM desbloqueado o compre un equipo a Claro al momento de la activación. Cen caso de que el cliente traiga un equipo Claro no garantiza su funcionamiento y el cliente asume el riesgo por la debida operación del equipo por lo que libera a Claro de cualquier reclamación o responsabilidad por tales situaciones. El cliente podría comprar un equipo compatible con la red de Claro. Otros términos y condiciones pueden aplicar según el plan que escoja el cliente.\n' +
            '\n' +
            '\t\t\t\t\t\t\tEn caso de que el cliente traiga su equipo, este debe funcionar en la red 3G UMTS, bandas 850 y 1900 MHz? o en la red 4LTE, bandas AWS, 700 MHz.\n' +
            '\n' +
            '\t\t\t\t\t\t\tEl cliente reconoce y acepta que es el único responsable por el mantenimiento y buen funcionamiento del equipo provisto para ser activado en este programa. Reconoce y acepta, además, que aquellos equipos que originalmente no fueron programados para funcionar en las redes de Claro, no necesariamente tendrán disponibles todas sus funcionalidades o capacidades, tales como, pero sin limitarse a, roaming, data, data roaming y mensajería de texto o multimedia (SMS/MMS). Claro no garantiza su funcionamiento y el cliente asume el riesgo por la debida operación del equipo por lo que libera a Claro de cualquier reclamación o responsabilidad por tales situaciones.\n' +
            '\n' +
            '\t\t\t\t\t\t\tCargos por activación y SIM Card podrían aplicar.\n' +
            '\n' +
            '\t\t\t\t\t\t\tNo se requiere verificación de crédito salvo que un cliente active más de 5 líneas. Máximo de 6 líneas por cliente. Esta limitación aplica sólo a cuentas familiares.\n' +
            '\n' +
            '\t\t\t\t\t\t\tEl plan ilimitado tendrá un paquete de data asignado en el cual el cliente podrá navegar dentro de la cobertura de la red 3G y 4G LTE. Una vez alcanzada la velocidad incluida, el cliente continuará navegando a 128kbps por el resto de su ciclo (30 días) de facturación, lo cual podría impactar la funcionalidad de algunas aplicaciones de data, tales como la transmisión continua (?streaming?) de audio o video y/o la navegación en el Internet. Al iniciar el siguiente ciclo, se tendrá nuevamente acceso a los recursos disponibles de la red hasta un máximo de 1.5mpbs. Servicio de Internet sujeto al área de cobertura. No puede ser utilizado como módem. El Internet Móvil incluido en este plan es para uso en PR y en EE.UU. Red LTE no está disponible en todo PR.\n' +
            '\n' +
            '\t\t\t\t\t\t\tEn el caso de los planes con data ilimitada sin reducción de velocidad. La data sin reducción de velocidad es solo para uso en PR, para data en Estados Unidos el cliente tendrá solo 2GB y luego reduce velocidad hasta un máximo de 128Kbps lo cual podría impactar la funcionalidad de algunas aplicaciones de data, tales como la transmisión continua (?streaming?) de audio o video y/o la navegación en el internet.? El servicio de Tethering es hasta 5GB.\n' +
            '\n' +
            '\t\t\t\t\t\t\tLos planes nacionales incluyen 100 minutos de llamadas de larga distancia internacional que aplican a 230 países. Costo por minuto de larga distancia internacional adicional a los 100 según el destino al que llame. Los minutos de larga distancia a EE.UU. incluidos en el plan son solo cuando el cliente realiza la llamada desde PR. No incluye larga distancia Internacional.\n' +
            '\n' +
            '\t\t\t\t\t\t\tAl activarte en un plan mensual de $40.00 con contrato de 2 años tienes derecho a recibir un bono de portabilidad de $170.00. El bono de portabilidad de $170 por cada número actual activo con la competencia que traigas. El ahorro se aplicará mediante un crédito a la cuenta por la cantidad de $5.83 mensuales por 24 meses y $30 aplicables al cargo por activación. El cliente pierde su derecho a continuar recibiendo el crédito mensual si se da de baja previo a los 24 meses. Esta oferta de bonificación no podrá ser combinada con ninguna otra oferta.\n' +
            '\n' +
            '\n' +
            '\t\t\t\t\t\t\tIMPORTANTE: todos los planes y servicios que ofrece Claro son únicamente para uso personal según contratado y para el diálogo y/o comunicación entre personas. El Cliente reconoce y acepta que todo uso comercial, fraudulento, comunitario o la reventa del servicio se consideran conducta de abuso y/o mal uso del servicio, tales como, pero no limitado a servicios de monitoreo, uso de su equipo como módem para su servicio de Internet, transmisión o difusión de materiales grabados, telemercadeo, llamadas auto-marcadas, y transferencias de llamadas u otras conexiones similares. El cliente reconoce y acepta que en caso de que Claro detecte que el uso del servicio se aparta de las normas de buen uso de dicho servicio, Claro podrá, a su sola discreción y sin previo aviso al cliente; (i) suspender el servicio; (ii) terminar el servicio, (iii) modificar su servicio, o (iv) cobrarle la tarifa a granel aplicable.?\n' +
            '\t\t\t\t\t\t\tPodrían aplicar cargos recurrentes o gubernamentales como 9-1-1, servicio universal local y federal, el programa de administración de números, portabilidad local de números e impuestos, los cuales están sujetos a cambio sin previo aviso. ? 2015, Claro Puerto Rico. Productos ofrecidos por Claro Puerto Rico. 072415/#070102/2015\n' +
            '\t\t\t\t\t\t\tSi usted agota el contenido de datos de su plan, usted puede comprar un paquete adicional desde $4.99, para continuar utilizando el Internet.';
    }
}

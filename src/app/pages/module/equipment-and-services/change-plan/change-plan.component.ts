import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../../services/utils.service';
import { IntentProvider } from '../../../../services/intent.provider';
import { BasePage } from '../../../base.page';
import { Utils } from '../../../../utils/utils';
import { SubscriberInfo } from '../../../../models/account.details';
import { IonSlides } from '@ionic/angular';

const ERROR_GETTING_PLANS = 'Estimado cliente en estos momentos no tenemos planes para este suscriptor.';

export interface Offer {
    name: string;
    price: string;
    soc: string;
    description: string;
    descriptionList: string[];
    id?: string;
}

@Component({
    selector: 'app-module-change-plan',
    templateUrl: './change-plan.component.html',
    styleUrls: ['./change-plan.component.scss'],
})
export class ChangePlanComponent extends BasePage implements OnInit {

    @ViewChild('slides', {static: false}) slider: IonSlides;
    @ViewChildren('slides') slides: QueryList<IonSlides>;

    formatSubscriber = Utils.formatSubscriber;
    typeOfTelephony = Utils.typeOfTelephony;
    subscribers: SubscriberInfo[] = [];
    selectedSubscriber: SubscriberInfo = undefined;
    isPrepaid = false;
    errorMessage = '';

    plansSuggested: Offer[] = [];

    slideOpts = {
        loop: true,
    };

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('change_plan');
    }

    ngOnInit() {
        this.subscribers = this.cacheStorage().subscriberList;
        const accountInfo = this.cacheStorage().accountInfo;
        this.isPrepaid = Utils.isPrepaid(accountInfo.accountTypeField, accountInfo.accountSubtypeField);
        if (this.subscribers.length === 1) {
            this.showDetails(this.subscribers[0]);
        }
    }

    slideDidChange(event) {
        // first and last slides are duplicate slides
        // this method is to allow events in the first and last slide
        this.slider.getActiveIndex()
            .then(async (activeIndex: number) => {
                const slideCount = await this.slider.length();
                if (activeIndex === slideCount - 1) {
                    await this.slider.slideTo(1, 0);
                }
                if (activeIndex === 0) {
                    await this.slider.slideTo(slideCount - 2, 0);
                }
            });
    }


    showDetails(subscriber: SubscriberInfo) {
        if (this.selectedSubscriber === subscriber) {
            this.selectedSubscriber = undefined;
        } else {
            this.selectedSubscriber = subscriber;
            const accountInfo = this.cacheStorage().accountInfo;
            if (Utils.isPostpaid(accountInfo.accountTypeField, accountInfo.accountSubtypeField, subscriber.productTypeField)) {
                this.getPlansSuggested(subscriber);
            } else if (Utils.isTelephony(accountInfo.accountTypeField, accountInfo.accountSubtypeField, subscriber.productTypeField)) {
                this.getPlansSuggestedDSL(subscriber);
            } else if (Utils.isPrepaid(accountInfo.accountTypeField, accountInfo.accountSubtypeField)) {
                this.getPlansSuggestedPrepaid(subscriber);
            }
        }
    }

    getPlanName(subscriber: SubscriberInfo): string {
        const plan = subscriber.planInfoField;
        return plan.sOCDescriptionField;
    }

    getPlanDescriptionList(subscriber: SubscriberInfo): string[] {
        const plan = subscriber.planInfoField;
        const planFullName = plan.sOCDescriptionField;
        if (planFullName != null) {
            let subPlans = [planFullName];
            if (planFullName.split(':').length > 1) {
                subPlans = planFullName.split(':')[1].split('-');
            }
            return subPlans;
        }
        return [];
    }

    getPlanAmount(subscriber: SubscriberInfo): string {
        const plan = subscriber.planInfoField;
        let planAmount = 0;
        if (plan.sOCDescriptionField != null) {
            planAmount = plan.socRateField;
        }
        return Utils.formatAmount(planAmount);
    }

    prev(index: number) {
        this.slides.toArray()[index].slidePrev().then();
    }

    next(index: number) {
        this.slides.toArray()[index].slideNext().then();
    }

    getPlansSuggested(subscriber: SubscriberInfo) {
        this.showProgress();
        this.plansSuggested = [];
        const accountInfo = this.cacheStorage().accountInfo;
        const plan = subscriber.planInfoField;
        this.services.getPlans(
            accountInfo.creditClassField,
            accountInfo.accountSubtypeField,
            accountInfo.accountTypeField,
            Utils.formatAmount(plan.socRateField),
            plan.sOCInfoField,
            'LTE',
            this.cacheStorage().tokenSession
        ).then(
            (success: any) => {
                this.dismissProgress();
                success.object.forEach((item: any) => {

                    const offerName = item.description.split(':')[0].trim();
                    let offerDescription = item.description.split(':')[1];
                    let offerDescriptionList = [];
                    if (offerDescription != null && offerDescription.includes('-')) {
                        offerDescriptionList = offerDescription.trim().split('-');
                    } else {
                        offerDescriptionList.push(offerName);
                    }
                    const offerPrice = item.rent;
                    const offerSoc = item.soc;
                    offerDescription = item.description;

                    this.plansSuggested.push({
                        name: offerName,
                        description: offerDescription,
                        descriptionList: offerDescriptionList,
                        price: offerPrice,
                        soc: offerSoc
                    });
                });

                if (success.object === undefined || success.object === null || success.object.length === 0) {
                    this.errorMessage = ERROR_GETTING_PLANS;
                }

            }, error => {
                if (error.withStatus200) {
                    this.errorMessage = error.message;
                } else {
                    this.errorMessage = ERROR_GETTING_PLANS;
                }
                this.dismissProgress();
            });
    }

    getPlansSuggestedDSL(subscriber: SubscriberInfo) {
        this.showProgress();
        this.plansSuggested = [];
        this.services.getPlansDSL(
            subscriber.subscriberNumberField,
            this.cacheStorage().tokenSession
        ).then(
            (success: any) => {
                this.dismissProgress();
                success.DSLCatalogItems.forEach((item: any) => {
                    const offerName = item.PRODUCT_NAME;
                    let offerDescription = item.DESCRIPTION.substring(0, item.DESCRIPTION.length - 1);
                    const offerDescriptionList = offerDescription.split('.');

                    const offerPrice = item.PRICE;
                    const offerSoc = item.ADA_PRODUCT_ID;
                    offerDescription = item.DESCRIPTION;

                    this.plansSuggested.push({
                        name: offerName,
                        description: offerDescription,
                        descriptionList: offerDescriptionList,
                        price: offerPrice,
                        soc: offerSoc
                    });
                });
            }, error => {
                if (error.withStatus200) {
                    this.errorMessage = error.message;
                } else {
                    this.errorMessage = ERROR_GETTING_PLANS;
                }
                this.dismissProgress();
            });
    }

    getPlansSuggestedPrepaid(subscriber: SubscriberInfo) {
        this.showProgress();
        this.plansSuggested = [];
        const accountInfo = this.cacheStorage().accountInfo;
        const plan = subscriber.planInfoField;
        this.services.getPlansPrepaid2(
            accountInfo.creditClassField,
            subscriber.productTypeField,
            accountInfo.accountSubtypeField,
            accountInfo.accountTypeField,
            Utils.formatAmount(plan.socRateField),
            plan.sOCInfoField,
            subscriber.equipmentInfoField.techField == null ? 'LTE' : subscriber.equipmentInfoField.techField,
            this.cacheStorage().tokenSession
        ).then(
            (success: any) => {
                this.dismissProgress();
                success.socList.forEach((item: any) => {
                    // if (parseFloat(item.price) > plan.socRateField) { TODO, consultar si sigue aplicando
                    const offerName = item.socDescription;
                    let offerDescription = item.description.split(':')[1];
                    let offerDescriptionList = [];
                    if (offerDescription != null && offerDescription.includes('-')) {
                        offerDescriptionList = offerDescription.trim().split('-');
                    } else {
                        offerDescriptionList.push(offerName);
                    }
                    const offerPrice = Utils.formatAmount(item.rent);
                    const offerSoc = item.soc;
                    offerDescription = item.description ? item.description : item.socDescription;

                    const offerId = item.id;

                    this.plansSuggested.push({
                        name: offerName,
                        description: offerDescription,
                        descriptionList: offerDescriptionList,
                        price: offerPrice,
                        soc: offerSoc,
                        id: offerId
                    });
                    // }
                });

                if (success.socList === undefined || success.socList === null || success.socList.length === 0) {
                    if (!success.success && success.error) {
                        this.errorMessage = success.error;
                    } else {
                        this.errorMessage = ERROR_GETTING_PLANS;
                    }
                } else {
                    this.errorMessage = '';
                }

            }, error => {
                if (error.withStatus200) {
                    this.errorMessage = error.message;
                } else {
                    this.errorMessage = ERROR_GETTING_PLANS;
                }
                this.dismissProgress();
            });
    }

    changePlan(subscriber: SubscriberInfo, plan: Offer) {
        this.cacheStorage().selectedSubscriber = subscriber;
        this.cacheStorage().selectedPlan = plan;
        this.goPage('module/change-plan/confirm');
    }
}














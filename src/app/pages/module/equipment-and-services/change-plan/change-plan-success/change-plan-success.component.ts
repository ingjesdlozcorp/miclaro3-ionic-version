import { Component, OnInit } from '@angular/core';
import { Utils } from '../../../../../utils/utils';
import { SubscriberInfo } from '../../../../../models/account.details';
import { Offer } from '../change-plan.component';
import { BasePage } from '../../../../base.page';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../../../services/utils.service';
import { IntentProvider } from '../../../../../services/intent.provider';

@Component({
    selector: 'app-module-change-plan-success',
    templateUrl: './change-plan-success.component.html',
    styleUrls: ['./change-plan-success.component.scss'],
})
export class ChangePlanSuccessComponent extends BasePage implements OnInit {

    formatSubscriber = Utils.formatSubscriber;
    typeOfTelephony = Utils.typeOfTelephony;
    subscriber: SubscriberInfo;
    selectedPlan: Offer;
    orderId = '';
    orderMessage = '';

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        const data = this.cacheStorage();
        this.orderId = data.orderInfo.id;
        this.orderMessage = data.orderInfo.message;
        this.subscriber = data.selectedSubscriber;
        this.selectedPlan = data.selectedPlan;
        if (this.orderId !== 'error') {
            this.reloadAccount();
        }
    }

    private reloadAccount() {
        this.showProgress();
        this.reloadCurrentAccount().finally(() => {
            this.dismissProgress();
        });
    }
}

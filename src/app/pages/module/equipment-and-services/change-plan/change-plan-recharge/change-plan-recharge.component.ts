import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import { BasePage } from '../../../../base.page';
import { Utils } from '../../../../../utils/utils';
import { Offer } from '../change-plan.component';
import { AccountInfo, SubscriberInfo } from '../../../../../models/account.details';
import { APP } from '../../../../../utils/const/appConstants';
import { BrowserProvider } from '../../../../../services/browser.provider';
import { keys } from '../../../../../utils/const/keys';
import { PAYMENT_VERSION } from '../../../../_shared/components/flash-payment/core/flash-payment-constants';
import { PaymentSupportService } from '../../../../_shared/components/flash-payment/core/services/payment-support.service';

@Component({
    selector: 'app-module-change-plan-recharge',
    templateUrl: './change-plan-recharge.component.html',
    styleUrls: ['./change-plan-recharge.component.scss'],
})
export class ChangePlanRechargeComponent extends BasePage implements OnInit {

    selectedPlan: Offer;
    subscriber: SubscriberInfo;

    productId = APP.PRODUCT_ID_RECHARGE_PREPAID;
    totalPrice: number;
    ivuRecharge: number;
    amounts = {price: '0', ivu: '0', totalPrice: '0', totalAmount: 0};
    selectAmount = true;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private browserProvider: BrowserProvider, private paymentSupport: PaymentSupportService) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.selectedPlan = this.cacheStorage().selectedPlan;
        this.subscriber = this.cacheStorage().selectedSubscriber;
        this.getPrepaidProducts();
    }

    getPrepaidProducts() {
        this.showProgress();
        const data = this.cacheStorage();
        this.services.listProductService(this.subscriber.subscriberNumberField, 2, data.tokenSession)
            .then(
                (response: any) => {
                    this.dismissProgress();
                    this.ivuRecharge = response.formProducts[response.formProducts.length - 1].ivuState;
                },
                error => {
                    this.dismissProgress();
                    this.showError(error.message, () => {
                        this.goHomePage();
                    });
                }
            );
    }


    getCurrentAmount(): number {
        const data = this.cacheStorage();
        return parseFloat(data.getFullCurrentSelectedSubscriber().prepaidBalanceField);
    }

    calculateAmountToPay(): string {
        const planPrice = Number(this.selectedPlan.price);
        const currentAmount = this.getCurrentAmount();
        let price = 0;
        if (currentAmount && currentAmount > 0) {
            price = currentAmount > planPrice ? currentAmount - planPrice : planPrice - currentAmount;
        } else {
            price = planPrice;
        }
        const ivu = price * Number(this.ivuRecharge);
        const totalPrice = price + ivu;
        this.amounts.totalAmount = totalPrice;
        this.amounts.price = Utils.formatAmount(price);
        this.amounts.ivu = Utils.formatAmount(ivu);
        this.amounts.totalPrice = Utils.formatAmount(totalPrice);
        return this.amounts.price;
    }

    async recharge() {
        const userPaymentVersion = await this.fetch(keys.PAYMENT.PAYMENT_VERSION);
        if (userPaymentVersion && userPaymentVersion === PAYMENT_VERSION.P2P) {
            let typeMessage = 'payment';
            if (this.cacheStorage().isPrepaidAccount) {
                typeMessage = 'prepaid';
            }
            this.paymentSupport.paymentRefactorProcess(typeMessage, this.amounts.totalAmount);
        } else {
            const accountInfo: AccountInfo = this.cacheStorage().accountInfo;
            const response = await this.paymentSupport.paymentExternalProcess(
                `${accountInfo.bANField}`,
                this.subscriber.subscriberNumberField,
                `${this.amounts.totalAmount}`,
                {productId: this.productId}
            );
            this.verifyPayment(response);
        }
    }

    verifyPayment(response) {
        this.dismissProgress();
        if (response && !response.hasError) {
            if (response.processEndState) {
                if (response.provisioningStatus === APP.PROVISIONING_STATUS.PROVISIONED) {
                    this.cacheStorage().PaymentProcess = {
                        subscriber: this.subscriber.subscriberNumberField,
                        totalAmount: String(this.totalPrice),
                        paymentToken: response.paymentToken,
                    };
                    this.cacheStorage().PaymentProcess.transactionResult = {
                        status: response.paymentStatus,
                        rechargeId: response.confirmationNumber,
                        transactionId: response.authorizationNumber,
                        date: new Date(new Date().getTime())
                    };
                    this.cacheStorage().selectedPlan = this.selectedPlan;
                    this.evaluatePlan();
                } else {
                    this.dismissProgress();
                    this.showAlert('Su pago no pudo ser procesado.');
                }
            } else {
                this.showError('Estimado cliente ha surgido un error en el procesamiento de su pago, por favor intente con otro método de pago.');
            }
        } else {
            this.showError(response.message);
        }
    }

    private evaluatePlan() {
        this.showProgress();
        const data = this.cacheStorage();
        this.services.getEvaluatePlanPrepaid(
            data.tokenSession,
            this.subscriber.subscriberNumberField,
            this.selectedPlan.soc)
            .then(success => {
                this.dismissProgress();
                this.goPage('module/change-plan/success');
            }, error => {
                this.dismissProgress();
                data.orderInfo = {id: 'error', message: 'conRecarga'};
                this.returnPayment(data.PaymentProcess.paymentToken, 'module/change-plan/success');
            });
    }

    returnPayment(paymentToken, redirectTo?) {
        this.showProgress();
        this.services.returnPayment(paymentToken).finally(() => {
            this.reloadCurrentAccount().finally(() => {
                this.dismissProgress();
                this.goPage(redirectTo);
            });
        });
    }

}

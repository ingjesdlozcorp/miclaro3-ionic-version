import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { BasePage } from 'src/app/pages/base.page';
import { Storage } from '@ionic/storage';
import { keys } from 'src/app/utils/const/keys';
import { BrowserProvider } from '../../../../services/browser.provider';
import { Platform } from '@ionic/angular';

@Component({
    selector: 'app-module-netflix',
    templateUrl: './netflix.component.html',
    styleUrls: ['./netflix.component.scss'],
})
export class NetflixComponent  extends BasePage implements OnInit {
    public accounts;
    public selectedTab;
    public selectedAccount;
    public accountSections;
    public showBackBth = true;
    public data;
    public accountInfo;
    public banner = 'assets/images/banner_netflix_app.jpg';

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider,
                public browserProvider: BrowserProvider, platform: Platform) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('netflix');
        this.data = this.cacheStorage();
        this.accounts = this.data.accountList;
        this.accountInfo = this.data.accountInfo;
        this.selectedAccount = this.cacheStorage().loginData.account;

        if (platform.width() > 500) {
            this.banner = 'assets/images/banner_netflix_web.jpg';
        }
    }

    ngOnInit() { }

    subscription() {
        this.store(keys.NETFLIX.SUBSCRIBER_LOADED, false);
        this.goPage('module/netflix/subscription' );
    }

    showTerms() {
        this.goPage('module/netflix/terms' );
    }

    faq() {
        this.goPage('module/netflix/faq' );
    }

    support() {
        this.goPage('module/netflix/support' );
    }

    recoverEmail() {
        this.store(keys.NETFLIX.SUBSCRIBER_LOADED, false);
        this.goPage('module/netflix/subscription' );
    }

    recoverPassword() {
        const url = 'https://www.netflix.com/co/LoginHelp';
        this.browserProvider.openExternalBrowser(url);
    }

}

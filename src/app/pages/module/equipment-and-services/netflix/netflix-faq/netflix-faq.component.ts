import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-module-netflix-faq',
  templateUrl: './netflix-faq.component.html',
  styleUrls: ['./netflix-faq.component.scss'],
})
export class NetflixFaqComponent implements OnInit {
  public open = true;
    public touched = false;
    public contentShow = [
        false,  false,  false, false,
        false, false, false, false,
        false, false, false
    ];
    public theOpenGroup = [];
  constructor() { }

  ngOnInit() {}
  searchDelete( param: number ) {
    for ( let i = 0; i < this.theOpenGroup.length; i++ ) {
        if ( param === this.theOpenGroup[i]) {
            this.theOpenGroup.splice(i, 1);
        }
    }
}
showingMenu(param: number) {
    let close = false;

    if (this.theOpenGroup.length > 0) {
        for (const group of this.theOpenGroup) {
            if (param === group) {
                close = true;
            }
        }
    }
    // para mostrar el contenido:
    console.log('quieres mostrar contenido');
    if ( close === false) {
        for ( let i = 0; i < this.contentShow.length; i++ ) {
            if ( param === i) {
                this.contentShow[i] = true;
                this.theOpenGroup.push(param);
                console.log(this.contentShow[i]);
            }
        }

    } else {
        // para cerrar el contenido;
        console.log('quieres ocultar contenido');
        this.searchDelete(param);
        for ( let i = 0; i < this.contentShow.length; i++ ) {
            if ( param === i) {
                this.contentShow[i] = false;
                console.log(this.contentShow[i]);
            }
        }
    }
    console.log(this.theOpenGroup);
}

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { BasePage } from 'src/app/pages/base.page';
import { Storage } from '@ionic/storage';
import { keys } from 'src/app/utils/const/keys';
@Component({
  selector: 'app-module-netflix-support',
  templateUrl: './netflix-support.component.html',
  styleUrls: ['./netflix-support.component.scss'],
})
export class NetflixSupportComponent  extends BasePage implements OnInit {

  public accounts;
  public selectedTab;
  public selectedAccount;
  public accountSections;
  public showBackBth = true;
  public data;
  public accountInfo;

  constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
              alertController: AlertController, utilsService: UtilsService,
              userStorage: IntentProvider) {
  super(router, storage, modelsServices, alertController, utilsService, userStorage);
  console.log(this.cacheStorage().loginData);
  this.data = this.cacheStorage();
  this.accounts = this.data.accountList;
  this.accountInfo = this.data.accountInfo;
  this.selectedAccount = this.cacheStorage().loginData.account;
  }

  ngOnInit() {}
  netflixSupport() {
    const url = 'https://help.netflix.com/es';
    this.openExternalBrowser(url);
  }
  support() {
    this.goPage('module/support');
  }
}

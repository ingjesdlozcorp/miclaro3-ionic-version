import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { BasePage } from 'src/app/pages/base.page';
import { Storage } from '@ionic/storage';
import { keys } from 'src/app/utils/const/keys';

@Component({
    selector: 'app-module-netflix-request',
    templateUrl: './netflix-request.component.html',
    styleUrls: ['./netflix-request.component.scss'],
})
export class NetflixRequestComponent extends BasePage implements OnInit {

    isTelephony = true;
    subscriberHidden = '';
    subscriber = '';
    emailHidden = '';
    email = '';
    bySubscriber = false;
    byEmail = false;

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.fetch(keys.RECOVER_PASSWORD.IS_TELEPHONY).then(
            success => {
                this.isTelephony = success;
            }
        );
        this.fetch(keys.RECOVER_PASSWORD.SUBSCRIBER).then(
            success => {
                this.subscriber = success;
                if (this.subscriber) {
                    this.subscriberHidden = success.substr(success.length - 4);
                    this.subscriberHidden = '********' + this.subscriberHidden;
                }
            }
        );
        this.fetch(keys.RECOVER_PASSWORD.EMAIL).then(
            success => {
                this.email = success;
                if (this.email) {
                    const first2 = success.substring(0, 2);
                    const emailCut = success.split('@');
                    this.emailHidden = first2 + '******@' + emailCut[1];
                }
            }
        );
    }

    nextStep() {
        if (!this.bySubscriber && !this.byEmail) {
            return;
        } else {
            this.showProgress();
            this.services.sendNetflixValidationCode(
                this.bySubscriber ? this.subscriber : '',
                this.byEmail ? this.email : '',
                this.cacheStorage().tokenSession
            ).then(
                (success: any) => {
                    this.showAlert(success.errorDisplay, () => {
                        this.goPage('module/netflix/redirect');
                    });
                    this.dismissProgress();
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }
}

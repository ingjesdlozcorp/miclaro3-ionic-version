import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { BasePage } from 'src/app/pages/base.page';
import { Storage } from '@ionic/storage';
import { keys } from 'src/app/utils/const/keys';

@Component({
    selector: 'app-module-netflix-redirect',
    templateUrl: './netflix-redirect.component.html',
    styleUrls: ['./netflix-redirect.component.scss'],
})
export class NetflixRedirectComponent extends BasePage implements OnInit {

    accept = false;
    account;
    subscriber;
    code: string;
    ip: string;

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.showProgress();
        this.services.getMyIp()
            .then(response => {
                this.ip = response;
                this.getDataNetflix();
            }).catch(error => {
                this.showAlert(error, () => {
                    this.goHomePage();
                });
            });
    }

    terms() {
        this.goPage('module/netflix/terms');
    }

    continue() {
        if (!this.accept) {
            this.showError( 'Debe seleccionar los términos y condiciones para poder continuar.');
            return;
        } else if (this.code.length !== 6) {
            this.showError('Debe colocar un código valido.');
            return;
        }
        this.showProgress();
        this.services.validateNetflixCode(this.code, this.cacheStorage().tokenSession).then(
            (response) => {
                this.redirect();
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    private redirect() {
        this.services.subscribeNetflix(String(this.account), String(this.subscriber), this.ip, this.cacheStorage().tokenSession).then(
            (response) => {
                this.dismissProgress();
                const url = response.urlField;
                this.openExternalBrowser(url);
                this.goHomePage();
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    getDataNetflix() {
        this.showProgress();
        Promise.all([
            this.fetch(keys.NETFLIX.ACCOUNT).then(
                result => {
                    this.account = result;
                }
            ),
            this.fetch(keys.NETFLIX.SUBSCRIBER_NUMBER).then(
                result => {
                    this.subscriber = result;
                }
            )
        ]).then(
            () => {
                this.dismissProgress();
            }
        );
    }
}

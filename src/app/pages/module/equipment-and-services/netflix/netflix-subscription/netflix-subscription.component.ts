import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { BasePage } from 'src/app/pages/base.page';
import { Storage } from '@ionic/storage';
import { keys } from 'src/app/utils/const/keys';
import { Utils } from '../../../../../utils/utils';

@Component({
    selector: 'app-module-netflix-subscription',
    templateUrl: './netflix-subscription.component.html',
    styleUrls: ['./netflix-subscription.component.scss'],
})
export class NetflixSubscriptionComponent extends BasePage implements OnInit {

    public accounts = [];
    public accountInfo;
    public selectedAccount;
    public selectedSubscriber;

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.accountInfo = this.cacheStorage().accountInfo;
        this.getAccounts();
    }

    getAccounts() {
        this.showProgress();
        this.services.getAccounts(this.cacheStorage().tokenSession).then(
            (response: any) => {
                this.dismissProgress();
                this.checkAccount(response.accounts);
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    checkAccount(accountsSubscribers) {
        if (accountsSubscribers === undefined || accountsSubscribers === null || accountsSubscribers.length === 0) {
            const message = 'En este momento estamos presentado inconvenientes o nuestro sistema están en ' +
                'mantenimiento. Por favor intente nuevamente en unos minutos.';
            this.dismissProgress();
            this.showError( message,
                function() {
                    this.goHomePage();
                });
            return;

        } else if (accountsSubscribers.length === 1
            && accountsSubscribers[0].subscribers.length === 1
            && accountsSubscribers[0].subscribers[0].netFlixBlackListed === 'true') {
            const message = '¡Gracias por su interés en subscribirse a Netflix, al momento su cuenta no cumple ' +
                'con los requisitos para completar su subscripción.  Para información adicional comuníquese con ' +
                'unos de nuestros Coordinadores de Servicio al Cliente a través de nuestro CHAT.';
            this.dismissProgress();
            this.showConfirm('', message,
                () => {
                    this.goHomePage();
                },
                () => {
                    this.goHomePage();
                }
            );

            return;
        } else if (accountsSubscribers.length === 1
            && accountsSubscribers[0].subscribers.length === 1
            && accountsSubscribers[0].subscribers[0].netFlixBlackListed !== 'true'
            && accountsSubscribers[0].subscribers[0].netFlixBlackListed !== 'false') {
            const message = 'En este momento estamos presentado inconvenientes o nuestro sistema están en ' +
                'mantenimiento. Por favor intente nuevamente en unos minutos.';
            this.dismissProgress();
            this.showError( message,
                () => {
                    this.goHomePage();
                });
            return;
        }

        const accountsN = [];
        const toShow1 = {
            index: 0,
            account_class: '',
            accountRadioChecked: false
        };
        accountsSubscribers.forEach(account => {
            const accountN = JSON.parse(JSON.stringify(account));
            toShow1.index = toShow1.index + 1;
            accountN.subscribers = [];
            const toShow = {
                index: 0,
                subscriber_class: '',
                subRadioCheck: false,
                collapse: true
            };
            account.subscribers.forEach(subscriber => {
                if (subscriber.netFlixBlackListed === 'false') {
                    const a =  Object.defineProperty(subscriber, 'htmlSubscriber', {
                        value: toShow,
                        configurable: true
                    });
                    accountN.subscribers.push(a);
                }
            });
            if (accountN.subscribers.length > 0) {
                toShow1.index = toShow.index + 1;
                const b =  Object.defineProperty(accountN, 'htmlAccount', {
                    value: toShow1,
                    configurable: true
                });
                accountsN.push(b);
            }
        });

        this.accounts = accountsN;
        if (accountsN.length === 0) {
            const message = '¡Gracias por su interés en subscribirse a Netflix, al momento su cuenta no cumple ' +
                'con los requisitos para completar su subscripción.  Para información adicional comuníquese con ' +
                'unos de nuestros Coordinadores de Servicio al Cliente a través de nuestro CHAT.';

            this.showConfirmCustom('', message, 'Chat', 'Salir',
                () => {
                    this.goPage('help/chat');
                },
                () => {
                    this.goHomePage();
                }
            );
        }
    }

    changeAccount(account) {
        this.selectedAccount = account;
        this.selectedSubscriber = undefined;
    }

    changeSubscriber(account, subscriber) {
        this.selectedAccount = account;
        this.selectedSubscriber = subscriber;
    }

    continue() {
        if (this.selectedAccount === undefined || this.selectedSubscriber === undefined) {
            this.showError( 'Debe seleccionar un número de cuenta y suscriptor para poder agregar el Servicio Netflix.');
        } else {
            this.store(keys.NETFLIX.ACCOUNT, this.selectedAccount.account);
            this.store(keys.NETFLIX.SUBSCRIBER_NUMBER, this.selectedSubscriber.subscriber);

            this.showProgress();
            this.services.getNetflixCodeOptions(this.selectedSubscriber.subscriber, this.cacheStorage().tokenSession).then(
                (response: any) => {
                    this.onGetValidationCodeOptions(response);
                    this.dismissProgress();
                },
                error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }

    private onGetValidationCodeOptions(response) {
        this.store(keys.RECOVER_PASSWORD.SUBSCRIBER, response.subscriber);
        this.store(keys.RECOVER_PASSWORD.EMAIL, response.email);

        this.store(keys.RECOVER_PASSWORD.IS_TELEPHONY, Utils.isTelephony(response.accountType, response.accountSubType, response.productType))
            .then(() => this.goPage('module/netflix/request'));
    }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../services/services.provider';
import { AlertController, IonContent } from '@ionic/angular';
import { UtilsService } from '../../../../services/utils.service';
import { IntentProvider } from '../../../../services/intent.provider';
import { BasePage } from '../../../base.page';
import { Utils } from '../../../../utils/utils';
import { AccountInfo, SubscriberInfo } from '../../../../models/account.details';
import { APP } from '../../../../utils/const/appConstants';
import { BrowserProvider } from '../../../../services/browser.provider';
import { keys } from '../../../../utils/const/keys';
import { PAYMENT_VERSION } from '../../../_shared/components/flash-payment/core/flash-payment-constants';
import { PaymentSupportService } from '../../../_shared/components/flash-payment/core/services/payment-support.service';

const PAY_TOTAL = 'P';
const PAY_QUOTES = 'U';

@Component({
    selector: 'app-module-device',
    templateUrl: './device.component.html',
    styleUrls: ['./device.component.scss'],
})
export class DeviceComponent extends BasePage implements OnInit {

    @ViewChild('content', {static: false}) private content: IonContent;

    formatSubscriber = Utils.formatSubscriber;
    formatAmount = Utils.formatAmount;
    typeOfTelephony = Utils.typeOfTelephony;
    subscribers: SubscriberInfo[] = [];
    selectedSubscriber: SubscriberInfo = undefined;
    dsl = false;
    tab = 1;
    public isGuest = false;

    payTotal = false;
    payQuote = false;
    quotes = 0;

    amountToPay: string = null;

    productId: string = APP.PRODUCT_ID_INSTALLMENTS;
    merchantId = '';

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private browserProvider: BrowserProvider,
                private paymentSupport: PaymentSupportService) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('device_and_services');
    }

    ngOnInit() {
        this.isGuest = this.cacheStorage().isGuest;
        const accountInfo = this.cacheStorage().accountInfo;
        this.dsl = Utils.isDLS(accountInfo.accountSubtypeField, accountInfo.accountTypeField);

        this.subscribers = this.cacheStorage().subscriberList;
        const substituteArrangement = [];
        for (const subscriber of this.subscribers) {
            const data = {
                onSig: false,
                sigMessage: '',
                sigReclamationDate: null
            };
            const completeSubscriber = Object.defineProperty(subscriber, 'SIG', {
                value: data,
                configurable: true
            });
            substituteArrangement.push(completeSubscriber);
        }
        this.subscribers = substituteArrangement;
        if (this.subscribers.length === 1) {
            this.showDetails(this.subscribers[0]);
            this.getSIG(this.subscribers[0]);
        }
        this.tab = 1;
    }

    showDetails(subscriber: SubscriberInfo) {
        if (this.selectedSubscriber === subscriber) {
            this.selectedSubscriber = undefined;
        } else {
            this.selectedSubscriber = subscriber;
            this.getSIG(this.selectedSubscriber);
        }
    }

    getPlanName(subscriber: SubscriberInfo): string {
        const plan = subscriber.planInfoField;
        return plan.sOCDescriptionField;
    }

    getPlanDescriptionList(subscriber: SubscriberInfo): string[] {
        const plan = subscriber.planInfoField;
        const planFullName = plan.sOCDescriptionField;
        if (planFullName != null) {
            let subPlans = [planFullName];
            if (planFullName.split(':').length > 1) {
                subPlans = planFullName.split(':')[1].split('-');
            }
            return subPlans;
        }
        return [];
    }

    getPlanAmount(subscriber: SubscriberInfo): string {
        const plan = subscriber.planInfoField;
        let planAmount = 0;
        if (plan.sOCDescriptionField != null) {
            planAmount = plan.socRateField;
        }
        return Utils.formatAmount(planAmount);
    }

    getTotalRent(subscriber: SubscriberInfo): string {
        const plan = subscriber.planInfoField;
        let planRent = 0;
        if (plan.sOCDescriptionField != null) {
            planRent = plan.totalRateField;
        }
        return Utils.formatAmount(planRent);
    }

    getImei(subscriber: SubscriberInfo): string {
        const equipmentInfo = subscriber.equipmentInfoField;
        const id = equipmentInfo.itemIdField;
        let imei = 'No disponible';
        subscriber.servEquipmentSerialsField.servEquipmentSerialField.forEach(
            serial => {
                if (serial.itemIdField === id) {
                    imei = serial.eSNField;
                }
            });
        return imei;
    }

    getBrand(subscriber: SubscriberInfo): string {
        const equipmentInfo = subscriber.equipmentInfoField;
        return equipmentInfo.itemBrandField === 'not_set' ? 'No disponible' : equipmentInfo.itemBrandField;
    }

    showBrand(subscriber: SubscriberInfo): boolean {
        const equipmentInfo = subscriber.equipmentInfoField;
        return equipmentInfo.itemBrandField !== 'not_set';
    }

    getDeviceImage(subscriber: SubscriberInfo): string {
        const equipmentInfo = subscriber.equipmentInfoField;
        return equipmentInfo.itemImageField;
    }

    getTotalAmountQuotes(subscriber: SubscriberInfo): string {
        const singleQuote = subscriber.equipmentInfoField.installmentValueField;
        const quotes = subscriber.equipmentInfoField.installmentsField;
        return (Number(singleQuote) * Number(quotes)).toFixed(2).toString();
    }

    changePlan(subscriber: SubscriberInfo) {
        this.goPage('module/change-plan');
    }

    getSIG(subscriber) {
        this.showProgress();
        this.services.checkWarrantyStatus(subscriber.subscriberNumberField, this.cacheStorage().tokenSession).then(
            success => {
                this.dismissProgress();
                if (success.statusDescription !== undefined && success.statusDescription !== null) {
                    if (success.statusDescription.length > 0) {
                        console.log(Utils.formatStringDateForSIG(success.warrantyReclamationDate));
                        subscriber.SIG.onSig = true;
                        subscriber.SIG.sigMessage = success.statusDescription;
                        subscriber.SIG.sigReclamationDate = 'Desde: ' + Utils.formatStringDateForSIG(success.warrantyReclamationDate);
                    }
                }
            },
            () => {
                this.dismissProgress();
            }
        );
    }

    onPayTotal(subscriber: SubscriberInfo) {
        this.payTotal = true;
        this.payQuote = false;
        this.amountToPay = null;
        this.getInstallments(subscriber, PAY_TOTAL, 1);
    }

    onPayQuote(subscriber: SubscriberInfo) {
        this.payQuote = true;
        this.payTotal = false;
        this.amountToPay = '0.00';
        this.quotes = 0;
        this.content.scrollToBottom(1000);
    }

    decreaseQuotes(subscriber: SubscriberInfo) {
        if (this.quotes > 0) {
            this.quotes--;
            if (this.quotes === 0) {
                this.onPayQuote(subscriber);
            } else {
                this.getInstallments(subscriber, PAY_QUOTES, this.quotes);
            }
        }
    }

    increaseQuotes(subscriber: SubscriberInfo) {
        if (this.quotes < Number(subscriber.equipmentInfoField.installmentsField)) {
            this.quotes++;
            this.getInstallments(subscriber, PAY_QUOTES, this.quotes);
        }
    }

    getInstallments(subscriber: SubscriberInfo, query, value) {
        this.showProgress();
        const accountInfo = this.cacheStorage().accountInfo;
        this.services.getInstallments(
            String(accountInfo.bANField),
            subscriber.subscriberNumberField,
            subscriber.productTypeField,
            query,
            value,
            this.cacheStorage().tokenSession)
            .then(
                success => {
                    this.dismissProgress();
                    this.amountToPay = Number(success.AmountToAccelerate).toFixed(2);
                    this.content.scrollToBottom(1000);
                    this.merchantId = success.merchantCodeId;
                },
                () => {
                    this.dismissProgress();
                }
            );
    }

    isAvailableToPay(): boolean {
        return Number(this.amountToPay) > 0;
    }

    async pay() {
        const userPaymentVersion = await this.fetch(keys.PAYMENT.PAYMENT_VERSION);
        if (userPaymentVersion && userPaymentVersion === PAYMENT_VERSION.P2P) {
            this.paymentSupport.paymentRefactorProcess(
                'installment', +this.amountToPay,
                this.selectedSubscriber.equipmentInfoField.mInvoiceStoreIdField,
                this.selectedSubscriber.equipmentInfoField.mInvoiceOIdField,
                this.quotes
            );
        } else {
            const accountInfo: AccountInfo = this.cacheStorage().accountInfo;
            const response = await this.paymentSupport.paymentExternalProcess(
                `${accountInfo.bANField}`,
                this.selectedSubscriber.subscriberNumberField,
                this.amountToPay,
                {
                    productId: this.productId,
                    locationId: this.selectedSubscriber.equipmentInfoField.mInvoiceStoreIdField,
                    invoiceNumber: this.selectedSubscriber.equipmentInfoField.mInvoiceOIdField
                }
            );
            this.verifyPayment(response);
        }
    }

    verifyPayment(response: any) {
        this.dismissProgress();
        if (response && !response.hasError) {
            if (response.processEndState) {
                this.showProgress('Actualizando su cuenta...');
                this.reloadCurrentAccount().then(() => {
                    this.dismissProgress();
                    this.goHomePage();
                });
            } else {
                this.dismissProgress();
                this.showAlert('Su pago aun no ha sido procesado, si ya realizo el pago espere mientras lo procesamos.',
                    () => {
                        this.goHomePage();
                    });
            }
        } else {
            this.showError(response.message);
        }
    }
}























import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import {BasePage} from '../../../base.page';

@Component({
    selector: 'app-module-fault-step3',
    templateUrl: './fault.step3.component.html',
    styleUrls: ['./fault.step3.component.scss'],
})
export class FaultStep3Component extends BasePage implements OnInit {

    public reportNumber;
    public selectedSubscriber;
    public typeOfBreakdownText;
    public typeReportSelected = '';

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.selectedSubscriber = this.services.temporaryStorage.selectedSubscriber;
        this.reportNumber = this.services.temporaryStorage.ticket;
    }

    ngOnInit() {
        this.typeReportSelected = this.getReportText()
    }

    home() {
        this.goHomePage();
    }

    getReportText() {
        const faultSelectedLine = this.services.temporaryStorage.faultSelectedLine;
        const faultSelectedType = this.services.temporaryStorage.faultSelectedType;
        if (faultSelectedType === 1 && faultSelectedLine === 1) {
            return 'Línea + DS';
        } else if (faultSelectedType === 1 && faultSelectedLine === 2) {
            return 'Solo Línea';
        } else if (faultSelectedType === 2 && faultSelectedLine === 1) {
            return 'Sin internet y sin voz';
        } else if (faultSelectedType === 2 && faultSelectedLine === 2) {
            return 'Sin internet y con voz';
        } else {
            return '';
        }
    }

}

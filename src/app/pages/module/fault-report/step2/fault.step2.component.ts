import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import {BasePage} from '../../../base.page';

@Component({
    selector: 'app-module-fault-step2',
    templateUrl: './fault.step2.component.html',
    styleUrls: ['./fault.step2.component.scss'],
})
export class FaultStep2Component extends BasePage implements OnInit {

    public selectedSubscriber;
    public typeOfBreakdown = 1;
    public description = '';
    public number1 = '';
    public number2 = '';
    public typeReportSelected = '';
    public faultSelectedType;

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.selectedSubscriber = this.services.temporaryStorage.selectedSubscriber;
    }
    ngOnInit() {
        this.faultSelectedType = this.services.temporaryStorage.faultSelectedType;
        this.typeReportSelected = this.services.temporaryStorage.faultSelectedType === 2 ? 'Línea + DSL' : 'Solo Línea';
    }

    next() {
        if (this.number1.length === 0 || this.number2.length === 0) {
            const message = 'Debes ingresar los numeros de contacto.';
            this.showError(message);
            return;
        }
        if (this.number1.length !== 10 || this.number2.length !== 10) {
            const message = 'El formato de los numeros de contacto es incorrecto.';
            this.showError(message);
            return;
        }
        if (this.description.length === 0) {
            const message = 'Debes ingresar una breve explicacíon.';
            this.showError(message);
            return;
        }
        this.services.temporaryStorage.faultSelectedLine = this.typeOfBreakdown;
        this.services.temporaryStorage.contact1 = this.number1;
        this.services.temporaryStorage.contact2 = this.number2;
        this.services.temporaryStorage.description = this.description;
        const data = {
            subscriberNumber: this.selectedSubscriber,
            ban: String(this.cacheStorage().accountInfo.bANField),
            contactNumber1: this.number1,
            contactNumber2: this.number2,
            outageType: this.getOutageType(),
            operatorID: this.services.temporaryStorage.faultSelectedLine, // ????
            customerReport: this.cacheStorage().accountInfo.firstNameField + ' ' +  this.cacheStorage().accountInfo.lastNameField,
            remark: this.description,
            productType: this.services.temporaryStorage.productTypeField,
        };

        this.showProgress();

        this.services.setRaiseIssueTicket(data).then(
            (success: any) => {
                this.dismissProgress();
                this.services.temporaryStorage.ticket = success.result[0].tt_id;
                this.goPage('module/fault/step3');
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    validateInput(param: number) {
        if (param === 1) {
            if (this.number1.length === 0) {
                this.showError('Debes ingresar al menos 1 número de contacto.');
            } else if (this.number1.length > 10) {
                this.showError('Debe insertar un número telefonico válido');
                this.number1 = '';
            } else if (this.hasNumericCharacter(this.number1).hasSpecial) {
                this.showError('No se adminten letras ni carácteres especiales en este campo');
                this.number1 = '';
            }
        } else {
            if (this.number2.length > 10) {
                this.showError('El segundo número telefonico no es válido');
                this.number2 = '';
            } else if (this.hasNumericCharacter(this.number2).hasSpecial) {
                this.showError('No se adminten letras ni carácteres especiales en este campo');
                this.number2 = '';
            }
        }
    }

    hasNumericCharacter(numberS) {
        const text = numberS;
        const numbers = '1234567890';
        const special = {
            specialIndex: 0,
            hasSpecial: false,
            validText: ''
        };

        for (let i = 0; i < text.length; i++) {
            if (numbers.indexOf(text.charAt(i), 0) === -1) {
                special.specialIndex = i;
                special.hasSpecial = true;
                if (special.specialIndex > 0) {
                    special.validText = text.substring(0, special.specialIndex);
                    if (special.validText.length === 1) { special.validText = ''; }
                    break;
                }
            }
        }
        return special;
    }

    back() {
        this.goBack();
    }

    getOutageType() {
        if (this.faultSelectedType === 1 && this.typeOfBreakdown === 1) {
            return 'LINE|TONELESS';
        } else if (this.faultSelectedType === 1 && this.typeOfBreakdown === 2) {
            return 'NOISE|LINE';
        } else if (this.faultSelectedType === 2 && this.typeOfBreakdown === 1) {
            return 'LINE|DSL|NO_INTERNET_NO_VOICE';
        } else if (this.faultSelectedType === 2 && this.typeOfBreakdown === 2) {
            return 'LINE|DSL|NO_INTERNET_WITH_VOICE';
        }
    }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { BasePage } from '../../../base.page';
import { Utils } from 'src/app/utils/utils';
import { SubscriberInfo } from '../../../../models/account.details';
import * as moment from 'moment';


@Component({
    selector: 'app-module-fault-step1',
    templateUrl: './fault.step1.component.html',
    styleUrls: ['./fault.step1.component.scss'],
})
export class FaultStep1Component extends BasePage implements OnInit {

    public selectedAccount;
    public selectedSubscriber;
    public accounts = [];
    public subscribers = [];
    public faultSelectedType = 1;
    available = false;
    containsReport = false;

    reportNumber = '';
    reportDate = '';

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.selectedAccount = this.cacheStorage().accountInfo.bANField;
        this.accounts = this.cacheStorage().accountList;
        this.utils.registerScreen('fault_report');
    }

    ngOnInit() {
        const subscriberList: SubscriberInfo[] = this.services.data.SubscriberInfo;
        for (const subscriber of subscriberList) {
            if (Utils.typeOfTelephony(subscriber.productTypeField) === 'Fijo' && subscriber.subscriberNumberField.startsWith('787')) {
                this.subscribers.push(subscriber);
            }
        }
        if (this.subscribers.length === 1) {
            this.selectedSubscriber = this.subscribers[0].subscriberNumberField;
            this.changeSubscriber(this.subscribers[0].subscriberNumberField);
        }
    }

    changeSubscriber(selectedSubscriber) {
        this.available = false;
        this.containsReport = false;
        if (this.selectedSubscriber) {
            this.showProgress();
            this.services.checkIfTicketExists(selectedSubscriber).then(
                (success: any) => {
                    this.dismissProgress();
                    if (success && success.result && success.result.length > 0) {
                        const ticket = success.result[0];
                        this.available = ticket.openTicket === 'N';
                        this.containsReport = ticket.openTicket === 'Y';
                        this.reportNumber = ticket.q_id;
                        if (ticket.commitmentDate) {
                            const dateMoment = moment(ticket.commitmentDate.substring(6, 9) + '/' + ticket.commitmentDate.substring(4, 6) + '/' + ticket.commitmentDate.substring(0, 4), 'DD/MM/YYYY');
                            this.reportDate = dateMoment.subtract(Number(ticket.openDays), 'days').format('MM/DD/YYYY');
                        }
                    } else {
                        this.available = true;
                        this.containsReport = false;
                    }
                },
                error => {
                    this.dismissProgress();
                    this.showError(UtilsService.errorGeneral, () => {
                        this.selectedSubscriber = undefined;
                    });
                }
            );
        }
    }

    next() {
        if (this.selectedSubscriber === null || this.selectedSubscriber === undefined) {
            this.showError('Debe selecionar un numero telefónico');
            return;
        }

        let subscriberList = [];
        subscriberList = this.services.data.SubscriberInfo;
        for (const subscriber of subscriberList) {
            if (subscriber.subscriberNumberField === this.selectedSubscriber) {
                this.services.temporaryStorage.productTypeField = subscriber.productTypeField;
                break;
            }
        }
        this.services.temporaryStorage.selectedSubscriber = this.selectedSubscriber;
        this.services.temporaryStorage.faultSelectedType = this.faultSelectedType;
        this.goPage('module/fault/step2');
    }

    back() {
        this.goBack();
    }
}

import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-popup-refer-terms',
    templateUrl: './popup-refer-terms.component.html',
    styleUrls: ['./popup-refer-terms.component.scss'],
})
export class PopupReferTermsComponent {

    @Output() close = new EventEmitter();

    constructor() {
    }

    closeTerms() {
        this.close.emit();
    }
}


import { Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'app-module-refer-questions',
    templateUrl: './questions.component.html',
    styleUrls: ['./questions.component.scss'],
})
export class QuestionsComponent extends BasePage implements OnInit {

    public open = true;
    public touched = false;
    public contentShow = [
        false, false, false, false,
        false, false, false
    ];
    public theOpenGroup = [];

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
    }

    searchDelete(param: number) {
        for (let i = 0; i < this.theOpenGroup.length; i++) {
            if (param === this.theOpenGroup[i]) {
                this.theOpenGroup.splice(i, 1);
            }
        }
    }

    showingMenu(param: number) {
        let close = false;

        if (this.theOpenGroup.length > 0) {
            for (const item of this.theOpenGroup) {
                if (param === item) {
                    close = true;
                }
            }
        }
        // to show content
        if (!close) {
            for (let i = 0; i < this.contentShow.length; i++) {
                if (param === i) {
                    this.contentShow[i] = true;
                    this.theOpenGroup.push(param);
                    console.log(this.contentShow[i]);
                }
            }

        } else {
            // to close content
            this.searchDelete(param);
            for (let i = 0; i < this.contentShow.length; i++) {
                if (param === i) {
                    this.contentShow[i] = false;
                }
            }
        }
    }

    return() {
        this.goBack();
    }

}

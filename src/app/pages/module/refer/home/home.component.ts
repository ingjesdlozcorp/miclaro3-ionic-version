import { Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import { Utils } from 'src/app/utils/utils';
import { APP } from '../../../../utils/const/appConstants';
import { BrowserProvider } from '../../../../services/browser.provider';
import { keys } from '../../../../utils/const/keys';
import { PAYMENT_VERSION } from '../../../_shared/components/flash-payment/core/flash-payment-constants';
import { AccountInfo } from '../../../../models/account.details';
import { PaymentSupportService } from '../../../_shared/components/flash-payment/core/services/payment-support.service';


@Component({
    selector: 'app-module-refer-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent extends BasePage implements OnInit {
    public selectedAccount;
    public selectedSubscriber;
    public name;
    public accountSections;

    public data;
    public accounts = [];
    public subscribers = [];
    public popupController = {
        popupTerm: false,
        popupCant: false,
        popupSelect: false,
        popupUpdate: false,
        popupBalancePost: false,
        popupBalancePre: false,
    };
    public ReferrerData;
    public hobbiesList = [];
    public selectedHobby = '';
    public radioPaperless = 0;
    public referrerLoaded = false;
    public date;
    public email;
    public route = '';

    productId: string = APP.PRODUCT_ID_INVOICE_PAYMENTS;
    merchantId = '';

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private browserProvider: BrowserProvider,
                private paymentSupport: PaymentSupportService) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('referrals_system');
        this.data = this.cacheStorage();
        this.name = this.cacheStorage().loginData.username;
        this.selectedAccount = this.cacheStorage().accountInfo.bANField;
        this.date = this.cacheStorage().accountInfo.dOBField;
        this.email = this.cacheStorage().accountInfo.emailField;
    }

    ngOnInit() {
        this.initHome();
    }

    initHome() {
        const accountInfo = this.cacheStorage().accountInfo;
        if (accountInfo.banStatusField === 'S') {
            const amountDue = accountInfo.billBalanceField;
            let amountPayable = amountDue.includes('CR') ? '0' : amountDue;
            amountPayable = parseFloat(String(amountPayable
                .replace(',', '')))
                .toFixed(2);
            this.showConfirmCustom('Aviso',
                `Su cuenta ${String(accountInfo.bANField)} esta suspendida. Para activar la misma, favor realice su pago.`,
                'Pagar', 'Cancelar', () => {
                    this.payment(
                        String(accountInfo.bANField),
                        accountInfo.defaultSubscriberField,
                        amountPayable
                    );
                }, () => {
                    this.goHomePage();
                });
        }
    }

    async getAccountListRefer(): Promise<any> {
        return new Promise((resolve, reject) => {
            const accounts = this.cacheStorage().accountListRefer;
            if (accounts) {
                resolve(accounts);
            } else {
                this.services.getAccounts(this.cacheStorage().tokenSession)
                    .then((response) => {
                        this.cacheStorage().accountListRefer = response.accounts;
                        resolve(response.accounts);
                    }, error => {
                        reject(error.message);
                    }
                );
            }
        });
    }

    getAccounts() {
        this.showProgress();
        this.getAccountListRefer().then(
            (accounts) => {
                this.accounts = accounts;
                this.referrerLoaded = true;
                this.dismissProgress();
                this.showPopupSelect();
                this.changeAccount();
            },
            error => {
                this.dismissProgress();
                this.showError(error).then();
            }
        );
    }

    showPopupSelect() {
        this.popupController.popupSelect = true;
        for (const account of this.accounts) {
            if (account.account === this.selectedAccount) {
                this.subscribers = account.subscribers;
                break;
            }
        }
    }

    changeAccount() {
        let subscribers = [];
        this.subscribers = [];

        for (const account of this.accounts) {
            if ((this.selectedAccount + '') === (account.account + '')) {
                subscribers = account.subscribers;
                this.email = account.email;
            }
        }

        for (const subscriber of subscribers) {
            if (subscriber.status !== 'C') {
                if (subscriber.defaultSubcriber) {
                    this.selectedSubscriber = subscriber.subscriber;
                }
                this.subscribers.push(subscriber);
            }
        }
    }

    changeSubscriber(subscriber) {
        this.selectedSubscriber = subscriber;
    }

    onNextBalancePost() {
        this.goPage('module/invoice-summary');
    }

    onCancelBalancePost() {
        this.popupController.popupBalancePost = false;
    }

    onNextBalancePre() {
        this.goPage('module/invoice-summary');
    }

    onCancelBalancePre() {
        this.popupController.popupBalancePre = false;
    }

    onNextUpdate() {
        let idHobby = 0;
        for (const hobbyItem of this.hobbiesList) {
            if (hobbyItem.hobbie === this.selectedHobby) {
                idHobby = hobbyItem.idHobbies;
                break;
            }
        }
        const date = this.date;
        const email = this.email;
        const paperless = this.radioPaperless === 1;

        if (!(idHobby > 0)) {
            this.showError('Debe seleccionar un hobbie');
            return;
        }
        if (date === undefined || email.length === 0) {
            this.showError('Debe ingresar los datos solicitados.');
            return;
        }
        if (!Utils.validateEmail(email)) {
            this.showError('Debe ingresar un correo electrónico válido.');
            return;
        }
        if (!paperless) {
            this.showError('Esta opcion esta habilitada solo para usuarios con factura electronica activa.');
            return;
        }

        let hobby = null;
        for (const hobbyItem of this.hobbiesList) {
            if (hobbyItem.idHobbies === idHobby) {
                hobby = hobbyItem.hobbie;
            }
        }
        const ReferrerData = this.services.temporaryStorage.referData;
        let selectedAccount = null;
        for (const account of this.accounts) {
            if (account.account === (ReferrerData.account + '')) {
                selectedAccount = account;
                break;
            }

        }

        let selectedSubscriber;
        if (selectedAccount) {
            for (const subscriber of selectedAccount.subscribers) {
                if (subscriber.subscriber === ReferrerData.subscriber) {
                    selectedSubscriber = subscriber;
                    break;
                }
            }
        }

        const referrerMember = {
            campaingID: 1 + '',
            accountName: this.name,
            account: ReferrerData.account + '',
            subscriber: ReferrerData.subscriber + '',
            accountSubType: selectedAccount.accountSubType + '',
            accountType: selectedAccount.accountType + '',
            productType: selectedSubscriber.productType + '',
            source: 'app',
            dob: date,
            hobby: this.selectedHobby,
            email,
            paperless: 'Y',
            token: this.cacheStorage().tokenSession
        };

        this.showProgress();
        this.popupController.popupUpdate = false;

        this.services.updateBillParameters(ReferrerData.account, this.cacheStorage().tokenSession).then(
            response => {
                this.services.addMember(referrerMember, this.cacheStorage().tokenSession).then(
                    (success) => {
                        this.dismissProgress();
                        this.showAlert('Su perfil ha sido actualizado con éxito.', (e) => {
                                if (success.memberID === undefined ||
                                    success.memberID === null ||
                                    success.memberID === '' ||
                                    success.memberID === 0) {
                                    this.getMemberAgain();
                                } else {
                                    this.services.temporaryStorage.referrerMember.memberID = success.memberID;
                                    if (this.route !== '') {
                                        this.goPage(this.route);
                                    }
                                }
                            }
                        );
                    },
                    error => {
                        this.dismissProgress();
                        this.showError(error.message);
                    }
                );
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    getMemberAgain() {
        this.showProgress();
        const referrerData = this.ReferrerData;
        this.services.getMember(referrerData.account, this.cacheStorage().tokenSession).then(
            (success) => {
                this.dismissProgress();
                if (success.dob !== '' && success.dob !== undefined && success.dob !== null) {
                    this.date = success.dob;
                } else {
                    if (this.cacheStorage().accountInfo.dOBField !== ''
                        && this.cacheStorage().accountInfo.dOBField !== undefined
                        && this.cacheStorage().accountInfo.dOBField !== null) {
                        this.date = this.cacheStorage().accountInfo.dOBField;
                    } else {
                        this.date = '1980-05-02';
                    }
                }
                this.services.temporaryStorage.referrerMember = success;
                if (this.route !== '') {
                    this.goPage(this.route);
                }
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    onCancelUpdate() {
        this.popupController.popupUpdate = false;
    }

    onCancelCant() {
        this.popupController.popupCant = false;
    }


    onNextSelect() {

        const account = this.selectedAccount;
        const subscriber = this.selectedSubscriber;
        if (!account) {
            this.showError('Debes seleccionar un número de cuenta');
            return;
        }
        if (!subscriber) {
            this.showError('Debes seleccionar un número de telefono');
            return;
        }

        this.popupController.popupSelect = false;
        this.showProgress();
        this.validateReferrer(account, subscriber);
    }

    onCancelSelect() {
        this.popupController.popupSelect = false;
    }

    validateReferrer(account, subscriber) {
        const self = this;
        this.services.getValidateReferrer(account, subscriber, this.cacheStorage().tokenSession).then(
            (success) => {
                this.dismissProgress();
                const ReferrerData = {
                    balance: success.Balance,
                    paperless: success.Paperless,
                    registerUpdated: success.registerUpdated,
                    solvent: success.solvent,
                    account,
                    subscriber,
                };
                this.ReferrerData = ReferrerData;
                this.services.temporaryStorage.referData = ReferrerData;
                if (!success.solvent) {
                    this.openPopupPay();
                } else if (!success.Paperless) {
                    this.getMember(account, subscriber);
                } else if (!success.registerUpdated) {
                    this.getMember(account, subscriber);
                } else {
                    this.services.getMember(account, this.cacheStorage().tokenSession).then(
                        (successM) => {
                            this.services.temporaryStorage.referrerMember = successM;
                            if (this.route !== '') {
                                this.goPage(this.route);
                            }
                        },
                        error => {
                            this.showError(error.message);
                        }
                    );
                }
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    openPopupPay() {
        this.popupController.popupBalancePost = true;
    }

    getMember(account, subscriber) {
        this.showProgress();
        this.services.getMember(account, this.cacheStorage().tokenSession).then(
            (success) => {
                this.dismissProgress();
                if (success.dob !== '' && success.dob !== undefined && success.dob !== null) {
                    this.date = success.dob;
                } else {
                    if (this.cacheStorage().accountInfo.dOBField !== ''
                        && this.cacheStorage().accountInfo.dOBField !== undefined
                        && this.cacheStorage().accountInfo.dOBField !== null) {
                        this.date = this.cacheStorage().accountInfo.dOBField;
                    } else {
                        this.date = '1980-05-02';
                    }
                }
                this.services.temporaryStorage.referrerMember = success;
                this.getListHobbies(account, subscriber);
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    getListHobbies(account, subscriber) {
        this.showProgress();
        this.services.getHobbies(account, this.cacheStorage().tokenSession).then(
            (success) => {
                this.dismissProgress();
                this.hobbiesList = success.hobbiesList;
                this.openPopupHobbies();

            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    openPopupHobbies() {
        const ReferrerData = this.ReferrerData;
        if (ReferrerData.paperless) {
            this.radioPaperless = 1;
        } else {
            this.radioPaperless = 0;
        }

        let date = this.date;

        if (date !== null && date !== '') {
            if (date.includes('-')) {
                this.date = date;
            } else if (date.includes('/')) {
                const mld = date.split('/');
                date = mld[2] + '-' + mld[0] + '-' + mld[1];
                this.date = date;
            }
        } else {
            date = this.cacheStorage().accountInfo.dOBField;
            if (date != null && date !== '') {
                const ld = date.split('/');
                date = ld[2] + '-' + ld[1] + '-' + ld[0];
                this.date = date;
            }
        }
        const accountInfo = this.cacheStorage().accountInfo;
        this.popupController.popupUpdate = true;
    }

    changeHobbie() {
    }

    refer() {
        if (this.cacheStorage().accountInfo.banStatusField === 'S') {
            this.popupController.popupCant = true;
        } else {
            this.route = 'module/refer/invite';
            if (this.referrerLoaded) {
                this.showPopupSelect();
            } else {
                this.getAccounts();
            }
        }
    }

    redeem() {
        if (this.cacheStorage().accountInfo.banStatusField === 'S') {
            this.popupController.popupCant = true;
        } else {
            this.route = 'module/refer/redeem';
            if (this.referrerLoaded) {
                this.showPopupSelect();
            } else {
                this.getAccounts();
            }
        }
    }

    questions() {
        this.goPage('module/refer-questions');
    }

    showTerms() {
        this.popupController.popupTerm = true;
    }

    closeTerms() {
        this.popupController.popupTerm = false;
    }

    async payment(account, subscriber, amount) {
        const userPaymentVersion = await this.fetch(keys.PAYMENT.PAYMENT_VERSION);
        if (userPaymentVersion && userPaymentVersion === PAYMENT_VERSION.P2P) {
            let typeMessage = 'payment';
            if (this.cacheStorage().isPrepaidAccount) {
                typeMessage = 'prepaid';
            }
            this.paymentSupport.paymentRefactorProcess(typeMessage, parseFloat(this.cacheStorage().selectedPacket.price));
        } else {
            const response = await this.paymentSupport.paymentExternalProcess(account, subscriber, amount);
            this.verifyPayment(response);
        }
    }

    verifyPayment(response: any) {
        this.dismissProgress();
        if (response && !response.hasError) {
            if (response.processEndState) {
                this.showProgress('Actualizando su balance...');
                this.reloadCurrentAccount().then(() => {
                    this.dismissProgress();
                    this.initHome();
                });
            } else {
                this.dismissProgress();
                this.showAlert('Su pago aun no ha sido procesado, si ya realizo el pago espere mientras lo procesamos.',
                    () => {
                        this.goHomePage();
                    });
            }
        } else {
            this.showError(response.message);
        }
    }
}

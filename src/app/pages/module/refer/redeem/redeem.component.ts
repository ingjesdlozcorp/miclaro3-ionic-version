import { Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import { Utils } from 'src/app/utils/utils';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
    selector: 'app-module-refer-redeem',
    templateUrl: './redeem.component.html',
    styleUrls: ['./redeem.component.scss'],
})
export class RedeemComponent extends BasePage implements OnInit {
    public data;
    public name;
    public selectedAccount;
    public date;
    public email;
    public message = 'Actualmente estamos teniendo inconvenientes para obtener sus datos, por favor intente mas tarde.';
    public referrerData;
    public account;
    public subscriber;
    public accountName = '';
    public totalReferer = '';
    public totalRedeem = '';
    public totalPending = '';
    public sumAvialable;
    public items = [];
    public redeemControl = false;

    constructor(
        router: Router,
        storage: Storage,
        modelsServices: ServicesProvider,
        alertController: AlertController, utilsService: UtilsService, userStorage: IntentProvider,
        private socialSharing: SocialSharing) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);

        this.data = this.cacheStorage();
        this.name = this.cacheStorage().loginData.username;
        this.selectedAccount = this.cacheStorage().accountInfo.bANField;
        const parts = this.cacheStorage().accountInfo.dOBField.split('/');
        this.date = Number(parts[2]) + '-' + Number(parts[1]) + '-' + Number(parts[0]);
        this.email = this.cacheStorage().accountInfo.emailField;
        this.referrerData = this.services.temporaryStorage.referData;
        this.account = this.referrerData.account;
        this.subscriber = this.referrerData.subscriber;
    }

    ngOnInit() {
        this.getUserCredits();
    }

    getUserCredits() {
        const selectedAccount = this.selectedAccount;
        this.services.getCredits(String(selectedAccount), this.cacheStorage().tokenSession).then(
            (success) => {
                this.accountName = success.CreditItems[0].accountName;
                this.totalReferer = success.CreditItems[0].TotalReferer;
                this.totalRedeem = success.CreditItems[0].CountRedeems;
                this.totalPending = success.CreditItems[0].CountPending;
                this.sumAvialable = success.CreditItems[0].TotalAvailable;
                if (Utils.isTelephony(selectedAccount.mAccountType, selectedAccount.mAccountSubType, selectedAccount.mProductType)) {
                    if (this.sumAvialable > 0) {
                        this.sumAvialable = '50%';
                    } else {
                        this.sumAvialable = '0%';
                    }
                } else {
                    this.sumAvialable = '$' + Utils.formatAmount(this.sumAvialable);
                }
                this.getUserRedeemCredits();
            },
            error => {
                this.showError(error.message, () => {
                    this.goBack();
                });
            }
        );
    }

    getUserRedeemCredits() {
        const selectedAccountValue = this.selectedAccount;
        this.services.getReferrerAccountsAllStatus(selectedAccountValue, this.cacheStorage().tokenSession).then(
            (success) => {
                if (success.objItems) {
                    if (!success.objItems || !success.objItems.length) {
                        this.redeemControl = false;
                    } else {
                        for (const item of success.objItems) {
                            let discount = item.discountType + '' + item.discountMember;
                            if (item.discountType === '%') {
                                discount = item.discountMember + '' + item.discountType;
                            }
                            const a = Object.defineProperty(item, 'discount', {
                                value: discount,
                                configurable: true
                            });
                            this.items.push(a);
                        }
                        console.log(this.items);
                        this.redeemControl = true;
                    }
                }
            },
            error => {
                this.showError(error.message, () => {
                    this.goBack();
                });
            }
        );
    }

    payInvoice() {
        this.goHomePage();
    }

    getDiscount(discountType, discountMember) {
        let discount = discountType + '' + discountMember;
        if (discountType === '%') {
            discount = discountMember + '' + discountType;
        }
        return discount;
    }

}

import { Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import { Utils } from 'src/app/utils/utils';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';

@Component({
    selector: 'app-module-refer-invite',
    templateUrl: './invite.component.html',
    styleUrls: ['./invite.component.scss'],
})
export class InviteComponent extends BasePage implements OnInit {

    public selectedAccount;
    public selectedSubscriber;
    public name;
    public data;
    public accounts = [];
    public subscribers = [];
    public popupController = {
        popupTerm: false,
        popupErrorEmail: false,
        popupSuccess: false,
    };
    public ReferrerData;
    public referrerLoaded = false;
    public referrerMember;
    public date;
    public email;
    public referrerValidMember;
    public route = '';
    public totalReferer;
    public totalRedeem;
    public sumAvialable;
    public shareFace;
    public shareTwitter;
    public shareWp;
    public shareMail;
    public shareLink;
    public check = false;
    public emailW;
    public emailsSuccess = [];
    public emailsFailure = [];
    public emailsWaiting = [];
    public memberID;
    public tags = [];
    public share = false;
    public wpAvaliable = true;

    constructor(
        router: Router,
        storage: Storage,
        modelsServices: ServicesProvider,
        alertController: AlertController, utilsService: UtilsService, userStorage: IntentProvider,
        private socialSharing: SocialSharing,
        private clipboard: Clipboard) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.data = this.cacheStorage();
        this.name = this.cacheStorage().loginData.username;
        this.selectedAccount = this.cacheStorage().accountInfo.bANField;
        const parts = this.cacheStorage().accountInfo.dOBField.split('/');
        this.date = Number(parts[2]) + '-' + Number(parts[1]) + '-' + Number(parts[0]);
        this.email = this.cacheStorage().accountInfo.emailField;
        if (Utils.getPlatformInfo().ipad) {
            this.wpAvaliable = false;
        }
    }

    ngOnInit() {
        this.getUserCredits();
    }

    deleteEmail(email) {
        for (let i = 0; i < this.tags.length; i++) {
            if (email === this.tags[i]) {
                this.tags.splice(i, 1);
            }
        }
    }


    addEmail() {
        if (Utils.validateEmail(this.emailW)) {
            this.tags.push(this.emailW);
            this.emailW = '';
        } else {
            this.showError('Debe ingresar un correo electrónico válido.');
            this.emailW = '';
        }
    }

    getUserCredits() {
        const selectedAccount = this.selectedAccount;
        this.services.getCredits(selectedAccount + '', this.cacheStorage().tokenSession).then(
            (success) => {
                if (!success.CreditItems.length) {
                    this.showAlert('¡Gracias por su interés en nuestro Programa Refiere y Gana! Su cuenta no cumple con los requisitos de participación del Programa. Para información adicional comuníquese con unos de nuestros Coordinadores de Servicio al Cliente a través de nuestro CHAT, o revise las reglas, términos y condiciones de esta promoción.',
                        () => this.goHomePage());
                } else {
                    const totalReferer = success.CreditItems[0].TotalReferer;
                    const totalRedeem = success.CreditItems[0].CountRedeems;
                    const sumAvialable = success.CreditItems[0].TotalAvailable;
                    this.totalReferer = totalReferer + '';
                    this.totalRedeem = totalRedeem + '';
                    if (Utils.isTelephony(selectedAccount.mAccountType, selectedAccount.mAccountSubType, selectedAccount.mProductType)) {
                        if (sumAvialable > 0) {
                            this.sumAvialable = '50%';
                        } else {
                            this.sumAvialable = '50%';
                        }
                    } else {
                        this.sumAvialable = '$' + Utils.formatAmount(sumAvialable);
                    }
                    this.getSharingMedia();
                }
            },
            error => {
                this.showError(error.message, () => {
                    this.goBack();
                });

            }
        );
    }

    getSharingMedia() {
        let memberID;
        this.services.getSharingMediaByUser(this.services.temporaryStorage.referrerMember.memberID, this.cacheStorage().tokenSession).then(
            (success) => {
                if (success.objItems) {
                    const list: any[] = success.objItems;
                    for (const item of list) {
                        if (item.socialMedia === 'facebook') {
                            this.shareFace = item.linkCode;
                        } else if (item.socialMedia === 'twitter') {
                            this.shareTwitter = item.linkCode;
                        } else if (item.socialMedia === 'whatsapp') {
                            this.shareWp = item.linkCode;
                        } else if (item.socialMedia === 'email') {
                            this.shareMail = item.linkCode;
                        } else if (item.socialMedia === 'web') {
                            this.shareLink = item.linkCode;
                            memberID = item.memberID;
                        }
                    }
                }
            },
            error => {
                this.showError(error.message, () => {
                    this.goBack();
                });
            }
        );
    }

    next() {
        if (!this.check) {
            this.alertRequiredTerms();
            return;
        }
        if (this.emailW.length > 0) {
            if (!Utils.validateEmail(this.emailW)) {
                this.emailW = '';
            }
        }
        if (this.tags.length === 0) {
            const message = 'Debe ingresar al menos un correo válido.';
            this.showError(message);
            return;
        }
        const emails = this.tags;
        this.resolveEmail(emails);
    }

    resolveEmail(emails) {
        this.emailsSuccess = [];
        this.emailsFailure = [];
        this.emailsWaiting = emails;
        this.resolveNextEmail();
    }

    resolveNextEmail() {
        this.showProgress();
        if (this.emailsWaiting.length > 0) {
            const email = this.emailsWaiting.shift();
            this.verifyEmail(email);
        } else {
            this.sharedCoupon();
        }
    }

    verifyEmail(email) {
        this.services.getVerifyEmail(email, this.cacheStorage().tokenSession).then(
            (success) => {
                this.dismissProgress();
                if (success.hasErrorField) {
                    this.emailsFailure.push(email);
                } else {
                    this.emailsSuccess.push(email);
                }
                this.resolveNextEmail();
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    sharedCoupon() {
        if (this.emailsSuccess.length > 0) {
            const emails = this.emailsSuccess.join(',');
            const ReferrerData = this.services.temporaryStorage.referData;
            const link = this.shareLink;
            const memberID: any = this.services.temporaryStorage.referrerMember;
            this.services.sharedCoupons(memberID.memberID,
                ReferrerData.account, ReferrerData.subscriber, emails, link, this.cacheStorage().tokenSession).then(
                (success) => {
                    this.onEmailsError();
                    this.tags = [];
                    this.popupController.popupSuccess = true;
                },
                error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        } else {
            this.onEmailsError();
        }
    }

    onEmailsError() {
        this.dismissProgress();
        if (this.emailsFailure.length > 0) {
            this.popupController.popupErrorEmail = true;
        }
    }

    okSuccess() {
        this.popupController.popupSuccess = false;
    }

    showTerms() {
        const check = this.check;
        if (check) {
            this.popupController.popupTerm = true;
            this.share = true;
        } else {
            this.share = false;
        }
    }

    closeTerms() {
        this.popupController.popupTerm = false;
    }

    closePopupErrors() {
        this.popupController.popupErrorEmail = false;
        this.tags = [];
    }

    invertCheckbox() {
        this.check = !this.check;
        this.showTerms();
    }

    copy() {
        const check = this.check;
        if (check) {
            const copyText = this.shareLink;
            this.setClipboardText(copyText);
        } else {
            this.alertRequiredTerms();
        }
    }

    shareViaEmail() {
        const check = this.check;
        if (check) {
            const message = this.getMessage() + '\n\n' + this.shareMail;
            this.socialSharing.shareViaEmail(
                message,
                'Refiere y Gana',
                null,
                null,
                null,
                null,
            ).then(
                () => {

                }).catch(
                e => {
                    this.showAlert('Correo no instalado, vaya a su configuración y agregue una cuenta de correo electrónico.');
                }
            );

        } else {
            this.alertRequiredTerms();
        }
    }

    shareViaFacebook() {
        const check = this.check;
        let nameApp: string;
        if (Utils.getPlatformInfo().ios) {
            nameApp = 'com.apple.social.facebook';
        } else {
            nameApp = 'com.facebook.katana';
        }
        if (check) {
            if (Utils.getPlatformInfo().android) {
                this.socialSharing.shareVia(nameApp, this.getMessage(), null, null, this.shareFace)
                    .then(() => { }).catch(() => {this.onShareError('Facebook');}
                );
            }
            if (Utils.getPlatformInfo().ios) {
                this.socialSharing.shareViaFacebook(this.getMessage(), null, this.shareFace)
                    .then(() => { }).catch(() => {}
                );
            }
        } else {
            this.alertRequiredTerms();
        }
    }

    shareViaTwitter() {
        const check = this.check;
        let nameApp: string;
        if (Utils.getPlatformInfo().ios) {
            nameApp = 'com.apple.social.twitter';
        } else {
            nameApp = 'twitter';
        }
        if (check) {
            if (Utils.getPlatformInfo().android) {
                this.socialSharing.shareViaTwitter(this.getMessage(), null, this.shareTwitter)
                    .then(() => { }).catch(() => {this.onShareError('twitter')}
                );

            }
            if (Utils.getPlatformInfo().ios) {
                this.socialSharing.shareViaTwitter(this.getMessage(), null, this.shareTwitter)
                    .then(() => { }).catch(() => {}
                );
            }
        } else {
            this.alertRequiredTerms();
        }
    }

    shareViaWhatsApp() {
        const check = this.check;
        if (check) {
            if (Utils.getPlatformInfo().android) {
                this.socialSharing.shareViaWhatsApp(this.getMessage(), null, this.shareWp)
                    .then(() => { }).catch(() => {this.onShareError('WhatsApp')}
                );
            }
            if (Utils.getPlatformInfo().ios) {
                this.socialSharing.shareViaWhatsApp(this.getMessage(), null, this.shareWp)
                    .then(() => { }).catch(() => {}
                );
            }

        } else {
            this.alertRequiredTerms();
        }
    }

    onShareError(name) {
        this.showError('Aplicación no instalada, vaya a su app store instale ' + name + ' y vuelva a intentar.');
    }

    alertRequiredTerms() {
        this.showError('Debe seleccionar el campo de Términos y Condiciones para continuar.');
    }

    questions() {
        this.goPage('module/refer-questions');
    }

    getMessage() {
        return 'Usted ha sido referido para disfrutar de descuentos al activar tu cuenta con Claro. ' +
            'Para más información favor de acceder al siguiente enlace para redimir su cupón y/o visitar una de nuestras localidades.';
    }

    setClipboardText(text) {
        this.clipboard.copy(this.shareLink).then(
            () => {
                this.showAlert('El enlace ha sido copiado al portapapeles.');
            },
            error => {
                console.log('Unable to copy.');
            }
        );
        /*
        let id = "mycustom-clipboard-textarea-hidden-id";
        let existsTextarea: any = document.getElementById(id);

        if(!existsTextarea){
            console.log("Creating textarea");
            let textarea = document.createElement("textarea");
            textarea.id = id;
            // Place in top-left corner of screen regardless of scroll position.
            textarea.style.position = 'fixed';
            textarea.style.top = '0';
            textarea.style.left = '0';

            // Ensure it has a small width and height. Setting to 1px / 1em
            // doesn't work as this gives a negative w/h on some browsers.
            textarea.style.width = '1px';
            textarea.style.height = '1px';

            // We don't need padding, reducing the size if it does flash render.
            textarea.style.padding = '0';

            // Clean up any borders.
            textarea.style.border = 'none';
            textarea.style.outline = 'none';
            textarea.style.boxShadow = 'none';

            // Avoid flash of white box if rendered for any reason.
            textarea.style.background = 'transparent';
            document.querySelector("body").appendChild(textarea);
            console.log("The textarea now exists :)");
            existsTextarea = document.getElementById(id);
        }else{
            console.log("The textarea already exists :3")
        }

        existsTextarea.innerText = text;

            var range, selection;
            range = document.createRange().selectNodeContents(existsTextarea);
            selection = window.getSelection().removeAllRanges()
            selection.addRange(range);
            existsTextarea.setSelectionRange(0, 999999);

        //existsTextarea.select(null);
        //document.getElementById('#mycustom-clipboard-textarea-hidden-id').blur();
        try {
            var status = document.execCommand('copy');
            if(!status){
                console.error("Cannot copy text");
            }else{
                console.log("The text is now on the clipboard");
                this.showAlert('El enlace ha sido copiado al portapapeles.');
            }
        } catch (err) {
            console.log('Unable to copy.');
        }*/
    }


}

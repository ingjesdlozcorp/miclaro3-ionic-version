import { Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from 'src/app/services/services.provider';

@Component({
    selector: 'app-module-support',
    templateUrl: './support.component.html',
    styleUrls: ['./support.component.scss'],
})
export class SupportComponent extends BasePage implements OnInit {

    constructor(public router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('support');
    }

    ngOnInit() {}

    faq() {
        this.goPage('help/faq');
    }

}

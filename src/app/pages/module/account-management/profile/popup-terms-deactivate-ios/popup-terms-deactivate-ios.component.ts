import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-popup-deactivate-ios-terms',
    templateUrl: './popup-terms-deactivate-ios.component.html',
    styleUrls: ['./popup-terms-deactivate-ios.component.scss'],
})
export class PopupTermsDeactivateIosComponent {

    @Input() terms: string;
    @Output() close = new EventEmitter();

    constructor() {
    }

    closeEmit() {
        this.close.emit();
    }
}


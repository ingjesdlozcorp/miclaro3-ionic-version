import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../../services/utils.service';
import { IntentProvider } from '../../../../services/intent.provider';
import { BasePage } from '../../../base.page';
import { AccountInfo } from '../../../../models/account.details';
import { Utils } from '../../../../utils/utils';

@Component({
    selector: 'app-module-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent extends BasePage implements OnInit {

    tabIndex = 1;
    accountInfo: AccountInfo;

    fullName;
    phone;
    phoneFixed;

    email;
    newEmail;
    newEmailRepeat;

    oldPassword;
    newPassword;
    passwordRepeat;

    address1;
    address2;
    city;
    state;
    county;
    zipCode;

    questions: any[] = [];
    question1;
    question2;
    answer1 = '';
    answer2 = '';

    username;
    newUsername;

    questionsSelected: any[] = [];
    personalData: any;
    private sub: any;

    slideOpts = {
        slidesPerView: Utils.getPlatformInfo().ios ? 6 : 5,
        scrollbar: {
            hide: true,
            draggable: true
        },
        breakpoints: {
            480: {
                slidesPerView: 3.5,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 4.5,
                spaceBetween: 10
            }
        }
    };
    messageDeactivation: string;
    isPlatformIos: boolean = Utils.getPlatformInfo().ios;
    terms: boolean;
    termsDeactivation: string;
    popupController = {
        popupTerms: false
    };

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private route: ActivatedRoute) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('profile');
    }

    ngOnInit() {
        this.accountInfo = this.cacheStorage().accountInfo;
        this.getProfile();
        this.getDataDeactivation();
        this.sub = this.route.params.subscribe(params => {
            const tab = params.tab;
            if (tab === 'postal') {
                this.tabIndex = 5;
            } else if (tab === 'questions') {
                this.tabIndex = 4;
            } else if (tab === 'password') {
                this.tabIndex = 3;
            } else if (tab === 'email') {
                this.tabIndex = 2;
            } else if (tab === 'deactivate') {
                this.tabIndex = 6;
            } else {
                this.tabIndex = 1;
            }
        });
    }

    getProfile() {
        this.showProgress();
        this.services.getUserProfile(
            String(this.accountInfo.bANField),
            this.cacheStorage().tokenSession
        ).then(
            (response: any) => {
                this.personalData = response;
                this.dismissProgress();
                this.setupData(response);

            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
    }

    setupData(data: any) {
        this.newEmail = '';
        this.newUsername = '';
        this.newEmailRepeat = '';
        this.oldPassword = '';
        this.newPassword = '';
        this.passwordRepeat = '';
        this.answer1 = '';
        this.answer2 = '';

        this.username = this.cacheStorage().loginData.username;
        this.fullName = data.FirstName + ' ' + data.LastName;
        this.phone = data.CelularPhone;
        this.phoneFixed = data.PhoneNumber;
        this.email = data.Email;
        this.address1 = data.AddressDet;
        this.address2 = data.AddressDet2;
        this.city = data.City;
        this.state = data.State;
        this.county = data.Country;
        this.zipCode = data.ZipCode;

        this.getSecurityQuestions();
    }

    getSecurityQuestions() {
        this.showProgress();
        this.services.getSecurityQuestions(
            this.cacheStorage().tokenSession)
            .then(
                (response: any) => {
                    this.questions = response.QuestionsItemsList;
                    this.dismissProgress();
                    this.getChallengeQuestions();
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                    this.goLoginPage();
                }
            );
    }

    getChallengeQuestions() {
        this.showProgress();
        this.services.getChallengeQuestions2(
            this.cacheStorage().tokenSession)
            .then(
                (response: any) => {
                    this.dismissProgress();
                    this.questionsSelected = response.ResponseList;
                    if (this.questionsSelected) {
                        this.questions.forEach(question => {
                            if (question.questionID === this.questionsSelected[0].questionID) {
                                this.question1 = question;
                            } else if (question.questionID === this.questionsSelected[1].questionID) {
                                this.question2 = question;
                            }
                        });
                    } else {
                        this.question1 = this.questions[0];
                        this.question2 = this.questions[1];
                    }
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                    this.goLoginPage();
                }
            );
    }

    savePersonalData() {
        if (this.phone.toString().length !== 10) {
            this.showError('El número de teléfono celular es invalido.');
        } else if (this.phoneFixed.toString().length !== 10) {
            this.showError('El número de teléfono de hogar es invalido.');
        } else {
            this.showProgress();
            this.services.updatePersonalData(
                String(this.accountInfo.bANField),
                this.email,
                this.phone,
                this.phoneFixed,
                this.cacheStorage().tokenSession)
                .then(
                    (response: any) => {
                        this.showAlert(response.ErrorDesc, () => {
                            this.getProfile();
                        });
                        this.dismissProgress();
                    }, error => {
                        this.dismissProgress();
                        this.showError(error.message);
                    }
                );
        }
    }

    saveEmail() {
        if (!Utils.validateEmail(this.newEmail)) {
            this.showError('Debe ingresar un correo electrónico válido.');
        } else if (this.newEmail !== this.newEmailRepeat) {
            this.showError('Los correos no coinciden.');
        } else {
            this.showProgress();
            this.services.getVerifyEmail(this.newEmail, this.cacheStorage().tokenSession)
                .then(
                    () => {
                        this.services.updatePersonalData(
                            String(this.accountInfo.bANField),
                            this.newEmail,
                            this.personalData.CelularPhone,
                            this.personalData.PhoneNumber,
                            this.cacheStorage().tokenSession)
                            .then(
                                (response: any) => {
                                    this.showAlert(response.ErrorDesc, () => {
                                        this.getProfile();
                                    });
                                    this.dismissProgress();
                                }, error => {
                                    this.dismissProgress();
                                    this.showError(error.message);
                                }
                            );
                    }, error => {
                        this.dismissProgress();
                        this.showError(error.message);
                    }
                );
        }
    }

    saveUsername() {
        if (this.newUsername.length === 0) {
            this.showError('Debe ingresar su nuevo usuario.');
        } else if (!Utils.validateEmail(this.newUsername)) {
            this.showError('Debe ingresar un correo electrónico válido.');
        } else {
            this.showProgress();
            this.services.updateUsername(
                this.newUsername,
                this.cacheStorage().tokenSession)
                .then(() => {
                        this.cacheStorage().loginData.username = this.newUsername;
                        this.showAlert('Se actualizo su usuario con éxito.', () => {
                            this.getProfile();
                        });
                        this.dismissProgress();
                    }, error => {
                        this.dismissProgress();
                        this.showError(error.message);
                    }
                );
        }
    }

    savePassword() {
        if (this.oldPassword.length === 0) {
            this.showError('Debe colocar su contaseña anterior.');
        } else if (this.newPassword.length < 8 || this.newPassword.length > 15) {
            this.showError('Su contaseña nueva debe tener entre 8 y 15 caracteres.');
        } else if (!this.lowercaseValidator(this.newPassword)) {
            this.showError('Su contaseña nueva debe tener al menos 1 letra minúscula.');
        } else if (!this.uppercaseValidator(this.newPassword)) {
            this.showError('Su contaseña nueva debe tener al menos 1 letra mayúscula.');
        } else if (!this.twoNumberValidator(this.newPassword)) {
            this.showError('Su contaseña nueva debe tener al menos 2 números.');
        } else if (!this.specialCharacterValidator(this.newPassword)) {
            this.showError('No estan permitidos caracteres especiales.');
        } else if (this.newPassword !== this.passwordRepeat) {
            this.showError('Las contraseñas no coinciden.');
        } else {
            this.showProgress();
            this.services.updatePassword(
                this.newPassword,
                this.oldPassword,
                String(this.accountInfo.bANField),
                this.cacheStorage().tokenSession).then(
                (response: any) => {
                    this.showAlert(response.response, () => {
                        this.getProfile();
                    });
                    this.dismissProgress();
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }

    saveAddress() {
        if (this.address1.length === 0) {
            this.showAlert('Por favor indique su dirección.');
        } else if (this.city.length === 0) {
            this.showAlert('Por favor indique su ciudad.');
        } else if (this.zipCode.length === 0) {
            this.showAlert('Por favor indique su codigo postal.');
        } else {
            this.showProgress();
            this.services.updatePersonalAddress(
                String(this.accountInfo.bANField),
                this.address1,
                this.address2,
                this.city,
                this.zipCode,
                this.cacheStorage().tokenSession).then(
                (response: any) => {
                    this.showAlert(response.ErrorDesc, () => {
                        this.getProfile();
                    });
                    this.dismissProgress();
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }

    saveQuestions() {
        if (this.answer1.length === 0) {
            this.showAlert('Por favor introduzca su respuesta a la primera pregunta de Seguridad.');
        } else if (this.answer2.length === 0) {
            this.showAlert('Por favor introduzca su respuesta a la segunda pregunta de Seguridad.');
        } else {
            this.showProgress();
            this.services.setChallengeQuestions(this.question1.questionID, this.answer1, this.cacheStorage().tokenSession)
                .then(() => {
                        this.dismissProgress();
                        this.saveSecondAnswer();
                    }, error => {
                        this.dismissProgress();
                        this.showError(error.message);
                    }
                );
        }
    }

    saveSecondAnswer() {
        this.showProgress();
        this.services.setChallengeQuestions(this.question2.questionID, this.answer2, this.cacheStorage().tokenSession)
            .then(() => {
                    this.dismissProgress();
                    this.showAlert('Se actualizaron sus preguntas de seguridad con éxito.',
                        () => {
                            this.getProfile();
                        });
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
    }

    lowercaseValidator(c: string) {
        const regex = /[a-z]/g;
        return regex.test(c);
    }

    uppercaseValidator(c: string) {
        const regex = /[A-Z]/g;
        return regex.test(c);
    }

    twoNumberValidator(c: string) {
        const numbers = '0123456789';
        let count = 0;
        for (let i = 0; i < c.length; i++) {
            if (numbers.indexOf(c.charAt(i), 0) !== -1) {
                count++;
            }
        }
        return count >= 2;
    }

    specialCharacterValidator(c: string) {
        return c.match('^[A-z0-9]+$');
    }

    private async getDataDeactivation() {
        this.showProgress();
        const deactivateMessage = await this.services.getMessage(MESSAGES.DEACTIVATE_MESSAGE, this.cacheStorage().tokenSession).catch();
        const deactivateTerms = await this.services.getMessage(MESSAGES.DEACTIVATE_TERMS, this.cacheStorage().tokenSession).catch();
        this.messageDeactivation = deactivateMessage.errorDisplay;
        this.termsDeactivation = deactivateTerms.errorDisplay;
        this.dismissProgress();
    }

    showTerms() {
        console.log('showTerms');
        this.popupController.popupTerms = true;
    }

    closeTerms() {
        this.popupController.popupTerms = false;
    }

    validateCanDeactivateUser() {
        if (!this.terms) {
            this.showError('Por favor verifique que ha leído los términos y condiciones');
            return;
        }
        this.deactivateUser();
    }

    deactivateUser() {
        this.showProgress();
        this.services.deactivateUser(this.cacheStorage().tokenSession)
            .then((resp: any) => {
                this.dismissProgress();
                this.showError(resp.errorDisplay,
                    () => this.utils.closeSession());
            }, error => {
                console.log(error);
                this.dismissProgress();
                if (error.message === 'errror.to.disable.user') {
                    this.showError('Lo sentimos, su cuenta no ha podido ser desactivada, intente de nuevo o comuníquese al soporte técnico');
                } else {
                    this.showError(error.message);
                }
            });
    }
}

enum MESSAGES {
    DEACTIVATE_MESSAGE = 'deactivate.message',
    DEACTIVATE_TERMS = 'deactivate.terms',
}

import { AfterViewInit, Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import {Utils} from '../../../../utils/utils';
import { keys } from 'src/app/utils/const/keys';

@Component({
    selector: 'app-module-add-accounts',
    templateUrl: './add-accounts.component.html',
    styleUrls: ['./add-accounts.component.scss'],
})
export class AddAccountsComponent extends BasePage implements OnInit {

    public typeOfTelephony;
    public subscribers = [];
    public accounts = [];
    public selectedTab;
    public selectedAccount;
    public accountSections;
    public accountInfo;
    public data;
    public token;
    public ssn = '';
    public number = '';
    public availableNextClass = false;
    public checked = false;
    public selectedSection = '';
    public showAddAccount = false;
    public subscriberNow;
    public accountChecked;
    selectedSubscribers: any[] = [];


    constructor(public router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private route: ActivatedRoute) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.data = this.cacheStorage().loginData.account;
        this.accountInfo = this.services.data.AccounInfo;
        this.selectedAccount = this.cacheStorage().loginData.account;
        this.token = this.cacheStorage().tokenSession;
        this.accountChecked = this.selectedAccount;
        this.utils.registerScreen('accounts');
    }

    ngOnInit() {
        this.accounts = [];
        this.showProgress();
        this.route.params.subscribe(params => {
            const tab = params.tab;
            if (tab === '1') {
                this.expandAddAccount();
            }
        });
        this.services.getAccounts(this.token).then(
            (response) => {
                this.accounts = response.accounts;
                const fullAccount = [];
                let typeField;
                this.accounts.forEach(account => {
                    let defaultSubscriber;
                    account.subscribers.forEach(subscriber => {
                        if (subscriber.defaultSubcriber) {
                            defaultSubscriber = subscriber;
                        }
                    });
                    if (defaultSubscriber) {
                        account.productType = defaultSubscriber.productType;
                        account.defaultSubscriber = defaultSubscriber.subscriber;
                    } else {
                        account.productType = account.subscribers[0].productType;
                    }
                    this.selectedSubscribers[this.accounts.indexOf(account)] = defaultSubscriber;
                    if (Utils.isPostpaid(account.accountType, account.accountSubType, account.productType)) {
                        typeField = 'Postpago';
                    } else if (Utils.isPrepaid(account.accountType, account.accountSubType)) {
                        typeField = 'Prepago';
                    } else if (Utils.isTelephony(account.accountType, account.accountSubType, account.productType)) {
                        typeField = 'Servicio Fijo';
                    }  else {
                        typeField = 'Indefinida';
                    }
                    const completeAccount =  Object.defineProperty(account, 'typeField', {
                        value: typeField
                    });
                    fullAccount.push(completeAccount);
                });
                this.accounts = fullAccount;
                this.markDefaultAccount();
                this.store(keys.ACCOUNT_DETAILS.ACCOUNT_LIST_SUBSCRIBER, response.accounts);
                this.store(keys.ACCOUNT_DETAILS.ACCOUNT_LIST_SUBSCRIBER_IS_LOADED, true);
                this.dismissProgress();
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        return !(charCode > 31 && (charCode < 48 || charCode > 57));
    }

    markDefaultAccount() {
        this.accounts.forEach(account => {
            if (account.defaultAccount) {
                this.accountChecked = account.account;
            }
        });
    }

    getStoredList() {
        let list = [];
        this.fetch(keys.ACCOUNT_DETAILS.ACCOUNT_LIST_SUBSCRIBER).then(
            (result) => {
                list = result;
            }
        );
        return list;
    }

    expandAddAccount() {
        this.showAddAccount = true;
    }

    collapseAddAccount() {
        this.showAddAccount = false;
    }

    ssnChanged() {
        let ssnNumber = this.ssn;
        if (ssnNumber.length > 4) {
            ssnNumber = ssnNumber.slice(0, 4);
            this.ssn = ssnNumber;
        }
        this.availableNext();
    }

    numberChanged() {
        let changeNumber = '';
        changeNumber  = this.number + '';
        if (changeNumber.length > 9) {
            changeNumber = changeNumber.slice(0, 9);
            this.number = changeNumber;
        }
        this.availableNext();
    }

    availableNext() {
        const ssn = this.ssn;
        let nextNumber = '';
        nextNumber = this.number + '';
        if (ssn.length === 4 && nextNumber.length === 9) {
            this.availableNextClass = true;
        } else {
            this.availableNextClass = false;
        }
    }

    saveAccount() {
        this.showProgress();
        const ssn = this.ssn;
        const account = this.number;
        let message = '';
        if (ssn.length !== 4) {
            message = 'Debe ingresar los datos solicitados.';
            this.showError(message, );
            return;
        }
        if (account.length < 9) {
            message = 'Debe ingresar los datos solicitados.';
            this.showError(message, );
            return;
        }
        const check = this.checked;
        this.services.addAccount(account, ssn, check, this.token).then(
            (response) => {
                this.dismissProgress();
                this.store(keys.ACCOUNT_DETAILS.ACCOUNT_LIST_SUBSCRIBER_IS_LOADED, false);
                this.showAlert('Su cuenta ha sido agregada exitosamente. Para visualizarla debe cerrar su sesión y volver a ingresar a la aplicación.',
                    () => {
                        this.ngOnInit();
                    });
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    changeDefaultAccount(account: any) {
        this.accountChecked = account.account;
        let defaultSubscriber = account.defaultSubscriber;
        if (defaultSubscriber === undefined) {
            defaultSubscriber = account.subscribers[0].subscriber;
        }
        this.showConfirm(
            'Cambio de Cuenta Principal',
            '¿Seguro que desea seleccionar el BAN ' + account.account + ' como cuenta principal?',
            () => {
                this.setDefaultAccount(
                    String(account.account),
                    String(defaultSubscriber),
                    account.accountType,
                    account.accountSubType,
                    account.productType,
                    false);
            },
            () => {
                this.markDefaultAccount();
            }
        );
    }

    changeDefaultAccountAndSubscriber(account) {
        this.showProgress();
        const index = this.accounts.indexOf(account);
        const subscriber = this.selectedSubscribers[index];
        this.setDefaultAccount(
            String(account.account),
            String(subscriber.subscriber),
            account.accountType,
            account.accountSubType,
            subscriber.productType,
            true);
    }

    setDefaultAccount(account, defaultSubscriber, accountType, accountSubType, productType, tooSubscriber) {
        this.showProgress();
        this.services.setDefaultAccount(
            account,
            defaultSubscriber,
            accountType,
            accountSubType,
            productType,
            this.token).then(
            (response) => {
                this.dismissProgress();
                this.showAlert( tooSubscriber ?
                    'Su cuenta y subscriptor principal han sido modificados con exito.' :
                    'Su cuenta principal ha sido modificada con exito.',
                    () => {
                        this.ngOnInit();
                    });
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
                this.markDefaultAccount();
            }
        );
    }

    deleteAccount(account) {
        this.showConfirm(
            'Eliminar Cuenta',
            '¿Seguro que desea eliminar la cuenta ' + account.account + '?',
            () => {
                this.showProgress();
                this.services.deleteAccount( account.account + '', this.token).then(
                    (response) => {
                        this.dismissProgress();
                        const accountsSubscribers = this.getStoredList();
                        accountsSubscribers.splice(accountsSubscribers.indexOf(account) , 1);
                        this.store(keys.ACCOUNT_DETAILS.ACCOUNT_LIST_SUBSCRIBER, accountsSubscribers);
                        this.showAlert('La cuenta se ha eliminado con exito.',
                            () => {
                                this.ngOnInit();
                            });
                    },
                    error => {
                        this.dismissProgress();
                        this.showError(error.message);
                    }
                );
            },
            () => {
            }
        );
    }

    getSub(subscriber) {
        this.subscriberNow = subscriber;
    }

    changeAccount(e) {
        for (const account of this.accounts) {
            if (account === e) {
                if (this.selectedSection !== e) {
                    this.selectedSection = e;
                    this.selectedAccount = e;
                } else {
                    this.selectedSection = undefined;
                    this.selectedAccount = undefined;
                }
                break;
            }
        }
    }


}

import { Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import {Utils} from '../../../../utils/utils';
import {Account} from '../../../../models/authenticate';


@Component({
    selector: 'app-notification-preference',
    templateUrl: './notification-preference.component.html',
    styleUrls: ['./notification-preference.component.scss'],
})
export class NotificationPreferenceComponent extends BasePage implements OnInit {
    public typeOfTelephony;
    public accounts = [];
    public formatNumber;
    public selectedTab;
    public selectedAccount;
    public accountSections;
    public showBackBth;

    public subscribers = [];
    public accountInfo;
    public data;
    public selectedSection = '';
    public loaded = false;
    public updatenot = false;
    response;
    status;


    constructor(public router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('manage_notifications');
    }

    ngOnInit() {
        this.getInitDetails();
    }
    getInitDetails() {
        this.data = this.cacheStorage().loginData; console.log(this.data);
        this.accounts = this.data.accounts.AccountList;
        this.accountInfo = this.services.data.AccounInfo; console.log('account info es:'); console.log(this.accountInfo); // AccountDetails
        this.selectedAccount = this.cacheStorage().accountInfo.bANField;
        this.subscribers = []; // array a  llenar con data especifica tratada para mostrar el contenido html
        const subscriberList = this.services.data.SubscriberInfo; // array de subscribers del servidor o crudo.

        let subscriber = { subscriber: '', code: '', body: '', type: ''};
        for (const subscriberInfo of subscriberList) {
            // Esto es  para dar el  formato deseado a cada numero de suscriptor
            subscriber.subscriber = subscriberInfo.subscriberNumberField;
            subscriber.code = '(' + subscriberInfo.subscriberNumberField.substring(0, 3) + ')';
            subscriber.body = subscriberInfo.subscriberNumberField.substring(3, 6) + '-' + subscriberInfo.subscriberNumberField.substring(6, subscriberInfo.subscriberNumberField.lenght);

            const accountP: Account = this.cacheStorage().getAccountByBan(this.selectedAccount);
            subscriber.type = Utils.typeOfTelephony(subscriberInfo.productTypeField);

            // Añado al subcriber el formato y el plan detallado con los datos que me importa mostrar de momento:
            const a =  Object.defineProperty(subscriberInfo, 'fixedData', {
                value: subscriber
            });
            const listToShow = [];
            const b =  Object.defineProperty(a, 'listToShow', {
                value: listToShow
            });
            // Lleno array de subscribers con nuevo objeto:
            this.subscribers.push(b);
            // Limpio varables para el proximo elemento:
            subscriber = { subscriber: '', code: '', body: '', type: ''};
        }
        console.log(this.subscribers);
        if (this.subscribers.length === 1) {
            this.changeSubscriber(this.subscribers[0].subscriberNumberField, this.subscribers[0].fixedData.type);
            this.showingInfo(this.subscribers[0].subscriberNumberField);
        }
    }
    changeSubscriber(index, type) {
        this.response = null;
        this.status = null;
        this.updatenot = false;
        for (const subscriber of this.subscribers) {
            if (subscriber.subscriberNumberField === index) {
                let list = [];
                list = subscriber.listToShow;
                if (subscriber.listToShow.length === 0) {
                    this.showProgress();
                    this.services.getPersonalAlertsStatus(this.accountInfo.bANField + '',
                        subscriber.subscriberNumberField + '', this.cacheStorage().tokenSession).then(
                        (success) => {
                            console.log(success);
                            let htmlVars = {
                                classDisabled: false,
                                type: 'checkbox',
                                checked: false,
                                question: '',
                                data_question_id: null,
                                response: null,
                                status: null,
                            };
                            success.QuestionList.forEach((question) => {
                                let disabled = false;
                                if (question.Question.includes('impresa en papel')) {
                                    disabled = true;
                                }
                                const accountP: Account = this.cacheStorage().getAccountByBan(this.selectedAccount);
                                if ( type === 'Fijo'
                                    && question.Question.includes('SMS')) {
                                    disabled = true;
                                }
                                // Distribución de data obtenida
                                htmlVars.classDisabled = disabled;
                                htmlVars.checked = question.Checked;
                                htmlVars.question = question.Question;
                                htmlVars.data_question_id = question.QuestionID;
                                htmlVars.response = success.response;
                                htmlVars.status = success.status;
                                // insercion en array de subscribers:
                                subscriber.listToShow.push(htmlVars);
                                // limpieza de variable para el proximo item:
                                htmlVars = {
                                    classDisabled: false,
                                    type: 'checkbox',
                                    checked: false,
                                    question: '',
                                    data_question_id: null,
                                    response: null,
                                    status: null,
                                };
                            });

                            this.loaded = true;
                            this.dismissProgress();
                        },
                        error => {
                            this.dismissProgress();
                            this.showError(error.message);
                        }
                    );
                    break;
                }
            }
        }

    }
    updateNotToCall(subscriberNumberField, status) {
        this.services.updateNotToCall(
            this.accountInfo.bANField + '',
            subscriberNumberField + '',
            status, this.cacheStorage().tokenSession).then(
            (response) => {
                if (!response.HasError) {
                    this.showAlert('Se ha cambiado el estado de NO LLAMAR con exito.'); // TODO, revisar respuesta
                    response.status = 'Inactivar';
                    response.response = 'Este suscriptor se encuentra en la lista \'No Llamar\' con estatus de \'Inactivo\'. Su vigencia es entre 11/28/2018 a 11/27/2023';
                    this.response = response.response;
                    this.status = response.status;
                    this.updatenot = true;
                }
            },
            error => {
                this.showError(error.message);
            }
        );
    }
    changeValueCheck(subscriber, question) {
        let list = [];
        list = subscriber.listToShow;
        for (const item of list) {
            if (item.question === question) {
                item.checked = !item.checked;
                break;
            }
        }
    }
    updateAlerts(subscriber) {
        let list = [];
        list = subscriber.listToShow;
        let mvar = '';
        for (const item of list) {
            if (item.checked) {
                mvar = mvar + item.data_question_id + ':Y,';
            } else {
                mvar = mvar +  item.data_question_id + ':N,';
            }
        }
        if (mvar.length > 0) {
            this.showProgress();
            mvar = mvar.substring(0, mvar.length - 1);

            this.services.updateAlerts(
                this.accountInfo.bANField + '',
                subscriber.subscriberNumberField + '',
                mvar, this.cacheStorage().tokenSession).then(
                (response) => {
                    if (!response.HasError) {
                        this.dismissProgress();
                        this.showAlert('Se ha cambiado el estado de las notificaciónes con exito.');
                    }
                },
                error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }

    showingInfo(index) {
        if (this.selectedSection !== index) {
            this.selectedSection = index;
        } else {
            this.selectedSection = undefined;
        }
    }

}

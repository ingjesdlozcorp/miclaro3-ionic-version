import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController, IonInput } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import { BasePage } from '../../../base.page';
import { AccountInfo, SubscriberInfo } from '../../../../models/account.details';
import { Utils } from '../../../../utils/utils';
import { APP } from '../../../../utils/const/appConstants';
import { BrowserProvider } from '../../../../services/browser.provider';
import { keys } from '../../../../utils/const/keys';
import { PAYMENT_VERSION } from '../../../_shared/components/flash-payment/core/flash-payment-constants';
import { PaymentSupportService } from '../../../_shared/components/flash-payment/core/services/payment-support.service';


@Component({
    selector: 'app-module-recharge-confirm',
    templateUrl: './recharge-confirm.component.html',
    styleUrls: ['./recharge-confirm.component.scss'],
})
export class RechargeConfirmComponent extends BasePage implements OnInit {

    @ViewChild('otherAmount', {static: false}) inputAnotherAmount: IonInput;

    subscriber: SubscriberInfo = {};
    prepaidRecharge: any = {};
    productId: string = APP.PRODUCT_ID_RECHARGE_PREPAID;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private browserProvider: BrowserProvider,
                private paymentSupport: PaymentSupportService) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.subscriber = this.cacheStorage().getFullCurrentSelectedSubscriber();
        this.prepaidRecharge = this.cacheStorage().PaymentProcess;

    }

    getPrepaidBalance(): string {
        if (this.subscriber !== undefined) {
            return Utils.formatAmount(this.subscriber.prepaidBalanceField);
        }
        return '0.00';
    }

    async continue() {
        const userPaymentVersion = await this.fetch(keys.PAYMENT.PAYMENT_VERSION);
        if (userPaymentVersion && userPaymentVersion === PAYMENT_VERSION.P2P) {
            let typeMessage = 'payment';
            if (this.cacheStorage().isPrepaidAccount) {
                typeMessage = 'prepaid';
            }
            this.paymentSupport.paymentRefactorProcess(typeMessage, parseFloat(this.prepaidRecharge.totalAmount));
        } else {
            const accountInfo: AccountInfo = this.cacheStorage().accountInfo;
            const amount = `${parseFloat(this.prepaidRecharge.totalAmount)}`;
            const response = await this.paymentSupport.paymentExternalProcess(
                `${accountInfo.bANField}`,
                accountInfo.defaultSubscriberField, amount,
                {productId: this.productId}
            );
            this.verifyPayment(response);
        }
    }

    verifyPayment(response: any) {
        this.dismissProgress();
        if (response && !response.hasError) {
            if (response.processEndState) {
                if (response.provisioningStatus === APP.PROVISIONING_STATUS.PROVISIONED) {
                    this.cacheStorage().PaymentProcess.transactionResult = {
                        status: response.paymentStatus,
                        rechargeId: response.confirmationNumber,
                        transactionId: response.authorizationNumber,
                        date: new Date(new Date().getTime())
                    };
                    this.goPage('module/recharge/success');
                } else {
                    this.dismissProgress();
                    this.showAlert('Su pago no pude ser procesado.');
                }
            } else {
                this.showAlert('Su pago no fue recibido o aun se encuentra en proceso, por favor espere un momento mientras procesamos la información.',
                    () => {
                        this.goHomePage();
                    });
            }
        } else {
            this.showError(response.message);
        }
    }
}

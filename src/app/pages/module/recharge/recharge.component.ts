import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import {AlertController, IonInput} from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import {BasePage} from '../../base.page';
import {SubscriberInfo} from '../../../models/account.details';
import {Utils} from '../../../utils/utils';


@Component({
    selector: 'app-module-recharge',
    templateUrl: './recharge.component.html',
    styleUrls: ['./recharge.component.scss'],
})
export class RechargeComponent extends BasePage implements OnInit {

    @ViewChild('otherAmount', {static: false}) inputAnotherAmount: IonInput;

    subscriber: SubscriberInfo = {};
    prepaidAddress: any[] = [];
    prepaidPayments: any[] = [];
    prepaidProducts: any[] = [];
    planAmount = 0;
    selectedProduct;
    anotherAmount = '0.00';

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('recharge');
    }

    ngOnInit() {
        this.subscriber = this.cacheStorage().getFullCurrentSelectedSubscriber();
        this.planAmount = Utils.formatAmount(this.subscriber.planInfoField.socRateField);
        // this.planAmount = 15.5;
        this.getPrepaidProducts();
    }

    // getPrepaidAddress() {
    //     this.showProgress();
    //     const data = this.cacheStorage();
    //     this.services.listPrepaidAddress(this.subscriber.subscriberNumberField, data.tokenSession)
    //         .then(
    //             (response: any) => {
    //                 this.dismissProgress();
    //                 this.prepaidAddress = response.address;
    //                 this.getPrepaidPayments();
    //             },
    //             error => {
    //                 this.dismissProgress();
    //                 this.showError(error.message, () => {
    //                     this.goHomePage();
    //                 });
    //             }
    //         );
    // }
    //
    // getPrepaidPayments() {
    //     this.showProgress();
    //     const data = this.cacheStorage();
    //     this.services.listPrepaidPaymentsType(this.subscriber.subscriberNumberField, data.tokenSession)
    //         .then(
    //             (response: any) => {
    //                 this.dismissProgress();
    //                 this.prepaidPayments = response.formPaymentTypes;
    //                 this.getPrepaidProducts();
    //             },
    //             error => {
    //                 this.dismissProgress();
    //                 this.showError(error.message, () => {
    //                     this.goHomePage();
    //                 });
    //             }
    //         );
    // }

    getPrepaidProducts() {
        this.showProgress();
        const data = this.cacheStorage();
        this.services.listProductService(this.subscriber.subscriberNumberField, 2, data.tokenSession)
            .then(
                (response: any) => {
                    this.dismissProgress();
                    this.prepaidProducts = response.formProducts;
                    this.createPlanAmountProduct();
                },
                error => {
                    this.dismissProgress();
                    this.showError(error.message, () => {
                        this.goHomePage();
                    });
                }
            );
    }

    createPlanAmountProduct() {
        const planProduct = Object.assign({}, this.getOtherProduct());
        planProduct.amountRecharge = this.planAmount;
        planProduct.idProduct = 999;
        this.prepaidProducts.push(planProduct);
    }

    getPrepaidBalance(): string {
        if (this.subscriber !== undefined) {
            return Utils.formatAmount(this.subscriber.prepaidBalanceField);
        }
        return '0.00';
    }

    getProducts() {
        const products = [];
        this.prepaidProducts.forEach(product => {
            if (product.idProduct !== 24 && product.idProduct !== 999) {
                products.push(product);
            }
        });
        return products;
    }

    getOtherProduct() {
        let item;
        this.prepaidProducts.forEach(product => {
            if (product.idProduct === 24) {
                item = product;
            }
        });
        return item;
    }

    getPlanProduct() {
        let item;
        this.prepaidProducts.forEach(product => {
            if (product.idProduct === 999) {
                item = product;
            }
        });
        return item;
    }

    selectPlanAmount($event) {
        $event.stopPropagation();
        this.selectedProduct = this.getPlanProduct();
        this.anotherAmount = '0.00';
    }

    selectProduct($event, product) {
        $event.stopPropagation();
        this.selectedProduct = product;
        this.anotherAmount = '0.00';
    }

    clickAnotherAmount(event) {
        event.stopPropagation();
        this.selectedProduct = this.getOtherProduct();
        this.anotherAmount = '';
        this.inputAnotherAmount.setFocus().then();
    }

    canContinue(): boolean {
        return ((this.selectedProduct !== undefined && this.selectedProduct.idProduct !== 24) ||
            (this.selectedProduct !== undefined && this.selectedProduct.idProduct === 24 && parseFloat(this.anotherAmount) >= 5) && parseFloat(this.anotherAmount) <= 150);
    }

    continue() {
        this.getOtherProduct().amountRecharge = Number(this.anotherAmount);
        if (this.selectedProduct && Number(this.selectedProduct.amountRecharge) > 0) {
            const amount = Utils.formatAmount(this.selectedProduct.amountRecharge);
            const ivu = Utils.formatAmount(this.selectedProduct.amountRecharge * this.selectedProduct.ivuState);
            const totalAmount = Utils.formatAmount(Number(amount) + Number(ivu));
            this.cacheStorage().PaymentProcess = {
                subscriber: this.subscriber.subscriberNumberField,
                payments: this.prepaidPayments,
                products: this.prepaidProducts,
                address: this.prepaidAddress,
                selectedProduct: this.selectedProduct,
                amount,
                ivu,
                totalAmount,
            };
            this.goPage('module/recharge/confirm');
        }
    }
}

import {Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import {AlertController, IonInput} from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import {BasePage} from '../../../base.page';
import {SubscriberInfo} from '../../../../models/account.details';
import {Utils} from '../../../../utils/utils';
import {APP} from '../../../../utils/const/appConstants';


@Component({
    selector: 'app-module-recharge-success',
    templateUrl: './recharge-success.component.html',
    styleUrls: ['./recharge-success.component.scss'],
})
export class RechargeSuccessComponent extends BasePage implements OnInit {

    prepaidRecharge: any = {};

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.prepaidRecharge = this.cacheStorage().PaymentProcess;
        this.showProgress();
        this.reloadCurrentAccount().then(
            (response: any) => {
                this.dismissProgress();
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }
}

import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController, IonContent } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import {BasePage} from '../../../base.page';
import {Utils} from '../../../../utils/utils';
import {SubscriberInfo} from '../../../../models/account.details';

const ERROR_EMPTY_TRANSACTIONS = 'Estimado cliente no existen registros de acuerdo a su busqueda.';

@Component({
    selector: 'app-module-recharge-history-v2',
    templateUrl: './transactions-v2.component.html',
    styleUrls: ['./transactions-v2.component.scss'],
})
export class TransactionsV2Component extends BasePage implements AfterViewInit {

    @ViewChild('content', { static: false }) private content: IonContent;
    @ViewChildren('scrollableItem') containerTransactions: QueryList<ElementRef>;

    formatSubscriber = Utils.formatSubscriber;
    typeOfTelephony = Utils.typeOfTelephony;
    formatAmount = Utils.formatAmount;
    subscribers: SubscriberInfo[] = [];
    selectedSubscriber: SubscriberInfo = undefined;
    isIOS = Utils.getPlatformInfo().ios;

    transactions: any[] = [];
    errorMessage = '';

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('transactions_prepaid');
    }

    ngAfterViewInit() {
        this.subscribers = this.cacheStorage().subscriberList;
        if (this.subscribers.length === 1) {
            this.showDetails(this.subscribers[0]);
        }
    }

    showDetails(subscriber: SubscriberInfo) {
        if (this.selectedSubscriber === subscriber) {
            this.selectedSubscriber = undefined;
        } else {
            this.selectedSubscriber = subscriber;
            this.search();
        }
    }

    search() {
        this.showProgress();
        this.transactions = [];
        this.errorMessage = '';
        const data: IntentProvider = this.cacheStorage();
        this.services.getPrepaidTransactions(
            this.selectedSubscriber.subscriberNumberField,
            data.tokenSession).then(
                (response: any) => {
                    this.dismissProgress();
                    this.transactions = response.replenishListItemsField;
                    if (this.transactions === undefined || this.transactions == null || this.transactions.length === 0) {
                        this.errorMessage = ERROR_EMPTY_TRANSACTIONS;
                    }
                    setTimeout(() => {
                        this.containerTransactions.toArray()[this.subscribers.indexOf(this.selectedSubscriber)]
                            .nativeElement.scrollIntoView();
                    }, 300);
                },
                error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
    }
}

import {Component, ElementRef, OnInit, QueryList, ViewChildren} from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import {AlertController} from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import {BasePage} from '../../base.page';
import {Utils} from '../../../utils/utils';
import {SubscriberInfo} from '../../../models/account.details';

const ERROR_EMPTY_TRANSACTIONS = 'Estimado cliente no existen registros de acuerdo a su busqueda.';

/**
 * This versions is not in use
 */

@Component({
    selector: 'app-module-recharge-history',
    templateUrl: './transactions.component.html',
    styleUrls: ['./transactions.component.scss'],
})
export class TransactionsComponent extends BasePage implements OnInit {

    @ViewChildren('scrollableItem') scrollableItem: QueryList<ElementRef>;

    formatSubscriber = Utils.formatSubscriber;
    typeOfTelephony = Utils.typeOfTelephony;
    formatAmount = Utils.formatAmount;
    formatDate = Utils.formatStringDate;
    subscribers: SubscriberInfo[] = [];
    selectedSubscriber: SubscriberInfo = undefined;

    months = Utils.getCardMonths();
    years = Utils.getLast10Years();
    selectedMonth = Utils.getCurrentMonth();
    selectedYear = Utils.getCurrentYear();
    status = ['Estatus', 'Aprobada', 'Rechazada', 'Error'];
    types = ['Transacciones', 'Recarga de Prepago', 'Transaferencia de Dinero', 'Cambio de Plan'];
    selectedStatus = 'Estatus';
    selectedType = 'Transacciones';

    transactions: any[] = [];
    errorMessage = '';

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('transactions_prepaid');
    }

    ngOnInit() {
        this.subscribers = this.cacheStorage().subscriberList;
        if (this.subscribers.length === 1) {
            this.showDetails(this.subscribers[0]);
        }
    }

    showDetails(subscriber: SubscriberInfo) {
        if (this.selectedSubscriber === subscriber) {
            this.selectedSubscriber = undefined;
        } else {
            this.selectedSubscriber = subscriber;
        }
    }

    search() {
        this.showProgress();
        this.transactions = [];
        this.errorMessage = '';
        const data: IntentProvider = this.cacheStorage();
        this.services.prepaidHistory(
            String(data.accountInfo.bANField),
            this.selectedSubscriber.subscriberNumberField,
            data.prepaidCustomerId,
            0,
            parseFloat(this.selectedYear),
            parseFloat(this.selectedMonth),
            this.selectedStatus,
            this.selectedType,
            data.tokenSession).then(
                (response: any) => {
                    this.dismissProgress();
                    this.transactions = response.formTransactions;
                    if (this.transactions === undefined || this.transactions == null || this.transactions.length === 0) {
                        this.errorMessage = ERROR_EMPTY_TRANSACTIONS;
                    }
                    setTimeout(() => {
                        this.scrollableItem.toArray()[this.subscribers.indexOf(this.selectedSubscriber)]
                            .nativeElement.scrollIntoView();
                    }, 300);
                },
                error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
    }
}

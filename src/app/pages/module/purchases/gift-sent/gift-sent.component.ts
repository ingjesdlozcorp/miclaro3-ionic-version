import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {ServicesProvider} from '../../../../services/services.provider';
import {AlertController} from '@ionic/angular';
import {UtilsService} from '../../../../services/utils.service';
import {IntentProvider} from '../../../../services/intent.provider';
import {BasePage} from '../../../base.page';
import { keys } from 'src/app/utils/const/keys';

@Component({
    selector: 'app-module-gift-sent',
    templateUrl: './gift-sent.component.html',
    styleUrls: ['./gift-sent.component.scss'],
})
export class GiftSentComponent extends BasePage implements OnInit {

    public text;
    typeOfTelephony;
    accounts;
    selectedAccount;
    data;
    accountInfo;

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.data = this.cacheStorage();
        this.accounts = this.data.accountList;
        this.accountInfo = this.data.accountInfo; console.log(this.accountInfo);
        this.selectedAccount = this.cacheStorage().accountInfo.bANField;
    }

    ngOnInit() {
        this.getSentText();
    }

    getSentText() {
        this.fetch(keys.PAYMENT.GIFT_SENT_TEXT).then(
            result => {
                this.text = result;
            }
        );
    }
    close() {
        this.goHomePage();
    }

}

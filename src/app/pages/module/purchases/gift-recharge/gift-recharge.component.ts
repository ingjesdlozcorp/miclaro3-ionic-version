import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../../services/utils.service';
import { IntentProvider } from '../../../../services/intent.provider';
import { BasePage } from '../../../base.page';
import { keys } from 'src/app/utils/const/keys';
import { Utils } from '../../../../utils/utils';
import { APP } from '../../../../utils/const/appConstants';
import { BrowserProvider } from '../../../../services/browser.provider';
import { PAYMENT_VERSION } from '../../../_shared/components/flash-payment/core/flash-payment-constants';
import { AccountInfo } from '../../../../models/account.details';
import { PaymentSupportService } from '../../../_shared/components/flash-payment/core/services/payment-support.service';

@Component({
    selector: 'app-module-gift-recharge',
    templateUrl: './gift-recharge.component.html',
    styleUrls: ['./gift-recharge.component.scss'],
})
export class GiftRechargeComponent extends BasePage implements OnInit {
    public accounts;
    public selectedTab;
    public selectedAccount;
    public accountSections;
    public accountInfo;
    data;

    public accept = false;
    public terms = false;
    public subscriberTo = '';
    public accountTo;
    public text = '';
    public amount = 0;
    productId: string = APP.PRODUCT_ID_RECHARGE_PREPAID;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private browserProvider: BrowserProvider,
                private paymentSupport: PaymentSupportService) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('gift_recharge');
        this.data = this.cacheStorage();
        this.accounts = this.data.accountList;
        this.accountInfo = this.data.accountInfo;
        this.selectedAccount = this.cacheStorage().loginData;
    }

    ngOnInit() {
    }

    validateInput(param) {
        this.subscriberTo = '' + this.subscriberTo;
        if (param === 1) {
            if (this.subscriberTo.length > 10) {
                this.subscriberTo = this.subscriberTo.slice(0, 10);
            }
        } else {
            if (this.text.length > 200) {
                this.text = this.text.slice(0, 200);
            }
        }
    }

    validateData() {
        const check = this.accept;

        if (!check) {
            this.showError('Debe seleccionar los términos y condiciones para poder continuar.');
        } else if (this.subscriberTo.length === 0) {
            this.showError('Debe ingresar el numero al que desea recaragar.');
        } else if (this.subscriberTo.length !== 10) {
            this.showError('Debe ingresar un numero valido.');
        } else if (this.text.length > 200) {
            this.showError('El mensaje no puede contener mas de 200 caracteres.');
        } else if (this.amount === 0) {
            this.showError('Por favor seleccione el monto de la recarga.');
            return;
        } else {
            this.getSubscriber(this.subscriberTo, this.text, this.amount);
        }
    }

    getSubscriber(num, message, amount) {
        this.showProgress();
        this.services.getSubscriber(num, this.cacheStorage().tokenSession).then(
            (success) => {
                const accountTo = success.Account;
                if (accountTo === '' || accountTo == null) {
                    this.showError('El número de teléfono ingresado no se encuentra registrado' +
                        ' en nuestros sistemas, su formato es incorrecto o no pertenece a nuestra red.'
                    );
                    this.dismissProgress();
                } else {
                    if (Utils.isPrepaid(success.AccountType, success.AccountSubType)) {
                        this.accountTo = accountTo;
                        this.validateCredit(0, amount, accountTo, message, num);
                    } else {
                        this.dismissProgress();
                        this.showError('El número de teléfono ingresado no es prepago.');
                    }
                }
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    validateCredit(accountTotalRent, productPrice, accountTo, message, num) {
        const self = this;
        // to this service:
        const accountInfo = this.accountInfo;
        const account = String(accountInfo.bANField);
        productPrice = parseFloat(productPrice);
        productPrice = String(productPrice);
        // to next services:

        this.services.validateCreditLimit(account, accountTotalRent, productPrice, this.cacheStorage().tokenSession).then(
            success => {

                const data = self.createParameters(accountTo, productPrice, message, num);
                const availableCredit = parseFloat(success.AvailableCredit).toFixed(2);
                const subscriberHasAvailableCredit = parseFloat(availableCredit) >= parseFloat(productPrice);
                if (subscriberHasAvailableCredit) {
                    self.sendRecharge(data);
                } else {
                    self.goToPaymentStep(data);
                }

            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    goToPaymentStep(data) {
        const self = this;

        this.dismissProgress();

        const confirmMessage = 'Estimado cliente esta recarga no califica ' +
            'para cargo en factura, debe efectuar el pago de la misma usando su tarjeta de crédito.';

        this.showConfirm('Pago con tarjeta de crédito', confirmMessage, () => {
            const details = [
                {
                    name: 'Producto',
                    description: 'Regala una Recarga de $' + Utils.formatAmount(data.amount)
                }
            ];
            const accountInfo = this.accountInfo;

            const selectedAccount = this.selectedAccount;

            this.store(keys.PAYMENT.PAYMENT_DETAILS, details);
            this.store(keys.PAYMENT.PAYMENT_AMOUNT, String(self.amount));
            this.store(keys.PAYMENT.PAYMENT_SUBSCRIBER, selectedAccount.DefaultSubscriber);
            this.store(keys.PAYMENT.PAYMENT_SUBSCRIBER_TYPE, selectedAccount.mProductType);
            this.store(keys.PAYMENT.PAYMENT_ACCOUNT, accountInfo.bANField);
            this.store(keys.PAYMENT.PAYMENT_EMAIL, accountInfo.emailField);
            this.store(keys.PAYMENT.PAYMENT_DESCRIPTION, 'Regala una Recarga');
            // this.store(keys.PAYMENT.PAYMENT_TYPE, self.PAY.SEND_RECHARGE);

            this.store(keys.PAYMENT.PAYMENT_GIFT_RECHARGE, data);
            /*
            app.router.navigate('payment_step_1', {trigger: true});
            */
            this.payment();
        });
    }

    createParameters(accountTo, amount, message, subscriberTo) {

        const selectedAccount = this.selectedAccount;
        const nameSender = this.cacheStorage().loginData.username;

        const accountSender = this.cacheStorage().accountInfo.bANField;
        const accountReceiver = String(accountTo);

        const subscriberSender = this.cacheStorage().accountInfo.defaultSubscriberField;
        const subscriberReceiver = String(subscriberTo);

        return {
            UserName: nameSender,
            accountFrom: accountSender,
            accountTo: accountReceiver,
            amount: Utils.formatAmount(amount),
            message,
            source: 'mobile',
            subscriberFrom: subscriberSender,
            subscriberTo: subscriberReceiver
        };
    }

    sendRecharge(data) {
        const self = this;
        self.services.sendGiftRecharge(data, this.cacheStorage().tokenSession).then(
            (success) => {
                this.store(keys.PAYMENT.GIFT_SENT_TEXT,
                    'Gracias: La recarga se ha enviado con');
                this.dismissProgress();
                this.goPage('module/gift/sent');
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    showTerms() {
        this.terms = true;
    }

    closeTerms() {
        this.terms = false;
    }

    return() {
        this.goHomePage();
    }

    async payment() {
        const userPaymentVersion = await this.fetch(keys.PAYMENT.PAYMENT_VERSION);
        if (userPaymentVersion && userPaymentVersion === PAYMENT_VERSION.P2P) {
            let typeMessage = 'payment';
            if (this.cacheStorage().isPrepaidAccount) {
                typeMessage = 'prepaid';
            }
            this.paymentSupport.paymentRefactorProcess(typeMessage, parseFloat(this.cacheStorage().selectedPacket.price));
        } else {
            const accountInfo: AccountInfo = this.cacheStorage().accountInfo;
            const response = await this.paymentSupport.paymentExternalProcess(
                `${accountInfo.bANField}`,
                this.subscriberTo,
                `${parseFloat(this.cacheStorage().selectedPacket.price)}`,
                {productId: this.productId}
            );
            this.verifyPayment(response);
        }
    }

    verifyPayment(response: any) {
        this.dismissProgress();
        if (response && !response.hasError) {
            if (response.processEndState) {
                if (response.provisioningStatus === APP.PROVISIONING_STATUS.PROVISIONED) {
                    this.cacheStorage().PaymentProcess = {};
                    this.cacheStorage().PaymentProcess.transactionResult = {
                        status: response.paymentStatus,
                        rechargeId: response.confirmationNumber,
                        transactionId: response.authorizationNumber,
                        date: new Date(new Date().getTime())
                    };
                    this.store(keys.PAYMENT.GIFT_SENT_TEXT, 'Gracias: La recarga se ha enviado con');
                    this.dismissProgress();
                    this.goPage('module/gift/sent');
                } else {
                    this.dismissProgress();
                    this.showAlert('Su pago no pudo ser procesado.');
                }
            } else {
                this.showAlert('Su pago no fue recibido o aun se encuentra en proceso, por favor espere un momento mientras procesamos la información.',
                    () => {
                        this.goHomePage();
                    });
            }
        } else {
            this.showError(response.message);
        }
    }
}

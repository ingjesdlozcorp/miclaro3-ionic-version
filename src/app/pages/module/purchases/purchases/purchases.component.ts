import { Component, OnInit } from '@angular/core';
import { BasePage } from '../../../base.page';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'app-module-purchases',
    templateUrl: './purchases.component.html',
    styleUrls: ['./purchases.component.scss'],
})
export class PurchasesComponent extends BasePage implements OnInit {

    isTelephony = false;

    constructor(public router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('purchases');
    }

    ngOnInit() {
        this.isTelephony = this.cacheStorage().isTelephonyAccount;
    }

    go(param) {
        if (param < 4) {
            const routes = [
                'gift/recharge',
                'data-plan',
                'gift/one-gb',
                'service-additional'
            ];
            this.goPage('module/' + routes[param]);
        } else {
            this.openStore();
        }

    }
}

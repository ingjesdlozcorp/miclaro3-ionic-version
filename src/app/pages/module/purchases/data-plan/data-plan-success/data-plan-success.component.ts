import { Component, OnInit } from '@angular/core';
import {Utils} from '../../../../../utils/utils';
import {SubscriberInfo} from '../../../../../models/account.details';
import {BasePage} from '../../../../base.page';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {ServicesProvider} from '../../../../../services/services.provider';
import {AlertController} from '@ionic/angular';
import {UtilsService} from '../../../../../services/utils.service';
import {IntentProvider} from '../../../../../services/intent.provider';
import {Packet} from '../data-plan.component';

@Component({
    selector: 'app-module-data-plan-success',
    templateUrl: './data-plan-success.component.html',
    styleUrls: ['./data-plan-success.component.scss'],
})
export class DataPlanSuccessComponent extends BasePage implements OnInit {

    formatSubscriber = Utils.formatSubscriber;
    typeOfTelephony = Utils.typeOfTelephony;
    subscriber: SubscriberInfo;
    selectedPacket: Packet;
    orderId = '';

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.orderId = this.cacheStorage().orderInfo.id;
        this.subscriber = this.cacheStorage().selectedSubscriber;
        this.selectedPacket = this.cacheStorage().selectedPacket;
    }

}

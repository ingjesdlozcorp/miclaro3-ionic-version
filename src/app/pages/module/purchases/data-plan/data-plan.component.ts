import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../services/services.provider';
import { AlertController, IonSlides } from '@ionic/angular';
import { UtilsService } from '../../../../services/utils.service';
import { IntentProvider } from '../../../../services/intent.provider';
import { BasePage } from '../../../base.page';
import { Utils } from '../../../../utils/utils';
import { AccountInfo, SubscriberInfo } from '../../../../models/account.details';
import { PAY, Payment } from '../../../../models/payment';
import { APP } from '../../../../utils/const/appConstants';
import { BrowserProvider } from '../../../../services/browser.provider';
import { keys } from '../../../../utils/const/keys';
import { PAYMENT_VERSION } from '../../../_shared/components/flash-payment/core/flash-payment-constants';
import { PaymentSupportService } from '../../../_shared/components/flash-payment/core/services/payment-support.service';

const ERROR_GETTING_PACKETS = 'En estos momentos no tenemos Paquetes Adicionales de Datos para este suscriptor.';
const ESPECIAL_PACKAGE = 'DIARIO10';
export interface Packet {
    name: string;
    price: string;
    id: string;
}

@Component({
    selector: 'app-module-data-plan',
    templateUrl: './data-plan.component.html',
    styleUrls: ['./data-plan.component.scss'],
})
export class DataPlanComponent extends BasePage implements OnInit {

    @ViewChildren('slides') slides: QueryList<IonSlides>;

    formatSubscriber = Utils.formatSubscriber;
    typeOfTelephony = Utils.typeOfTelephony;
    subscribers: SubscriberInfo[] = [];
    selectedSubscriber: SubscriberInfo = undefined;
    accountInfo: AccountInfo = undefined;

    packetsSuggested: Packet[] = [];
    errorMessage = '';

    slideOpts = {
        loop: true
    };
    isBusiness = false;

    productId: string = APP.PRODUCT_ID_PURCHASE_DATA;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private browserProvider: BrowserProvider,
                private paymentSupport: PaymentSupportService) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('data_plan_packages');
        this.isBusiness = this.cacheStorage().isBusiness();
    }

    ngOnInit() {
        this.subscribers = this.cacheStorage().subscriberList;
        this.accountInfo = this.cacheStorage().accountInfo;
        if (this.subscribers.length === 1) {
            this.showDetails(this.subscribers[0]);
        }
    }

    showDetails(subscriber: SubscriberInfo) {
        if (this.selectedSubscriber === subscriber) {
            this.selectedSubscriber = undefined;
        } else {
            this.selectedSubscriber = subscriber;
            this.getDataPackets(subscriber);
        }
    }

    getDataPackets(subscriber: SubscriberInfo) {
        this.showProgress();
        this.packetsSuggested = [];
        const groupID = subscriber.groupIDField == null ? '' : subscriber.groupIDField;
        const transactionId = '12345'; // TODO, should will be change
        this.services.getDataPackets(
            groupID,
            transactionId,
            subscriber.subscriberNumberField,
            this.cacheStorage().tokenSession,
            this.applyForRoaming(this.accountInfo.accountTypeField, this.accountInfo.accountSubtypeField, subscriber.productTypeField) ? 'DIARIO10' : undefined,
        ).then(
            (success: any) => {
                this.dismissProgress();
                success.Offers.forEach((item: any) => {
                    this.packetsSuggested.push({
                        name: item.DisplayName,
                        price: Utils.formatAmount(item.DPrice),
                        id: item.OfferId
                    });
                });
                if (success.Offers === undefined || success.Offers === null || success.Offers.length === 0) {
                    this.errorMessage = ERROR_GETTING_PACKETS;
                }
            }, error => {
                this.dismissProgress();
                if (error.withStatus200) {
                    this.errorMessage = error.message;
                } else {
                    this.errorMessage = ERROR_GETTING_PACKETS;
                }
            });
    }

    applyForRoaming(accountType, accountSubType, productType) {
        let contains = false;
        if ((accountType === 'I2' && accountSubType === '4') ||
            (accountType === 'I3' && accountSubType === '4') ||
            (accountType === 'I' && accountSubType === 'S') ||
            (accountType === 'I' && accountSubType === 'W')) {
            if (productType === 'G') {
                contains = true;
            }
        }
        return contains;
    }

    prev(index: number) {
        this.slides.toArray()[index].slidePrev().then();
    }

    next(index: number) {
        this.slides.toArray()[index].slideNext().then();
    }

    getMainPacketName(subscriber: SubscriberInfo): string {
        const usageActiveSubscriber = subscriber.usageInfoField;
        if (usageActiveSubscriber.dataOffersField != null && usageActiveSubscriber.dataOffersField.length > 0) {
            const plan = usageActiveSubscriber.dataOffersField[0];
            return plan.displayNameField;
        }
        return '';
    }

    getMainPacketConsumption(subscriber: SubscriberInfo): string {
        const usageActiveSubscriber = subscriber.usageInfoField;
        if (usageActiveSubscriber.dataOffersField != null && usageActiveSubscriber.dataOffersField.length > 0) {
            const plan = usageActiveSubscriber.dataOffersField[0];
            return plan.usedTextField;
        }
        return '';
    }

    getMainPacketConsumptionFrom(subscriber: SubscriberInfo): string {
        const usageActiveSubscriber = subscriber.usageInfoField;
        if (usageActiveSubscriber.dataOffersField != null && usageActiveSubscriber.dataOffersField.length > 0) {
            const plan = usageActiveSubscriber.dataOffersField[0];
            if (plan.displayNameField.includes('PUJ')
                || plan.displayNameField.includes('Ilimitado')
                || plan.displayNameField.includes('Unlimited')) {
                return '';
            } else {
                return 'de ' + plan.quotaTextField;
            }
        }
        return '';
    }

    getMainPacketRemain(subscriber: SubscriberInfo): string {
        const usageActiveSubscriber = subscriber.usageInfoField;
        if (usageActiveSubscriber.dataOffersField != null && usageActiveSubscriber.dataOffersField.length > 0) {
            const plan = usageActiveSubscriber.dataOffersField[0];
            const remain = parseFloat(plan.quotaField) - parseFloat(plan.usedField);
            return Utils.transformAvailable(remain);
        }
        return '';
    }

    getMainPacketPercentageUsage(subscriber: SubscriberInfo): string {
        const usageActiveSubscriber = subscriber.usageInfoField;
        if (usageActiveSubscriber.dataOffersField != null && usageActiveSubscriber.dataOffersField.length > 0) {
            const plan = usageActiveSubscriber.dataOffersField[0];
            let usagePercentage = Math.round(100.0 * (parseFloat(plan.usedField) / parseFloat(plan.quotaField)));
            if (usagePercentage === 0 && parseFloat(plan.usedField) > 0) {
                usagePercentage = 1;
            }
            return 'p' + usagePercentage;
        }
        return 'p0';
    }

    buyPacket(subscriber: SubscriberInfo, offer: Packet) {
        const transactionId = '12345'; // TODO, should will be change
        if (offer.id === '') {
            this.showError('Debe seleccionar algun paquete.');
        } else {
            this.showProgress();
            this.services.getReadSubscriber(
                subscriber.subscriberNumberField,
                transactionId,
                this.cacheStorage().tokenSession
            ).then(
                (success: any) => {
                    this.dismissProgress();
                    let sw = 0;
                    success.Offers.forEach((currentOffer: any) => {
                        if (currentOffer.OfferGroupOrder !== '1') { /* excluding basic offer */
                            if (currentOffer.Balances[0].BalanceAmount === 0 && (/ROAM/.test(currentOffer.OfferGroup)) === false) {
                                sw = 1;
                            }
                        }
                    });
                    if (sw === 0) {
                        if (this.cacheStorage().isBusiness()) {
                            this.goPaymentConfirm(subscriber, offer);
                        } else {
                            this.validateCreditLimit(subscriber, offer);
                        }
                    } else {
                        this.showError('Ya cuentas con un paquete de data adicional en espera de uso.');
                    }
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                });
        }
    }

    validateCreditLimit(subscriber: SubscriberInfo, offer: Packet) {
        this.showProgress();
        this.services.validateCreditLimit(
            String(this.accountInfo.bANField),
            String(this.accountInfo.billBalanceField).replace('CR', ''),
            offer.price,
            this.cacheStorage().tokenSession
        ).then(
            (success: any) => {
                this.dismissProgress();
                const availableCredit = parseFloat(success.AvailableCredit.replace('$', '')).toFixed(2);
                const offerPrice = parseFloat(offer.price.replace('$', '')).toFixed(2);

                const subscriberHasAvailableCredit = parseFloat(availableCredit) >= parseFloat(offerPrice);
                if (subscriberHasAvailableCredit) {
                    this.supplyConfirmation(subscriber, offer);
                } else {
                    this.goPaymentConfirm(subscriber, offer);
                }
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
    }

    supplyConfirmation(subscriber: SubscriberInfo, offer: Packet) {
        const message = 'Estimado Cliente: El paquete seleccionado será agregado con cargo ' +
            'a su próxima factura. ¿Está seguro de agregar el paquete: ' +
            offer.name + ' ?';
        this.showConfirm('Aviso', message,
            () => {
                this.supply(subscriber, offer);
            },
            () => {
                // nothing to do
            });
    }

    supply(subscriber: SubscriberInfo, offer: Packet) {
        if (offer.id !== ESPECIAL_PACKAGE) {
            this.addOfferProcess(subscriber, offer);
        } else {
            this.processForPackageTen(subscriber, offer);
        }
    }

    addOfferProcess(subscriber, offer) {
        this.showProgress();
        const transactionId = '12345'; // TODO, should will be change
        this.services.addOfferToSubscriber(
            transactionId,
            1 + subscriber.subscriberNumberField,
            offer.id,
            '1',
            this.accountInfo.billCycleField,
            '',
            this.cacheStorage().loginData.username,
            this.cacheStorage().tokenSession
        ).then(
            (success: any) => {
                this.dismissProgress();
                this.cacheStorage().selectedPacket = offer;
                this.cacheStorage().selectedSubscriber = subscriber;
                this.cacheStorage().orderInfo = {
                    id: success.PCRFTransaID,
                    message: success.Description
                };
                this.goPage('module/data-plan/success');
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
    }

    processForPackageTen(subscriber, offer) {
        this.showProgress();
        subscriber.planInfoField.commitmentStartDateField = this.formatDateToPackageTen(subscriber.planInfoField.commitmentStartDateField);
        subscriber.planInfoField.commitmentEndDateField = this.formatDateToPackageTen(subscriber.planInfoField.commitmentEndDateField);
        subscriber.planInfoField.effectiveDateField = this.formatDateToPackageTen(subscriber.planInfoField.effectiveDateField);
        const payload = {
            token: this.cacheStorage().tokenSession,
            ban: btoa(this.cacheStorage().accountDetails.AccounInfo.bANField.toString()),
            productType: btoa(subscriber.productTypeField),
            subscriber: btoa(subscriber.subscriberNumberField),
            oldSocCode: btoa(subscriber.sOCInfoField),
            socCode: btoa(offer.id),
            commitmentStartDate: btoa(subscriber.planInfoField.commitmentStartDateField),
            expirationDate: btoa(subscriber.planInfoField.commitmentEndDateField),
            effectiveDate: btoa(subscriber.planInfoField.effectiveDateField),
            relatedSocCode: btoa(subscriber.planInfoField.socRateField),
            method: 'updateSubscriberPricePlanSocsDIARIO10'
        };
        this.services.addPackageTen(payload).then(
            (success: any) => {
                this.dismissProgress();
                this.cacheStorage().selectedPacket = offer;
                this.cacheStorage().selectedSubscriber = subscriber;
                this.cacheStorage().orderInfo = {
                    id: 'N/A',
                    message: 'Se ha agregado el Paquete Adicional de Data'
                };
                this.goPage('module/data-plan/success');
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    formatDateToPackageTen(dateString) {
        let ini = '';
        if (!!dateString && dateString !== '0') {
            ini = dateString.split('/');
            dateString = ini[2] + '-' + ini[1] + '-' + ini[0];
        } else {
            dateString = '';
        }
        return dateString;
    }

    goPaymentConfirm(subscriber: SubscriberInfo, offer: Packet) {
        const message = 'Estimado cliente el paquete de data adicional seleccionado no califica ' +
            'para cargo en factura, por favor seleccione un paquete de menor costo o continúe su ' +
            'compra efectuando el pago inmediato usando sus tarjeta de crédito.';

        this.showConfirm('Pago con tarjeta de crédito', message,
            () => {
                this.openPaymentStep(subscriber, offer);
            },
            () => {
                // nothing to do
            });
    }

    openPaymentStep(subscriber: SubscriberInfo, offer: Packet) {
        const details = [
            {
                name: 'Paquete',
                description: offer.name
            }
        ];

        const data = {
            TransactionId: '123456',
            SubscriberId: '1' + subscriber.subscriberNumberField,
            OfferId: offer.id,
            Charge: '0',
            Cicle: this.accountInfo.billCycleField,
            paymentID: '',
            UserID: this.cacheStorage().loginData.username,
        };

        this.cacheStorage().payment = {
            data,
            details,
            amount: offer.price,
            subscriber: subscriber.subscriberNumberField,
            subscriberType: subscriber.productTypeField,
            account: String(this.accountInfo.bANField),
            email: this.accountInfo.emailField,
            description: offer.name,
            type: PAY.ADDITIONAL_DATA
        };

        this.cacheStorage().selectedPacket = offer;

        this.payment();
    }

    async showTerms(onDismiss?) {
        const alert = await this.getAlert().create({
            header: 'Términos y Condiciones',
            message:
                '<div class="termr-t">\n' +
                '\t\t\t\t\t\t\tLos Paquetes Adicionales de Datos pueden ser comprados por clientes de Banda Ancha y clientes de Bundle de Voz con Internet Móvil.\n' +
                '\n' +
                '\t\t\t\t\t\t\tEl paquete seleccionado será agregado con cargo a la próxima factura, siempre y cuando la cuenta no haya llegado a su límite de crédito, ' +
                'de lo contrario el pago deberá ser realizado al momento con cargo a una tarjeta de crédito.\n' +
                '\n' +
                '\t\t\t\t\t\t\tCaducidad: Tienes hasta fin de ciclo de facturación/mes para usarlo ó se expira si hay remanente.\n' +
                '\n' +
                '\t\t\t\t\t\t\tNo se podrá comprar paquetes de datos adicionales antes de que tu data haya sido bloqueada/reducida.\n' +
                '\n' +
                '\t\t\t\t\t\t\tSon para uso local en Puerto Rico y en Estados Unidos.\n' +
                '\n' +
                '\t\t\t\t\t\t</div>',
            buttons: [
                {
                    text: 'Cerrar',
                    role: 'cancel',
                    handler: () => {
                        if (onDismiss) {
                            onDismiss();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    async payment() {
        const userPaymentVersion = await this.fetch(keys.PAYMENT.PAYMENT_VERSION);
        if (userPaymentVersion && userPaymentVersion === PAYMENT_VERSION.P2P) {
            let typeMessage = 'payment';
            if (this.cacheStorage().isPrepaidAccount) {
                typeMessage = 'prepaid';
            }
            this.paymentSupport.paymentRefactorProcess(typeMessage, parseFloat(this.cacheStorage().selectedPacket.price));
        } else {
            const accountInfo: AccountInfo = this.cacheStorage().accountInfo;
            const response = await this.paymentSupport.paymentExternalProcess(
                `${accountInfo.bANField}`,
                this.selectedSubscriber.subscriberNumberField,
                `${parseFloat(this.cacheStorage().selectedPacket.price)}`,
                {productId: this.productId, merchantId: this.cacheStorage().selectedPacket.id}
            );
            this.verifyPayment(response);
        }
    }

    verifyPayment(response: any) {
        this.dismissProgress();
        if (response && !response.hasError) {
            if (response.paymentStatus === APP.BANK_PAYMENT_SUCCESS) {
                this.cacheStorage().selectedSubscriber = this.selectedSubscriber;
                this.cacheStorage().orderInfo = {
                    id: response.confirmationNumber,
                    message: response.paymentStatus
                };
                this.dismissProgress();
                this.goPage('module/data-plan/success');
            } else {
                this.showAlert('Su pago fue recibido, favor esperar un momento mientras procesamos el mismo.',
                    () => {
                        this.goHomePage();
                    });
            }
        } else {
            this.showError(response.message);
        }
    }
}












































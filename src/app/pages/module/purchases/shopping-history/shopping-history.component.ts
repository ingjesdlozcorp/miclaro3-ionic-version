import { Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import { keys } from 'src/app/utils/const/keys';

@Component({
    selector: 'app-module-shopping-history',
    templateUrl: './shopping-history.component.html',
    styleUrls: ['./shopping-history.component.scss'],
})
export class ShoppingHistoryComponent extends BasePage implements OnInit {

    public showOrders;
    public subscribersHistory;
    public accounts = [];
    public selectedTab;
    public selectedAccount;
    public accountSections;

    public data;
    public accountInfo;
    public listHistory = [];
    public count = 0;
    load = false;
    public hasPackages;
    public packageShowItems;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('shopping_history');
        this.store(keys.ORDERS.IS_LOADED, false);
        this.data = this.cacheStorage();
        this.accounts = this.data.accountList;
        this.accountInfo = this.data.accountInfo;
        this.selectedAccount = this.cacheStorage().accountInfo.bANField;

    }

    ngOnInit() {
        this.getOrders();
    }

    getOrders() {
        this.showProgress();
        this.services.getHistoryOrders(this.accountInfo.bANField, this.cacheStorage().tokenSession).then(
            (response) => {
                this.dismissProgress();
                this.listHistory = response.Subscribers;
                this.updateCount();
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    updateCount() {
        this.count = 0;
        if (this.listHistory != null && this.listHistory !== undefined && this.listHistory.length > 0) {
            this.listHistory.forEach((subscriber) => {
                this.count += subscriber.Packages.length;
            });
        }
    }

    changeRenewal(subscriberId, OfferId, BaseOfferID, renewalActive) {
        this.showProgress();
        const data = {
            subcriberId: subscriberId,
            OfferId,
            BaseOfferID
        };
        const before = renewalActive;
        const now = !renewalActive;

        console.log(now);

        for (const history of this.listHistory) {
            if (history.SubcriberNumber === data.subcriberId) {
                let packages = [];
                packages = history.Packages;
                for (const pack of packages) {
                    if (pack.OfferId === data.OfferId) {
                        pack.renewalActive = now;
                        break;
                    }
                }
                break;
            }
        }

        if (now === true) {
            this.services.subscriptionAutomaticRenewalAdd(data, this.cacheStorage().tokenSession).then(
                (success) => {
                    this.dismissProgress();
                },
                error => {
                    for (const history of this.listHistory) {
                        if (history.SubcriberNumber === data.subcriberId) {
                            let packages = [];
                            packages = history.Packages;
                            for (const pack of packages) {
                                if (pack.OfferId === data.OfferId) {
                                    pack.renewalActive = before;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );

        } else {
            this.services.subscriptionAutomaticRenewalRemove(data, this.cacheStorage().tokenSession).then(
                (success) => {
                    this.dismissProgress();
                },
                error => {
                    for (const history of this.listHistory) {
                        if (history.SubcriberNumber === data.subcriberId) {
                            let packages = [];
                            packages = history.Packages;
                            for (const pack of packages) {
                                if (pack.OfferId === data.OfferId) {
                                    pack.renewalActive = before;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }
}

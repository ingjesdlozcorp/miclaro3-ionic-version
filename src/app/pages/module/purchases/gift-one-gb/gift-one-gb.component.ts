import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../../services/utils.service';
import { IntentProvider } from '../../../../services/intent.provider';
import { BasePage } from '../../../base.page';
import { keys } from 'src/app/utils/const/keys';
import { Utils } from '../../../../utils/utils';
import { APP } from '../../../../utils/const/appConstants';
import { BrowserProvider } from '../../../../services/browser.provider';
import { PAYMENT_VERSION } from '../../../_shared/components/flash-payment/core/flash-payment-constants';
import { AccountInfo } from '../../../../models/account.details';
import { PaymentSupportService } from '../../../_shared/components/flash-payment/core/services/payment-support.service';

@Component({
    selector: 'app-module-gift-one-gb',
    templateUrl: './gift-one-gb.component.html',
    styleUrls: ['./gift-one-gb.component.scss'],
})
export class GiftOneGbComponent extends BasePage implements OnInit {

    public accounts;
    public selectedTab;
    public selectedAccount;
    public accountSections;
    public accountInfo;
    data;

    public accept = false;
    public terms = false;
    public subscriberTo = '';
    public accountTo;
    public text = '';
    public amount = 9.99;
    productId: string = APP.PRODUCT_ID_PURCHASE_DATA;
    merchantId = 'REGALA1GB';

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private browserProvider: BrowserProvider,
                private paymentSupport: PaymentSupportService) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('gift_1gb');
        this.data = this.cacheStorage();
        this.accounts = this.data.accountList;
        this.accountInfo = this.data.accountInfo;
        this.selectedAccount = this.cacheStorage().accountDetails;
    }

    ngOnInit() {
    }

    validateInput(param) {
        this.subscriberTo = '' + this.subscriberTo;
        if (param === 1) {
            if (this.subscriberTo.length > 10) {
                this.subscriberTo = this.subscriberTo.slice(0, 10);
            }
        } else {
            if (this.text.length > 200) {
                this.text = this.text.slice(0, 200);
            }
        }
    }

    validateData() {
        const check = this.accept;
        if (!check) {
            this.showError('Debe seleccionar los términos y condiciones para poder continuar.', 'OK');
        } else if (this.cacheStorage().containsSubscriber(this.subscriberTo)) {
            this.showError('Este suscriptor no es valido para el regalo.');
        } else if (this.subscriberTo.length === 0) {
            this.showError('Debe ingresar el numero al que desea recaragar.');
        } else if (this.subscriberTo.length !== 10) {
            this.showError('Debe ingresar un numero valido.');
        } else if (this.text.length > 200) {
            this.showError('El mensaje no puede contener mas de 200 caracteres.');
        } else {
            this.getSubscriber(this.subscriberTo, this.text);
        }
    }


    getSubscriber(num, message) {
        this.showProgress();
        this.services.getSubscriber(num, this.cacheStorage().tokenSession).then(
            (success) => {
                const accountTo = success.Account;
                if (accountTo === '' || accountTo == null) {
                    this.dismissProgress();
                    this.showError('El número de teléfono ingresado no se encuentra registrado' +
                        ' en nuestros sistemas, su formato es incorrecto o no pertenece a nuestra red.'
                    );
                } else {
                    if (Utils.isPostpaid(success.AccountType, success.AccountSubType, 'G')) {
                        this.accountTo = accountTo;
                        this.validateCredit(0, this.amount, accountTo, message, num);
                    } else {
                        this.dismissProgress();
                        this.showError('El número de teléfono ingresado no es pospago.');
                    }
                }
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    validateCredit(accountTotalRent, productPrice, accountTo, message, num) {
        const self = this;
        const accountInfo = this.accountInfo;
        const account = String(accountInfo.bANField);

        this.services.validateCreditLimit(account, accountTotalRent, productPrice, this.cacheStorage().tokenSession).then(
            (success) => {

                const data = self.createParameters(accountTo, message, num);
                const availableCredit = parseFloat(success.AvailableCredit).toFixed(2);
                const subscriberHasAvailableCredit = parseFloat(availableCredit) >= parseFloat(productPrice);
                if (subscriberHasAvailableCredit) {
                    self.sendRecharge(data);
                } else {
                    self.goToPaymentStep(data);
                }
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    goToPaymentStep(data) {
        this.dismissProgress();
        const self = this;

        const confirmMessage = 'Estimado cliente este regalo no califica ' +
            'para cargo en factura, debe efectuar el pago del mismo usando su tarjeta de crédito.';

        this.showConfirm('Pago con tarjeta de crédito', confirmMessage, () => {
            const details = [
                {
                    name: 'Producto',
                    description: 'Regala 1GB'
                }
            ];
            const accountInfo = this.accountInfo;

            const selectedAccount = this.selectedAccount;
            this.store(keys.PAYMENT.PAYMENT_DETAILS, details);
            this.store(keys.PAYMENT.PAYMENT_AMOUNT, String(self.amount));
            this.store(keys.PAYMENT.PAYMENT_SUBSCRIBER, selectedAccount.DefaultSubscriber);
            this.store(keys.PAYMENT.PAYMENT_SUBSCRIBER_TYPE, selectedAccount.mProductType);
            this.store(keys.PAYMENT.PAYMENT_ACCOUNT, accountInfo.bANField);
            this.store(keys.PAYMENT.PAYMENT_EMAIL, accountInfo.emailField);
            this.store(keys.PAYMENT.PAYMENT_DESCRIPTION, 'Regala 1GB');
            // this.store(keys.PAYMENT.PAYMENT_TYPE,  self.PAY.SEND_1GB);
            this.store(keys.PAYMENT.PAYMENT_GIFT_1GB, data);
            /*
            app.router.navigate('payment_step_1', {trigger: true});
            */
            this.payment();
        });

    }

    createParameters(accountTo, message, subscriberTo) {
        const nameSender = this.cacheStorage().loginData.username;

        const accountSender = this.cacheStorage().accountInfo.bANField;
        const accountReceiver = String(accountTo);

        const subscriberSender = this.cacheStorage().accountInfo.defaultSubscriberField;
        const subscriberReceiver = String(subscriberTo);

        return {
            BANReceiver: accountReceiver,
            BANSender: accountSender,
            Charge: '1', // 0 credito - 1 pago factura contra factura
            Message: message,
            NameSender: nameSender,
            PaymentID: '',
            SubscriberReceiver: subscriberReceiver,
            SubscriberSender: subscriberSender
        };

    }

    sendRecharge(data) {
        this.services.sendGift1GB(data, this.cacheStorage().tokenSession).then(
            (success) => {
                this.store(keys.PAYMENT.GIFT_SENT_TEXT, 'Gracias: El regalo se ha enviado con');
                this.dismissProgress();
                this.goPage('module/gift/sent');
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    showTerms() {
        this.terms = true;
    }

    closeTerms() {
        this.terms = false;
    }

    return() {
        this.goHomePage();
    }

    async payment() {
        const userPaymentVersion = await this.fetch(keys.PAYMENT.PAYMENT_VERSION);
        if (userPaymentVersion && userPaymentVersion === PAYMENT_VERSION.P2P) {
            let typeMessage = 'payment';
            if (this.cacheStorage().isPrepaidAccount) {
                typeMessage = 'prepaid';
            }
            this.paymentSupport.paymentRefactorProcess(typeMessage, parseFloat(this.cacheStorage().selectedPacket.price));
        } else {
            const accountInfo: AccountInfo = this.cacheStorage().accountInfo;
            const response = await this.paymentSupport.paymentExternalProcess(
                `${accountInfo.bANField}`,
                this.subscriberTo,
                `${parseFloat(this.cacheStorage().selectedPacket.price)}`,
                {productId: this.productId, merchantId: this.merchantId}
            );
            this.verifyPayment(response);
        }
    }

    verifyPayment(response: any) {
        this.dismissProgress();
        if (response && !response.hasError) {
            if (response.processEndState) {
                if (response.provisioningStatus === APP.PROVISIONING_STATUS.PROVISIONED) {
                    this.cacheStorage().PaymentProcess = {};
                    this.cacheStorage().PaymentProcess.transactionResult = {
                        status: response.paymentStatus,
                        rechargeId: response.confirmationNumber,
                        transactionId: response.authorizationNumber,
                        date: new Date(new Date().getTime())
                    };
                    this.store(keys.PAYMENT.GIFT_SENT_TEXT, 'Gracias: El regalo se ha enviado con');
                    this.dismissProgress();
                    this.goPage('module/gift/sent');
                } else {
                    this.dismissProgress();
                    this.showAlert('Su pago no pudo ser procesado.');
                }
            } else {
                this.showAlert('Su pago no fue recibido o aun se encuentra en proceso, por favor espere un momento mientras procesamos la información.',
                    () => {
                        this.goHomePage();
                    });
            }
        } else {
            this.showError(response.message);
        }
    }
}

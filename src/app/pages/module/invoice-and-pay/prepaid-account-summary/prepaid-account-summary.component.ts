import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController, IonContent } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import {BasePage} from '../../../base.page';
import {Utils} from '../../../../utils/utils';
import {SubscriberInfo} from '../../../../models/account.details';
import { BrowserProvider } from '../../../../services/browser.provider';

@Component({
    selector: 'app-module-prepaid-account-summary',
    templateUrl: './prepaid-account-summary.component.html',
    styleUrls: ['./prepaid-account-summary.component.scss'],
})
export class PrepaidAccountSummaryComponent extends BasePage implements OnInit {

    @ViewChild('content', { static: false }) private content: IonContent;

    subscribers: SubscriberInfo[] = [];
    selectedSubscriber: SubscriberInfo = undefined;

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private browserProvider: BrowserProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('prepaid-summary');
    }

    ngOnInit() {
        this.subscribers = this.cacheStorage().subscriberList;
        this.selectedSubscriber = this.cacheStorage().getFullCurrentSelectedSubscriber();
    }

    changePrepaidSubscriber() {
        this.showProgress();
        this.dismissProgress();
        this.cacheStorage().prepaidSelectedSubscriber = this.selectedSubscriber.subscriberNumberField;
        this.continueSelectAccount(this.cacheStorage().loginData, false);
    }

    getWalletBalance(): string {
        if (this.selectedSubscriber !== undefined) {
            return Utils.formatAmount(this.selectedSubscriber.eWalletBalanceField);
        }
        return '0.00';
    }

    getPrepaidBalance(): string {
        if (this.selectedSubscriber !== undefined) {
            return Utils.formatAmount(this.selectedSubscriber.prepaidBalanceField);
        }
        return '0.00';
    }

    getLastPrepaidRechargeAmount(): string {
        if (this.selectedSubscriber !== undefined) {
            return Utils.formatAmount(this.selectedSubscriber.lastPrepaidReplenishmentField);
        }
        return '0.00';
    }

    getPlanName(): string {
        const plan = this.selectedSubscriber.planInfoField;
        return plan.sOCDescriptionField;
    }

    getPlanDescriptionList(): string[] {
        const plan = this.selectedSubscriber.planInfoField;
        const planFullName = plan.sOCDescriptionField;
        if (planFullName != null) {
            let subPlans = [planFullName];
            if (planFullName.split(':').length > 1) {
                subPlans = planFullName.split(':')[1].split('-');
            }
            return subPlans;
        }
        return [];
    }

    getLastPrepaidRechargeDate(): string {
        let lastDate = this.selectedSubscriber.lastPrepaidReplenishmentDateField;
        if (!lastDate) {
            lastDate = 'N/A';
        }
        return lastDate;
    }

    getNextRecharge(): string {
        let nextRecharge = 'N/A';
        if (this.selectedSubscriber !== undefined && this.selectedSubscriber.planInfoField !== undefined) {
            nextRecharge = this.selectedSubscriber.planInfoField.endDateField;
            if (nextRecharge === null || nextRecharge === undefined || nextRecharge === '') {
                nextRecharge = 'N/A';
            }
        }
        return nextRecharge;
    }

    getPlanAmount(): string {
        const plan = this.selectedSubscriber.planInfoField;
        let planAmount = 0;
        if (plan.sOCDescriptionField != null) {
            planAmount = plan.socRateField;
        }
        return Utils.formatAmount(planAmount);
    }

    rechargeNow() {
        this.goPage('/module/recharge');
    }

    rechargeStores() {
        this.browserProvider.openExternalBrowser('https://www.claropr.com/personas/servicios/servicios-moviles/prepago/metodos-recargas/');
    }

    rechargeMethods() {
        this.browserProvider.openExternalBrowser('https://www.claropr.com/personas/soporte/atencion-cliente/tiendas/');
    }

    transactionHistory() {
        this.goPage('/module/recharge/history');
    }

    printTransactionHistory() {

    }

    attentionCenters() {
        this.browserProvider.openExternalBrowser('https://www.claropr.com/personas/soporte/atencion-cliente/');
    }
}

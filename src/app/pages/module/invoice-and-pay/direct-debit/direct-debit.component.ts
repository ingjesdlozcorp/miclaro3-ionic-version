import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { formatDate } from '@angular/common';
import { BasePage } from '../../../base.page';
import { Utils } from 'src/app/utils/utils';
import { banks } from 'src/app/utils/const/bank';

@Component({
    selector: 'app-module-direct-debit',
    templateUrl: './direct-debit.component.html',
    styleUrls: ['./direct-debit.component.scss'],
})
export class DirectDebitComponent extends BasePage implements OnInit {
    public today = new Date();
    public jstoday = '';
    public years = [];
    public months = [];
    public subscribers = [];
    public accounts = [];
    public selectedAccount;
    public accountSections;
    public typeOfTelephony;

    public selectedTab;
    public showBackBth;

    public bankList = [];

    public now;
    public data;
    public loaded = false;
    public accountInfo;
    public selectedyear = 0;
    public selectedMonth = '01';

    public accountType;
    public creditCardType;
    public selectType;
    public isActive = false;
    public typeCard = false;
    public cardName = '';
    public cardNumber = '';
    public cardCVV = '';
    public savedData = {
        type: '',
        method: '',
        name: '',
        card: '',
        account: ''
    };
    public type = '';
    public addBank = true;
    public selectedBank;
    public bankNumber = '';
    public cardType;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('debit_direct');
        this.bankList = banks.list;
        this.jstoday = formatDate(this.today, 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
        this.now = Number(this.jstoday.substring(6, 10));
        this.selectedyear = this.now;
        for (let i = 0; i < 10; i++) {
            this.years.push(this.now);
            this.now = this.now + 1;
        }
        for (let j = 1; j <= 12; j++) {
            this.months.push((j < 10) ? '0' + j : j);
        }
    }

    ngOnInit() {
        this.getInitDetails();
    }

    getInitDetails() {
        this.showProgress();
        this.data = this.cacheStorage().loginData;
        this.accounts = this.cacheStorage().accountList;
        this.accountType = this.data.accountType;
        this.accountInfo = this.services.data.AccounInfo;
        this.selectedAccount = this.cacheStorage().accountInfo.bANField;
        this.subscribers = this.cacheStorage().subscriberList;
        let productType;
        for (const account of this.accounts) {
            if (account.account === this.selectedAccount) {
                productType = account.account.productType;
                break;
            }
        }
        this.typeOfTelephony = !Utils.subscriberIsMobile(productType);
        this.loaded = true;
        this.type = '1';

        this.getDebitDirect();
    }

    getDebitDirect() {
        this.services.getDirectDebitInfo('' + this.selectedAccount, this.cacheStorage().tokenSession).then(
            success => {
                const isActive = (success.mType !== '' && success.mType !== null);
                if (isActive === true) {
                    this.isActive = true;
                    this.setupData(success);
                } else {
                    this.dismissProgress();
                    this.isActive = false;
                }
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    setupData(data) {
        let type = '';
        if (data.mType === 'CH') {
            type = 'Cuenta de Cheques';
            this.typeCard = false;
        } else {
            type = 'Tarjeta de Cr&eacute;dito';
            this.typeCard = true;
        }
        let bankName = '';
        for (const bank of this.bankList) {
            if (bank.value === data.mBankCode) {
                bankName = bank.name;
            }
        }
        let method = '';
        switch (data.mCreditCardType) {
            case 'MC':
                method = 'MasterCard';
                break;
            case 'VS':
                method = 'Visa';
                break;
            case 'AM':
                method = 'American';
                break;
            default:
                method = bankName;
                return;
        }

        // asignación de variables mediante interpolación
        this.savedData.type = type;
        this.savedData.method = method;
        this.savedData.name = data.mCreditCardMemberName;
        this.savedData.card = '**** **** **** ' + data.mCreditCardNo;
        this.savedData.account = data.mAccountNo;
        this.dismissProgress();
        if (this.savedData.type === 'Tarjeta de Cr&eacute;dito') {
            this.savedData.type = 'Tarjeta de Crédito';
        }
    }

    // añadir tipo de pago
    changeType(e) {
        if (this.type === '1') {
            this.addBank = true;
        } else {
            this.addBank = false;
        }

    }

    cancel() {
        // navegar a invoice sumary
        this.goPage('/module/invoice-summary');
    }

    continue() {

        // condicional para añadir cuenta bancaria o tarjeta de credito.
        if (this.type === '1') {
            this.addBankAccount();
        } else {
            this.addCreditCard();
        }
    }

    addBankAccount() {

        const selectedAccount = this.selectedAccount;

        const bank = this.selectedBank;
        const bankNumber = this.bankNumber;
        if (bank === '') {
            this.showError('Por favor seleccione su banco');
            return;
        }
        if (bankNumber.length === 0) {
            this.showError('Por favor indique un número de cuenta válido');
            return;
        }
        this.showProgress();
        const actualYear = Number(this.jstoday.substring(6, 10));
        const actualMonth = Number(this.jstoday.substring(3, 5));
        const actualDay = (this.jstoday.substring(0, 2));
        const startDate = actualYear + '-' + actualMonth + '-' + actualDay + 'T01:01:01.000-04:00';

        const requestDirectDebit = {
            Ban: '' + this.selectedAccount,
            mAccountNo: bankNumber,
            mBankCode: bank,
            mCreditCardMemberName: '',
            mCreditCardNo: '',
            mCreditCardType: '',
            mCreditCardExpDate: '',
            mReason: '',
            mStartDate: startDate,
            mEndDate: '',
            mStatus: 'A',
            mType: 'CH'
        };

        this.saveDebitDirect(requestDirectDebit);
    }

    addCreditCard() {

        const selectedAccount = this.selectedAccount;

        const cardType = this.cardType;
        const cardNumber = this.cardNumber;
        const cardName = this.cardName;
        const cardMonth = this.selectedMonth;
        const cardYear = this.selectedyear;
        const cardCVV = this.cardCVV;

        if (cardType === undefined) {
            this.showError('Por favor seleccione un método de pago');
            return;
        }

        if (cardName.length === 0) {
            this.showError('Por favor indique su nombre y apellido');
            return;
        }
        let pattern;
        switch (cardType) {
            case 'MC':
                pattern = /^5[1-5][0-9]{14}$/;
                break;
            case 'VS':
                pattern = /^4[0-9]{12}(?:[0-9]{3})?$/;
                break;
            case 'AM':
                pattern = /^3[4,7][0-9]{13}$/;
                break;
            default:
                return;
        }

        if (!pattern.test(cardNumber)) {
            this.showError('Por favor indique un número de tarjeta válido');
            return;
        }

        if (cardCVV.length === 0) {
            this.showError('Por favor indique un código de verificación válido');
            return;
        }
        this.showProgress();

        const expDate = cardYear + '-' + cardMonth + '-01T01:01:01.000-04:00';

        const actualYear = Number(this.jstoday.substring(6, 10));
        const actualMonth = Number(this.jstoday.substring(3, 5));
        const actualDay = (this.jstoday.substring(0, 2));
        const startDate = actualYear + '-' + actualMonth + '-' + actualDay + 'T01:01:01.000-04:00';

        const requestDirectDebit = {
            Ban: '' + this.selectedAccount,
            mAccountNo: '',
            mBankCode: '021502011',
            mCreditCardMemberName: cardName,
            mCreditCardNo: cardNumber,
            mCreditCardType: cardType,
            mCreditCardExpDate: expDate,
            mReason: '',
            mStartDate: startDate,
            mEndDate: '',
            mStatus: 'A',
            mType: 'CC'
        };

        this.saveDebitDirect(requestDirectDebit);
    }

    saveDebitDirect(data) {
        this.services.updateDirectDebit(data, this.cacheStorage().tokenSession).then(
            (success) => {
                if (!success.HasError) {
                    this.dismissProgress();
                    this.store('pay-debit-direct', data);
                    this.showAlert('Afiliación exitosa');
                    this.getInitDetails();
                }
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    validateInput(param) {
        let cardNumber = null;
        switch (param) {
            case 1:
                let name = this.cardName;
                if (name.length > 50) {
                    name = name.slice(0, 50);
                    this.cardName = name;
                }
                break;
            case 2:
                cardNumber = this.cardNumber;
                if (cardNumber.length > 25) {
                    cardNumber = cardNumber.slice(0, 25);
                    this.cardNumber = cardNumber;
                }
                break;
            case 3:
                cardNumber = this.cardCVV;
                if (cardNumber.length > 6) {
                    cardNumber = cardNumber.slice(0, 6);
                    this.cardCVV = cardNumber;
                }
                break;
        }
    }

    changeYear(e) {
        this.selectedyear = e;
    }

}

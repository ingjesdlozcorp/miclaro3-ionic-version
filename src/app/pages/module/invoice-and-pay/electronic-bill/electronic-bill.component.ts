import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import {BasePage} from '../../../base.page';

@Component({
    selector: 'app-module-electronic-bill',
    templateUrl: './electronic-bill.component.html',
    styleUrls: ['./electronic-bill.component.scss'],
})
export class ElectronicBillComponent extends BasePage implements OnInit {

    accountInfo;
    check = false;
    paperless = false;

    public data: IntentProvider;

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('electronic_bill');
    }

    ngOnInit() {
        this.getInitDetails();
    }

    getInitDetails() {
        this.data = this.cacheStorage();
        this.accountInfo = this.data.accountInfo;
        this.paperless = this.accountInfo.paperlessField;
        this.check = this.paperless;
    }

    changePaperless(check) {
        if (check) {
            setTimeout(
                () => {
                    this.showProgress();
                    this.services.updateBillParameters(String(this.accountInfo.bANField), this.data.tokenSession).then(
                        () => {
                            this.reloadCurrentAccount().then((response: any) => {
                                    this.paperless = true;
                                    this.check = true;
                                    this.dismissProgress();
                                }, error => {
                                    this.dismissProgress();
                                    this.showError(error.message);
                                }
                            );
                        },
                        error => {
                            this.dismissProgress();
                            this.showError(error.message);
                            this.paperless = false;
                            this.check = false;
                        }
                    );
                },
                500);
        }
    }

}

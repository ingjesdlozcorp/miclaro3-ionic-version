import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { formatDate } from '@angular/common';
import { BasePage } from '../../../base.page';

@Component({
    selector: 'app-module-payment-history',
    templateUrl: './payment-history.component.html',
    styleUrls: ['./payment-history.component.scss'],
})
export class PaymentHistoryComponent extends BasePage implements OnInit {

    public today = new Date();
    public jsToday = '';
    public years = [];
    public selectedTab;
    public selectedAccount;
    public now;
    public accountSections;

    data;
    loading = false;
    accountInfo;
    payments = [];
    selectedYear = 0;

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('payment_history');
    }

    ngOnInit() {
        this.jsToday = formatDate(this.today, 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
        this.now = Number(this.jsToday.substring(6, 10));
        this.selectedYear = this.now;
        for (let i = 2; i > 0; i--) {
            this.years.push(this.now);
            this.now = this.now - 1;
        }
        this.getInitDetails();
    }

    getInitDetails() {
        this.data = this.cacheStorage().loginData;
        this.accountInfo = this.services.data.AccounInfo;
        this.selectedAccount = this.cacheStorage().accountInfo.bANField;

        const year = Number(this.jsToday.substring(6, 10));
        this.paymentHistory(year);
    }

    paymentHistory(year) {
        this.loading = true;
        this.showProgress();
        const account = this.selectedAccount;
        const token = this.cacheStorage().tokenSession;
        this.payments = [];
        this.services.paymentHistory(account, year, token).then(
            (success) => {
                this.payments = [];
                const first = success.PaymentHistory;
                for (let i = 0; i < first.length; i++) {
                    const isPair = (i % 2 === 0);
                    const completeInvoice = Object.defineProperty(first[i], 'isPair', {
                        value: isPair,
                        configurable: true
                    });
                    this.payments.push(completeInvoice);
                }
                this.loading = false;
                this.dismissProgress();
            },
            (error) => {
                this.loading = false;
                this.dismissProgress();
                this.showError('En este momento no podemos establecer comonucación inrente nuevamente más tarde');
            }
        );
    }
}

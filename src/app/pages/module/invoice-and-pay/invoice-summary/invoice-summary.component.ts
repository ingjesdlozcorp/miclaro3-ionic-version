import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { BasePage } from '../../../base.page';
import { AccountInfo } from '../../../../models/account.details';
import { Utils } from '../../../../utils/utils';
import {APP} from '../../../../utils/const/appConstants';
import { BrowserProvider } from '../../../../services/browser.provider';
import { PaymentSupportService } from '../../../_shared/components/flash-payment/core/services/payment-support.service';
import { keys } from '../../../../utils/const/keys';
import { PAYMENT_VERSION } from '../../../_shared/components/flash-payment/core/flash-payment-constants';


@Component({
    selector: 'app-module-invoice-summary',
    templateUrl: './invoice-summary.component.html',
    styleUrls: ['./invoice-summary.component.scss'],
})
export class InvoiceSummaryComponent extends BasePage implements OnInit {

    billBalance = '';
    billDate = '';
    billDueDate = '';

    public accountInfo: AccountInfo;
    public lastPaymentAmount;
    public amountPayable;
    public selectedTab;
    public accountSections;
    public isGuest = false;

    productId: string = APP.PRODUCT_ID_INVOICE_PAYMENTS;
    merchantId = '';

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private browserProvider: BrowserProvider,
                private paymentSupport: PaymentSupportService) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('invoice');
    }

    ngOnInit() {
        this.getInitDetails();
    }

    getInitDetails() {
        const data = this.cacheStorage();
        this.isGuest = data.isGuest;
        this.accountInfo = data.accountInfo;
        this.billBalance = String(this.accountInfo.billBalanceField);
        this.billDate = data.accountInfo.billDateField;
        this.billDueDate = data.accountInfo.billDueDateField;
        this.lastPaymentAmount = Utils.formatAmount(String(data.accountInfo.lastPaymentAmountField));

        const amountDue = data.accountInfo.billBalanceField;
        let amountPayable = amountDue.includes('CR') ? '0' : amountDue;
        amountPayable = parseFloat(String(amountPayable
            .replace(',', '')))
            .toFixed(2);

        this.amountPayable = amountPayable;
    }

    going(index) {
        const routes = [
            'direct-debit',
            'invoice-download',
            'payment-history'
        ];
        for (let i = 0; i < routes.length; i++) {
            if (index === i) {
                this.goPage('/module/' + routes[i]);
                break;
            }
        }
    }

    async billPayment() {
        const userPaymentVersion = await this.fetch(keys.PAYMENT.PAYMENT_VERSION);
        this.amountPayable = parseFloat(String(this.amountPayable)).toFixed(2);
        if (userPaymentVersion && userPaymentVersion === PAYMENT_VERSION.P2P) {
            let typeMessage = 'payment';
            if (this.cacheStorage().isPrepaidAccount) {
                typeMessage = 'prepaid';
            }
            this.paymentSupport.paymentRefactorProcess(typeMessage, +this.amountPayable);
        } else {
            const accountInfo: AccountInfo = this.cacheStorage().accountInfo;
            let creditAmountDue = accountInfo.billBalanceField.includes('CR') ? accountInfo.billBalanceField.replace('CR', '') : 0;
            let billBalance = accountInfo.billBalanceField.includes('CR') ? 0 : accountInfo.billBalanceField;
            creditAmountDue = parseFloat(String(creditAmountDue).replace(',', ''));
            billBalance = parseFloat(String(billBalance).replace(',', ''));

            this.amountPayable = parseFloat(String(this.amountPayable)).toFixed(2);
            if (Number(this.amountPayable) < 5) {
                this.showError('El monto no puede ser menor a $5.00');
            } else if (Number(this.amountPayable) > 800) {
                this.showError('El monto no puede ser mayor a $800.00');
            } else if (creditAmountDue > 0 && (creditAmountDue + Number(this.amountPayable)) > 800) {
                this.showError('El monto total abonado en su cuenta no puede ser mayor a $800.00');
            } else if (Number(this.amountPayable) > billBalance) {
                this.showConfirm(
                    'Confirmación',
                    'La cantidad ingresada es mayor al balance de su factura, la diferencia será acreditada a su cuenta.',
                    () => {
                        this.doPayment(`${accountInfo.bANField}`, accountInfo.defaultSubscriberField, this.amountPayable);
                    },
                    () => {
                        // Nothing to do
                    },
                );
            } else {
                this.doPayment(`${accountInfo.bANField}`, accountInfo.defaultSubscriberField, this.amountPayable);
            }
        }
    }

    async doPayment(account: string, subscriber: string, amount: string) {
        const response = await this.paymentSupport.paymentExternalProcess(account, subscriber, amount);
        this.dismissProgress();
        if (response && !response.hasError) {
            if (response.processEndState) {
                this.showProgress('Actualizando su balance...');
                this.reloadCurrentAccount().then(() => {
                    this.dismissProgress();
                    this.getInitDetails();
                });
            } else {
                this.dismissProgress();
                this.showAlert('Su pago aun no ha sido procesado, si ya realizo el mismo espere mientras lo procesamos.',
                    () => {
                        this.goHomePage();
                    });
            }
        } else {
            this.showError(response.message);
        }
    }
}

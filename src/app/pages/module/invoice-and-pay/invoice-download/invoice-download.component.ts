import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import {formatDate } from '@angular/common';
import { BasePage } from '../../../base.page';
import { Utils } from '../../../../utils/utils';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
    selector: 'app-module-invoice-download',
    templateUrl: './invoice-download.component.html',
    styleUrls: ['./invoice-download.component.scss'],
})
export class InvoiceDownloadComponent extends BasePage implements OnInit {

    public today = new Date();
    public jsToday = '';
    public years = [];
    public selectedTab;
    public selectedAccount;
    public now;
    public accountSections;

    data;
    loading = false;
    accountInfo;
    invoices = [];
    selectedYear;

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider,
                private fileOpener: FileOpener,
                private file: File) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('invoice_download');
    }

    ngOnInit() {
        this.jsToday = formatDate(this.today, 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
        this.now = Number(this.jsToday.substring(6, 10));
        this.selectedYear = this.now;
        for (let i = 2; i > 0; i--) {
            this.years.push(this.now);
            this.now = this.now - 1;
        }
        this.getInitDetails();
    }

    getInitDetails() {
        this.data = this.cacheStorage().loginData;
        this.accountInfo = this.services.data.AccounInfo;
        this.selectedAccount = this.cacheStorage().accountInfo.bANField;

        const year = Number(this.jsToday.substring(6, 10));
        this.getInvoiceHistory(year);
    }

    getInvoiceHistory(year) {
        this.loading = true;
        this.showProgress();
        const account = this.selectedAccount;
        const token = this.cacheStorage().tokenSession;
        this.invoices = [];
        this.services.getInvoiceHistory(account, year, token).then(
            (success) => {
                this.invoices = [];
                const first = success.Invoices;
                for (let i = 0; i < first.length; i++) {
                    const isPair = (i % 2 === 0);
                    const completeInvoice =  Object.defineProperty(first[i], 'isPair', {
                        value: isPair,
                        configurable: true
                    });
                    this.invoices.push(completeInvoice);
                }
                this.loading = false;
                this.dismissProgress();
            },
            (error) => {
                this.loading = false;
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    open(invoice: any) {
        // this.showProgress();
        this.saveAndOpenPdf(invoice.invoiceContent, 'invoice');
        // window.open('data:application/pdf;base64, ' + invoice.invoiceContent);
        // window.open('data:application/pdf,' + escape(invoice.invoiceContent));
        // const token = this.cacheStorage().tokenSession;
        // this.services.getDownloadInvoice(invoice.invoiceContent, token).then(
        //     (success) => {
        //         console.log(success);
        //         this.dismissProgress();
        //     },
        //     (error) => {
        //         this.loading = false;
        //         this.dismissProgress();
        //         this.showError(error.message);
        //     }
        // );

    }

    saveAndOpenPdf(pdf: string, filename: string) {
        const writeDirectory = Utils.getPlatformInfo().ios ? this.file.dataDirectory : this.file.externalDataDirectory;
        console.log(writeDirectory);
        this.file.writeFile(writeDirectory, filename, Utils.convertBase64ToBlob(pdf, 'data:application/pdf;base64'), {replace: true})
            .then(() => {
                this.fileOpener.open(writeDirectory + filename, 'application/pdf')
                    .catch(() => {
                        this.showAlert('No posee ninguna aplicación para abrir su factura, por favor diríjase a la tienda para descargar una.');
                    });
            })
            .catch(() => {
                console.error('Error writing pdf file');
            });
    }
}

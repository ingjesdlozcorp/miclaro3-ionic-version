import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import {BasePage} from '../../base.page';
import {Utils} from '../../../utils/utils';
import {Account} from '../../../models/authenticate';
import { SubscriberInfo } from '../../../models/account.details';


@Component({
    selector: 'app-module-consumption',
    templateUrl: './consumption.component.html',
    styleUrls: ['./consumption.component.scss'],
})
export class ConsumptionComponent extends BasePage implements OnInit {

    formatSubscriber = Utils.formatSubscriber;
    typeOfTelephony = Utils.typeOfTelephony;

    public selectedAccount;
    public subscribers = [];
    public accountInfo;
    public dsl;
    public wirelessAccount;
    public subscribersCount;
    public accountSections;
    public data;
    public loaded = false;
    public accounts: Account[] = [];
    public accountType;
    public prepaid = [];
    public postpaid = [];
    public telephony = [];
    public selectedTab;
    // control styles
    pressed = [ true, false, true, false, false ];
    selectedSection = '';
    cycle = '';
    isBusiness = false;


    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('consumption');
        this.isBusiness = this.cacheStorage().isBusiness();
    }

    ngOnInit() {
        this.loadedData(1);
    }

    // obteniendo data del almacenamiento del dispositivo:
    getInitDetails() {
        this.data = this.cacheStorage().loginData;
        this.accounts = this.cacheStorage().accountList;
        this.accountType = this.data.accountType;
        this.accountInfo = this.services.data.AccounInfo;
        this.selectedAccount = this.cacheStorage().accountInfo.bANField;
        this.dsl = (this.accountInfo.accountSubtypeField === 'W' && this.accountInfo.accountTypeField === 'I');
        this.loaded = true;
    }
    // Cargando el resto de la data: Este metodo sirve cuando se inicia el componente y cuando se cambia el select
    loadedData(event: string | number) {
        this.getInitDetails();
        // terminando de llenar las variables.
        setTimeout(() => {
            let productType;
            for (const account of this.accounts) {
                if (account.account === this.selectedAccount) {
                    productType = account.productType;
                    break;
                }
            }
            this.wirelessAccount = Utils.subscriberIsMobile(productType);
            this.subscribersCount = this.accounts.length;
            this.dismissProgress();
        }, 300);

        // Cargando la información de subcribers para mostrar:
        setTimeout(() => {
            this.subscribers = []; // array a  llenar con data especifica tratada para mostrar el contenido html
            const subscriberList = this.services.data.SubscriberInfo; // array de subscribers del servidor o crudo.

            let subscriber = { subscriber: '', code: '', body: '', type: ''};
            let plans = { mainPlans : [], roamingPlans : [], additionalPlans: []};

            for (const subscriberInfo of subscriberList) {
                // Esto es  para dar el  formato deseado a cada numero de suscriptor
                subscriber.subscriber = subscriberInfo.subscriberNumberField;
                subscriber.code = '(' + subscriberInfo.subscriberNumberField.substring(0, 3) + ')';
                subscriber.body = subscriberInfo.subscriberNumberField.substring(3, subscriberInfo.subscriberNumberField.lenght);
                subscriber.type = Utils.typeOfTelephony(subscriberInfo.productTypeField);

                // fin de formato del numero de subcriptor.

                // Obtencion de  planes:
                let myPlans = [];
                myPlans = subscriberInfo.usageInfoField.dataOffersField;
                for (const plan of myPlans) {
                    // Variable para mostrar los planes por subscriptor en el html:
                    const showPlan = {unlimited : false, availableStyle: true, usagePercentage: 0, htmlUsage : {partA: '', partB: ''}, remain: 0, disponibles: '',
                                        class: '',  consumidos: ''
                                    };
                    // logica de llenado de variable para mostrar el contenido
                    showPlan.unlimited = false;
                    if (plan.displayNameField.includes('PUJ') || plan.displayNameField.includes('Ilimitado') || plan.displayNameField.includes('Unlimited')) {
                        showPlan.unlimited = true;
                        showPlan.availableStyle = false;
                    }
                    showPlan.usagePercentage = Math.round(100.0 * (plan.usedField / plan.quotaField));
                    if (showPlan.usagePercentage === 0 && plan.usedField > 0) {
                        showPlan.usagePercentage = 1;
                    }
                    showPlan.class = 'c100 p' + showPlan.usagePercentage + ' text-center center vcenter';
                    showPlan.htmlUsage.partA = plan.usedTextField;
                    showPlan.htmlUsage.partB = 'de ' + plan.quotaTextField;
                    if (showPlan.unlimited) {
                        showPlan.htmlUsage.partB = '';
                    }
                    showPlan.remain = plan.quotaField - plan.usedField;
                    showPlan.consumidos = 'Consumidos (' + this.transformAvailable(plan.usedField) + ')';
                    showPlan.disponibles = 'Disponibles (' + this.transformAvailable(showPlan.remain) + ')';
                    /*+++*/

                    // Creación de nueva propiedad en el objeto plan con el contenido listo para mostrar:
                    const completePlan =  Object.defineProperty(plan, 'showPlan', {
                        value: showPlan,
                        configurable: true
                    });

                    // clasificacion y almacenamiento del plan:
                    if (plan.offerGroupField.includes('BASE')) {
                        plans.mainPlans.push(completePlan);
                    } else if (plan.offerGroupField.includes('ROAMING') > 0) {
                        plans.roamingPlans.push(completePlan);
                    } else {
                        plans.additionalPlans.push(completePlan);
                    }
                }

                // Añado al subcriber el formato y el plan detallado con los datos que me importa mostrar de momento:
                const a =  Object.defineProperty(subscriberInfo, 'fixedData', {
                    value: subscriber,
                    configurable: true
                });

                const b =  Object.defineProperty(a, 'planes', {
                    value: plans,
                    configurable: true
                });

                // Lleno array de subscribers con nuevo objeto:
                this.subscribers.push(b);

                // Limpio varables para el proximo elemento:
                subscriber = { subscriber: '', code: '', body: '', type: ''};
                plans = { mainPlans : [], roamingPlans : [], additionalPlans: []};
            }
            if (this.subscribers.length === 1) {
                this.showingInfo(this.subscribers[0].subscriberNumberField);
              }
        }, 200);
    }

    presedtab(param) {
        if ( param === 0) {
            this.pressed[0] = true;
            this.pressed[2] = true;
            this.pressed[1] = false;
        } else {
            for (let i = 0; i < this.pressed.length; i++ ) {
                if (param !== i) {
                    this.pressed[i] = false;
                } else {
                    this.pressed[i] = true;
                }
            }
        }
    }
    showingInfo(index) {
        if (this.selectedSection !== index) {
            this.selectedSection = index;
        } else {
            this.selectedSection = undefined;
        }
    }

    transformAvailable(remaining) { // receive in kilobytes
        const remainingMB = (remaining / 1024) / 1024;
        const remainingGB = ((remaining / 1024) / 1024) / 1024;

        let text = remainingMB.toFixed(2) + ' MB';
        if (remainingMB > 2048) {
            text = remainingGB.toFixed(2) + ' GB';
        }
        return text;
    }
    addData() {
        this.goPage('module/data-plan');
    }

    isMobile(subscriber: SubscriberInfo): boolean {
        return subscriber.productTypeField === 'C' || subscriber.productTypeField === 'G';
    }
}

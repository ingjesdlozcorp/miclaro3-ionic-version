import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import {BasePage} from '../../../base.page';
import {Utils} from '../../../../utils/utils';
import {DataOffersField, SubscriberInfo} from '../../../../models/account.details';

@Component({
    selector: 'app-module-consumption-prepaid',
    templateUrl: './consumption-prepaid.component.html',
    styleUrls: ['./consumption-prepaid.component.scss'],
})
export class ConsumptionPrepaidComponent extends BasePage implements OnInit {

    formatSubscriber = Utils.formatSubscriber;
    typeOfTelephony = Utils.typeOfTelephony;
    subscribers: SubscriberInfo[] = [];
    selectedSubscriber: SubscriberInfo = undefined;

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('consumption_prepaid');
    }

    ngOnInit() {
        this.subscribers = this.cacheStorage().subscriberList;
        if (this.subscribers.length === 1) {
            this.showDetails(this.subscribers[0]);
        }
    }

    showDetails(subscriber: SubscriberInfo) {
        if (this.selectedSubscriber === subscriber) {
            this.selectedSubscriber = undefined;
        } else {
            this.selectedSubscriber = subscriber;
        }
    }

    getMinutes(subscriber: SubscriberInfo): string {
        const usageInfo = subscriber.usageInfoField;
        return usageInfo.minutesUsageField;
    }

    getSMS(subscriber: SubscriberInfo): string {
        const usageInfo = subscriber.usageInfoField;
        const sms = parseFloat(usageInfo.sMSUSageField) + parseFloat(usageInfo.mMSUsageField);
        return String(sms);
    }

    getData(subscriber: SubscriberInfo): string {
        const usageInfo = subscriber.usageInfoField;
        let used = 0;
        let quota = 0;
        const dataInfo: DataOffersField[] = usageInfo.dataOffersField;
        if (dataInfo != null && dataInfo.length > 0) {
            dataInfo.forEach(data => {
                used += parseFloat(data.usedField);
                quota += parseFloat(data.quotaField);
            });
        }
        return Utils.transformAvailable(used) + ' USADOS';
    }

    getPercentage(subscriber: SubscriberInfo): number {
        const usageInfo = subscriber.usageInfoField;
        let used = 0;
        let quota = 0;
        const dataInfo: DataOffersField[] = usageInfo.dataOffersField;
        if (dataInfo != null && dataInfo.length > 0) {
            dataInfo.forEach(data => {
                used += parseFloat(data.usedField);
                quota += parseFloat(data.quotaField);
            });
        }
        let usagePercentage = Math.round(100.0 * (used / quota));
        if (usagePercentage === 0 && used > 0) {
            usagePercentage = 1;
        } else if (!usagePercentage) {
            usagePercentage = 0;
        }
        return usagePercentage;
    }
}

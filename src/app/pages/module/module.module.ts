import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../_shared/shared.module';
import { DirectDebitComponent } from './invoice-and-pay/direct-debit/direct-debit.component';
import { ElectronicBillComponent } from './invoice-and-pay/electronic-bill/electronic-bill.component';
import { InvoiceDownloadComponent } from './invoice-and-pay/invoice-download/invoice-download.component';
import { PaymentHistoryComponent } from './invoice-and-pay/payment-history/payment-history.component';
import { ConsumptionComponent } from './consumption/consumption.component';
import { ChangePlanComponent } from './equipment-and-services/change-plan/change-plan.component';
import { NetflixComponent } from './equipment-and-services/netflix/netflix.component';
import { NetflixTermsComponent } from './equipment-and-services/netflix/netflix-terms/netflix-terms.component';
import { HomeComponent } from './refer/home/home.component';
import { PurchasesComponent } from './purchases/purchases/purchases.component';
import { GiftOneGbComponent } from './purchases/gift-one-gb/gift-one-gb.component';
import { GiftRechargeComponent } from './purchases/gift-recharge/gift-recharge.component';
import { InvoiceSummaryComponent } from './invoice-and-pay/invoice-summary/invoice-summary.component';
import { ProfileComponent } from './account-management/profile/profile.component';
import { NotificationPreferenceComponent } from './account-management/notification-preference/notification-preference.component';
import { SupportComponent } from './account-management/support/support.component';
import { IonicModule } from '@ionic/angular';
import { SwiperModule, SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { DeviceComponent } from './equipment-and-services/device/device.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { DataPlanComponent } from './purchases/data-plan/data-plan.component';
import { ShoppingHistoryComponent } from './purchases/shopping-history/shopping-history.component';
import { NetflixFaqComponent } from './equipment-and-services/netflix/netflix-faq/netflix-faq.component';
import { NetflixSupportComponent } from './equipment-and-services/netflix/netflix-support/netflix-support.component';
import { NetflixSubscriptionComponent } from './equipment-and-services/netflix/netflix-subscription/netflix-subscription.component';
import { ChangePlanConfirmComponent } from './equipment-and-services/change-plan/change-plan-confirm/change-plan-confirm.component';
import { NetflixRedirectComponent } from './equipment-and-services/netflix/netflix-redirect/netflix-redirect.component';
import { ChangePlanSuccessComponent } from './equipment-and-services/change-plan/change-plan-success/change-plan-success.component';
import { DataPlanSuccessComponent } from './purchases/data-plan/data-plan-success/data-plan-success.component';
import { GiftSentComponent } from './purchases/gift-sent/gift-sent.component';
import { NgxPopperModule } from 'ngx-popper';
import { AddAccountsComponent } from './account-management/add-accounts/add-accounts.component';
import { InviteComponent } from './refer/invite/invite.component';
import { RedeemComponent } from './refer/redeem/redeem.component';
import { QuestionsComponent } from './refer/questions/questions.component';
import { ClubComponent } from './claro-club/club/club.component';
import { ClubTermsComponent } from './claro-club/club-terms/club-terms.component';
import { ClubFaqComponent } from './claro-club/club-faq/club-faq.component';
import { ConsumptionPrepaidComponent } from './consumption/prepaid/consumption-prepaid.component';
import { RechargeComponent } from './recharge/recharge.component';
import { RechargeConfirmComponent } from './recharge/confirm/recharge-confirm.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { RechargeSuccessComponent } from './recharge/success/recharge-success.component';
import { FaultStep1Component } from './fault-report/step1/fault.step1.component';
import { FaultStep2Component } from './fault-report/step2/fault.step2.component';
import { FaultStep3Component } from './fault-report/step3/fault.step3.component';
import { PopupReferTermsComponent } from './refer/popup-terms/popup-refer-terms.component';
import { TransactionsV2Component } from './transactions/v2/transactions-v2.component';
import { PrepaidAccountSummaryComponent } from './invoice-and-pay/prepaid-account-summary/prepaid-account-summary.component';
import { NetflixRequestComponent } from './equipment-and-services/netflix/netflix-request/netflix-request.component';
import { IptvComponent } from './equipment-and-services/iptv/iptv.component';
import { PopupDeviceStreamingComponent } from './equipment-and-services/iptv/iptv-buy/popup-device-streaming/popup-device-streaming.component';
import { IptvFaqComponent } from './equipment-and-services/iptv/iptv-faq/iptv-faq.component';
import { IptvBuyComponent } from './equipment-and-services/iptv/iptv-buy/iptv-buy.component';
import { IptvTermsComponent } from './equipment-and-services/iptv/iptv-terms/iptv-terms.component';
import { IptvSelectPlanComponent } from './equipment-and-services/iptv/iptv-select-plan/iptv-select-plan.component';
import { IptvSupportComponent } from './equipment-and-services/iptv/iptv-support/iptv-support.component';
import { AdditionalServiceComponent } from './equipment-and-services/iptv/iptv-buy/popup-adittional-services/additional-service/additional-service.component';
import { PopupTermsOnPopupComponent } from './equipment-and-services/iptv/iptv-buy/popup-terms-on-popup/popup-terms-on-popup.component';
import { PopupCreateAccountComponent } from './equipment-and-services/iptv/iptv-buy/popup-create-account/popup-create-account.component';
import { PopupPaymentShippingComponent } from './equipment-and-services/iptv/iptv-buy/popup-payment-shipping/popup-payment-shipping.component';
import { IptvSuccessComponent } from './equipment-and-services/iptv/iptv-success/iptv-success.component';
import { PopupAdditionalServicesComponent } from './equipment-and-services/iptv/iptv-buy/popup-adittional-services/popup-additional-services.component';
import { IptvChannelsGuideComponent } from './equipment-and-services/iptv/iptv-channels-guide/iptv-channels-guide.component';
import {ChangePlanRechargeComponent} from './equipment-and-services/change-plan/change-plan-recharge/change-plan-recharge.component';
import { FlashPaymentModule } from '../_shared/components/flash-payment/flash-payment.module';
import { PopupTermsDeactivateIosComponent } from './account-management/profile/popup-terms-deactivate-ios/popup-terms-deactivate-ios.component';

export const routes: Routes = [
    // invoice and pay
    { path: 'prepaid-summary', component: PrepaidAccountSummaryComponent },
    { path: 'invoice-summary', component: InvoiceSummaryComponent },
    { path: 'invoice-download', component: InvoiceDownloadComponent },
    { path: 'payment-history', component: PaymentHistoryComponent },
    { path: 'electronic-bill', component: ElectronicBillComponent },
    { path: 'direct-debit', component: DirectDebitComponent },
    // consumption
    { path: 'consumption', component: ConsumptionComponent },
    { path: 'consumption/prepaid', component: ConsumptionPrepaidComponent },
    // Equipment and Services
    {path: 'device', component: DeviceComponent},
    {path: 'change-plan', component: ChangePlanComponent},
    {path: 'change-plan/confirm', component: ChangePlanConfirmComponent},
    { path: 'change-plan/recharge', component: ChangePlanRechargeComponent },
    {path: 'change-plan/success', component: ChangePlanSuccessComponent},
    {path: 'netflix', component: NetflixComponent},
    {path: 'netflix/terms', component: NetflixTermsComponent},
    {path: 'netflix/faq', component: NetflixFaqComponent},
    {path: 'netflix/support', component: NetflixSupportComponent},
    {path: 'netflix/subscription', component: NetflixSubscriptionComponent},
    {path: 'netflix/redirect', component: NetflixRedirectComponent},
    {path: 'netflix/request', component: NetflixRequestComponent},
    {path: 'refer/home', component: HomeComponent},
    {path: 'refer/invite', component: InviteComponent},
    {path: 'refer/redeem', component: RedeemComponent},
    {path: 'refer-questions', component: QuestionsComponent},
    {path: 'club/home', component: ClubComponent},
    {path: 'club/terms', component: ClubTermsComponent},
    {path: 'club/faq', component: ClubFaqComponent},
    {path: 'iptv', component: IptvComponent},
    {path: 'iptv/terms', component: IptvTermsComponent},
    {path: 'iptv/faq', component: IptvFaqComponent},
    {path: 'iptv/support', component: IptvSupportComponent},
    {path: 'iptv/select-plan', component: IptvSelectPlanComponent},
    {path: 'iptv/buy', component: IptvBuyComponent},
    {path: 'iptv/success', component: IptvSuccessComponent},
    // purchases
    { path: 'purchases', component: PurchasesComponent },
    { path: 'data-plan', component: DataPlanComponent },
    { path: 'data-plan/success', component: DataPlanSuccessComponent },
    { path: 'shopping-history', component: ShoppingHistoryComponent },
    { path: 'gift/one-gb', component: GiftOneGbComponent  },
    { path: 'gift/recharge', component: GiftRechargeComponent },
    { path: 'gift/sent', component: GiftSentComponent },
    // account
    { path: 'add-account', component: AddAccountsComponent },
    { path: 'add-account/:tab', component: AddAccountsComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'profile/:tab', component: ProfileComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'notifications-preference', component: NotificationPreferenceComponent },
    { path: 'support', component: SupportComponent },
    // fault Report
    { path: 'fault/step1', component: FaultStep1Component },
    { path: 'fault/step2', component: FaultStep2Component },
    { path: 'fault/step3', component: FaultStep3Component },
    // recharge prepaid
    { path: 'recharge', component: RechargeComponent },
    { path: 'recharge/confirm', component: RechargeConfirmComponent },
    { path: 'recharge/success', component: RechargeSuccessComponent },
    // New prepaid transaction module, maybe change
    { path: 'recharge/history', component: TransactionsV2Component },

];

@NgModule({
    declarations: [
        AddAccountsComponent,
        ClubComponent,
        ClubTermsComponent,
        ClubFaqComponent,
        InvoiceSummaryComponent,
        InvoiceDownloadComponent,
        DirectDebitComponent,
        ElectronicBillComponent,
        PaymentHistoryComponent,
        ElectronicBillComponent,
        FaultStep1Component,
        FaultStep2Component,
        FaultStep3Component,
        ConsumptionComponent,
        ConsumptionPrepaidComponent,
        ChangePlanComponent,
        ChangePlanConfirmComponent,
        ChangePlanRechargeComponent,
        ChangePlanSuccessComponent,
        NetflixComponent,
        NetflixTermsComponent,
        NetflixFaqComponent,
        NetflixSupportComponent,
        NetflixSubscriptionComponent,
        NetflixRedirectComponent,
        NetflixRequestComponent,
        IptvComponent,
        IptvTermsComponent,
        IptvFaqComponent,
        IptvSupportComponent,
        IptvSelectPlanComponent,
        IptvChannelsGuideComponent,
        IptvBuyComponent,
        IptvSuccessComponent,
        PopupAdditionalServicesComponent, AdditionalServiceComponent,
        PopupCreateAccountComponent,
        PopupDeviceStreamingComponent,
        PopupPaymentShippingComponent,
        PopupTermsOnPopupComponent,
        HomeComponent,
        InviteComponent,
        RedeemComponent,
        QuestionsComponent,
        PurchasesComponent,
        GiftOneGbComponent,
        GiftRechargeComponent,
        GiftSentComponent,
        ProfileComponent, PopupTermsDeactivateIosComponent,
        NotificationPreferenceComponent,
        SupportComponent,
        DeviceComponent,
        NotificationsComponent,
        DeviceComponent,
        DataPlanComponent,
        DataPlanSuccessComponent,
        ShoppingHistoryComponent,
        RechargeComponent,
        RechargeConfirmComponent,
        RechargeSuccessComponent,
        TransactionsComponent,
        TransactionsV2Component,
        PopupReferTermsComponent,
        PrepaidAccountSummaryComponent
    ],
    exports: [RouterModule],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        HttpClientModule,
        SharedModule,
        ReactiveFormsModule,
        IonicModule,
        SwiperModule,
        FlashPaymentModule,
        NgxPopperModule.forRoot({placement: 'top', styles: {'background-color': 'white'}}),
    ],
})
export class ModuleModule { }

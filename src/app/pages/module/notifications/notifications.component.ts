import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { Storage } from '@ionic/storage';
import {BasePage} from '../../base.page';
import { keys } from 'src/app/utils/const/keys';

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent extends BasePage implements OnInit {

    public notifications;
    public name;
    public convertCaseStr;
    public accounts;
    public selectedTab;
    public selectedAccount;
    public accountSections;
    public token;

    public messagesList = [];

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('notifications');
        this.store(keys.NOTIFICATION.NOTIFICATION_LIST, this.services.data.Messages);
        this.token = this.cacheStorage().tokenSession;
    }

    ngOnInit() {
        this.callNotifications();
    }

    callNotifications() {
        const model = this.services;
        const token = this.token;
        const self = this;
        if (this.services.data.Messages.MessagesList != null) {
            this.messagesList = this.services.data.Messages.MessagesList;
            this.messagesList.forEach(notification => {
                if (notification) {
                    if (notification.status === 0) {
                        model.updateNotification(notification.id_message, notification.account, token).then(
                            (response) => {
                                if (!response.hasError) {
                                    const notificationsNew = self.services.data.Messages;

                                    notificationsNew.newMessageCounter = notificationsNew.newMessageCounter - 1;
                                    notificationsNew.MessagesList.forEach((notificationNew) => {
                                        if (notificationNew.id_message === notification.id_message) {
                                            notificationNew.status = 1;
                                        }
                                    });
                                    self.store('notifications', notificationsNew);
                                }
                            },
                            (error) => {
                                // nothing to do
                            }
                        );

                    }
                }
            });
        }
    }

}

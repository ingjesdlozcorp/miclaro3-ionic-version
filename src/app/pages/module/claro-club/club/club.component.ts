import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { BasePage } from 'src/app/pages/base.page';
import { Storage } from '@ionic/storage';
import { PlatformInfo, Utils } from '../../../../utils/utils';

@Component({
    selector: 'app-module-club',
    templateUrl: './club.component.html',
    styleUrls: ['./club.component.scss'],
})

export class ClubComponent extends BasePage implements OnInit {

    platform: PlatformInfo = Utils.getPlatformInfo();

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('club');
    }

    ngOnInit() {}

    subscription() {
        if (this.platform.android) {
            this.openExternalBrowser('https://play.google.com/store/apps/details?id=com.mlstudio.burea');
        } else if (this.platform.ios) {
            this.openExternalBrowser('https://apps.apple.com/us/app/burea/id978694147');
        } else {
            this.openExternalBrowser('https://bureapr.com/');
        }
    }

    showTerms() {
        this.goPage('module/club/terms');
    }

    faq() {
        this.goPage('module/club/faq');
    }

}

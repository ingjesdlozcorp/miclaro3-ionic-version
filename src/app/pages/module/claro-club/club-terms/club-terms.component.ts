import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { BasePage } from 'src/app/pages/base.page';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-module-club-terms',
  templateUrl: './club-terms.component.html',
  styleUrls: ['./club-terms.component.scss'],
})
export class ClubTermsComponent extends BasePage implements OnInit {
  public contentShow = true;
  constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
              alertController: AlertController, utilsService: UtilsService,
              userStorage: IntentProvider) {
    super(router, storage, modelsServices, alertController, utilsService, userStorage);
  }

  ngOnInit() {}

  showingContent() {
    this.contentShow = !this.contentShow;
  }

}

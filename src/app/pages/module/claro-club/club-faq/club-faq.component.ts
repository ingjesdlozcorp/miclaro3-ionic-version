import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from 'src/app/services/intent.provider';
import { BasePage } from 'src/app/pages/base.page';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-module-club-faq',
  templateUrl: './club-faq.component.html',
  styleUrls: ['./club-faq.component.scss'],
})
export class ClubFaqComponent extends BasePage implements OnInit {
  public open = true;
  public touched = false;
  public contentShow = [false,  false,  false, false, ];
  public theOpenGroup = [];
  constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
              alertController: AlertController, utilsService: UtilsService,
              userStorage: IntentProvider) {
    super(router, storage, modelsServices, alertController, utilsService, userStorage);
  }

  ngOnInit() {}
  searchDelete( param: number ) {
    for ( let i = 0; i < this.theOpenGroup.length; i++ ) {
        if ( param === this.theOpenGroup[i]) {
            this.theOpenGroup.splice(i, 1);
        }
    }
  }
  showingMenu(param: number) {
      let close = false;

      if (this.theOpenGroup.length > 0) {
          for (const group of this.theOpenGroup) {
              if (param === group) {
                  close = true;
              }
          }
      }
      // para mostrar el contenido:
      if ( close === false) {
          for ( let i = 0; i < this.contentShow.length; i++ ) {
              if ( param === i) {
                  this.contentShow[i] = true;
                  this.theOpenGroup.push(param);
                  console.log(this.contentShow[i]);
              }
          }
      } else {
          // para cerrar el contenido;
          this.searchDelete(param);
          for ( let i = 0; i < this.contentShow.length; i++ ) {
              if ( param === i) {
                  this.contentShow[i] = false;
                  console.log(this.contentShow[i]);
              }
          }
      }
      console.log(this.theOpenGroup);
  }
}

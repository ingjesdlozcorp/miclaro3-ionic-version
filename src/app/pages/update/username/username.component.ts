import { Component, OnInit } from '@angular/core';
import { BasePage } from '../../base.page';
import { Router } from '@angular/router';
import { ServicesProvider } from '../../../services/services.provider';
import { Storage } from '@ionic/storage';
import { keys } from '../../../utils/const/keys';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../services/utils.service';
import { Utils } from '../../../utils/utils';
import { IntentProvider } from '../../../services/intent.provider';
import { Authenticate } from '../../../models/authenticate';

@Component({
    selector: 'app-update-username',
    templateUrl: './username.component.html',
    styleUrls: ['./username.component.scss'],
})
export class UsernameComponent extends BasePage implements OnInit {

    oldUsername = '';
    newUsername = '';
    number = '';
    oldPassword = '';
    newPassword = '';
    repeatPassword = '';

    loginData: Authenticate = {};

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.loginData = this.cacheStorage().loginData;
        this.oldUsername = this.loginData.username;
        this.number = this.loginData.subscriber;
    }

    save() {
        if (this.newUsername.length === 0) {
            this.showError('Debe ingresar su nuevo usuario.');
        } else if (!Utils.validateEmail(this.newUsername)) {
            this.showError('Debe ingresar un correo electrónico válido.');
        } else if (this.oldPassword.length === 0) {
            this.showError('Debe colocar su contaseña anterior.');
        } else if (this.newPassword.length < 8 || this.newPassword.length > 15) {
            this.showError('Su contaseña nueva debe tener entre 8 y 15 caracteres.');
        } else if (!this.lowercaseValidator(this.newPassword)) {
            this.showError('Su contaseña nueva debe tener al menos 1 letra minúscula.');
        } else if (!this.uppercaseValidator(this.newPassword)) {
            this.showError('Su contaseña nueva debe tener al menos 1 letra mayúscula.');
        } else if (!this.twoNumberValidator(this.newPassword)) {
            this.showError('Su contaseña nueva debe tener al menos 2 números.');
        } else if (!this.specialCharacterValidator(this.newPassword)) {
            this.showError('No estan permitidos caracteres especiales.');
        } else if (this.newPassword !== this.repeatPassword) {
            this.showError('Las contraseñas no coinciden.');
        } else {
            this.showProgress();
            this.services.updateUsername(this.newUsername, this.loginData.token).then(
                (success: any) => {
                    this.dismissProgress();
                    this.updatePassword();
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }

    updatePassword() {
        this.showProgress();
        this.services.updatePassword(this.newPassword, this.oldPassword, this.loginData.account, this.loginData.token).then(
            (success: any) => {
                this.dismissProgress();
                this.showAlert('Se actualizo su usuario y contraseña con éxito.',
                    () => {
                        this.goLoginPage();
                    });
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    lowercaseValidator(c: string) {
        const regex = /[a-z]/g;
        return regex.test(c);
    }

    uppercaseValidator(c: string) {
        const regex = /[A-Z]/g;
        return regex.test(c);
    }

    twoNumberValidator(c: string) {
        const numbers = '0123456789';
        let count = 0;
        for (let i = 0; i < c.length; i++) {
            if (numbers.indexOf(c.charAt(i), 0) !== -1) {
                count++;
            }
        }
        return count >= 2;
    }

    specialCharacterValidator(c: string) {
        return c.match('^[A-z0-9]+$');
    }

}

import { Component, OnInit } from '@angular/core';
import {BasePage} from '../../base.page';
import {Router} from '@angular/router';
import {ServicesProvider} from '../../../services/services.provider';
import { Storage } from '@ionic/storage';
import {keys} from '../../../utils/const/keys';
import {AlertController, NavController} from '@ionic/angular';
import {UtilsService} from '../../../services/utils.service';
import {IntentProvider} from '../../../services/intent.provider';
import {Authenticate} from '../../../models/authenticate';

@Component({
    selector: 'questions-step1',
    templateUrl: './questions.component.html',
    styleUrls: ['./questions.component.scss'],
})
export class QuestionsComponent extends BasePage implements OnInit {

    questions: any[] = [];
    answer1 = '';
    answer2 = '';

    question1;
    question2;

    loginData: Authenticate = {};

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.fetch(keys.LOGIN.DATA).then(
            (data: Authenticate) => {
                this.loginData = data;
                this.getSecurityQuestions();
            }
        );
    }

    getSecurityQuestions() {
        this.showProgress();
        this.services.getSecurityQuestions(this.loginData.token).then(
            (response: any) => {
                this.questions = response.QuestionsItemsList;
                this.dismissProgress();
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
                this.goLoginPage();
            }
        );
    }

    save() {
        console.log(this.question1);
        console.log(this.question2);
        if (this.answer1.length === 0 || this.answer2.length === 0) {
            this.showError('Por favor introduzca sus respuestas de seguridad.');
        } else if (this.question1 === this.question2) {
            this.showError('Debe seleccionar dos preguntas de seguridad distintas.');
        } else {
            this.showProgress();
            this.services.setChallengeQuestions(this.question1, this.answer1, this.loginData.token).then(
                (success: any) => {
                    this.dismissProgress();
                    this.saveSecondAnswer();
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }

    saveSecondAnswer() {
        this.showProgress();
        this.services.setChallengeQuestions(this.question2, this.answer2, this.loginData.token).then(
            (success: any) => {
                this.dismissProgress();
                this.showAlert('Se actualizo sus preguntas de seguridad con éxito.',
                    () => {
                        this.goLoginPage();
                    });
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }
}

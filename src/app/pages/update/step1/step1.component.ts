import { Component, OnInit } from '@angular/core';
import {BasePage} from '../../base.page';
import {Router} from '@angular/router';
import {ServicesProvider} from '../../../services/services.provider';
import { Storage } from '@ionic/storage';
import {keys} from '../../../utils/const/keys';
import {AlertController} from '@ionic/angular';
import {UtilsService} from '../../../services/utils.service';
import {IntentProvider} from '../../../services/intent.provider';
import {Utils} from '../../../utils/utils';

@Component({
    selector: 'register-step1',
    templateUrl: './step1.component.html',
    styleUrls: ['./step1.component.scss'],
})
export class Step1Component extends BasePage implements OnInit {

    number: number;
    token: '';

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.fetch(keys.UPDATE_REGISTRATION.TOKEN).then(
            success => {
                this.token = success;
            }
        );
    }

    nextStep() {
        if (String(this.number).length !== 10) {
            this.showError('Debe ingresar un número de suscriptor válido.');
        } else {
            this.showProgress();
            this.services.validateSubscriberUpdate(String(this.number), this.token).then(
                (success: any) => {
                    this.dismissProgress();
                    this.processResponse(success);
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }

    processResponse(response) {
        this.store(keys.UPDATE_REGISTRATION.SUBSCRIBER, String(this.number));
        this.store(keys.UPDATE_REGISTRATION.PRODUCT_TYPE_G, response.productType === 'G');
        this.store(keys.UPDATE_REGISTRATION.IS_TELEPHONY, Utils.isTelephony(
            response.accountType, response.accountSubType, response.productType));
        this.goPage('update/step2');
    }

}

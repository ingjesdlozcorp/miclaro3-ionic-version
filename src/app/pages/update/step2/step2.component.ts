import { Component, OnInit } from '@angular/core';
import {BasePage} from '../../base.page';
import {Router} from '@angular/router';
import {ServicesProvider} from '../../../services/services.provider';
import { Storage } from '@ionic/storage';
import {keys} from '../../../utils/const/keys';
import {AlertController, NavController} from '@ionic/angular';
import {UtilsService} from '../../../services/utils.service';
import {IntentProvider} from '../../../services/intent.provider';

@Component({
    selector: 'register-step2',
    templateUrl: './step2.component.html',
    styleUrls: ['./step2.component.scss'],
})
export class Step2Component extends BasePage implements OnInit {

    code = '';
    ssn = '';
    subscriber = '';
    isProductTypeG = true;
    isTelephony = false;
    token = '';

    username = '';
    password = '';
    keepAuthenticated = false;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.fetch(keys.UPDATE_REGISTRATION.SUBSCRIBER).then(
            success => {
                this.subscriber = success;
            }
        );
        this.fetch(keys.UPDATE_REGISTRATION.PRODUCT_TYPE_G).then(
            success => {
                this.isProductTypeG = success;
            }
        );
        this.fetch(keys.UPDATE_REGISTRATION.IS_TELEPHONY).then(
            success => {
                this.isTelephony = success;
            }
        );
        this.fetch(keys.UPDATE_REGISTRATION.TOKEN).then(
            success => {
                this.token = success;
            }
        );
        this.fetch(keys.UPDATE_REGISTRATION.USERNAME_USED).then(
            success => {
                this.username = success;
            }
        );
        this.fetch(keys.UPDATE_REGISTRATION.PASSWORD_USED).then(
            success => {
                this.password = success;
            }
        );
        this.fetch(keys.UPDATE_REGISTRATION.KEEP_AUTH).then(
            success => {
                this.keepAuthenticated = success;
            }
        );
    }

    nextStep() {
        if (this.isProductTypeG && this.code.length !== 6) {
            this.showError('Debe ingresar los datos solicitados.');
        } else if (!this.isProductTypeG && this.ssn.length !== 4) {
            this.showError('Debe ingresar los datos solicitados.');
        } else {
            this.showProgress();
            this.services.updateValidateAccount(this.subscriber, this.code, this.ssn, this.token).then(
                (success: any) => {
                    this.dismissProgress();
                    this.processResponse(success);
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }

    processResponse(response) {
        this.store(keys.REGISTER.SUBSCRIBER, this.subscriber);
        this.store(keys.LOGIN.USERNAME, this.username);
        this.store(keys.LOGIN.PASSWORD, this.password);
        this.store(keys.LOGIN.KEEP, this.keepAuthenticated);
        this.store(keys.LOGIN.IS_LOGGED, true);
        this.store(keys.LOGIN.TRY_IN, true);
        this.store(keys.SESSION_TOKEN, this.token);
        this.goLoginPage();
    }

    resend() {
        this.showProgress();
        this.services.validateSubscriberUpdate(this.subscriber, this.token).then(
            (success: any) => {
                this.dismissProgress();
                this.showAlert('Código de seguridad enviado.');
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../_shared/shared.module';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { UpdateComponent } from './update.component';
import { FormsModule } from '@angular/forms';
import { NgxPopperModule } from 'ngx-popper';
import { RouterModule, Routes } from '@angular/router';
import { UsernameComponent } from './username/username.component';
import { QuestionsComponent } from './questions/questions.component';

export const routes: Routes = [
    {path: 'step1', component: Step1Component},
    {path: 'step2', component: Step2Component},
    {path: 'username', component: UsernameComponent},
    {path: 'questions', component: QuestionsComponent},
];

@NgModule({
    declarations: [
        UsernameComponent,
        Step1Component,
        Step2Component,
        UpdateComponent,
        QuestionsComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        SharedModule,
        FormsModule,
        NgxPopperModule.forRoot({placement: 'top', styles: {'background-color': 'white'}})
    ]
})
export class UpdateModule {
}

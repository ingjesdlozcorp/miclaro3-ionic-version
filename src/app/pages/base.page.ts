import { Router } from '@angular/router';
import { ServicesProvider } from '../services/services.provider';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { BodyPopupBase, UtilsService } from '../services/utils.service';
import { IntentProvider } from '../services/intent.provider';
import { Account, Authenticate } from '../models/authenticate';
import { Utils } from '../utils/utils';
import { environment } from '../../environments/environment';
import { pages } from '../utils/const/pages';
import {keys} from '../utils/const/keys';

export class BasePage {

    public services: ServicesProvider;
    public utils: UtilsService;
    public visited = [];

    constructor(
        protected router: Router,
        private storage: Storage,
        private modelsServices: ServicesProvider,
        private alertController: AlertController,
        private utilsService: UtilsService,
        private userStorage: IntentProvider) {
        this.services = modelsServices;
        this.utils = utilsService;
        if (this.utilsService.isMenuOpen) {
            this.utilsService.openMenu.emit(false);
        }
    }

    getAlert(): AlertController {
        return this.alertController;
    }

    cacheStorage(): IntentProvider {
        return this.userStorage;
    }

    goPage(page: string) {
        this.utils.goTo(page);
    }

    goLoginPage(guest = false) {
        if (guest) {
            this.utils.goTo(pages.GUEST);
        } else {
            this.utils.goTo(pages.LOGIN);
        }
    }

    goHomePage() {
        if (this.cacheStorage().isBusiness()) {
            this.clearStore();
            this.utils.goTo('module/consumption');
        } else {
            this.utils.goTo('home/dashboard');
        }
    }

    goBack() {
        this.utils.back();
    }

    async openStore() {
        const SSOAccessToken = await this.fetch(keys.SSO_ACCESS_TOKEN);
        this.utils.registerScreen('store');
        const url = environment.STORE_URL + 'auth?token=&ssoToken=' + SSOAccessToken;
        this.openExternalBrowser(url);
    }

    /**
     * openExternalBrowser:
     *   to open navigation web in a external browser like safari or chrome
     *   this method is called immediately
     */
    openExternalBrowser(url) {
        this.utils.browserProvider.openExternalBrowser(url);
    }

    public store(key: string, value: any): Promise<any> {
        return new Promise<any>(resolve => {
            this.utils.storage.set(key, value).then(
                () => {
                    console.log('stored key ' + key);
                    resolve();
                }
            );
        });
    }

    public fetch(key: string): Promise<any> { // TODO, se debe remove el storage import del constructor del base component
        return this.utils.storage.get(key);
    }

    public clearStore(): Promise<any> {
        return this.utils.clearStore();
    }

    showProgress(message?: string) {
        this.utilsService.showLoader.emit({show: true, message});
    }

    dismissProgress() {
        this.utilsService.showLoader.emit({show: false, message: undefined});
    }

    showPopup(title: string, body: BodyPopupBase) {
        this.utilsService.popupBase.emit({open: true, title, body});
    }

    selectAccount(account: Account, fromLogin: boolean = false) {
        this.utils.resetTimer.emit(true);
        this.showProgress();
        const authenticateInfo: Authenticate = this.userStorage.loginData;
        this.userStorage.prepaidCustomerId = undefined;
        this.userStorage.hasConfirmDue = false;
        this.userStorage.accountListRefer = undefined;
        this.userStorage.tokenIptv = undefined;
        this.services.loadAccount(account.account, account.subsriberByDefault, this.userStorage.tokenSession, this.userStorage.isGuest)
            .then(() => {
                    this.cacheStorage().firstTimeDashboardTelephony = true;
                    if (Utils.isPrepaid(account.accountType, account.accountSubType)) {
                        this.cacheStorage().prepaidSelectedSubscriber = account.subsriberByDefault;
                    }
                    this.dismissProgress();
                    this.continueSelectAccount(authenticateInfo, fromLogin);
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message).then();
                }
            );
    }

    continueSelectAccount(authenticateInfo: Authenticate, fromLogin: boolean) {
        if (authenticateInfo.requiredAccountUpdate && !this.userStorage.isGuest) {
            this.goPage('update/username');
        } else if (authenticateInfo.requiredQuestions && !this.userStorage.isGuest) {
            this.showConfirmCustom('',
                'Por tu seguridad, debes configurar tus Preguntas de Seguridad.',
                'Configurar ahora',
                'Configurar luego',
                () => {
                    this.goPage('update/questions');
                },
                () => {
                    if (fromLogin && this.utils.biometricOptions.activated) {
                        this.goPage('touch');
                    } else {
                        this.goHomePage();
                    }
                }
            ).then();
        } else {
            if (fromLogin && this.utils.biometricOptions.activated) {
                this.goPage('touch');
            } else {
                this.goHomePage();
            }
        }
    }

    reloadCurrentAccount() {
        const account = this.userStorage.accountInfo.bANField;
        const subscriber = this.userStorage.accountInfo.defaultSubscriberField;
        return this.services.loadAccount(String(account), subscriber, this.userStorage.tokenSession, this.userStorage.isGuest);
    }

    public goPageNoAssociated() {
        this.goPage('/home/no-associated');
    }

    async showError(message, onDismiss?) {
        const alert = await this.alertController.create({
            header: 'Aviso',
            message,
            buttons: [{
                text: 'Ok',
                handler: () => {
                    if (onDismiss) {
                        onDismiss();
                    }
                }
            }],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showAlert(message, onDismiss?, okText?) {
        const alert = await this.alertController.create({
            message,
            buttons: [{
                text: okText === undefined ? 'Ok' : okText,
                handler: () => {
                    if (onDismiss) {
                        onDismiss();
                    }
                }
            }],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showConfirm(header, message, onAccept?, onCancel?) {
        const alert = await this.alertController.create({
            header,
            message,
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: () => {
                        if (onCancel) {
                            onCancel();
                        }
                    }
                },
                {
                    text: 'Aceptar',
                    handler: () => {
                        if (onAccept) {
                            onAccept();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showConfirmCustom(header, message, acceptButton, cancelButton, onAccept?, onCancel?) {
        const alert = await this.alertController.create({
            header,
            message,
            buttons: [
                {
                    text: cancelButton,
                    role: 'cancel',
                    handler: () => {
                        if (onCancel) {
                            onCancel();
                        }
                    }
                },
                {
                    text: acceptButton,
                    handler: () => {
                        if (onAccept) {
                            onAccept();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showChat(message, onChat, onDismiss?) {
        const alert = await this.alertController.create({
            cssClass: 'chat-alert',
            message:
                '        <div class="basicrow f-black f-mildt roboto-m">\n' +
                '            <img style="width: 40px; height: 40px;" src="assets/images/miclaro-icon.png" alt="">\n' +
                '            Bienvenido a Mi <b>Claro Asistencia</b>\n' +
                '        </div>\n' +
                '\n' +
                '        <div class="basicrow m-top-i">\n' +
                '            <div class="logline full"></div>\n' +
                '        </div>\n' +
                '\n' +
                '        <div class="basicrow f-reg f-black roboto-r m-top-i text-justify" id="chat-error-text">\n' +
                '            <b>Incidencia:</b> ' + message + '\n' +
                '        </div>\n' +
                '\n' +
                '        <div class="basicrow m-top-i">\n' +
                '            <div class="logline full"></div>\n' +
                '        </div>\n' +
                '\n' +
                '        <div class="basicrow f-reg f-black roboto-r m-top-ii text-justify">\n' +
                '            ¿Desea que uno de nuestros agentes de servicio al cliente te asista en línea a través de nuestro Chat?\n' +
                '        </div>',
            buttons: [
                {
                    text: 'Conectar Via Chat',
                    handler: () => {
                        if (onChat) {
                            onChat();
                        }
                    }
                },
                {
                    text: 'No, Gracias',
                    role: 'cancel',
                    handler: () => {
                        if (onDismiss) {
                            onDismiss();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showAlertSessionExpired(onDismiss?) {
        const alert = await this.alertController.create({
            header: 'Sesión Expirada',
            message: 'Estimado cliente su sesión ha expirado.',
            buttons: [{
                text: 'Ok',
                handler: () => {
                    if (onDismiss) {
                        onDismiss();
                    }
                    location.reload();
                }
            }],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showAlertPleaseLogin(onDismiss?) {
        const self = this;
        const alert = await this.alertController.create({
            header: 'Acceso Restringido',
            message: 'Estimado cliente, para ingresar a este y otros modulos, debes iniciar sesión.',
            buttons: [
                {
                    text: 'Ir al inicio de sesión',
                    handler: () => {
                        self.goLoginPage();
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: () => {
                        if (onDismiss) {
                            onDismiss();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showAlertOnlyOnWeb(onDismiss?) {
        const self = this;
        const alert = await this.alertController.create({
            header: 'Acceso Restringido',
            message: 'Estimado cliente, este servicio solo se encuentra disponible en  <b>Mi Claro Web</b>.',
            buttons: [
                {
                    text: 'Ir a Mi Claro Web',
                    handler: () => {
                        self.openExternalBrowser('https://miclaro.claropr.com/');
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: () => {
                        if (onDismiss) {
                            onDismiss();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    async showAlertAccessLimited(onOpenRegister?, onOpenLogin?, onDismiss?) {
        const alert = await this.alertController.create({
            message: 'Actualmente, estas en modo de usuario invitado. Si eres el dueño de la cuenta, debes autenticarte y/o registrarte para esta y otras secciones transacciones solo disponibles para el administrador.',
            buttons: [
                {
                    text: 'Ir a registro',
                    handler: () => {
                        if (onOpenRegister) {
                            onOpenRegister();
                        }
                        this.goPage('register/step1');
                    }
                },
                {
                    text: 'Autenticarse',
                    handler: () => {
                        if (onOpenLogin) {
                            onOpenLogin();
                        }
                        this.goPage('login');
                    }
                },
                {
                    text: 'Continuar como invitado',
                    role: 'cancel',
                    handler: () => {
                        if (onDismiss) {
                            onDismiss();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

    private redirectUserToModule(url, browser) {
        browser.close(); // Close el browser
        console.log(`CHAT - redirect to: ${url}`);

        if (url === 'https://tienda.claropr.com/' || url === 'https://miclaro.claropr.com/') {
            this.openExternalBrowser(url);
            return;
        }

        const isGuest = this.cacheStorage().isGuest;
        const isLogged = this.cacheStorage().logged;

        const typeBan = {
            postpaid: this.cacheStorage().isPostpaidAccount,
            prepaid: this.cacheStorage().isPrepaidAccount,
            telephony: this.cacheStorage().isTelephonyAccount
        };

        /**
         * Go to accounts manage
         */
        if (url.endsWith('account_management/1')) {
            if (isLogged) {
                if (!isGuest) {
                    this.goToPageModule('add-account');
                } else {
                    this.showAlertAccessLimited();
                }
            } else {
                this.showAlertPleaseLogin();
            }
        } else

        /**
         * Go to payment history
         */
        if (url.endsWith('bills-history')) {
            if (isLogged) {
                if (!isGuest) {
                    if (!typeBan.prepaid) {
                        this.goToPageModule('invoice-summary');
                    } else {
                        this.showAlert('Estimado cliente, este servicio no se encuentra disponible para clientes prepago');
                    }
                } else {
                    this.showAlertAccessLimited();
                }
            } else {
                this.showAlertPleaseLogin();
            }
        } else

        /**
         * Go to change plan
         */
        if (url.endsWith('changeplan')) {
            if (isLogged) {
                if (!isGuest) {
                    this.goToPageModule('change-plan');
                } else {
                    this.showAlertAccessLimited();
                }
            } else {
                this.showAlertPleaseLogin();
            }
        } else

        /**
         * Go to claro club
         */
        if (url.endsWith('club')) {
            if (isLogged) {
                this.goToPageModule('club/home');
            } else {
                this.showAlertPleaseLogin();
            }
        } else

        /**
         * Go to purchase additional data
         */
        if (url.endsWith('datapurchase')) {
            if (isLogged) {
                if (!isGuest) {
                    if (!typeBan.prepaid) {
                        this.goToPageModule('data-plan');
                    } else {
                        this.showAlert('Estimado cliente, este servicio no se encuentra disponible para clientes prepago');
                    }
                } else {
                    this.showAlertAccessLimited();
                }
            } else {
                this.showAlertPleaseLogin();
            }
        } else

        /**
         * Go to dashboard
         */
        if (url.endsWith('home')) {
            if (isLogged) {
                this.goHomePage();
            } else {
                this.showAlertPleaseLogin();
            }
        } else

        /**
         * Go to profile manage
         */
        if (url.endsWith('modifyaccounts/1')) {
            if (isLogged) {
                if (!isGuest) {
                    this.goToPageModule('profile');
                } else {
                    this.showAlertAccessLimited();
                }
            } else {
                this.showAlertPleaseLogin();
            }
        } else

        /**
         * Go to netflix
         */
        if (url.endsWith('netflix')) {
            if (!Utils.getPlatformInfo().ios) {
                if (isLogged) {
                    if (!isGuest) {
                        this.goToPageModule('netflix');
                    } else {
                        this.showAlertAccessLimited();
                    }
                } else {
                    this.showAlertPleaseLogin();
                }
            } else {
                this.showAlertOnlyOnWeb();
            }
        } else

        /**
         * Go to recharge prepaid
         */
        if (url.includes('prepaidrecharge')) {
            if (isLogged) {
                if (typeBan.prepaid) {
                    this.goToPageModule('recharge');
                } else {
                    this.showAlert('Estimado cliente, este servicio solo se encuentra disponible para clientes prepago');
                }
            } else {
                this.showAlertPleaseLogin();
            }
        } else

        /**
         * Go to referred system
         */
        if (url.endsWith('referred')) {
            if (isLogged) {
                if (!isGuest) {
                    if (!typeBan.prepaid) {
                        this.goToPageModule('refer/home');
                    } else {
                        this.showAlert('Estimado cliente, este servicio no se encuentra disponible para clientes prepago');
                    }
                } else {
                    this.showAlertAccessLimited();
                }
            } else {
                this.showAlertPleaseLogin();
            }
        } else

        /**
         * Go to device info
         */
        if (url.endsWith('servicesandequipment')) {
            if (isLogged) {
                if (!isGuest) {
                    if (!typeBan.prepaid) {
                        this.goToPageModule('device');
                    } else {
                        this.showAlert('Estimado cliente, este servicio no se encuentra disponible para clientes prepago');
                    }
                } else {
                    this.showAlertAccessLimited();
                }
            } else {
                this.showAlertPleaseLogin();
            }
        } else

        /**
         * Go to consumption
         */
        if (url.endsWith('usage')) {
            if (isLogged) {
                if (typeBan.prepaid) {
                    this.goToPageModule('consumption/prepaid');
                } else {
                    this.goToPageModule('consumption');
                }
            } else {
                this.showAlertPleaseLogin();
            }
        } else {
        /**
         * if url dont have any recognition
         */
            this.showAlert('Estimado cliente, el modulo al cual desea acceder no se encuentra disponible en este momento.');
        }
    }

    private goToPageModule(path: string) {
        this.goPage(`module/${path}`);
    }

    appReset() {
        this.userStorage.redirectData = undefined;
        this.userStorage.isAppReady = false;
    }

    async showAlertRateApp(later?, evaluate?, noMore?) {
        const alert = await this.alertController.create({
            header: 'Califica nuestra aplicación',
            message: '¿Estás disfrutando de Mi Claro PR? Tómese un momento para calificarla en la tienda de aplicaciones.',
            buttons: [
                {
                    text: 'Calificar ahora',
                    cssClass: 'primary-button-rate-dialog',
                    handler: () => {
                        if (evaluate) {
                            evaluate();
                        }
                    }
                },
                {
                    text: 'Luego',
                    cssClass: 'secondary-button-rate-dialog',
                    handler: () => {
                        if (later()) {
                            later();
                        }
                    }
                },
                {
                    text: 'No volver a mostrar',
                    cssClass: 'secondary-button-rate-dialog',
                    handler: () => {
                        if (noMore) {
                            noMore();
                        }
                    }
                },
            ],
            backdropDismiss: false
        });
        await alert.present();
    }
}

import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../services/utils.service';
import { IntentProvider } from '../../../services/intent.provider';
import { BasePage } from '../../base.page';
import { Account } from '../../../models/authenticate';
import { PlatformInfo, Utils } from '../../../utils/utils';
import { Page, Section } from '../../../models/account.access';
import { AccessFilter } from '../../../models/access.filter';
import { AccountInfo, SubscriberInfo } from '../../../models/account.details';
import { keys } from 'src/app/utils/const/keys';
import { APP } from '../../../utils/const/appConstants';
import { RedirectProvider } from '../../../services/redirect.provider';
import { BrowserProvider } from '../../../services/browser.provider';
import { BannerData, FirestoreService } from '../../../services/firestore.service';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { PaymentSupportService } from '../../_shared/components/flash-payment/core/services/payment-support.service';
import { PAYMENT_VERSION } from '../../_shared/components/flash-payment/core/flash-payment-constants';
import { Popups } from 'src/app/utils/const/popups';


const EXTERNAL_ACTION = 'REDIRECT_EXTERNAL';
const INTERNAL_ACTION = 'REDIRECT_INTERNAL';

@Component({
    selector: 'app-home-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent extends BasePage implements OnInit, AfterViewInit {

    accounts: Account[] = [];
    subscribers: SubscriberInfo[] = [];

    billBalance = '';
    lastPayment = '';
    billDueDate = '';
    billDate = '';
    amountPayable = '';
    check = false;
    paperless = false;
    typeBan = {
        postpaid: false,
        prepaid: false,
        telephony: false,
        byop: false
    };
    access: Section[] = [];
    selectedSubscriber: SubscriberInfo;
    product;
    showPopupGiftReceived = false;
    showPopupSplashClaroTv = false;
    isGuest = false;

    platform: PlatformInfo = Utils.getPlatformInfo();

    prepaidAddress: any[] = [];
    prepaidPayments: any[] = [];
    prepaidProducts: any[] = [];

    totalAvailable = 0;
    creditsToApply = 0;
    tempAvailableCredits = 0;
    checkAvailableCredits = false;
    canApplyCredits = false;
    discount = '$0.00';
    creditAmount = '0.00';

    productId: string = APP.PRODUCT_ID_INVOICE_PAYMENTS;
    bannerList: BannerData[] = [];

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private redirectProvider: RedirectProvider,
                private browserProvider: BrowserProvider, private firestoreService: FirestoreService,
                private appVersion: AppVersion, private paymentSupport: PaymentSupportService) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('dashboard');
    }

    ngOnInit() {
        this.getBanners();
        const data = this.cacheStorage();
        this.isGuest = data.isGuest;
        this.typeBan = {
            postpaid: data.isPostpaidAccount,
            prepaid: data.isPrepaidAccount,
            telephony: data.isTelephonyAccount,
            byop: data.isByop
        };
        this.access = data.access;
        this.showPopupSplashClaroTv = (data.firstTimeDashboardTelephony && data.isTelephonyAccount);

        this.billBalance = String(data.accountInfo.billBalanceField);
        this.lastPayment = Utils.formatAmount(String(data.accountInfo.lastPaymentAmountField));
        this.billDueDate = data.accountInfo.billDueDateField;
        this.billDate = data.accountInfo.billDateField;

        const amountDue = data.accountInfo.billBalanceField;
        let amountPayable = amountDue.includes('CR') ? '0' : amountDue;
        amountPayable = parseFloat(String(amountPayable
            .replace(',', '')))
            .toFixed(2);

        this.amountPayable = amountPayable;

        this.paperless = data.accountInfo.paperlessField;
        this.check = this.paperless;
        this.subscribers = data.subscriberList;
    }

    ngAfterViewInit() {
        this.showProgress();
        this.loadData().then(() => this.dismissProgress());
    }

    checkIfBanIsSuspend() {
        const accountInfo = this.cacheStorage().accountInfo;
        if (accountInfo.banStatusField === 'S' && !this.cacheStorage().hasConfirmDue) {
            this.showConfirmCustom('Aviso',
                'Su cuenta esta suspendida. Para activar la misma, favor realice su pago.',
                'Pagar', 'Cancelar', async () => {
                    const userPaymentVersion = await this.fetch(keys.PAYMENT.PAYMENT_VERSION);
                    if (userPaymentVersion && userPaymentVersion === PAYMENT_VERSION.P2P) {
                        let typeMessage = 'payment';
                        if (this.cacheStorage().isPrepaidAccount) {
                            typeMessage = 'prepaid';
                        }
                        this.paymentSupport.paymentRefactorProcess(typeMessage, +this.amountPayable);
                    } else {
                        this.doPayment(`${accountInfo.bANField}`, accountInfo.defaultSubscriberField, this.amountPayable);
                    }
                }, () => {
                    this.cacheStorage().hasConfirmDue = true;
                });
        }
    }

    async loadData() {
        if (this.typeBan.postpaid) {
            await this.get1GBReceived();
        }
        if (this.typeBan.prepaid) {
            this.selectedSubscriber = this.cacheStorage().getFullCurrentSelectedSubscriber();
        } else {
            await this.getUserCredits();
        }
        this.checkIfBanIsSuspend();
        this.redirectProvider.checkIfRedirectIsWaiting();
        // test
        this.showCustomerMessage();
    }

    async getUserCredits() {
        await this.services.getCredits(String(this.cacheStorage().accountInfo.bANField), this.cacheStorage().tokenSession).then(
            (response: any) => {
                this.totalAvailable = 0;
                if (response.CreditAsReferer !== null && response.CreditAsReferer !== undefined) {
                    if (response.CreditAsReferer.discount !== null && response.CreditAsReferer.discount !== undefined) {
                        this.totalAvailable = response.CreditAsReferer.discount;
                    }
                }
                if (response.CreditItems !== null && response.CreditItems !== undefined) {
                    if (response.CreditItems.length > 0) {
                        if (response.CreditItems[0].TotalAvailable > 0) {
                            if (response.CreditItems[0].TotalAvailable > this.totalAvailable) {
                                this.totalAvailable = response.CreditItems[0].TotalAvailable;
                            }
                        }
                    }
                }

                let mountToCompare = 50;

                if (this.typeBan.prepaid || this.typeBan.byop) {
                    mountToCompare = 25;
                }

                if (this.totalAvailable >= mountToCompare) {
                    this.creditsToApply = mountToCompare;
                } else {
                    this.creditsToApply = 0;
                }

                let discountType = '$';
                if (this.typeBan.telephony) {
                    discountType = '%';
                    this.discount = this.creditsToApply + ' ' + discountType;
                } else {
                    this.discount = discountType + '' + Utils.formatAmount(this.totalAvailable);
                    this.creditAmount = discountType + '' + Utils.formatAmount(this.creditsToApply);
                }

                this.canApplyCredits = this.totalAvailable > 0;
                if (this.checkAvailableCredits) {
                    this.checkAvailableCredits = false;
                    if (this.tempAvailableCredits === this.totalAvailable) {
                        this.showAlert('Aviso', 'Gracias por su interés en nuestro Programa Refiere y Gana! Su ' +
                            'balance se estará actualizando próximamente.');
                    } else {
                        this.showProgress();
                        this.reloadCurrentAccount().then(() => {
                            this.dismissProgress();
                            this.goHomePage();
                        }, error => {
                            this.dismissProgress();
                            this.showError(error.message);
                        });
                    }
                }
                this.tempAvailableCredits = this.totalAvailable;
            }, () => {
                this.dismissProgress();
            }
        );
    }

    applyCredits() {

        const accountInfo = this.cacheStorage().accountInfo;
        const paperless = accountInfo.paperlessField;

        let amount = 50;
        if (!this.typeBan.telephony) {
            amount = parseFloat(String(this.creditAmount.replace('$', '')));
        }

        const billBalance = accountInfo.billBalanceField;
        let debt = billBalance.includes('CR') ? 0 : billBalance;
        let creditsAvailable = this.totalAvailable;
        debt = parseFloat(String(debt));
        creditsAvailable = parseFloat(String(creditsAvailable));

        if (!paperless) {
            this.showError('Para participar en nuestro Programa Refiere y Gana, debe suscribirse a facturación ' +
                'electrónica. Para detalles, verifique los términos y condiciones de este Programa.');
        } else if ((this.typeBan.postpaid) && (billBalance.includes('CR') || parseFloat(billBalance) === 0)) {
            this.showError('Para aplicar su cupón de descuento deberá tener un balance pendiente.');
        } else if (creditsAvailable === 0) {
            this.showError('En este momento no cuenta con cupones disponibles.');
        } else if (this.typeBan.telephony) {
            this.showConfirmCustom(
                'Confirmación',
                'Al aplicar su cupón, el mismo se verá reflejado en su próximo ciclo de facturación.',
                'Aplicar', 'Regresar',
                () => {
                    this.applyCreditsToAccount(amount);
                }, () => {
                }
            );
        } else if (creditsAvailable < amount) {
            this.showError('En este momento no cuenta con cupones disponibles.');
        } else if (this.typeBan.prepaid || amount === debt) {
            this.showConfirmCustom(
                'Confirmación',
                'Una vez acepte redimir su cupón el mismo perderá su validez, este no podrá ser\n' +
                'cancelado, reversado, reembolsado o transferido.',
                'Aplicar', 'Regresar',
                () => {
                    this.applyCreditsToAccount(amount);
                }, () => {
                }
            );
        } else if (amount > debt) {
            this.showConfirmCustom(
                'Confirmación',
                'El valor del cupón (descuento) redimido es mayor al balance pendiente de su factura, al ' +
                'redimir este cupón usted perderá el valor del descuento restante.',
                'Aplicar', 'Regresar',
                () => {
                    this.applyCreditsToAccount(amount);
                }, () => {
                }
            );
        } else if (amount < debt) {
            this.showConfirmCustom(
                'Confirmación',
                'El valor del cupón (descuento) redimido es menor al balance pendiente de su factura. Favor ' +
                'de realizar el pago remanente de su factura en o antes de la fecha de vencimiento.',
                'Aplicar', 'Regresar',
                () => {
                    this.applyCreditsToAccount(amount);
                }, () => {
                }
            );
        }
    }

    applyCreditsToAccount(amount: number) {
        const accountInfo = this.cacheStorage().accountInfo;
        this.showProgress();
        this.services.applyCredits(
            String(accountInfo.bANField),
            accountInfo.defaultSubscriberField,
            amount,
            this.cacheStorage().tokenSession).then(
            (response: any) => {
                this.showAlert(response.errorDisplay, () => {
                    this.checkAvailableCredits = true;
                    this.getUserCredits();
                }, 'Continuar');
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    changePaperless(check) {
        if (check) {
            const data = this.cacheStorage();
            setTimeout(
                () => {
                    this.showProgress();
                    this.services.updateBillParameters(String(data.accountInfo.bANField), data.tokenSession).then(
                        () => {
                            this.reloadCurrentAccount().then((response: any) => {
                                    this.dismissProgress();
                                    this.paperless = true;
                                    this.check = true;
                                }, error => {
                                    this.dismissProgress();
                                    this.showError(error.message);
                                }
                            );
                        },
                        error => {
                            this.dismissProgress();
                            this.showError(error.message);
                            this.paperless = false;
                            this.check = false;
                        }
                    );
                },
                500);
        }
    }

    checkAccessPermission(id: number) {
        let permitted = false;
        this.access.forEach((section: Section) => {
            section.Pages.forEach((page: Page) => {
                if (page.accessID === id) {
                    permitted = true;
                    return permitted;
                }
            });
        });
        return permitted;
    }

    getExtraClass(id: number): string {
        let classE = '';
        this.access.forEach((section: Section) => {
            section.Pages.forEach((page: Page) => {
                if (page.accessID === id) {
                    classE = page.extraClass;
                    return classE;
                }
            });
        });
        return classE;
    }

    going(id: number) {
        const page: Page = this.cacheStorage().getAccessPageByID(id);
        if (this.cacheStorage().isGuest && !page.allowAsGuest) {
            this.showAlertAccessLimited();
        } else {
            if (id === 20) {
                this.openStore();
            } else {
                if (!this.isGuest && !this.services.isAccountDetailsUpdated() && this.pageIsNeedingSubscribers(page)) {
                    const account = this.cacheStorage().accountInfo.bANField;
                    const subscriber = this.cacheStorage().accountInfo.defaultSubscriberField;
                    this.showProgress();
                    this.services.loadAccount(String(account), subscriber, this.cacheStorage().tokenSession, this.cacheStorage().isGuest)
                        .then(() => {
                            this.dismissProgress();
                            this.services.setAccountDetailsUpdate();
                            this.filterAndGo(page);
                        });
                } else {
                    this.filterAndGo(page);
                }
            }
        }
    }

    pageIsNeedingSubscribers(page): boolean {
        for (const route of AccessFilter.routesToGo) {
            if (page.accessID === route.accessID && route.path !== '') {
                if (route.needSubscribers) {
                    return true;
                }
            }
        }
        return false;
    }

    filterAndGo(page) {
        AccessFilter.routesToGo.forEach(route => {
            if (page.accessID === route.accessID && route.path !== '') {
                this.goPage('module/' + route.path);
                return;
            }
        });
    }

    async billPayment() {
        const userPaymentVersion = await this.fetch(keys.PAYMENT.PAYMENT_VERSION);
        this.amountPayable = parseFloat(String(this.amountPayable)).toFixed(2);
        if (userPaymentVersion && userPaymentVersion === PAYMENT_VERSION.P2P) {
            let typeMessage = 'payment';
            if (this.cacheStorage().isPrepaidAccount) {
                typeMessage = 'prepaid';
            }
            this.paymentSupport.paymentRefactorProcess(typeMessage, +this.amountPayable);
        } else {
            const accountInfo: AccountInfo = this.cacheStorage().accountInfo;
            let creditAmountDue = accountInfo.billBalanceField.includes('CR') ? accountInfo.billBalanceField.replace('CR', '') : 0;
            let billBalance = accountInfo.billBalanceField.includes('CR') ? 0 : accountInfo.billBalanceField;
            creditAmountDue = parseFloat(String(creditAmountDue).replace(',', ''));
            billBalance = parseFloat(String(billBalance).replace(',', ''));

            if (Number(this.amountPayable) < 5) {
                this.showError('El monto no puede ser menor a $5.00');
            } else if (Number(this.amountPayable) > 800) {
                this.showError('El monto no puede ser mayor a $800.00');
            } else if (creditAmountDue > 0 && (creditAmountDue + Number(this.amountPayable)) > 800) {
                this.showError('El monto total abonado en su cuenta no puede ser mayor a $800.00');
            } else if (Number(this.amountPayable) > billBalance) {
                this.showConfirm('Confirmación', 'La cantidad ingresada es mayor al balance de su factura, la diferencia será acreditada a su cuenta.',
                    () => {
                        this.doPayment(`${accountInfo.bANField}`, accountInfo.defaultSubscriberField, this.amountPayable);
                    },
                    () => {
                        // Nothing to do
                    },
                );
            } else {
                this.doPayment(`${accountInfo.bANField}`, accountInfo.defaultSubscriberField, this.amountPayable);
            }
        }
    }

    async doPayment(account: string, subscriber: string, amount: string) {
        const response = await this.paymentSupport.paymentExternalProcess(account, subscriber, amount);
        this.dismissProgress();
        if (response && !response.hasError) {
            if (response.processEndState) {
                this.showProgress('Actualizando su balance...');
                this.reloadCurrentAccount().then(() => {
                    this.dismissProgress();
                    this.goHomePage();
                });
            } else {
                this.dismissProgress();
                this.showAlert('Su pago aun no ha sido procesado, si ya realizo el pago espere mientras lo procesamos.',
                    () => {
                        this.goHomePage();
                    });
            }
        } else {
            this.showError(response.message);
        }
    }

    recharge() {
        const rechargeAmount = parseFloat(this.amountPayable);
        this.amountPayable = parseFloat(String(rechargeAmount)).toFixed(2);

        if (Number.isNaN(rechargeAmount)) {
            this.amountPayable = '0.00';
            this.showError('El monto de recarga no es un número válido.');
            return;
        } else if (rechargeAmount < 5) {
            this.showError('El monto de recarga no puede ser menor a $5.00');
        } else if (rechargeAmount > 150) {
            this.showError('El monto de recarga no puede ser mayor a $150.00');
        } else {
            this.showConfirm(
                'Confirmación',
                '¿Desea proceder a recargar $' + Utils.formatAmount(rechargeAmount) + '?',
                () => {
                    this.getPrepaidProducts(rechargeAmount);
                },
                () => {
                    // Nothing to do
                },
            );
        }
    }

    getPrepaidProducts(rechargeAmount) {
        this.showProgress();
        const data = this.cacheStorage();
        this.services.listProductService(this.selectedSubscriber.subscriberNumberField, 2, data.tokenSession)
            .then(
                (response: any) => {
                    this.dismissProgress();
                    this.prepaidProducts = response.formProducts;
                    this.goToRechargeConfirm(rechargeAmount);
                },
                error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
    }

    getOtherProduct() {
        let item;
        this.prepaidProducts.forEach(product => {
            if (product.idProduct === 24) {
                item = product;
            }
        });
        return item;
    }

    goToRechargeConfirm(rechargeAmount) {
        const product = this.getOtherProduct();
        product.amountRecharge = rechargeAmount;
        const amount = Utils.formatAmount(product.amountRecharge);
        const ivu = Utils.formatAmount(product.amountRecharge * product.ivuState);
        const totalAmount = Utils.formatAmount(Number(amount) + Number(ivu));
        this.cacheStorage().PaymentProcess = {
            subscriber: this.selectedSubscriber.subscriberNumberField,
            payments: this.prepaidPayments,
            products: this.prepaidProducts,
            address: this.prepaidAddress,
            selectedProduct: product,
            amount,
            ivu,
            totalAmount,
        };
        this.goPage('/module/recharge/confirm');
    }

    getWalletBalance(): string {
        if (this.selectedSubscriber !== undefined) {
            return Utils.formatAmount(this.selectedSubscriber.eWalletBalanceField);
        }
        return '0.00';
    }

    getPrepaidBalance(): string {
        if (this.selectedSubscriber !== undefined) {
            return Utils.formatAmount(this.selectedSubscriber.prepaidBalanceField);
        }
        return '0.00';
    }

    getNextRecharge(): string {
        let nextRecharge = 'N/A';
        if (this.selectedSubscriber !== undefined && this.selectedSubscriber.planInfoField !== undefined) {
            nextRecharge = this.selectedSubscriber.planInfoField.endDateField;
            if (nextRecharge === null || nextRecharge === undefined || nextRecharge === '') {
                nextRecharge = 'N/A';
            }
        }
        return nextRecharge;
    }

    async get1GBReceived() {
        const data = this.cacheStorage();
        await this.services.getGift1GBSend(String(data.accountInfo.bANField), data.tokenSession).then(
            (response) => {
                if (response.Gift1GBsents.length > 0) {
                    this.product = response.Gift1GBsents[0];
                    this.store(keys.GIFT.DATA_GIFT, this.product);
                    this.showPopupGiftReceived = true;
                }
            }
        );
    }

    closePopup1GB() {
        this.showPopupGiftReceived = false;
        this.showProgress();
        const data = this.cacheStorage();
        this.services.getGift1GBByGUI(this.product.BANReceiver, this.product.GUI, data.tokenSession).then(
            (response) => {
                this.dismissProgress();
                this.showAlert('El Regalo ha sido aceptado con Éxito',
                    () => {
                        this.goHomePage();
                    }
                );
            },
            error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    changePrepaidSubscriber() {
        this.showProgress();
        this.dismissProgress();
        this.cacheStorage().prepaidSelectedSubscriber = this.selectedSubscriber.subscriberNumberField;
        this.continueSelectAccount(this.cacheStorage().loginData, false);
    }

    openChat() {
        this.redirectProvider.openChat();
    }

    async getBanners() {
        let versionString;
        if (!Utils.getPlatformInfo().desktop) {
            versionString = await this.appVersion.getVersionNumber();
        }
        const version = versionString ? versionString.split('.').join('') : null;
        this.firestoreService.getAllBanners().subscribe(
            (bannerListSnapshot) => {
                const bannerList = [];
                bannerListSnapshot.map((bannerData: any) => {
                    const banner = bannerData.payload.doc.data();
                    if ((!banner.version || !version) || +version >= banner.version) {
                        bannerList.push(banner);
                    }
                });
                console.log(bannerList);
                this.bannerList = bannerList;
            }
        );
    }

    checkBannerVisibility(banner): boolean {
        return this.firestoreService.checkBannerVisibility(this.typeBan, banner);
    }

    goForBannerAction(banner: BannerData) {
        if (banner.isClickable) {
            if (banner.actionType === EXTERNAL_ACTION) {
                this.openExternalBrowser(banner.action);
            } else {
                if (banner.action.toLowerCase().includes('iptv')) {
                    this.cacheStorage().processIptvFrom = 'banner';
                    this.utils.registerScreen('iptv');
                }
                this.utils.goTo(banner.action);
            }
        }
    }

    closePopupClaroTv(action: boolean) {
        if (action) {
            this.going(40);
            this.cacheStorage().processIptvFrom = 'splash';
            this.utils.registerScreen('iptv', {from: 'splash'});
        }
        this.showPopupSplashClaroTv = false;
        this.cacheStorage().firstTimeDashboardTelephony = false;
    }

    async showCustomerMessage() {
        console.log('entrando');
        const messagesList = this.cacheStorage().accountDetails.Messages.MessagesList;
        const ban = this.cacheStorage().accountInfo.bANField;
        const SSOAccessToken = await this.fetch(keys.SSO_ACCESS_TOKEN);
        const token = this.cacheStorage().tokenSession;
        const method = 'updMessageStatus';
        const title = 'Tienes un mensaje!';
        let contentSplash;
        let idNotic;
        if (!this.typeBan.telephony) {
            messagesList.forEach((element) => {
                if (element.alert_type === 2 && element.status === 0) {
                    idNotic = element.id_message;
                    contentSplash = element.message;
                    contentSplash = contentSplash.replaceAll(/click.aspx\?/ig, 'click.aspx?token=&ssoToken=' + SSOAccessToken + '&notificationDetailId=' + idNotic + '&');
                    this.showPopup(title, {
                        name: Popups.ACCOUNT_MESSAGE, data: {
                            contentSplash,
                            idNotic,
                            token,
                            ban,
                            method
                        }
                    });
                }
            });
        }

    }
}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { SharedModule } from '../_shared/shared.module';
import { NoAssociatedComponent } from './no-associated/no-associated.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxPopperModule } from 'ngx-popper';
import { IonicModule } from '@ionic/angular';
import { FlashPaymentModule } from '../_shared/components/flash-payment/flash-payment.module';

export const routes: Routes = [
    {path: 'dashboard', component: DashboardComponent},
    {path: 'no-associated', component: NoAssociatedComponent}
];

@NgModule({
    declarations: [
        HomeComponent,
        DashboardComponent,
        NoAssociatedComponent
    ],
    exports: [RouterModule],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        SharedModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        IonicModule,
        NgxPopperModule.forRoot({placement: 'top', styles: {'background-color': 'white'}}),
        FlashPaymentModule
    ]
})
export class HomeModule {
}

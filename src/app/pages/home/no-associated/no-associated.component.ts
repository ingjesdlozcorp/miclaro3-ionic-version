import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {ServicesProvider} from '../../../services/services.provider';
import {AlertController} from '@ionic/angular';
import {UtilsService} from '../../../services/utils.service';
import {IntentProvider} from '../../../services/intent.provider';
import {BasePage} from '../../base.page';
import {Account} from '../../../models/authenticate';

@Component({
    selector: 'app-home-no-associated',
    templateUrl: './no-associated.component.html',
    styleUrls: ['./no-associated.component.scss'],
})
export class NoAssociatedComponent extends BasePage implements OnInit {

    accounts: Account[] = [];

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {

    }

    openAccountsManage() {
        this.goPage('module/add-account');
    }
}

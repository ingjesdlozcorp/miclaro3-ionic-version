import {Component, OnInit} from '@angular/core';
import {ServicesProvider} from '../../../services/services.provider';
import {BasePage} from 'src/app/pages/base.page';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {AlertController} from '@ionic/angular';
import {UtilsService} from '../../../services/utils.service';
import {keys} from '../../../utils/const/keys';
import {IntentProvider} from '../../../services/intent.provider';
import {environment} from '../../../../environments/environment';
import {KeychainTouchId} from '@ionic-native/keychain-touch-id/ngx';
import {Utils} from '../../../utils/utils';

@Component({
    selector: 'app-touch',
    templateUrl: './touch.component.html',
    styleUrls: ['./touch.component.scss'],
})

export class TouchComponent extends BasePage implements OnInit {

    biometricIsTouch = true;
    username;
    ios = Utils.getPlatformInfo().ios;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider,
                private biometric: KeychainTouchId) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('biometric_configuration');
    }

    ngOnInit() {
        this.username = this.cacheStorage().loginData.username;
        this.biometricIsTouch = this.utils.biometricOptions.type !== 'face';
    }

    cancel() {
        this.goHomePage();
    }

    configure() {
        this.biometric.save(environment.BIOMETRIC_SECRET_KEY, this.utils.biometricOptions.password).then(
            (res: any) => {
                if (this.utils.biometricOptions.ios) {
                    this.checkBiometric();
                } else {
                    this.completeConfiguration();
                }
            },
            (error: any) => {
                this.store(keys.LOGIN.BIOMETRIC, false);
                console.log(error);
            });
    }

    checkBiometric() {
        let text = '';
        if (this.utils.biometricOptions.type === 'face') {
            text = 'Ponga su rostro por favor';
        } else {
            text = 'Ponga su huella dactilar por favor';
        }
        this.biometric.verify(environment.BIOMETRIC_SECRET_KEY, text).then(
            (res: any) => {
                this.completeConfiguration();
            },
            (error: any) => {
                this.store(keys.LOGIN.BIOMETRIC, false);
                console.log(error);
            });
    }

    completeConfiguration() {
        this.store(keys.LOGIN.BIOMETRIC, true);
        this.goHomePage();
    }
}

import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { IntentProvider } from "src/app/services/intent.provider";
import { ServicesProvider } from "src/app/services/services.provider";
import { StorageProvider } from "src/app/services/storage.provider";
import { UtilsService } from "src/app/services/utils.service";
import { BasePage } from "../../base.page";
import { Storage } from "@ionic/storage";
import { Device } from "@ionic-native/device/ngx";
import { keys } from "src/app/utils/const/keys";

@Component({
    selector: "app-push-confirmation",
    templateUrl: "./push-confirmation.component.html",
    styleUrls: ["./push-confirmation.component.scss", "../mfa/mfa.component.scss"],
})
export class PushConfirmationComponent extends BasePage implements OnInit {
    code = "";
    device = "";
    locationDevice = "";
    time: any;
    email: string = "";
    constructor(
        router: Router,
        storage: Storage,
        modelsServices: ServicesProvider,
        alertController: AlertController,
        utilsService: UtilsService,
        userStorage: IntentProvider,
        private storageProvider: StorageProvider,
        private _device: Device
    ) {
        super(
            router,
            storage,
            modelsServices,
            alertController,
            utilsService,
            userStorage
        );
    }

    ngOnInit() {
      console.log(this.router.url);
      this.evaluateDataFromRedirect();
      // https://miclaromfauat.claropr.com/authentication-callback?code=e4809c51-60d1-461e-878c-f1d236627b4a
      this.getConfirmationData();
    }

    evaluateDataFromRedirect() {
      const code = this.router.parseUrl(this.router.url).queryParams['code'];
      this.code = code;
      console.log(code);
    }

    getConfirmationData() {
      this.showProgress();
      this.services.getConfirmationData(this.code).then(
        (resp: any) => {
          this.dismissProgress();
          console.log(resp);
          if (resp.hasError) {
            this.router.navigate(['/login']);
            this.showAlert(resp.errorDisplay);
          } else {
            this.device = resp.data.deviceUserAgent
            this.locationDevice = resp.data.location
            this.time = new Date()
            this.email = resp.data.email
          }
        },
        (error) => {
          this.dismissProgress();
          this.showAlert(error.message);
        }
      );
    }

    sendConfirmationAnswer(isOk: boolean) {
      const payload = {
        token: this.code,
        answer: isOk ? 'ACCEPTED' : 'NEGATED',
      };
      this.showProgress();
      this.services.sendConfirmationAnswer(payload).then(
        (resp: any) => {
          this.dismissProgress();
          if (!resp.hasError) {
            this.showAlert('Gracias por validar el acceso.');
          } else {
            //this.router.navigate(['login']);
            this.showAlert(resp.errorDisplay);
          }
          window.close();
        },
        (error) => {
          this.dismissProgress();
          this.showAlert(error.message);
        }
      );
    }
}

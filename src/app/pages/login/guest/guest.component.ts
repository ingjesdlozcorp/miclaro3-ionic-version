import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { BasePage } from '../../base.page';
import { UtilsService } from '../../../services/utils.service';
import { keys } from '../../../utils/const/keys';
import { IntentProvider } from '../../../services/intent.provider';
import { RedirectProvider } from '../../../services/redirect.provider';
import { StorageProvider } from '../../../services/storage.provider';

@Component({
    selector: 'app-login-guest',
    templateUrl: './guest.component.html',
    styleUrls: ['./guest.component.scss'],
})
export class GuestComponent extends BasePage implements OnInit {

    number: number;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private redirectProvider: RedirectProvider,
                private storageProvider: StorageProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('guest');
    }

    async ngOnInit() {
        const isLogged = await this.fetch(keys.LOGIN.IS_LOGGED);
        const isGuest = await this.fetch(keys.LOGIN.IS_GUEST);
        const subscriber = await this.fetch(keys.LOGIN.SUBSCRIBER);

        if (isLogged && isGuest && subscriber) {
            this.number = subscriber;
            this.login();
        } else {
            this.redirectProvider.checkIfRedirectIsWaiting();
        }
    }

    async login() {
        if (String(this.number).length !== 10) {
            this.showError('Debe ingresar un número de suscriptor válido.').then();
        } else {
            this.showProgress();
            const pushToken = await this.storageProvider.getNotificationPushToken();
            this.services.loginGuest(String(this.number), pushToken).then(
                (success: any) => {
                    this.cacheStorage().loginData = success;
                    this.cacheStorage().loginData.enterAsGuest = true;
                    if (success.requiredPasswordReset) {
                        this.dismissProgress();
                        this.goPage('recover/step5');
                    } else {
                        this.store(keys.LOGIN.SUBSCRIBER, String(this.number));
                        this.store(keys.LOGIN.IS_LOGGED, true);
                        this.store(keys.LOGIN.IS_GUEST, true);
                        this.store(keys.LOGIN.BIOMETRIC, false);
                        this.selectAccount(this.cacheStorage().getAccountByBan(success.account));
                        this.utils.storeDataForWidgets(success.account, success.subscriber, success.productType);
                    }
                }, error => {
                    this.dismissProgress();
                    if (error.desc && error.desc.includes('device token incorrecto')) {
                        this.update();
                    } else {
                        this.validateUser();
                    }
                }
            );
        }
    }

    validateUser() {
        this.showProgress();
        this.services.validateUser(String(this.number)).then(
            (success: any) => {
                this.dismissProgress();
                if (!success.AccountExist) {
                    this.showError('El número de teléfono ingresado no se encuentra registrado en nuestros sistemas, ' +
                        'su formato es incorrecto o no pertenece a nuestra red. Por favor intente nuevamente.').then();
                } else {
                    if (success.userExist) {
                        this.update();
                    } else {
                        this.register();
                    }
                }
            }, error => {
                this.dismissProgress();
                this.showError(error.message).then();
            }
        );
    }

    async register() {
        this.showProgress();
        const pushToken = await this.storageProvider.getNotificationPushToken();
        this.services.registerGuest(String(this.number), pushToken).then(
            (success: any) => {
                this.dismissProgress();
                this.store(keys.REGISTER.SUBSCRIBER, String(this.number));
                this.store(keys.REGISTER.TOKEN, success.token);
                this.store(keys.REGISTER.IS_GUEST_UPDATE, false);
                this.goPage('/register/guest');
            }, error => {
                this.dismissProgress();
                this.showError(error.message).then();
            }
        );
    }

    update() {
        this.showProgress();
        this.services.resendGuestCode(String(this.number)).then(
            (success: any) => {
                this.dismissProgress();
                this.store(keys.REGISTER.SUBSCRIBER, String(this.number));
                this.store(keys.REGISTER.TOKEN, success.token);
                this.store(keys.REGISTER.IS_GUEST_UPDATE, true);
                this.goPage('/register/guest');
            }, error => {
                this.dismissProgress();
                this.showError(error.message).then();
            }
        );

    }
}

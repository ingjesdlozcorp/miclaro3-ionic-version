import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AlertController} from '@ionic/angular';
import {IntentProvider} from 'src/app/services/intent.provider';
import {ServicesProvider} from 'src/app/services/services.provider';
import {UtilsService} from 'src/app/services/utils.service';
import {BasePage} from '../../base.page';
import {Storage} from '@ionic/storage';
import {keys} from 'src/app/utils/const/keys';

const MFA_STRINGS = {
    ACCEPTED: 'ACCEPTED',
    NEGATED: 'NEGATED',
    PENDING: 'PENDING',
    EXPIRED: 'EXPIRED',
    WITHOUT_PERMISSION: 'No se ha concedido el permiso',

}



@Component({
    selector: 'app-mfa',
    templateUrl: './mfa.component.html',
    styleUrls: ['./mfa.component.scss'],
})
export class MfaComponent extends BasePage implements OnInit {
    actualStep = 0;
    stepOneData: any;
    verifyCount = 0;
    stop = false;
    mfaToken = '';

    constructor(
        router: Router,
        storage: Storage,
        modelsServices: ServicesProvider,
        alertController: AlertController,
        utilsService: UtilsService,
        userStorage: IntentProvider
    ) {
        super(
            router,
            storage,
            modelsServices,
            alertController,
            utilsService,
            userStorage
        );
    }

    async ngOnInit() {
        this.mfaToken = await this.fetch(keys.MFA_TOKEN);
    }

    updateStep(event: any) {
        console.log(event);
        this.stepOneData = event;
        this.actualStep = event.actualStep;
        if (this.stepOneData.isPush && event.actualStep > 0) {
            this.verifyCount = 0;
            this.verifyMfaStatus();
        }

        if (event.actualStep === 0) {
            this.resetView();
        }
    }

    async verifyMfaStatus() {
        const payload = {
            token: await this.fetch(keys.MFA_TOKEN),
            answer:
                this.stepOneData.authOptionSelected === 0
                    ? MFA_STRINGS.ACCEPTED
                    : MFA_STRINGS.NEGATED,
        };
        this.services.verifyMFAStatus(payload).then(
            (resp: any) => {
                if (!resp.hasError) {
                    if (resp.data.status === MFA_STRINGS.PENDING) {
                        this.verifyAggain();
                    } else {
                        this.validateMfaStatusResponse(resp);
                    }
                } else {
                    this.showAlert(resp.errorDisplay);
                    this.actualStep = 0;
                }
            },
            (error) => {
                this.actualStep = 0;
                this.showAlert(error.message);
                this.router.navigate(['/login']);
            }
        );
    }

    validateMfaStatusResponse(resp: any) {
        if (resp.data.status !== MFA_STRINGS.NEGATED && resp.data.status !== MFA_STRINGS.EXPIRED) {
            this.callPermission();
            if (this.stepOneData.remenberOptionSelected === 0) {
                this.registerDevice();
            }
        }  else {
            const message = MFA_STRINGS.WITHOUT_PERMISSION
            this.showError(message, this.logOut()).then();
        }
    }

    verifyAggain() {
        if (!this.stop && this.verifyCount >= 0) {
            this.verifyCount++;
            setTimeout(() => {
                this.verifyMfaStatus();
            }, 3000);
        }
    }

    async callPermission() {
        const keepAuthActivated = await this.fetch(keys.LOGIN.KEEP);
        this.selectAccount(
            this.cacheStorage().getAccountByBan(
                this.cacheStorage().loginData.account
            ),
            !this.utils.biometricOptions.activated
        );
        if (keepAuthActivated || this.utils.biometricOptions.activated) {
            this.utils.storeDataForWidgets(
                this.cacheStorage().loginData.account,
                this.cacheStorage().loginData.subscriber,
                this.cacheStorage().loginData.productType
            );
        }
    }

    async registerDevice() {
        const payload = {
            token: this.mfaToken,
            answer: '',
            bearerToken: await this.fetch(keys.SESSION_TOKEN)
        };
        this.services.registerMFAUserDevice(payload).then(
            (resp) => {
                console.log(resp);
            },
            (error) => {
                this.showAlert(error.message);
            }
        );
    }

    resetView() {
        this.actualStep = 0;
        this.verifyCount = -5;
        this.showProgress();
        if (this.stepOneData.isPush) {
            this.stop = true;
            setTimeout(() => {
                this.stop = false;
                this.dismissProgress();
            }, 5000);
        }
    }

    logOut() {
        this.router.navigate(['/login']);
    }
}


import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {AlertController} from '@ionic/angular';
import {IntentProvider} from 'src/app/services/intent.provider';
import {ServicesProvider} from 'src/app/services/services.provider';
import {UtilsService} from 'src/app/services/utils.service';
import {Storage} from '@ionic/storage';
import {BasePage} from 'src/app/pages/base.page';
import {keys} from 'src/app/utils/const/keys';

@Component({
    selector: 'app-mfa-step-two',
    templateUrl: './mfa-step-two.component.html',
    styleUrls: ['./mfa-step-two.component.scss', '../../mfa.component.scss'],
})
export class MfaStepTwoComponent extends BasePage implements OnInit {
    @Input() actualStep;
    @Input() isEmail;
    @Input() isSms;
    @Input() emailOptionData;
    @Input() phoneNumberSelected;
    @Input() userId = '';
    @Input() rememberOptionSelected;
    @Input() phoneOptionData;
    @Input() optionCode = '';
    @Input() isPush = false;
    @Input() pushDeviceInfo: any = '';
    authorizeOptions = [0, 1];
    authOptionSelected = 0;
    part1 = '';
    part2 = '';
    part3 = '';
    part4 = '';
    part5 = '';
    part6 = '';
    errorCode = false;
    @Output() next = new EventEmitter<any>();

    constructor(
        router: Router,
        storage: Storage,
        modelsServices: ServicesProvider,
        alertController: AlertController,
        utilsService: UtilsService,
        userStorage: IntentProvider
    ) {
        super(
            router,
            storage,
            modelsServices,
            alertController,
            utilsService,
            userStorage
        );
    }

    ngOnInit() {
        console.log(this.rememberOptionSelected)
    }

    prepareEmail() {
        let emailToShow = '';
        if (this.emailOptionData.device.email.length > 0) {
            const separator = this.emailOptionData.device.email.indexOf('@');
            emailToShow = `${this.emailOptionData.device.email.substring(
                0,
                3
            )}*****${this.emailOptionData.device.email.substring(
                separator,
                this.emailOptionData.device.email.length
            )}`;
            return emailToShow;
        } else {
            return '';
        }
    }

    preparePhone(phone: string) {
        return `*** ***  ${phone.substring(phone.length - 4, phone.length)}`;
    }

    changeInput(e: any, p: any, c: any, n: any) {
        const length = c.value.length;
        const maxLength = c.getAttribute('maxlength');
        if (length == 1) {
            if (n !== '') {
                n.focus();
            }
        }

        if (e.key === 'Backspace') {
            if (p !== '') {
                p.focus();
            }
        }
    }

    processMFAOption() {
        this.cleanCode();
        this.showProgress();
        const payload = {
            userId: Number(this.userId),
            code: this.optionCode,
        };
        console.log('payload', payload);
        this.services.processMFAOption(payload).then(
            (resp: any) => {
                this.dismissProgress();
                console.log(resp);
                this.evaluateMfaOptions(resp);
            },
            (error) => {
                this.dismissProgress();
                this.showAlert(error.message);
            }
        );
    }

    evaluateMfaOptions(resp: any) {
        if (!resp.hasError) {
            if (!this.isPush) {
                const data = {
                    isEmail: this.isEmail,
                    isSms: this.isSms,
                    emailOptionData: this.emailOptionData,
                    phoneOptionData: this.phoneOptionData,
                };
                if (this.isSms) {
                    data.phoneOptionData.device.phone = this.preparePhone(
                        this.phoneNumberSelected
                    );
                }

                const message = this.isSms ?
                    `Se envió un SMS al ${data.phoneOptionData.device.phone} con el código de acceso` :
                    ` El código de acceso fue enviado a su correo`;

                this.showAlert(message);
            } else {
                if (resp.data.brand && resp.data.model) {
                    this.pushDeviceInfo =
                        resp.data.brand + ' ' + resp.data.model;
                } else {
                    this.pushDeviceInfo = null;
                }
            }

            this.actualStep = 1;
        } else {
            this.showAlert(resp.errorDisplay);
        }
    }

    back() {
        this.cleanCode();
        this.actualStep = 0;
    }

    cleanCode() {
        this.part1 = '';
        this.part2 = '';
        this.part3 = '';
        this.part4 = '';
        this.part5 = '';
        this.part6 = '';
    }

    verifyCode() {
        this.errorCode = false;
        const code =
            this.part1 +
            this.part2 +
            this.part3 +
            this.part4 +
            this.part5 +
            this.part6;

        if (code.length < 6) {
            this.errorCode = true;
            return;
        }
        this.validateWithService(code);
    }

    validateWithService(code: string) {
        this.showProgress();
        let payload: any;
        if (this.isSms) {
             payload = {
                subscriber: this.isSms ? this.phoneNumberSelected : null,
                code
            };
        } else {
             payload = {
                subscriber: this.isSms ? this.phoneNumberSelected : null,
                code,
                userId: this.isSms ? null : Number(this.userId),
            };
        }
        console.log('payload', payload);
        this.showProgress();
        this.services.validateOtpCode(payload).then(
            (resp: any) => {
                this.dismissProgress();
                this.processValidateCodeResponse(resp);
            },
            (error) => {
                console.warn(error);
                this.dismissProgress();
                this.showAlert(error.message);
            }
        );
    }

    processValidateCodeResponse(resp: any) {
        if (!resp.hasError) {
            this.isOkResponse();
        } else {
            this.failValidationProcess(resp);
        }
    }

    isOkResponse() {
        if (this.rememberOptionSelected === 0) {
            this.registerDevice();
        }
        this.callPermission();
    }

    failValidationProcess(resp: any) {
        const message: string = resp.errorDisplay;
        this.errorCode = true;
        const data: any = {
            isError: true,
            message,
        };
        this.showError(data.message);
        if (
            message.trim().toLocaleLowerCase() ===
            'su sesión ha vencido, por favor vuelva a intentarlo'
        ) {
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 2000);
        }
    }

    exit() {
        this.next.emit({actualStep: 0});
        sessionStorage.clear();
        localStorage.clear();
        this.router.navigate(['/login']);
        this.utils.mfaCanceled = true
    }

    stepBefore() {
        this.next.emit({actualStep: 0});
    }

    async callPermission() {
        const biometric = await this.fetch(keys.LOGIN.BIOMETRIC);
        const keepAuthActivated = await this.fetch(keys.LOGIN.KEEP);
        this.selectAccount(this.cacheStorage().getAccountByBan(this.cacheStorage().loginData.account), !biometric);
        if (keepAuthActivated || this.utils.biometricOptions.activated) {
            this.utils.storeDataForWidgets(this.cacheStorage().loginData.account, this.cacheStorage().loginData.subscriber, this.cacheStorage().loginData.productType);
        }
    }

    async registerDevice() {
        const payload = {
            token: await this.fetch(keys.MFA_TOKEN),
            answer: '',
            bearerToken: this.cacheStorage().loginData.token,
        };
        this.services.registerMFAUserDevice(payload).then(
            (resp) => {
                console.log(resp);
            },
            (error) => {
                this.showAlert(error.message);
            }
        );
    }
}

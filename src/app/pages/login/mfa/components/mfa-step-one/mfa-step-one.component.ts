import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {AlertController} from '@ionic/angular';
import {BasePage} from 'src/app/pages/base.page';
import {IntentProvider} from 'src/app/services/intent.provider';
import {RedirectProvider} from 'src/app/services/redirect.provider';
import {ServicesProvider} from 'src/app/services/services.provider';
import {StorageProvider} from 'src/app/services/storage.provider';
import {UtilsService} from 'src/app/services/utils.service';
import {Storage} from '@ionic/storage';
import {Device} from '@ionic-native/device/ngx';
import {keys} from 'src/app/utils/const/keys';

@Component({
    selector: 'app-mfa-step-one',
    templateUrl: './mfa-step-one.component.html',
    styleUrls: ['./mfa-step-one.component.scss', '../../mfa.component.scss'],
})
export class MfaStepOneComponent extends BasePage implements OnInit {
    phoneNumberList: any[] = [];
    isEmail = true;
    isSms = false;
    isPush = false;
    emailOptionData: any;
    phoneOptionData: any;
    pushOptionData: any;
    optionCode = '';
    rememberOptions = [0, 1];
    rememberOptionSelected = 0;
    phoneNumberSelected: any;
    userId = '';
    userData: any[] = [];
    pushDeviceInfo: any = '';
    actualStep = 0;
    name = '';
    authOptionSelected = 0;
    @Output() next = new EventEmitter<any>();


    constructor(
        router: Router,
        storage: Storage,
        modelsServices: ServicesProvider,
        alertController: AlertController,
        utilsService: UtilsService,
        userStorage: IntentProvider,
        private redirectProvider: RedirectProvider,
        private storageProvider: StorageProvider,
        private device: Device
    ) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    async ngOnInit() {
        this.name = 'test';
        this.userId = await this.fetch(keys.USER_ID);
        this.getMFAOptions(this.userId);
    }

    selectOption(id: number) {
        this.resetSelectionValues();
        setTimeout(() => {
            this.evaluateChoice(id);
        }, 100);
    }

    resetSelectionValues() {
        this.isEmail = false;
        this.isSms = false;
        this.isPush = false;
    }

    evaluateChoice(id: number) {
        if (id === 0) {
            this.isEmail = true;
            this.optionCode = this.emailOptionData.code;
        } else if (id === 1) {
            this.isSms = true;
            this.optionCode = this.phoneOptionData.code;
        } else if (id === 2) {
            this.isPush = true;
            this.optionCode = this.pushOptionData.code;
        }
    }

    prepareEmail() {
        let emailToShow = '';
        if (this.emailOptionData.device.email.length > 0) {
            const separator = this.emailOptionData.device.email.indexOf('@');
            emailToShow = `${this.emailOptionData.device.email.substring(
                0,
                3
            )}*****${this.emailOptionData.device.email.substring(
                separator,
                this.emailOptionData.device.email.length
            )}`;
            return emailToShow;
        } else {
            return '';
        }
    }

    preparePhone(phone: string) {
        return `*** ***  ${phone.substring(phone.length - 4, phone.length)}`;
    }

    remenberDevice() {
        setTimeout(() => {
            console.log(this.rememberOptionSelected);
        }, 500);
    }

    getMFAOptions(userId: string) {
        this.showProgress();
        this.services.getMFAOptions(userId).then(
            (resp: any) => {
                this.dismissProgress();
                console.log(resp);
                if (resp.data) {
                    this.userData = resp.data;
                    this.fillOptions();
                } else {
                    this.showAlert(resp.errorDisplay);
                    this.router.navigate(['/login']);
                }
            },
            (error) => {
                this.dismissProgress();
                this.showAlert(error.messgae);
                this.router.navigate(['/login']);
            }
        );
    }

    fillOptions() {
        this.userData.forEach((element) => {
            if (element.code === 'EMAIL') {
                this.emailOptionData = element;
                this.selectOption(0);
            }

            if (element.code === 'SMS') {
                this.phoneOptionData = element;
                this.fillPhoneData();
            }

            if (element.code === 'PUSH_NOTIFICATION') {
                this.pushOptionData = element;
            }
        });
    }

    fillPhoneData() {
        this.phoneNumberList.push(this.phoneOptionData.device.phone);
        this.phoneNumberSelected = this.phoneOptionData.device.phone;
    }

    processMFAOption() {
        this.showProgress();
        const payload = {
            userId: Number(this.userId),
            code: this.optionCode,
        };
        console.log('payload', payload);
        this.services.processMFAOption(payload).then(
            (resp: any) => {
                this.dismissProgress();
                console.log(resp);
                this.evaluateMfaOptions(resp);
            },
            (error) => {
                console.warn(error);
                this.dismissProgress();
                this.showAlert(error.message);
            }
        );
    }

    evaluateMfaOptions(resp: any) {
        const data = {
            phoneNumberList: this.phoneNumberList,
            isEmail: this.isEmail,
            isSms: this.isSms,
            isPush: this.isPush,
            emailOptionData: this.emailOptionData,
            phoneOptionData: this.phoneOptionData,
            pushOptionData: this.pushOptionData,
            optionCode: this.optionCode,
            rememberOptionSelected: this.rememberOptionSelected,
            phoneNumberSelected: this.phoneNumberSelected,
            userId: this.userId,
            userData: this.userData,
            pushDeviceInfo: this.pushDeviceInfo,
            actualStep: this.actualStep,
            name: this.name,
            authOptionSelected: this.authOptionSelected
        };
    
        if (!resp.hasError) {
            if (!this.isPush) {
                if (this.isSms) {
                    data.phoneOptionData.device.phone = this.preparePhone(
                        this.phoneNumberSelected
                    );
                }

                const message = this.isSms ?
                    `Se envió un SMS al ${data.phoneOptionData.device.phone} con el código de acceso` :
                    ` El código de acceso fue enviado a su correo`;

                this.showAlert(message);
            } else {
                if (resp.data.brand && resp.data.model) {
                    this.pushDeviceInfo = resp.data.brand + ' ' + resp.data.model;
                } else {
                    this.pushDeviceInfo = null;
                }
            }
            data.actualStep = 1;
            this.next.emit(data);
        } else {
            this.showAlert('No se pudo enviar el código en estos momentos por favor intente nuevamente más tarde');
        }
    }

    exit() {
        sessionStorage.clear();
        localStorage.clear();
        this.router.navigate(['/login']);
        this.utils.mfaCanceled = true
    }

    showTooltipInfo() {
        const message = 'Si selecciona la opción de recordar dispositivo, la aplicación registrará el equipo en el cuál esta ingresando y no le pedirá verificar nuevamente su identidad si inicia sesión desde el mismo equipo.'
        this.showAlert(message);
    }


}

import { Component, OnInit, NgZone } from '@angular/core';
import { ServicesProvider } from '../../../services/services.provider';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../services/utils.service';
import { keys } from '../../../utils/const/keys';
import { IntentProvider } from '../../../services/intent.provider';
import { environment } from '../../../../environments/environment';
import { KeychainTouchId } from '@ionic-native/keychain-touch-id/ngx';
import { Utils } from '../../../utils/utils';
import { RedirectProvider } from '../../../services/redirect.provider';
import { StorageProvider } from '../../../services/storage.provider';
import { BrowserProvider } from '../../../services/browser.provider';
import { Device } from '@ionic-native/device/ngx';

const DEFAULT_LOGIN_OPTIONS = {
    isLogged: false,
    keep: false,
    tryAgain: false,
    biometric: false,
    username: undefined,
    password: undefined
};

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})

export class LoginComponent extends BasePage implements OnInit {

    username = '';
    password = '';
    biometricAvailable = true;

    keepAuthActivated = false;
    biometricActivated = false;

    showKeepAuthenticate = true;
    showBiometricLogin = true;
    loginStore: {
        isLogged: boolean,
        keep: boolean,
        tryAgain: boolean,
        biometric: boolean,
        username: string,
        password: string
    };
    biometricAccepted = false;
    isTouchBiometric = true;
    ios = Utils.getPlatformInfo().ios;
    userId = 0;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private biometric: KeychainTouchId,
                private redirectProvider: RedirectProvider, private storageProvider: StorageProvider,
                private zone: NgZone, private browserProvider: BrowserProvider, private device: Device) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('login');
    }

    async ngOnInit() {

        this.zone.runOutsideAngular(() => {
            document.getElementById('password').addEventListener('change', (event) => {
              this.password = event.target['value'];
            });
            document.getElementById('username').addEventListener('change', (event) => {
                this.username = event.target['value'];
              });
          }
        );

        this.loginStore = Object.assign({}, DEFAULT_LOGIN_OPTIONS);

        const isLogged = await this.fetch(keys.LOGIN.IS_LOGGED);
        if (isLogged !== undefined && isLogged !== null) {
            this.loginStore.isLogged = isLogged;
        }

        const keep = await this.fetch(keys.LOGIN.KEEP);
        if (keep !== undefined && keep !== null) {
            this.loginStore.keep = keep;
        }

        const tryAgain = await this.fetch(keys.LOGIN.TRY_IN);
        if (tryAgain !== undefined && tryAgain !== null) {
            this.loginStore.tryAgain = tryAgain;
        }

        const biometric = await this.fetch(keys.LOGIN.BIOMETRIC);
        if (biometric !== undefined && biometric !== null) {
            this.loginStore.biometric = biometric;
        }

        const username = await this.fetch(keys.LOGIN.USERNAME);
        if (username !== undefined && username !== null) {
            this.loginStore.username = username;
        }

        const password = await this.fetch(keys.LOGIN.PASSWORD);
        if (password !== undefined && password !== null) {
            this.loginStore.password = password;
        }

        this.biometricAvailable = this.utils.biometricOptions.available;
        this.isTouchBiometric = this.utils.biometricOptions.type === 'touch';

        await this.validateKeepData();
    }

    async validateKeepData() {
        if (this.loginStore.isLogged && !this.utils.mfaCanceled) {
            if (this.loginStore.keep || this.loginStore.tryAgain) {
                this.username = this.loginStore.username;
                this.password = this.loginStore.password;
                this.keepAuthActivated = true;
                this.showBiometricLogin = false;
                this.showKeepAuthenticate = true;
                await this.store(keys.LOGIN.TRY_IN, false);
                this.login();
            } else {
                if (this.loginStore.biometric && this.utils.biometricOptions.available) {
                    this.verifyBiometricIdentity();
                }
            }
        } else {
            this.redirectProvider.checkIfRedirectIsWaiting();
        }
    }

    verifyBiometricIdentity() {
        let text = '';
        let textError = '';
        if (this.utils.biometricOptions.type === 'face') {
            text = 'Ponga su rostro por favor';
            textError = 'Por favor coloque el rostro que usa para desbloquear su dispositivo, ' +
                'de caso contrario seleccione cancelar para desactivar el Face ID.';
        } else {
            text = 'Ponga su huella dactilar por favor';
            textError = `Por favor coloque la huella que usa para desbloquear su dispositivo, ' +
                'de caso contrario seleccione cancelar para desactivar el ${ this.ios ? 'Touch ID' : 'Fingerprint' }.`;
        }
        this.biometric.verify(environment.BIOMETRIC_SECRET_KEY, text).then(
            (res: any) => {
                this.username = this.loginStore.username;
                this.password = this.loginStore.password;
                this.biometricActivated = true;
                this.biometricAccepted = true;
                this.onMarkBiometric();
                this.login();
            },
            (error: any) => {
                this.biometricAccepted = false;
                this.showConfirmCustom('', textError, 'Volver a Intentar', 'Cancelar',
                    () => {
                        this.verifyBiometricIdentity();
                    }, () => {
                        this.clearStore().then(() => {
                            this.loginStore = DEFAULT_LOGIN_OPTIONS;
                        });
                    }
                );
            });
    }

    identityEdited() {
        if (this.biometricAccepted) {
            this.biometricActivated = false;
            this.biometricAccepted = false;
        }
    }

    async login() {
        this.username = this.username.trim();
        this.password = this.password.trim();
        this.utils.hideKeyboard();

        if (!this.username || !this.password) {
            this.showError('Debe ingresar su usuario y contraseña.').then();
            return;
        }
        this.showProgress();
        const pushToken = await this.storageProvider.getNotificationPushToken();
        this.services.login(this.username, this.password, pushToken).then(
            async (success: any) => {
                this.checkRateApp();
                this.updateDeviceToken(success.token, pushToken);
                if (!Utils.isBusinessAccount(success.accountType)) {
                    this.cacheStorage().loginData = success;
                    if (success.requiredPasswordReset) {
                        await this.store(keys.RECOVER_PASSWORD.TOKEN, success.token);
                        this.dismissProgress();
                        this.goPage('recover/step5');
                    } else {
                        await this.store(keys.PAYMENT.PAYMENT_VERSION, success.userPaymentVersion);
                        const isGuest = await this.fetch(keys.LOGIN.IS_GUEST);
                        await this.store(keys.LOGIN.USERNAME, this.username);
                        await this.store(keys.LOGIN.PASSWORD, this.password);
                        await this.store(keys.LOGIN.KEEP, this.keepAuthActivated);
                        await this.store(keys.LOGIN.BIOMETRIC, false);
                        this.userId = success.accounts.AccountList[0].userID
                        await this.store(keys.USER_ID, this.userId);
                        await this.store(keys.SSO_ACCESS_TOKEN, success.SSOAccessToken);
                        if (this.biometricAccepted) {
                            await this.store(keys.LOGIN.BIOMETRIC, true);
                        }
                        await this.store(keys.LOGIN.IS_LOGGED, true);
                        if (isGuest && !this.keepAuthActivated) {
                            await this.store(keys.LOGIN.IS_GUEST, true);
                        } else {
                            await this.store(keys.LOGIN.IS_GUEST, false);
                        }
                        this.utils.biometricOptions.activated = this.biometricActivated;
                        this.utils.biometricOptions.password = this.password;
                      
                        if (success.mfaEnabled) {
                            this.validateIfIsNewDevice(pushToken);
                        } else {
                            this.callPermission();
                        }
                        /*
                        this.selectAccount(this.cacheStorage().getAccountByBan(success.account), !this.biometricAccepted);
                        if (this.keepAuthActivated || this.biometricActivated) {
                            this.utils.storeDataForWidgets(success.account, success.subscriber, success.productType);
                        }
                        */
                    }
                } else {
                    this.store(keys.LOGIN.USERNAME, this.username);
                    this.store(keys.LOGIN.PASSWORD, this.password);
                    this.store(keys.LOGIN.IS_LOGGED, true);
                    this.store(keys.LOGIN.IS_GUEST, false);
                    this.browserProvider.openExternalBrowser(`${environment.WEB_PAGE_BUSINESS}/login?token=${success.token}`);
                    this.dismissProgress();
                }
            }, async (error) => {
                const self = this;
                if (error.num === 100 && Utils.isBusinessAccount(error.accountType)) {
                    this.browserProvider.openExternalBrowser(`${environment.WEB_PAGE_BUSINESS}/login?token=${error.token}`);
                    this.dismissProgress();
                } else if (error.num === 30 || error.num === 32) {
                    this.dismissProgress();
                    this.showChat(error.message, () => {
                        this.redirectProvider.openChat();
                    }).then();
                } else if (error.num === 41) {
                    this.dismissProgress();
                    await this.store(keys.UPDATE_REGISTRATION.TOKEN, error.token);
                    await this.store(keys.UPDATE_REGISTRATION.USERNAME_USED, this.username);
                    await this.store(keys.UPDATE_REGISTRATION.PASSWORD_USED, this.password);
                    await this.store(keys.UPDATE_REGISTRATION.KEEP_AUTH, this.keepAuthActivated);
                    this.goPage('update/step1');
                } else {
                    this.dismissProgress();
                    if (error.message.includes('credenciales de acceso incorrectas')) {
                        await this.clearStore();
                    }
                    this.showError(error.message).then();
                }
            }
        );
    }

    onKeepAuthenticated() {
        this.utils.mfaCanceled = false;
        if (this.keepAuthActivated) {
            this.showConfirm(
                'Mantener Autenticado',
                'Usted permanecera conectado hasta que cierre la sesión.',
                () => {
                    this.keepAuthActivated = true;
                    this.showBiometricLogin = false;
                    this.showKeepAuthenticate = true;
                },
                () => {
                    this.keepAuthActivated = false;
                    this.showBiometricLogin = true;
                    this.showKeepAuthenticate = true;
                }
            ).then();
        } else {
            this.showBiometricLogin = true;
            this.showKeepAuthenticate = true;
        }
    }

    onMarkBiometric() {
        if (this.biometricActivated) {
            this.showBiometricLogin = true;
            this.showKeepAuthenticate = false;
        } else {
            this.showBiometricLogin = true;
            this.showKeepAuthenticate = true;
        }
    }

    checkRateApp() {
        this.storageProvider.isRated()
            .then(isRated => {
                if (!isRated) {
                    this.storageProvider.getOpenAppTimes()
                        .then(times => {
                            if (times >= 2) {
                                this.showAlertRateApp(
                                    () => { },
                                    () => {
                                        this.storageProvider.setRated(true).then();
                                        this.openAppStore();
                                    },
                                    () => {
                                        this.storageProvider.setRated(true).then();
                                    }
                                );
                                this.storageProvider.setOpenAppTimes(0).then();
                            } else {
                                this.storageProvider.increaseOpenAppTimes().then();
                            }
                        });
                }
            });
    }

    openAppStore() {
        if (Utils.getPlatformInfo().android) {
            this.openExternalBrowser(`market://details?id=${environment.PACKET_NAME}`);
        } else if (Utils.getPlatformInfo().ios) {
            this.openExternalBrowser('https://apps.apple.com/us/app/mi-claro-pr/id775322054');
        } else {
            this.openExternalBrowser('https://play.google.com/store/apps/details?id=com.todoclaro.miclaroapp');
        }
    }

    updateDeviceToken(token, pushToken) {
        const dto = {
            bearerToken: token,
            deviceToken: pushToken,
            userAgent: navigator.userAgent,
            deviceBrand: this.device.manufacturer,
            deviceModel: this.device.model
        };
        console.log(dto);
        this.services.updateDeviceToken(dto).then(
            (resp: any) => {
                if (!resp.hasError) {
                    console.log('device registered');
                } else {
                    const message = resp.errorDisplay ? resp.errorDisplay : resp.errorDesc;
                    this.showError(message).then();
                }
            }, error => {
                // TODO
                console.log(error);
            }
        )
    }

    validateIfIsNewDevice(deviceToken: string) {
        const payload = {
            userId: this.userId,
            userAgent: navigator.userAgent.toLowerCase(),
            brand: this.device.manufacturer,
            model: this.device.model,
            deviceToken: deviceToken,
            ip: '',
          };
          console.log(payload);
          this.services.validateNewDevice(payload).then(
            (resp: any) => {
                this.dismissProgress()
                if (resp.data.deviceStatus === "DEVICE_NOT_REGISTERED") {
                    this.goToMfa(resp.data.token);
                  } else {
                    this.callPermission();
                  }
            }, error =>{

                this.dismissProgress();
                console.log(error);
            }
          );
    }

    async goToMfa(mfaToken) {
        await this.store(keys.MFA_TOKEN, mfaToken);
        this.goPage('mfa')
    }

    callPermission() {
        this.selectAccount(this.cacheStorage().getAccountByBan(this.cacheStorage().loginData.account), !this.biometricAccepted);
        if (this.keepAuthActivated || this.biometricActivated) {
            this.utils.storeDataForWidgets(this.cacheStorage().loginData.account,   this.cacheStorage().loginData.subscriber, this.cacheStorage().loginData.productType);
        }
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { LoadingComponent } from 'src/app/pages/_shared/components/loading/loading.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MenuComponent } from './components/menu/menu.component';
import { AccountSelectComponent } from './components/account-select/account-select.component';
import { HeaderStaticComponent } from './components/header-static/header-static.component';
import { GiftReceivedComponent } from './components/popup/gift-received/gift-received.component';
import { PopupGeneralTermsComponent } from './components/popup/general-terms/popup-general-terms.component';
import { IptvPlansComponent } from './components/iptv/iptv-plans/iptv-plans.component';
import { PopupIptvChannelsComponent } from './components/iptv/popup-channels/popup-iptv-channels.component';
import { PopupTermsOfferComponent } from './components/iptv/popup-terms-offer/popup-terms-offer.component';
import { PopupTermsIptvComponent } from './components/iptv/popup-terms-iptv/popup-terms-iptv.component';
import { ClaroTvComponent } from './components/iptv/claro-tv/claro-tv.component';
import { SplashClaroTvComponent } from './components/popup/splash-claro-tv+/splash-claro-tv.component';
import { PopupBaseComponent } from './components/popup/base-popup/popup-base.component';
import { ModalAccessibilityComponent } from './components/modal-accessibility/modal-accessibility.component';
import { OnlyNumberDirective } from './directives/only-number.directive';
import { MessagePopupComponent } from './components/popup/message-popup/message-popup.component';

@NgModule({
    declarations: [
        LoadingComponent,
        HeaderComponent,
        FooterComponent,
        MenuComponent,
        AccountSelectComponent,
        HeaderStaticComponent,
        GiftReceivedComponent,
        SplashClaroTvComponent,
        PopupGeneralTermsComponent,
        IptvPlansComponent,
        PopupTermsIptvComponent,
        PopupIptvChannelsComponent,
        PopupTermsOfferComponent,
        ClaroTvComponent,
        PopupBaseComponent,
        ModalAccessibilityComponent,
        OnlyNumberDirective,
        MessagePopupComponent
    ],
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        IonicModule
    ],
    exports: [
        LoadingComponent,
        HeaderComponent,
        FooterComponent,
        MenuComponent,
        AccountSelectComponent,
        HeaderStaticComponent,
        GiftReceivedComponent,
        SplashClaroTvComponent,
        PopupGeneralTermsComponent,
        IptvPlansComponent,
        ClaroTvComponent,
        PopupIptvChannelsComponent,
        PopupTermsOfferComponent,
        PopupBaseComponent,
        MessagePopupComponent,
        ModalAccessibilityComponent,
        OnlyNumberDirective
    ],
    entryComponents: [ModalAccessibilityComponent],
})
export class SharedModule { }

import { Component, OnInit } from '@angular/core';
import {UtilsService} from '../../../../services/utils.service';

const GENERAL_MESSAGE = 'Espere un momento mientras es procesada la información.';

@Component({
    selector: 'app-loading',
    templateUrl: './loading.component.html',
    styleUrls: ['./loading.component.scss'],
})
export class LoadingComponent implements OnInit {

    message = GENERAL_MESSAGE;
    show = false;

    constructor(private service: UtilsService) {
        this.service.showLoader.subscribe((data: { show: boolean, message: string }) => {
            this.show = data.show;
            this.message = data.message ? data.message : GENERAL_MESSAGE;
        });
    }

    ngOnInit() {}

}

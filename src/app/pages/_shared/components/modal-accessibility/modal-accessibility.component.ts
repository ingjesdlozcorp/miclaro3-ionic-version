import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UtilsService } from '../../../../services/utils.service';
import { StorageProvider } from '../../../../services/storage.provider';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
    selector: 'app-modal-accesibility',
    templateUrl: './modal-accessibility.component.html',
    styleUrls: ['./modal-accessibility.component.scss'],
})
export class ModalAccessibilityComponent implements OnInit {

    constructor(private modalController: ModalController,
                private appVersion: AppVersion,
                private utilsService: UtilsService,
                private storageProvider: StorageProvider) {
    }

    ngOnInit() {
    }

    async continue() {
        const version = await this.appVersion.getVersionNumber();
        await this.storageProvider.setAccessAccepted({isAccepted: true, version});
        await this.modalController.dismiss();
    }

    exitApp() {
        this.utilsService.exitApp();
    }
}

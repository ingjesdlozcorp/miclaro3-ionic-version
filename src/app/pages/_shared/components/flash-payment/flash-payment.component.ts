import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController, IonContent, ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { FlashController } from './core/services/flash-controller.service';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../services/services.provider';
import { UtilsService } from '../../../../services/utils.service';
import { IntentProvider } from '../../../../services/intent.provider';
import { BasePage } from '../../../base.page';
import { Account } from '../../../../models/authenticate';
import { SubscriberInfo } from '../../../../models/account.details';
import { TRANSACTION_TYPES } from './core/flash-payment-constants';

@Component({
    selector: 'app-flash-payment',
    templateUrl: './flash-payment.component.html',
    styleUrls: ['./flash-payment.component.scss'],
})
export class FlashPaymentComponent extends BasePage implements OnInit {

    @ViewChild('contentFlashPayment', {static: true}) private content: IonContent;
    postpago;
    prepago;
    selectBan: SubscriberInfo;
    token: string;
    banlist: Account[];
    lastPaymentAmountField;
    cycleDateField;
    billBalanceField;
    cycleEndDateField;
    cycleStartDateField;
    billDueDateField;
    pastDueAmountField;
    initialize: Subscription;
    canUpdateAmount = false;
    editOperationAmount = '';

    constructor(private modalController: ModalController,
                public flashPaymentService: FlashController,
                router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.token = this.cacheStorage().tokenSession;
        this.checkIfHasData();
        this.subscribeToScrollBottom();
    }

    checkIfHasData() {
        if (!this.cacheStorage().flashPayment.flashData) {
            this.dismissModal();
        } else {
            this.flashPaymentService.controller = this.cacheStorage().flashPayment.flashData;
            this.fromInvoiceView();
            this.flashLogin();
            this.flashPaymentService.getTokenCards();
            this.flashPaymentService.getLocalData();
        }
    }

    fromInvoiceView() {
        if (this.token) {
            this.banlist = this.cacheStorage().accountList;
            this.selectBan = this.cacheStorage().selectedSubscriber;
            this.getAccountInfoFormStorage();
            this.dismissProgress();
        }
    }

    getAccountInfoFormStorage() {
        const accountDetails = this.cacheStorage().accountDetails;
        this.lastPaymentAmountField = accountDetails.AccounInfo.lastPaymentAmountField;
        this.cycleDateField = accountDetails.AccounInfo.cycleDateField;
        this.billBalanceField = accountDetails.AccounInfo.billBalanceField;
        this.cycleEndDateField = accountDetails.AccounInfo.cycleEndDateField;
        this.cycleStartDateField = accountDetails.AccounInfo.cycleStartDateField;
        this.billDueDateField = accountDetails.AccounInfo.billDueDateField;
        this.pastDueAmountField = accountDetails.AccounInfo.pastDueAmountField;
    }

    flashLogin() {
        this.flashPaymentService.paymentLogin();
    }

    changeAmount(event: {value?: string, edit: boolean}) {
        if (event.value.toString().substring((event.value.length - 2), (event.value.length)) === 'CR') {
            return;
        } else {
            if (+event.value >= 5 && +event.value <= 2000) {
                this.flashPaymentService.controller.operationAmount = event.value;
                this.editOperationAmount = event.value;
            } else {
                this.showError('La cantidad a pagar debe ser mayor a $5.00 o menor de $2000.00');
                return;
            }
        }
    }

    activateChange(event: {value?: string, edit: boolean}) {
        console.log(event);
        if (event.edit) {
            if (event.value) {
                if (+event.value >= 5 && +event.value <= 2000) {
                    this.flashPaymentService.controller.operationAmount = event.value;
                    this.editOperationAmount = event.value;
                } else {
                    this.showError('El monto debe ser mayor o igual a 5$ y menor o igual a $2000.00');
                    return;
                }
            }
        }
        if (this.flashPaymentService.controller.transactionType === TRANSACTION_TYPES[0]) {
            this.canUpdateAmount = !this.canUpdateAmount;
            if (this.canUpdateAmount) {
                this.editOperationAmount = this.flashPaymentService.controller.operationAmount;
            }
        }

    }

    dismissModal(isFinished = false) {
        this.flashPaymentService.initData();
        this.flashPaymentService.controller.openWeb = false;

        this.modalController.dismiss({isPayment: isFinished});
    }

    private subscribeToScrollBottom() {
        this.flashPaymentService.scrollToBottom.subscribe(async () => {
            await this.content.scrollToBottom(500);
        });
    }

    openStepOne() {
        if (this.flashPaymentService.controller.step3) {
            return;
        }
        this.flashPaymentService.controller.step1 = true;
        this.flashPaymentService.controller.step2 = false;
        this.flashPaymentService.controller.step3 = false;
    }
}

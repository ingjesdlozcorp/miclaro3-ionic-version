import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { FlashController } from '../../../core/services/flash-controller.service';
import { BasePage } from '../../../../../../base.page';
import { ServicesProvider } from '../../../../../../../services/services.provider';
import { UtilsService } from '../../../../../../../services/utils.service';
import { IntentProvider } from '../../../../../../../services/intent.provider';

@Component({
    selector: 'app-credit-card',
    templateUrl: './credit-card.component.html',
    styleUrls: ['../way-to-pay.component.scss']
})
export class CreditCardComponent extends BasePage implements OnInit {

    @Input() openForm: boolean;
    @Output() action = new EventEmitter<any>();

    constructor(public flashPaymentService: FlashController,
                router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit(): void {
        this.flashPaymentService.readyToFillForm();
    }

    changeIcon() {
        this.openForm = !this.openForm;
        if (this.flashPaymentService.controller.tokenInstrumentId !== 0) {
            this.flashPaymentService.controller.tokenInstrumentId = 0;
            this.flashPaymentService.controller.cardSelected = null;
            this.flashPaymentService.selectCard({id: -1});
        }
        if (this.openForm) {
            this.flashPaymentService.scrollBottom();
        }

        this.action.emit(true);
    }

    pay() {
        this.openDialog();
    }

    openDialog(): void {
        const card = this.flashPaymentService.controller.cardSelected;
        this.showConfirm('', `¿Deseas realizar el pago con la tarjeta ${card.franchiseName}  XXXX-XXXX-XXXX-${card.lastFourDigits}?`,
            () => {
                this.confirmCVV();
            });
    }

    private async confirmCVV() {
        const alert = await this.getAlert().create({
            header: 'Atención',
            message: 'Introduzca el CVV de su tarjeta para continuar.',
            inputs: [
                {
                    name: 'cvv',
                    type: 'number',
                    max: '9999',
                    min: '100',
                    placeholder: '022'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel'
                },
                {
                    text: 'Aceptar',
                    handler: (data) => {
                        console.log('-> data', data);
                        const cvv = data.cvv.trim().length;
                        if (!cvv) {
                            this.showError('El CVV es un campo obligatorio para poder continuar', () => this.confirmCVV());
                            return;
                        }
                        if (cvv < 3 || cvv > 4) {
                            this.showError('Su CVV no corresponde con la longitud estandar, verifique para continuar', () => this.confirmCVV());
                            return;
                        }
                        this.flashPaymentService.payWithCreditCard(data.cvv.trim());
                    }
                }
            ],
            backdropDismiss: false
        });
        await alert.present();
    }

}



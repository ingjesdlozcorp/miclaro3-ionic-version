import { Component, Input, OnInit } from '@angular/core';
import { FlashController } from '../../../core/services/flash-controller.service';

@Component({
    selector: 'app-wallet',
    templateUrl: './wallet.component.html',
    styleUrls: ['../way-to-pay.component.scss']
})
export class WalletComponent implements OnInit {

    constructor(public flashPaymentService: FlashController) {
    }

    ngOnInit(): void {
    }

}

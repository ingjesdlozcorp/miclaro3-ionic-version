import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FlashController } from '../../../core/services/flash-controller.service';
import { PAYMENT_FORM_METHOD } from '../../../core/flash-payment-constants';
import { IonSlides } from '@ionic/angular';

@Component({
    selector: 'app-options-for-payment',
    templateUrl: './options-for-payment.component.html',
    styleUrls: ['../way-to-pay.component.scss']
})
export class OptionsForPaymentComponent implements OnInit {

    @ViewChild('slides', {static: false}) slides: IonSlides;
    @Output() action = new EventEmitter<any>();
    openForm = false;

    constructor(public flashPaymentService: FlashController) {
    }

    ngOnInit(): void {
        setTimeout(() => {
            if (!this.flashPaymentService.controller.isPlaceToPayVisible) {
                this.selectForm(PAYMENT_FORM_METHOD.CARD);
            }
        }, 1500);
        this.flashPaymentService.restartWayToPay.subscribe(restart => {
            if (restart) {
                this.changePaymentMethod();
            }
        });
    }

    async selectForm(idForm: number) {
        this.flashPaymentService.initData();
        if (idForm === PAYMENT_FORM_METHOD.CARD) {
            setTimeout(() => {
                this.flashPaymentService.controller.useSavedInformation = false;
                this.flashPaymentService.autoFill();
            }, 1500);

            this.flashPaymentService.controller.isCard = true;
            this.flashPaymentService.controller.isACH = false;
            this.flashPaymentService.controller.isPlaceToPay = false;
            if (!this.flashPaymentService.controller.tokenCards.length) {
                this.openForm = true;
            }
        } else if (idForm === PAYMENT_FORM_METHOD.WALLET) {
            this.flashPaymentService.controller.isACH = false;
            this.flashPaymentService.controller.isCard = false;
            this.flashPaymentService.controller.isPlaceToPay = true;
        } else {
            this.flashPaymentService.controller.isCard = false;
            this.flashPaymentService.controller.isACH = true;
            this.flashPaymentService.controller.isPlaceToPay = false;
        }

        this.action.emit(
            {openForm: this.openForm}
        );
    }

    async changePaymentMethod() {
        const indexActive: number = await this.slides.getActiveIndex();
        if (this.flashPaymentService.controller.isCardVisible &&
            this.flashPaymentService.controller.isPlaceToPayVisible &&
            this.flashPaymentService.controller.isACHVisible) {
            this.selectForm(indexActive);
        } else if (this.flashPaymentService.controller.isCardVisible &&
            this.flashPaymentService.controller.isACHVisible &&
            !this.flashPaymentService.controller.isPlaceToPayVisible
        ) {
            if (indexActive === PAYMENT_FORM_METHOD.CARD) {
                this.selectForm(indexActive);
            } else if (indexActive === 1) {
                this.selectForm(PAYMENT_FORM_METHOD.ACH);
            }
        } else if (this.flashPaymentService.controller.isCardVisible &&
            !this.flashPaymentService.controller.isACHVisible &&
            this.flashPaymentService.controller.isPlaceToPayVisible) {
            if (indexActive === 0) {
                this.selectForm(indexActive);
            } else if (indexActive === 1) {
                this.selectForm(PAYMENT_FORM_METHOD.WALLET);
            }
        } else {
            if (indexActive === 0) {
                this.selectForm(PAYMENT_FORM_METHOD.WALLET);
            } else {
                this.selectForm(PAYMENT_FORM_METHOD.ACH);
            }
        }
    }
}

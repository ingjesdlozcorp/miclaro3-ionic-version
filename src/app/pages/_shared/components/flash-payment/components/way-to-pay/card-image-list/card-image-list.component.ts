import { Component, OnInit } from '@angular/core';
import { FlashController } from '../../../core/services/flash-controller.service';

@Component({
  selector: 'app-card-image-list',
  templateUrl: './card-image-list.component.html',
  styleUrls: ['../way-to-pay.component.scss']
})
export class CardImageListComponent implements OnInit {

  constructor(public flashPaymentService: FlashController) { }

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import { FlashController } from '../../../core/services/flash-controller.service';

@Component({
  selector: 'app-ach',
  templateUrl: './ach.component.html',
  styleUrls: ['../way-to-pay.component.scss']
})
export class AchComponent implements OnInit {

  constructor(public flashPaymentService: FlashController) { }

  ngOnInit(): void {
  }

}

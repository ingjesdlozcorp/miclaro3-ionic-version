import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FlashController } from '../../core/services/flash-controller.service';

@Component({
    selector: 'app-way-to-pay',
    templateUrl: './way-to-pay.component.html',
    styleUrls: ['./way-to-pay.component.scss'],
})
export class WayToPayComponent implements OnInit {

    openForm = false;
    listenToken: Subscription;

    constructor(public flashPaymentService: FlashController) {
        this.listenToken = this.flashPaymentService.mustOpenForm.subscribe(
            (data: boolean) => {
                this.openForm = data;
            }
        );
    }

    ngOnInit(): void {
        let amountFix = this.flashPaymentService.controller.operationAmount;
        amountFix = amountFix.replace('CR', '');
        this.flashPaymentService.controller.operationAmount = amountFix;
    }

    selectPaymentOption(evt) {
        this.openForm = evt.openForm;
    }

    changeIcon(even) {
        if (even) {
            this.openForm = !this.openForm;
        }
    }
}

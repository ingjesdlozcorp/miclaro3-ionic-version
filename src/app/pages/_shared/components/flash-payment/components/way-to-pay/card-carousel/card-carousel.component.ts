import { Component, OnInit } from '@angular/core';
import { FlashController } from '../../../core/services/flash-controller.service';

@Component({
    selector: 'app-card-carousel',
    templateUrl: './card-carousel.component.html',
    styleUrls: ['../way-to-pay.component.scss']
})
export class CardCarouselComponent implements OnInit {

    slideOpts = {};

    constructor(public flashPaymentService: FlashController) {
    }

    ngOnInit(): void {
    }

    test(event) {
        this.flashPaymentService.controller.tokenInstrumentId = 0;
        this.flashPaymentService.selectCard({id: -1});
    }

}

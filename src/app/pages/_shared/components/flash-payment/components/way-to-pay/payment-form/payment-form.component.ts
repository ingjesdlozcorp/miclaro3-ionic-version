import { Component, OnInit } from '@angular/core';
import { FlashController } from '../../../core/services/flash-controller.service';

@Component({
    selector: 'app-payment-form',
    templateUrl: './payment-form.component.html',
    styleUrls: ['../way-to-pay.component.scss']
})
export class PaymentFormComponent implements OnInit {

    constructor(public flashPaymentService: FlashController) {
    }

    ngOnInit(): void {
    }

}

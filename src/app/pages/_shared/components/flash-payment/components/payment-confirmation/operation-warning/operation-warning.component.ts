import { Component, OnInit } from '@angular/core';
import { FlashController } from '../../../core/services/flash-controller.service';

@Component({
  selector: 'app-operation-warning',
  templateUrl: './operation-warning.component.html',
  styleUrls: ['../payment-confirmation.component.scss']
})
export class OperationWarningComponent implements OnInit {

  constructor(public flashPaymentService: FlashController) { }

  ngOnInit(): void {
  }

}

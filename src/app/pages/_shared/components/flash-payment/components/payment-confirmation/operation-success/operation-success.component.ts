import { Component, OnInit } from '@angular/core';
import { FlashController } from '../../../core/services/flash-controller.service';

@Component({
  selector: 'app-operation-success',
  templateUrl: './operation-success.component.html',
  styleUrls: ['../payment-confirmation.component.scss']
})
export class OperationSuccessComponent implements OnInit {

  constructor(public flashPaymentService: FlashController) { }

  ngOnInit(): void {
  }

}

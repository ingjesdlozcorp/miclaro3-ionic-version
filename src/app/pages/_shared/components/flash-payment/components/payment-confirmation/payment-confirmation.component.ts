import { Component, OnInit } from '@angular/core';
import { FlashController } from '../../core/services/flash-controller.service';

@Component({
  selector: 'app-payment-confirmation',
  templateUrl: './payment-confirmation.component.html',
  styleUrls: ['./payment-confirmation.component.scss']
})
export class PaymentConfirmationComponent implements OnInit {

  constructor(public flashPaymentService: FlashController) { }

  ngOnInit(): void {
  }

}

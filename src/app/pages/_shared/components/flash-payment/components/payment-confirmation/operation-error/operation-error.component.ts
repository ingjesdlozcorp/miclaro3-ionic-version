import { Component, OnInit } from '@angular/core';
import { FlashController } from '../../../core/services/flash-controller.service';

@Component({
    selector: 'app-operation-error',
    templateUrl: './operation-error.component.html',
    styleUrls: ['../payment-confirmation.component.scss']
})
export class OperationErrorComponent implements OnInit {

    constructor(public flashPaymentService: FlashController) {
    }

    ngOnInit(): void {
    }

    restartProcess() {
        this.flashPaymentService.reloadProcess();
    }
}

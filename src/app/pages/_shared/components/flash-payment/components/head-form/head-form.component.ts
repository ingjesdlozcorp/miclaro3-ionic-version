import { Component, Input, EventEmitter, OnInit, Output } from '@angular/core';
import { FlashController } from '../../core/services/flash-controller.service';
import { TRANSACTION_TYPES } from '../../core/flash-payment-constants';
import { BasePage } from '../../../../../base.page';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../../../../services/utils.service';
import { IntentProvider } from '../../../../../../services/intent.provider';

const MARK = {
    balance: 0,
    pastDue: 1,
    lastPayment: 2
};

@Component({
    selector: 'app-head-form',
    templateUrl: './head-form.component.html',
    styleUrls: ['../../flash-payment.component.scss']
})
export class HeadFormComponent extends BasePage implements OnInit {

    @Input() billDueDateField: string;
    @Input() billBalanceField: string;
    @Input() pastDueAmountField: string;
    @Input() canUpdateAmount: boolean;
    @Input() editOperationAmount: string;
    @Input() lastPaymentAmountField: string;
    @Input() isInvoicePayment = false;
    @Input() isPrepaid = false;
    @Input() isInstallment = false;
    @Output() editAmount = new EventEmitter<any>();
    @Output() changeAmount = new EventEmitter<any>();

    TRANSACTION_TYPES = TRANSACTION_TYPES;
    prepaidBalance;
    mark = MARK;
    markSelected: number = null;

    constructor(public flashPaymentService: FlashController,
                router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit(): void {
        this.prepaidBalance = this.cacheStorage().accountInfo.defaultSubscriberObject.prepaidBalanceField;
    }

    replaceAmount(value: string = null, mark: number, edit = false) {
        console.log(value);
        if (value.toString().substring((value.length - 2), (value.length)) === 'CR') {
            value = value.toString().replace('CR', '');
            value = value.toString().replace(',', '');
        }
        if (!Number.isNaN(+value) && +value > 0) {
            this.markSelected = mark;
        }
        this.changeAmount.emit({value, edit});
    }

    activateChange(value: string = null, edit = false) {
        this.editAmount.emit({value, edit});
    }

    goToProcess() {
        console.log('this.flashPaymentService.controller.operationAmount', this.flashPaymentService.controller.operationAmount);
        if (+this.flashPaymentService.controller.operationAmount >= 5 && +this.flashPaymentService.controller.operationAmount <= 2000) {
            this.flashPaymentService.scrollBottom();
        }
    }

}

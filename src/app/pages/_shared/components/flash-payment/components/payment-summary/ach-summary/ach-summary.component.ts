import { Component, OnInit } from '@angular/core';
import { FlashController } from '../../../core/services/flash-controller.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../../../../services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../../../../../services/utils.service';
import { IntentProvider } from '../../../../../../../services/intent.provider';
import { BasePage } from '../../../../../../base.page';

@Component({
    selector: 'app-ach-summary',
    templateUrl: './ach-summary.component.html',
    styleUrls: ['../payment-summary.component.scss']
})
export class AchSummaryComponent extends BasePage implements OnInit {

    name: string;

    constructor(public flashPaymentService: FlashController,
                router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit(): void {
        const accountInfo = this.cacheStorage().accountInfo;
        this.name = `${accountInfo.firstNameField} ${accountInfo.lastNameField}`;
    }

}

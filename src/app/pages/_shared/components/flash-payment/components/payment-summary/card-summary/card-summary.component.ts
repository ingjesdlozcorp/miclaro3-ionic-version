import { Component, OnInit } from '@angular/core';
import { FlashController } from '../../../core/services/flash-controller.service';
import { TRANSACTION_TYPES } from '../../../core/flash-payment-constants';

@Component({
    selector: 'app-card-summary',
    templateUrl: './card-summary.component.html',
    styleUrls: ['../payment-summary.component.scss']
})
export class CardSummaryComponent implements OnInit {

    TRANSACTION_TYPES = TRANSACTION_TYPES;

    constructor(public flashPaymentService: FlashController) {
    }

    ngOnInit(): void {
    }

}

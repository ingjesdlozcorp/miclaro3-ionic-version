import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlashPaymentComponent } from './flash-payment.component';
import { HeadFormComponent } from './components/head-form/head-form.component';
import { TwoDigitDecimalNumberDirective } from './core/two-digit-decimal-number.directive';
import { IonicModule } from '@ionic/angular';
import { PaymentConfirmationComponent } from './components/payment-confirmation/payment-confirmation.component';
import { OperationErrorComponent } from './components/payment-confirmation/operation-error/operation-error.component';
import { OperationWarningComponent } from './components/payment-confirmation/operation-warning/operation-warning.component';
import { OperationSuccessComponent } from './components/payment-confirmation/operation-success/operation-success.component';
import { PaymentSummaryComponent } from './components/payment-summary/payment-summary.component';
import { AchSummaryComponent } from './components/payment-summary/ach-summary/ach-summary.component';
import { CardSummaryComponent } from './components/payment-summary/card-summary/card-summary.component';
import { WayToPayComponent } from './components/way-to-pay/way-to-pay.component';
import { PaymentFormComponent } from './components/way-to-pay/payment-form/payment-form.component';
import { WalletComponent } from './components/way-to-pay/wallet/wallet.component';
import { CardImageListComponent } from './components/way-to-pay/card-image-list/card-image-list.component';
import { CreditCardComponent } from './components/way-to-pay/credit-card/credit-card.component';
import { CardCarouselComponent } from './components/way-to-pay/card-carousel/card-carousel.component';
import { OptionsForPaymentComponent } from './components/way-to-pay/options-for-payment/options-for-payment.component';
import { AchComponent } from './components/way-to-pay/ach/ach.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared.module';
import { PaymentSupportService } from './core/services/payment-support.service';


@NgModule({
    declarations: [
        TwoDigitDecimalNumberDirective,
        FlashPaymentComponent,
        HeadFormComponent,
        OperationErrorComponent,
        // Payment confirmation
        PaymentConfirmationComponent,
        OperationWarningComponent,
        OperationSuccessComponent,
        // Payment Summary
        PaymentSummaryComponent,
        AchSummaryComponent,
        CardSummaryComponent,
        // Way to pay
        WayToPayComponent,
        WalletComponent,
        PaymentFormComponent,
        OptionsForPaymentComponent,
        CreditCardComponent,
        CardImageListComponent,
        CardCarouselComponent,
        AchComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        SharedModule
    ],
    exports: [
        FlashPaymentComponent,
        TwoDigitDecimalNumberDirective,
    ],
    entryComponents: [
        FlashPaymentComponent
    ],
    providers: [
        PaymentSupportService
    ]
})
export class FlashPaymentModule {
}

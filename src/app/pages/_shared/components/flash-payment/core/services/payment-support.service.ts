import { Injectable } from '@angular/core';
import { FlashController } from './flash-controller.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../../../../services/services.provider';
import { AlertController, ModalController } from '@ionic/angular';
import { UtilsService } from '../../../../../../services/utils.service';
import { IntentProvider } from '../../../../../../services/intent.provider';
import { BasePage } from '../../../../../base.page';
import { TRANSACTION_TYPES } from '../flash-payment-constants';
import { FlashPaymentComponent } from '../../flash-payment.component';
import { ExtraParamsPayment } from '../flash-payment-interface';

@Injectable({
    providedIn: 'root'
})
export class PaymentSupportService extends BasePage {

    constructor(private flashPaymentService: FlashController, private modalController: ModalController,
                router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    paymentRefactorProcess(typeMessage: string, amountPayable: number, locationId = '', invoiceNumber = '', quantityToAccelerate = 0) {
        const selectBan = `${this.cacheStorage().accountInfo.bANField}`;
        const token = this.cacheStorage().tokenSession;
        this.flashPaymentService.initData();
        const amount: string = amountPayable.toString();
        if (amount.substring(amount.length - 2, amount.length) === 'CR') {
            this.showError('El monto debe ser positivo');
            return;
        }

        const amountFix = amount.replace('$', '');
        if (+amountFix < 5 || +amountFix > 2000) {
            this.showError('La cantidad a pagar debe ser mayor a $5.00 o menor de $2000.00');
            return;
        }
        this.flashPaymentService.controller.operationAmount = amountFix;
        const mount = parseFloat(String(amountPayable).replace(',', ''));
        this.flashPaymentService.controller.newPaymentRequest = {
            productId: 1,
            typeMessage,
            currentBan: selectBan,
            subscriber: this.cacheStorage().accountInfo.defaultSubscriberField,
            amount: mount.toString(),
            email: this.cacheStorage().accountInfo.emailField,
            name: `${this.cacheStorage().accountInfo.firstNameField} ${this.cacheStorage().accountInfo.lastNameField}`,
            offerId: '',
            token,
        };
        this.flashPaymentService.controller.doPaymentRequest = {
            currentBan: selectBan,
            mount,
            token: this.cacheStorage().tokenSession,
        };
        this.cacheStorage().flashPayment.newPaymentRequest = this.flashPaymentService.controller.newPaymentRequest;
        this.cacheStorage().flashPayment.doPaymentRequest = this.flashPaymentService.controller.doPaymentRequest;
        this.cacheStorage().flashPayment.operationAmount = amount.substring(1, amount.length);
        this.flashPaymentService.controller.userData = this.cacheStorage().accountDetails;
        this.flashPaymentService.controller.subscriberInfo = this.cacheStorage().accountDetails.SubscriberInfo[0];
        this.flashPaymentService.controller.transactionType =
            typeMessage === 'prepaid'
                ? TRANSACTION_TYPES[1]
                : (typeMessage === 'payment' ? TRANSACTION_TYPES[0] : TRANSACTION_TYPES[2]);
        this.flashPaymentService.controller.locationId = locationId;
        this.flashPaymentService.controller.invoiceNumber = invoiceNumber;
        this.flashPaymentService.controller.quantityToAccelerate = quantityToAccelerate;
        this.flashPaymentService.controller.accountNumber = selectBan;
        this.cacheStorage().flashPayment.flashData = this.flashPaymentService.controller;

        this.openModalPayment();
    }

    private async openModalPayment() {
        const modal = await this.modalController.create({
            component: FlashPaymentComponent,
            backdropDismiss: false,
        });
        await modal.present();

        const {data} = await modal.onWillDismiss();
        if (data.isPayment) {
            this.showProgress();
            this.reloadCurrentAccount().then(() => {
                this.goHomePage();
                this.dismissProgress();
            });
        }
    }

    async paymentExternalProcess(account: string, subscriber: string, amount: string, extraParams: ExtraParamsPayment = {}): Promise<any> {
        return await this.flashPaymentService.getCreditCardPaymentOptions(account, subscriber, amount, extraParams);
    }
}

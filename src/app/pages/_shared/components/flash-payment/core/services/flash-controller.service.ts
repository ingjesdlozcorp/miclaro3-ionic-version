import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { Storage } from '@ionic/storage';
import { DomSanitizer } from '@angular/platform-browser';
import { AlertController } from '@ionic/angular';
import { InAppBrowser, InAppBrowserObject } from '@ionic-native/in-app-browser/ngx';
import { FlashApiService } from './flash-api.service';
import { EncryptWithKeyService } from './encrypt-with-key.service';
import { Subject } from 'rxjs';
import { VARS_NEEDED } from '../flash-payment-constants';
import { FlashPaymentUtil } from '../flash-payment.util';
import { BasePage } from '../../../../../base.page';
import { ServicesProvider } from '../../../../../../services/services.provider';
import { UtilsService } from '../../../../../../services/utils.service';
import { IntentProvider } from '../../../../../../services/intent.provider';
import { APP } from '../../../../../../utils/const/appConstants';
import { Utils } from '../../../../../../utils/utils';
import { BROWSER_GENERIC_THEME_OPTION, BrowserProvider } from '../../../../../../services/browser.provider';
import { ExtraParamsPayment, GetTransactionStatusRequest, PaymentOption, TokenCard } from '../flash-payment-interface';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';

declare let P: any;

const MI_CLARO = 'MICLARO';

@Injectable({
    providedIn: 'root'
})
export class FlashController extends BasePage {

    controller = VARS_NEEDED;
    mustOpenForm = new Subject();
    scrollToBottom = new Subject();
    restartWayToPay = new Subject();
    count = 0;
    personalizedMessages: any;

    constructor(private service: FlashApiService,
                private sanitizer: DomSanitizer,
                private encryptWithKeyService: EncryptWithKeyService,
                private browserProvider: BrowserProvider,
                private iab: InAppBrowser,
                private file: File,
                private fileOpener: FileOpener,
                router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);

        this.controller.expirationDate = FlashPaymentUtil.formatFromPickerToServer(this.controller.today);
        this.initData();
        this.getIp();
    }

    reloadProcess() {
        this.initData();
        this.restartWayToPay.next(true);
    }

    initData() {
        this.count = 0;
        this.controller.jstoday = formatDate(
            this.controller.today,
            'dd-MM-yyyy hh:mm:ss a',
            'en-US',
            '+0530'
        );
        this.controller.expirationYear = this.controller.jstoday.substring(6, 10);
        this.controller.expirationMonth = this.controller.jstoday.substring(3, 5);
        this.controller.cardNumber = '';
        this.controller.cardNumberString = '';
        this.controller.cardType = '';
        this.controller.cvv = '';
        this.controller.paymentMethod = '';
        this.controller.authorizationNumber = '';
        this.controller.paymentStatusDesription = '';
        this.controller.tokenInstrumentId = 0;
        this.controller.step1 = true;
        this.controller.step2 = false;
        this.controller.step3 = false;
        this.controller.cardSelected = null;
        this.controller.isPlaceToPay = true;
        this.controller.isACH = false;
        this.controller.isCard = false;
        this.controller.canActivateTokenizationProcess = false;
        this.controller.confirmationNumber = '';
        this.selectCard({id: -1});
    }

    private getIp() {
        this.service.getIPAddress().subscribe(
            (data: any) => {
                this.controller.ipAddress = data.ip;
            }, () => {
                this.controller.ipAddress = '0.0.0.0';
            }
        );
    }

    stepUpdate(step: number) {
        this.controller.paymentInProcess = true;
        if (step === 1) {
            this.checkWayProcess();
        } else if (step === 2) {
            this.executeAuthorizePayment();
        } else {
            this.showProgress();
            setTimeout(() => {
                this.dismissProgress();
                this.controller.step3 = true;
                this.controller.operationSuccess = true;
            }, 2000);
        }
    }

    private checkWayProcess() {
        this.dismissProgress();
        if (this.controller.isACH) {
            this.controller.step1 = false;
            this.achProcess();
        } else if (this.controller.isCard) {
            this.controller.step1 = false;
            this.cardProcess();
        } else {
            this.showProgress();
            this.placeToPayProcess();
        }
    }

    private placeToPayProcess() {
        this.continueToStep2();
    }

    private validateCardProccess(): boolean {
        if (!this.controller.name) {
            this.showError('El nombre de usuario es obligatorio');
            this.controller.step1 = true;
            return false;
        }

        if (FlashPaymentUtil.isInvalidCharacter(this.controller.name)) {
            this.showError('Nombre inválido');
            this.controller.name = '';
            this.controller.step1 = true;
            return false;
        }

        if (!this.validateCreditCardForm) {
            this.controller.step1 = false;
            return false;
        }

        if (!this.controller.cardNumberString) {
            this.showError('Debe introducir una tarjeta');
            this.controller.step1 = true;
            return false;
        }

        if (this.controller.paymentMethod !== 'A') {
            if (!this.controller.cvv) {
                this.showError('El ccv es un campo obligatorio');
                this.controller.step1 = true;
                return false;
            }
        }

        if (this.controller.expirationYear && this.controller.expirationMonth) {
            this.controller.expirationDate = `${this.controller.expirationYear}/${this.controller.expirationMonth}`;
        } else {
            this.showError('La fecha de expiración es obligatoria');
            this.controller.step1 = true;
            return false;
        }

        if (!this.controller.PostalCode) {
            this.showError('El código postal es obligatorio');
            this.controller.step1 = true;
            return false;
        }
        return true;
    }

    private cardProcess() {
        if (!this.validateCardProccess()) {
            this.controller.paymentInProcess = false;
            return;
        }

        this.showProgress();
        if (this.controller.paymentMethod === 'A') {
            if (this.count === 0) {
                this.count += 1;
                this.scrollBottom();
                this.buildAuthorizationRequest(true);
            }
        } else {
            this.continueToStep2();
        }
    }

    private achProcess() {
        this.controller.achFormComplete = true;
        this.showProgress();
        this.continueToStep2();
    }

    private continueToStep2() {
        this.dismissProgress();
        this.controller.paymentInProcess = false;
        this.controller.step2 = true;
        this.scrollBottom();
    }

    async getTokenCards() {
        this.showProgress();
        const userId = this.cacheStorage().accountList[0].userID;
        const payload = {
            customerEmail: this.cacheStorage().accountInfo.emailField,
            accountNumber: this.cacheStorage().accountInfo.bANField.toString(),
            source: MI_CLARO,
            externalId: userId
        };
        const dto = this.encryptWithKeyService.encryp(JSON.stringify(payload));
        this.service.getTokenCards(dto).then(
            (data: any) => {
                let resp;
                try {
                    const respString: string = this.encryptWithKeyService.decrypt(data.dataToken);
                    resp = JSON.parse(FlashPaymentUtil.formatEncryptData(respString));
                    this.setCards(resp);
                } catch (e) {
                    this.service.decryptTokenByCrypto(data.dataToken).then((data2: any) => {
                            const respString2: string = data2.dataToken.replace('pIvhi9.qodmyxcyhqow.', '');
                            resp = JSON.parse(atob(atob(atob(respString2))));
                            this.setCards(resp);
                        }, (error) => {
                            this.dismissProgress();
                            this.showError(error);
                            this.mustOpenForm.next(true);
                        }
                    );
                }
            },
            (error) => {
                this.dismissProgress();
                this.showError(error);
                this.mustOpenForm.next(true);
            }
        );
    }

    private setCards(resp: any) {
        this.controller.tokenCards = resp.TokenCards as TokenCard[];
        if (this.controller.tokenCards.length) {
            this.mustOpenForm.next(false);
        } else {
            this.mustOpenForm.next(true);
        }
        this.dismissProgress();
    }

    selectCard(card) {
        this.controller.tokenCards.forEach((item) => {
            if (item.id === card.id) {
                item.active = 'active';
                this.controller.paymentMethod = item.brandCode;
                this.controller.tokenInstrumentId = item.id;
                this.controller.cardSelected = item;
                if (item.payerName) {
                    this.controller.name = item.payerName;
                }
            } else {
                item.active = '';
            }
        });
    }

    deleteCard(card) {
        this.openDialog(2, card);
    }

    private startDeleteAction(card) {
        const payload = {
            tokenId: card.id,
            source: MI_CLARO
        };
        const dto = this.encryptWithKeyService.encryp(JSON.stringify(payload));
        this.showProgress();
        this.service.deleteInstrument(dto).then(
            (data: any) => {
                this.dismissProgress();
                const respString: string = this.encryptWithKeyService.decrypt(data.dataToken).toString();
                console.log('-> deleteInstrument', respString);
                if (respString.includes('"hasError":false')) {
                    this.showError('Tarjeta eliminada correctamente');
                    this.getTokenCards();
                } else {
                    this.showError('Ha ocurrido un error al intentar completar la operación, por favor intente nuevamente más tarde');
                }
            }, () => {
                this.showError('Ha ocurrido un error al intentar completar la operación, por favor intente nuevamente más tarde');
            }
        );
    }

    isValidCreditCardNumber(cardNumber: string) {
        cardNumber = cardNumber.trim();
        this.controller.isValidCard = true;
        this.controller.isTheCardNumberEmpty = false;
        if (FlashPaymentUtil.VisaCardnumber(cardNumber)) {
            this.controller.cardType = 'VISA';
            this.controller.paymentMethod = 'V';
        } else if (FlashPaymentUtil.MasterCardnumber(cardNumber)) {
            this.controller.cardType = 'MASTER CARD';
            this.controller.paymentMethod = 'M';
        } else if (FlashPaymentUtil.AmexCardnumber(cardNumber)) {
            this.controller.cardType = 'AMERICAN EXPRESS';
            this.controller.paymentMethod = 'X';
        } else if (FlashPaymentUtil.DiscoverCardnumber(cardNumber)) {
            this.controller.cardType = 'DISCOVER';
            this.controller.paymentMethod = 'D';
        } else if (FlashPaymentUtil.DinerClubCardnumber(cardNumber)) {
            this.controller.cardType = 'DINERCLUB';
            this.controller.paymentMethod = 'DIN';
        } else if (FlashPaymentUtil.JCBCardnumber(cardNumber)) {
            this.controller.cardType = 'JCB';
            this.controller.paymentMethod = 'J';
        } else if (FlashPaymentUtil.ATHcardnumber(cardNumber)) {
            this.controller.cardType = 'ATH';
            this.controller.paymentMethod = 'A';
        } else {
            this.controller.cardType = 'invalid card type';
        }

        if (this.controller.cardType === 'invalid card type') {
            this.controller.isValidCard = !this.controller.isValidCard;
            this.controller.cardNumberString = '';
        } else {
            this.controller.cardNumber = this.controller.cardNumberString;
            this.controller.cardNumberString = '************' + this.controller.cardNumber.substring(13, this.controller.cardNumber.length);
        }
        return this.controller.cardType;
    }

    ccvChanged() {
        this.controller.isTheCcvError = false;
        if (this.controller.cvv) {
            this.controller.isTheCcvEmpty = false;
        }

        if (this.controller.cvv.length < 3) {
            this.controller.isTheCcvError = true;
        }
    }

    zipCodeChanged() {
        this.controller.isValidZip = true;
        this.controller.isEmptyZipCode = false;
        if (FlashPaymentUtil.isNumericValidCharacter(this.controller.PostalCode)) {
            this.controller.PostalCode = '';
            this.controller.isValidZip = false;
            return;
        }
        if (this.controller.PostalCode) {
            if (this.controller.PostalCode.length !== 5) {
                this.controller.isValidZip = false;
            }
        } else {
            this.controller.isEmptyZipCode = true;
        }
    }

    private validateCreditCardForm() {
        let canContinue = true;
        if (!this.controller.cardNumberString) {
            this.controller.isTheCardNumberEmpty = true;
            canContinue = false;
        }

        if (!this.controller.cvv) {
            if (this.controller.paymentMethod !== 'A') {
                canContinue = false;
                this.controller.isTheCcvEmpty = true;
            } else {
                canContinue = true;
            }
        }

        if (this.controller.cardIsExpired) {
            canContinue = false;
        }

        if (this.controller.isEmptyZipCode || !this.controller.isValidZip) {
            canContinue = false;
        }
        return canContinue;
    }

    changeExpirationMonth() {
        this.controller.cardIsExpired = this.controller.expirationYear === this.controller.jstoday.substring(6, 10) &&
            +this.controller.jstoday.substring(3, 5) > +this.controller.expirationMonth;
    }

    payWithCreditCard(cvv: string) {
        if (!this.controller.step3) {
            if (this.count === 0) {
                this.count += 1;
                this.controller.cvv = cvv;
                this.buildAuthorizationRequest(false, false, false, false, true);
            }
        } else {
            this.openDialog(1);
        }

    }

    private openDialog(type: number, card = null): void {
        let message;
        if (type === 1) {
            message = 'Acabas de realizar un pago, ¿Deseas realizar otro pago?';
        } else {
            message = `Seguro que desea eliminar la tarjeta ${card.franchiseName} XXXX-XXXX-XXXX- ${card.lastFourDigits}`;
        }
        this.showConfirm('', message, () => {
            if (type === 1) {
                this.initData();
            }
            if (type === 2) {
                this.startDeleteAction(card);
            }
        });
    }

    buildAuthorizationRequest(isAth = false, isSubscription = false, isApiCard = false, isTokenizedCardSubscription = false, isPaymentWithStoredInstrument = false) {
        if (!this.controller.canRunPaymentMethod) {
            return;
        }
        this.controller.canRunPaymentMethod = false;
        this.controller.paymentInProcess = true;
        this.showProgress();
        if (isAth) {
            this.controller.paymentOptionSelected = this.findEnabledOption('ATH');
        } else if (isApiCard || isPaymentWithStoredInstrument || isTokenizedCardSubscription) {
            this.controller.paymentOptionSelected = this.findEnabledOption('APICALLMETHOD');
        } else {
            this.controller.paymentOptionSelected = this.findEnabledOption('WALLET');
            if (this.controller.tokenInstrumentId) {
                isPaymentWithStoredInstrument = true;
            }
        }
        const accountInfo = this.cacheStorage().accountInfo;
        const userId = this.cacheStorage().accountList[0].userID;
        console.log(this.controller.paymentOptionSelected);
        const payload: any = {
            customer: {
                accountNumber: accountInfo.bANField.toString(),
                customerName: accountInfo.firstNameField,
                customerLastName: accountInfo.lastNameField,
                customerEmail: accountInfo.emailField,
                subscriberNumber: accountInfo.defaultSubscriberField,
                buyerName: this.controller.name.split(' ')[0],
                buyerLastName: this.controller.name.split(' ')[1],
                address: {
                    address1: '',
                    address2: '',
                    city: '',
                    state: '',
                    zipCode: this.controller.PostalCode
                },
                transactionDescription: 'Pago',
                transactionAmount: isSubscription ? 0 : +this.controller.operationAmount
            },
            client: {
                ipAddress: this.controller.ipAddress,
                userAgent: 'test agent'
            },
            payment: {
                cardNumber: isAth || isApiCard ? this.controller.cardNumber : '',
                expirationDate: isAth || isApiCard
                    ? this.controller.expirationMonth + this.controller.expirationYear
                    : '',
                securityCode: isApiCard || isPaymentWithStoredInstrument ? this.controller.cvv : '',
                paymentMethod: isAth || isApiCard || isPaymentWithStoredInstrument
                    ? this.controller.paymentMethod
                    : '',
                amount: isSubscription || isTokenizedCardSubscription
                    ? 0
                    : +this.controller.operationAmount
            },
            subscription: {
                expirationDate: isSubscription || isTokenizedCardSubscription
                    ? `${this.controller.expirationMonth}/${this.controller.expirationYear.substring(2, this.controller.expirationYear.length)}`
                    : '',
                cardNumber: isSubscription || isTokenizedCardSubscription
                    ? this.controller.cardNumber
                    : '',
                securityCode: isSubscription || isTokenizedCardSubscription
                    ? this.controller.cvv
                    : '',
                merchantId: isSubscription ? 16 : isTokenizedCardSubscription ? 18 : 0,
                brandCode: '',
                source: MI_CLARO,
                tokenInstrumentId: 0,
                subscriptionActions: 0,
                externalId: userId
            },
            paymentToken: this.cacheStorage().tokenSession + Math.random() * 10000,
            paymentOptionId: isSubscription ? 0 : this.controller.paymentOptionSelected.id,
            transactionType: isSubscription ? 3 : 0,
            doProvisioningIndicator: 1,
            parentPaymentId: 0,
            externalId: '',
            locationId: this.controller.locationId,
            installmentUnits: this.controller.quantityToAccelerate,
            transactionDescription: 'Pago',
            invoiceNumber: this.controller.invoiceNumber
        };

        if (this.controller.tokenInstrumentId !== 0) {
            payload.payment.tokenInstrumentId = isSubscription || isTokenizedCardSubscription
                ? 0
                : this.controller.tokenInstrumentId;
        } else {
            payload.payment.tokenInstrumentId = 0;
        }
        const dto = this.encryptWithKeyService.encryp(JSON.stringify(payload));
        if (isTokenizedCardSubscription) {
            this.tokenizeCard(dto);
        } else {
            this.createTransaction(dto, isSubscription, isAth, isApiCard, isPaymentWithStoredInstrument);
        }
    }

    private findEnabledOption(typeProcess: string) {
        const paymentOptions: PaymentOption[] = this.controller.paymentOptions;
        let optionSelected = null;
        paymentOptions.map((element) => {
            if (element.merchantTypeCode.toString().trim().toLowerCase() === typeProcess.trim().toLowerCase()) {
                optionSelected = element;
            }
        });
        return optionSelected;
    }

    private async createTransaction(request,
                                    isSubscription: boolean,
                                    isAth = false,
                                    isApiCard = false,
                                    isPaymentWithStoredInstrument = false) {
        this.showProgress();
        this.service.getTransactionAuthorizationEncrypt(request)
            .then((data) => {
                let decryptedData = null;
                let correctObject = null;
                const respString: string = this.encryptWithKeyService.decrypt(data.dataToken);
                try {
                    correctObject = JSON.parse(FlashPaymentUtil.formatEncryptData(respString));
                    console.log('-> getTransactionAuthorizationEncrypt', correctObject);
                    decryptedData = correctObject;
                    this.decide(decryptedData, isSubscription, isAth, isApiCard, isPaymentWithStoredInstrument);
                } catch (e) {
                    this.tryWithService(data.dataToken, isSubscription, isAth, isApiCard, isPaymentWithStoredInstrument);
                }
            }, () => {
                this.manageErrorCreateTransaction();
            });
    }

    private decide(decryptedData: any, isSubscription: boolean, isAth: boolean, isApiCard: boolean, isPaymentWithStoredInstrument: boolean) {
        if (decryptedData) {
            this.manageCreateTransactionData(decryptedData, isSubscription, isAth, isApiCard, isPaymentWithStoredInstrument);
        } else {
            this.manageErrorCreateTransaction();
        }
    }

    private tryWithService(token, isSubscription, isAth, isApiCard, isPaymentWithStoredInstrument) {
        this.service.decryptTokenByCrypto(token)
            .then((data: any) => {
                    const respString: string = data.dataToken.replace('pIvhi9.qodmyxcyhqow.', '');
                    const decryptedData = JSON.parse(atob(atob(atob(respString))));
                    console.log('-> decryptTokenByCrypto', decryptedData);
                    if (decryptedData) {
                        this.decide(decryptedData, isSubscription, isAth, isApiCard, isPaymentWithStoredInstrument);
                    } else {
                        this.manageErrorCreateTransaction();
                    }
                }, () => {
                    this.manageErrorCreateTransaction();
                }
            );
    }

    private manageCreateTransactionData(resp: any, isSubscription: boolean, isAth: boolean, isApiCard: boolean, isPaymentWithStoredInstrument: boolean) {
        this.controller.canRunPaymentMethod = true;
        this.controller.paymentInProcess = false;
        this.dismissProgress();
        if (!resp.hasError) {
            const paymentID = resp.paymentId;
            this.cacheStorage().flashPayment.paymentId = paymentID;
            this.cacheStorage().flashPayment.flashData = this.controller;
            if (!isAth && !isApiCard && !isPaymentWithStoredInstrument) {
                this.lightBoxProcess(resp, isSubscription, paymentID);
            } else {
                if (isAth) {
                    this.athProcess(resp);
                } else if (isApiCard || isPaymentWithStoredInstrument) {
                    this.apiCallProcess(resp);
                }
            }
        } else {
            this.controller.errorMessage = resp.errorDisplay;
            if (isApiCard || isPaymentWithStoredInstrument) {
                this.apiCallProcess(resp);
            } else {
                this.showLastStepOperation(3);
            }
        }
    }

    private lightBoxProcess(resp, isSubscription, paymentID) {
        const processUrl: string = resp.webCheckOutAttribute.checkOutUrl;
        P.init(processUrl);

        P.on('response', (data) => {
            console.log('-> data', data);
            if (this.count === 0) {
                this.count += 1;
                if (!isSubscription) {
                    this.controller.step1 = false;
                    if (data.status.status === 'PENDING' || data.status.status === 'REJECTED') {
                        this.buildTransactionStatusRequest(+data.reference, '');
                    } else {
                        this.buildTransactionStatusRequest(paymentID, '');
                    }
                } else {
                    if (data.status.status !== 'REJECTED') {
                        this.showError('Suscripción realizada con éxito');
                    }
                }
            }

        });
    }

    private athProcess(resp) {
        this.controller.athURL = resp.athPinPadMethodAttributes.checkOutUrl;
        this.continueToStep2();
    }

    private apiCallProcess(resp) {
        if (resp.apiCallMethodAttributes.transactionInfo.payment.authorizationNumber) {
            this.controller.authorizationNumber = resp.apiCallMethodAttributes.transactionInfo.payment.authorizationNumber;

            this.controller.paymentStatusDesription = resp.errorSubject;
            this.controller.paymentMethod = resp.apiCallMethodAttributes.transactionInfo.payment.method.toString().trim().toUpperCase(); // corregir aca

            if (!resp.hasError) {
                this.controller.confirmationNumber = resp.apiCallMethodAttributes.transactionInfo.provisioning.referenceSequenceCode;
                this.controller.paymentStatusDesription = this.personalizedMessages.okMessage;
                this.count = 0;
                this.showLastStepOperation(1);
            } else {
                this.controller.warningDescription =
                    resp.apiCallMethodAttributes.transactionInfo.provisioning.errorDisplay;
                this.showLastStepOperation(2);
            }

            if (!resp.apiCallMethodAttributes.transactionInfo.subscription.token) {
                this.controller.showSubscriptionOption = true;
            }

            if (this.controller.canActivateTokenizationProcess) {
                if (this.count === 0) {
                    this.count += 1;
                    this.buildAuthorizationRequest(false, false, false, true, false);
                }

            }
        } else {
            this.showLastStepOperation(3);
        }
    }

    private manageErrorCreateTransaction() {
        this.controller.paymentInProcess = false;
        this.controller.canRunPaymentMethod = true;
        this.dismissProgress();
        const message = 'Ha ocurrido un error con su token de usuario, pruebe cerrar la session y vuelva a intentar';
        this.showError(message);
    }


    private tokenizeCard(payload) {
        this.showProgress();
        this.service.tokenizeCard(payload).then(
            (data: any) => {
                this.dismissProgress();
                this.controller.canRunPaymentMethod = true;
                this.controller.paymentInProcess = false;
                const respString: string = this.encryptWithKeyService.decrypt(
                    data.dataToken
                );
                try {
                    const resp = JSON.parse(FlashPaymentUtil.formatEncryptData(respString));
                    console.log('-> tokenizeCard', resp);
                    if (resp.hasError) {
                        this.showError(resp.errorDisplay);
                    } else {
                        this.showAlert('Su medio de pago ha sido almacenado correctamente');
                    }
                } catch (e) {
                    this.service.decryptTokenByCrypto(data.dataToken).then(
                        () => {
                            const respString2 = data.dataToken.replace('pIvhi9.qodmyxcyhqow.', '');
                            const decryptedData = JSON.parse(atob(atob(atob(respString2))));
                            this.showError('Su medio de pago ha sido almacenado correctamente');
                        }, () => {
                            // TODO
                        }
                    );
                }
            }, (error) => {
                if (error.errorDisplay) {
                    this.showError(error.errorDisplay);
                } else {
                    this.showError('Su medio de pago no ha podido almacenarse correctamente.');
                }
                this.controller.canRunPaymentMethod = true;
                this.controller.paymentInProcess = false;
                this.dismissProgress();
            }
        );
    }

    private processRedirectInfo(sessionToken: string, paymentId: string, typeResult, authorizationNumber = '', confirmationNumber = '') {
        this.buildTransactionStatusRequest(paymentId, typeResult, authorizationNumber, confirmationNumber);
    }

    private buildTransactionStatusRequest(paymentId, typeResult, authorizationNumber = '', confirmationNumber = '') {
        const payload: GetTransactionStatusRequest = {
            paymentId,
            paymentToken: '',
            bankToken: '',
            authorizationNumber,
            confirmationNumber
        };
        let dto = this.encryptWithKeyService.encryp(JSON.stringify(payload));
        if (paymentId) {
            this.getTransactionStatus(dto, typeResult);
        } else {
            if (this.cacheStorage().flashPayment.paymentId) {
                payload.paymentId = +this.cacheStorage().flashPayment.paymentId;
                dto = this.encryptWithKeyService.encryp(JSON.stringify(payload));
                this.getTransactionStatus(dto, '');
            }
        }
    }

    private getTransactionStatus(data, typeResult) {
        this.showProgress();
        this.service.getTransactionStatus(data).then(
            (response: any) => {
                const respString: string = this.encryptWithKeyService.decrypt(
                    response.dataToken
                );
                try {
                    const resp = JSON.parse(FlashPaymentUtil.formatEncryptData(respString));
                    console.log('-> getTransactionStatus', resp);
                    if (typeResult.toString().trim().toLowerCase() !== 'cancel') {
                        this.manageStatusResponse(resp);
                    } else {
                        this.manageErrorStatusResponse(resp);
                    }
                    this.dismissProgress();
                } catch (e) {
                    this.service.decryptTokenByCrypto(data.dataToken).then(
                        () => {
                            const respString2 = data.dataToken.replace('pIvhi9.qodmyxcyhqow.', '');
                            const decryptedData = JSON.parse(atob(atob(atob(respString2))));
                            console.log('-> decryptTokenByCrypto', decryptedData);
                            this.dismissProgress();
                            if (decryptedData) {
                                this.manageStatusResponse(decryptedData);
                            }
                        }, () => {
                            // TODO
                        }
                    );
                }


            }, (err) => {
                this.controller.paymentInProcess = false;
                this.dismissProgress();
                this.controller.step1 = true;
                this.showError(err.error || 'El proceso de pago ha sido cerrado');
            }
        );
    }

    private manageStatusResponse(resp) {
        this.controller.paymentInProcess = false;
        if (resp.transactionInfo.payment.authorizationNumber) {
            this.controller.authorizationNumber = resp.transactionInfo.payment.authorizationNumber;
            this.controller.paymentMethod = (resp.transactionInfo.payment.method || resp.transactionInfo.payment.methodLongDescription)
                .toString().trim().toUpperCase();
            this.controller.paymentStatusDesription = resp.errorSubject; // resp.transactionInfo.StatusDesription;
            if (!resp.hasError) {
                this.controller.confirmationNumber = resp.transactionInfo.provisioning.referenceSequenceCode;
                this.controller.paymentStatusDesription = this.personalizedMessages.okMessage;
                this.showLastStepOperation(1);

            } else {
                this.controller.errorMessage = resp.errorDisplay;
                this.showLastStepOperation(2);
            }
        } else {
            if (resp.transactionInfo.StatusId === 4) {
                this.controller.errorMessage = this.personalizedMessages.pendingMessage;
                this.showLastStepOperation(2);
            } else {
                if (resp.transactionInfo.StatusId === 5) {
                    this.controller.errorMessage = this.personalizedMessages.rejectedMessage;
                } else {
                    this.controller.errorMessage = resp.errorDisplay;
                }
                this.showLastStepOperation(3);
            }
        }
    }

    private showLastStepOperation(index) {
        this.controller.step1 = false;
        this.controller.step2 = false;
        this.controller.step3 = true;
        this.controller.operationError = false;
        this.controller.operationSuccess = false;
        this.controller.operationWarning = false;
        if (index === 1) {
            this.controller.operationSuccess = true;
        } else if (index === 2) {
            this.controller.operationWarning = true;
        } else {
            this.controller.operationError = true;
        }
        this.scrollBottom();
    }

    private manageErrorStatusResponse(resp) {
        this.controller.paymentInProcess = false;
        if (resp.errorDescription || resp.errorDisplay) {
            this.controller.errorMessage = resp.errorDisplay;
        }

        this.showLastStepOperation(3);
    }

    paymentLogin() {
        this.service.getPaymentsOptions().then(
            (data: any[]) => {
                let paymentOptions;
                data.map((element) => {
                    if (element.description.toString().trim() === this.controller.transactionType.toString().trim()) {
                        paymentOptions = element.paymentOptions;
                    }
                });
                this.controller.paymentOptions = paymentOptions;
                this.activateOption(this.controller.paymentOptions);
            }, () => {
                // Todo
            }
        );
    }

    private activateOption(data) {
        this.controller.isACHVisible = false;
        this.controller.isCardVisible = false;
        this.controller.isPlaceToPayVisible = false;
        data.forEach(element => {
            if (element.merchantTypeCode === 'ATH' || element.merchantTypeCode === 'APICALLMETHOD') {
                this.controller.isCardVisible = true;
            }

            if (element.merchantTypeCode === 'WALLET') {
                this.controller.isPlaceToPayVisible = true;
            }

            if (element.merchantTypeCode === 'CHECKOUT') {
                this.controller.isACHVisible = true;
            }
        });
    }

    async achPayment() {
        this.controller.paymentInProcess = true;
        const response = await this.getCreditCardPaymentOptions(
            this.controller.newPaymentRequest.currentBan,
            this.controller.newPaymentRequest.subscriber,
            this.controller.operationAmount.toString()
        );
        this.dismissProgress();
        this.controller.paymentInProcess = false;
        if (response && !response.hasError) {
            this.achProcessDone(response);
        } else {
            if (response.error) {
                this.achProcessDone(response);
            } else if (response.message) {
                this.showError(response.message);
            }
        }
    }

    private achProcessDone(resp) {
        if (resp.authorizationNumber) {
            this.controller.authorizationNumber = resp.authorizationNumber;
            this.controller.paymentStatusDesription = resp.paymentStatus;
            this.controller.confirmationNumber = resp.confirmationNumber;

            if (this.controller.confirmationNumber && !resp.hasError) {
                this.showLastStepOperation(1);
            } else {
                this.controller.errorMessage = resp.errorDisplay;
                this.showLastStepOperation(2);
            }
        } else {
            this.controller.errorMessage = resp.errorDisplay;
            this.showLastStepOperation(3);
        }
    }

    readyToFillForm() {
        let year = Number(this.controller.jstoday.substring(6, 10));
        this.controller.years = [];
        for (let i = 0; i < 10; i++) {
            this.controller.years.push(year.toString());
            year++;
        }
    }

    autoFill() {
        setTimeout(() => {
            const accountInfo = this.cacheStorage().accountInfo;
            if (this.controller.useSavedInformation) {
                this.controller.direction = accountInfo.addressLinesField.line1Field;
                this.controller.direction2 = accountInfo.addressLinesField.line2Field;
                this.controller.state = accountInfo.stateField;
                this.controller.town = accountInfo.cityField;
                this.controller.PostalCode = accountInfo.zIPField;
                if (!this.controller.direction && !!this.controller.direction2) {
                    this.controller.direction = this.controller.direction2;
                    this.controller.direction2 = '';
                }
            } else {
                this.controller.direction = '';
                this.controller.direction2 = '';
                this.controller.state = '';
                this.controller.town = '';
                this.controller.PostalCode = '';
            }
        }, 200);
    }

    private async executeAuthorizePayment() {
        if (this.validateCreditCardForm()) {
            if (!this.controller.athURL) {
                this.showProgress();
                this.requestBodyCreationForAuthorizePayment().then(
                    (result: any) => {
                        if (!result.changeProcess) {
                            this.dismissProgress();
                            if (result.errorDescription) {
                                this.showError(result.errorDescription);
                            } else {
                                this.controller.authorizationNumber = result.authorizationNumber;
                                this.controller.paymentStatusDesription =
                                    result.statusDescription;
                                this.controller.step3 = true;
                                this.controller.operationError = false;
                                this.controller.operationSuccess = true;
                                if (this.controller.paymentMethod === 'V') {
                                    this.controller.paymentMethod = 'Visa';
                                }
                                if (this.controller.paymentMethod === 'M') {
                                    this.controller.paymentMethod = 'Mastercard';
                                }
                                if (this.controller.paymentMethod === 'X') {
                                    this.controller.paymentMethod = 'American Express';
                                }
                                if (this.controller.paymentMethod === 'A') {
                                    this.controller.paymentMethod = 'ATH';
                                }
                            }
                        } else {
                            setTimeout(() => {
                                this.dismissProgress();
                            }, 2500);
                        }

                    },
                    (error) => {
                        this.dismissProgress();
                        this.showError(error);
                        this.router.navigate(['login']);
                    }
                );
            } else {
                const athData = await this.openWebViewAthPinPad();
                console.log('-> athData', athData);
                if (athData) {
                    const athList = athData[1].split(',');
                    const authorizationNumber = athList[0];
                    const confirmationNumber = athList[1];
                    this.processRedirectInfo('', '', '', authorizationNumber, confirmationNumber);
                } else {
                    this.dismissProgress();
                    this.initData();
                }
            }
        }
    }

    private openWebViewAthPinPad(): Promise<string> {
        let pinpadComplete = false;
        let apprCode: string;
        this.showProgress();
        return new Promise((resolve) => {
            if (!Utils.getPlatformInfo().android) {
                BROWSER_GENERIC_THEME_OPTION.location = 'no';
                BROWSER_GENERIC_THEME_OPTION.lefttoright = 'yes';
            }
            const browser: InAppBrowserObject = this.iab.create(this.controller.athURL, '_blank', BROWSER_GENERIC_THEME_OPTION);
            if (!Utils.getPlatformInfo().desktop) {
                browser.on('loaderror').subscribe(
                    (res) => {
                        console.log('-> res', res);
                        if (!pinpadComplete) {
                            pinpadComplete = true;
                        }
                        browser.close();
                        resolve(apprCode);
                    }
                );
                browser.on('loadstart').subscribe(
                    (res) => {
                        if (res.url.includes('apprCode')) {
                            pinpadComplete = true;
                            const athData = res.url.split('=');
                            apprCode = athData[1];
                            browser.close();
                            resolve(athData[1]);
                        }
                    }
                );
                browser.on('loadstop').subscribe(
                    (res) => {
                        if (!pinpadComplete) {
                            browser.show();
                        }
                        if (res.url.includes('login')) {
                            pinpadComplete = true;
                            browser.close();
                            resolve(apprCode);
                        }

                    }
                );
                browser.on('exit').subscribe(
                    (res) => {
                        console.log('-> res', res);
                        resolve(apprCode);
                    }
                );
            } else {
                browser.show();
                resolve();
            }
        });
    }

    private requestBodyCreationForAuthorizePayment() {
        const h2hActive = false;
        if (h2hActive) {
            let index: number;
            if (this.controller.isPrepaid) {
                index = this.controller.applyPaymentResponse.customerInfo
                    .map((x) => x.accountNumber.toString())
                    .indexOf(this.cacheStorage().accountInfo.defaultSubscriberField);
            } else {
                index = this.controller.applyPaymentResponse.customerInfo
                    .map((x) => x.accountNumber.toString())
                    .indexOf(this.controller.accountNumber.toString());
            }
            if (index === -1) {
                this.dismissProgress();
                this.showError(`La cuenta: ${this.controller.accountNumber}, no ha llegado en la respuesta del servidor`);
                return;
            } else {
                return new Promise(resolve => {
                    resolve({
                        data: '',
                        filler1: this.controller.isPrepaid ? 'PREPAGO' : '',
                        paymentToken: this.controller.applyPaymentResponse.paymentToken,
                        creditCardNumber: this.controller.cardNumber,
                        paymentMethod: this.controller.paymentMethod,
                        cardExpMonth: this.controller.expirationMonth,
                        cardExpYear: this.controller.expirationYear,
                        cardSecurityCode: this.controller.cvv,
                        accountNumber: atob(`${this.cacheStorage().accountInfo.bANField}`),
                        customerName: this.controller.name,
                        customerEmail: this.controller.userData.AccounInfo.emailField
                            ? this.controller.userData.AccounInfo.emailField
                            : 'noEmail@claropr.com',
                        address1: this.controller.direction
                            ? this.controller.direction
                            : this.controller.applyPaymentResponse.customerInfo[0].address1,
                        address2: this.controller.direction2
                            ? this.controller.direction2
                            : this.controller.applyPaymentResponse.customerInfo[0].address2,
                        city: this.controller.applyPaymentResponse.customerInfo[0].city,
                        state: this.controller.applyPaymentResponse.customerInfo[0].state,
                        zipCode: this.controller.PostalCode
                    });
                });
            }
        } else {
            if (this.count === 0) {
                this.count += 1;
                this.buildAuthorizationRequest(false, false, true);
            }

            return new Promise((resolve) => {
                resolve({
                    changeProcess: true
                });
            });
        }
    }

    getCreditCardPaymentOptions(account: string, subscriber: string, amount: string, extraParams: ExtraParamsPayment = {}): Promise<any> {
        return new Promise((resolve) => {
            this.showProgress('Está siendo redirigido a la página de el Banco Popular para realizar el pago.');

            this.services.getPaymentOptions(extraParams.productId || APP.PRODUCT_ID_INVOICE_PAYMENTS, extraParams.merchantId || '')
                .then(async (response: any) => {
                    if (!response.paymentOptions || !response.paymentOptions.length) {
                        resolve({
                            hasError: true,
                            message: 'Lo sentimos, actualmente no contamos con metodos de pagos disponibles.'
                        });
                    } else {
                        const options = response.paymentOptions[0];
                        const ready = await this.initPayment(account, subscriber, amount, options, extraParams);
                        resolve(ready);
                    }
                }, error => {
                    resolve({hasError: true, message: error.message});
                });
        });
    }

    private initPayment(account: string, subscriber: string, amount: string, options: any, extraParams: ExtraParamsPayment): Promise<any> {
        return new Promise((resolve) => {
            const data: IntentProvider = this.cacheStorage();
            this.services.initiatePaymentProcess(
                extraParams.productId || APP.PRODUCT_ID_INVOICE_PAYMENTS,
                options.paymentOptionId,
                +amount,
                data.accountInfo.emailField,
                subscriber,
                account,
                data.accountInfo.firstNameField + ' ' + data.accountInfo.lastNameField,
                options.paymentOptionName,
                extraParams.merchantId,
                extraParams.locationId,
                extraParams.invoiceNumber)
                .then((response: any) => {
                    this.store(APP.PAYMENT_STATUS_STORED, response.paymentToken);
                    this.browserProvider.openPaymentBrowser(response.redirectURL).then(done => {
                        console.log('-> done', done);
                        // if browser desktop, we wait 1 minute for payment
                        if (Utils.getPlatformInfo().desktop) {
                            setTimeout(async () => {
                                const ready = await this.verifyPaymentProcess();
                                resolve(ready);
                            }, 60 * 1000);
                        } else { // if mobile, we wait 15 seconds after payment done
                            if (done) {
                                this.showProgress('Verificando pago...');
                                setTimeout(async () => {
                                    const ready = await this.verifyPaymentProcess();
                                    resolve(ready);
                                }, 30 * 1000); // 30 seconds after finish payment for verify
                            } else {
                                resolve({hasError: true});
                            }
                        }
                    }).catch(err => {
                        resolve({hasError: true, message: err});
                    });
                }, error => {
                    resolve({hasError: true, message: error.message});

                });
        });
    }

    private verifyPaymentProcess(): Promise<any> {
        return new Promise<any>(async (resolve) => {
            this.fetch(APP.PAYMENT_STATUS_STORED).then(result => {
                this.services.verifyPayment(result).then((response: any) => {
                    resolve(response);
                }, error => {
                    resolve({hasError: true, message: error.message, error});
                });
            });
        });
    }

    downloadLastInvoice() {
        this.showProgress();
        this.service.downloadLastPdf(
            `${this.cacheStorage().accountInfo.bANField}`,
            this.controller.jstoday.substring(6, 10),
            'pdf',
            this.cacheStorage().tokenSession
        ).then((data: any) => {
            this.dismissProgress();
            console.log('invoice data', data);
            if (data.invoiceContent) {
                this.controller.invoiceContent = data.invoiceContent;
                this.openPdf();
            } else {
                this.showAlert('En estos momentos no está la información disponible favor intente nuevamente más tarde');
                return;
            }
        }, () => {
            this.dismissProgress();
            this.showAlert('Ha ocurrido un error por favor intente nuevamente mas tarde');
        });
    }

    private openPdf() {
        if (this.controller.invoiceContent) {
            const writeDirectory = Utils.getPlatformInfo().ios ? this.file.dataDirectory : this.file.externalDataDirectory;
            this.file.writeFile(
                writeDirectory, 'invoice',
                Utils.convertBase64ToBlob(this.controller.invoiceContent, 'data:application/pdf;base64'),
                {replace: true})
                .then(() => {
                    this.fileOpener.open(writeDirectory + 'invoice', 'application/pdf')
                        .catch(() => {
                            this.showAlert('No posee ninguna aplicación para abrir su factura, por favor diríjase a la tienda para descargar una.');
                        });
                })
                .catch(() => {
                    console.error('Error writing pdf file');
                });
        }
    }

    getLocalData() {
        this.personalizedMessages = {
            rejectedMessage: 'Estimado cliente, la transacciónn fue cancelada o fue rechazada por la institución bancaria.  Si tienes alguna pregunta sobre su pago. le recomendamos se comunique con un experto en Servicio al 787-775-0000.  También,  podrá comunicarse con nosotros las 24 horas al siguiente enlace. <br> <a href="https://ayuda.claropr.com/" target="_blank">https://ayuda.claropr.com/</a>',
            pendingMessage: 'Estimado cliente, la transacción esta pendiente de procesar. Una vez terminado el procesamiento, recibirá un correo electronico con el estado de su pago. \n\nSi tienes alguna pregunta sobre su pago, le recomendamos se comunique con un experto en Servicio al 787-775-0000.  También, podrá comunicarse con nosotros las 24 horas al siguiente enlace. <br> <a href="https://ayuda.claropr.com/">https://ayuda.claropr.com/</a>',
            okMessage: 'Pago autorizado y aplicado'
        };
    }

    scrollBottom() {
        this.scrollToBottom.next(true);
    }
}

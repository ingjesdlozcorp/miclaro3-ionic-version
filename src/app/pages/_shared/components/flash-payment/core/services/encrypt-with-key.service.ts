import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { KEYS } from '../flash-payment-constants';

@Injectable({
    providedIn: 'root',
})
export class EncryptWithKeyService {
    encryptionKey = KEYS.Encriptkey;
    encryptionIV = KEYS.Encriptiv;
    cypherOption: any;

    constructor() {
    }

    setKeys(key, iv) {
        this.encryptionKey = key;
        this.encryptionIV = iv;
        this.cypherOption = {
            iv: CryptoJS.enc.Utf8.parse(this.encryptionIV),
        };
    }

    encryp(data) {
        this.cypherOption = {
            iv: CryptoJS.enc.Utf8.parse(this.encryptionIV),
        };
        return CryptoJS.AES.encrypt(data, CryptoJS.enc.Utf8.parse(this.encryptionKey), this.cypherOption).toString();
    }

    decrypt(cryptText) {
        this.cypherOption = {
            iv: CryptoJS.enc.Utf8.parse(this.encryptionIV),
        };
        return CryptoJS.AES.decrypt(cryptText, CryptoJS.enc.Utf8.parse(this.encryptionKey), this.cypherOption).toString(CryptoJS.enc.Utf8);
    }
}

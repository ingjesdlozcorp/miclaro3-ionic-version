import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilsService } from '../../../../../../services/utils.service';

@Injectable({
    providedIn: 'root',
})
export class FlashApiService {

    constructor(
        private http: HttpClient,
        private util: UtilsService
    ) {
    }

    getTransactionStatus(dataToken) {
        const method = 'getTransaccionStatus';
        const parameters = JSON.stringify({
            dataToken,
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    decryptTokenByCrypto(dataToken) {
        const method = 'alternativeResponse';
        const parameters = JSON.stringify({
            dataToken,
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getTransactionAuthorizationEncrypt(dataToken) {
        const method = 'getTransactionAuthorizationEncrypt';
        const parameters = JSON.stringify({
            dataToken,
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    tokenizeCard(dataToken) {
        const method = 'tokenizeCard';
        const parameters = JSON.stringify({
            dataToken,
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getTokenCards(dataToken) {
        const method = 'getTokenCards';
        const parameters = JSON.stringify({
            dataToken,
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    deleteInstrument(dataToken) {
        const method = 'deleteInstrument';
        const parameters = JSON.stringify({
            dataToken,
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getHelpHeaders(): HttpHeaders {
        let header = new HttpHeaders();
        header = header.append('authToken', 'QADcsgShNdpN6M2y');
        header = header.append('appKey', 'COLLECTIONPATH');
        header = header.append('content-type', 'application/json; charset=utf-8');
        return header;
    }

    getPaymentsOptions() {
        const method = 'getPaymentsOptions';
        const parameters = JSON.stringify({
            method,
            applicationId: '1'
        });

        return this.util.serverListener(parameters).toPromise();
    }

    downloadLastPdf(Ban, year, format, token) {
        const method = 'downloadLastPdf';
        const parameters = JSON.stringify({
            Ban,
            year,
            format,
            token,
            method
        });

        return this.util.serverListener(parameters).toPromise();
    }

    getIPAddress() {
        return this.http.get('https://api.ipify.org/?format=json');
    }
}

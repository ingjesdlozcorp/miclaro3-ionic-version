export interface PlaceToPayOK {
    requestId: number;
    status: Status;
    request: FlashRequest;
    payment: PaymentElement[];
    subscription: null;
}

export interface PaymentElement {
    processorFields: ProcessorField[];
    status: Status;
    reference: string;
    internalReference: number;
    paymentMethod: string;
    paymentMethodName: string;
    issuerName: string;
    amount: PurpleAmount;
    authorization: string;
    receipt: number;
    franchise: string;
    refunded: boolean;
}

export interface PurpleAmount {
    factor: number;
    from: From;
    to: From;
}

export interface From {
    currency: string;
    total: number;
}

export interface ProcessorField {
    keyword: string;
    value: string;
    displayOn: string;
}

export interface Status {
    status: string;
    reason: string;
    message: string;
    date: Date;
}

export interface FlashRequest {
    locale: string;
    payer: Buyer;
    buyer: Buyer;
    payment: RequestPayment;
    subscription: null;
    returnUrl: string;
    paymentMethod: null;
    cancelUrl: null;
    ipAddress: string;
    userAgent: string;
    expiration: Date;
    captureAddress: boolean;
    skipResult: boolean;
    noBuyerFill: boolean;
    fields: Fields;
}

export interface Buyer {
    document: null;
    documentType: null;
    name: string;
    surname: string;
    company: null;
    email: string;
    mobile: string;
}

export interface Fields {
    item: ProcessorField[];
}

export interface RequestPayment {
    reference: string;
    description: string;
    amount: FluffyAmount;
    allowPartial: boolean;
    shipping: Buyer;
    items: Item[];
    recurring: Recurring;
    instrument: null;
    transaction: null;
}

export interface FluffyAmount {
    taxAmount: number;
    vatDevolutionBase: number;
    taxes: Tax[];
    details: null;
    currency: string;
    total: number;
}

export interface Tax {
    kind: string;
    amount: string;
    baseAmount: null;
}

export interface Item {
    sku: null;
    name: null;
    category: null;
    qty: number;
    price: number;
    tax: number;
}

export interface Recurring {
    periodicity: string;
    interval: number;
    nextPayment: Date;
    maxPeriods: number;
    dueDate: Date;
    notificationUrl: string;
}

export interface AuthorizationRequest {
    customer: Customer;
    client: Client;
    payment: Payment;
    subscription: Subscription;
    paymentToken: string;
    paymentOptionId: number;
    transactionType: number;
    doProvisioningIndicator: number;
    parentPaymentId: number;
    externalId: string;
    locationId: string;
    invoiceNumber: string;
    installmentUnits: number;
    transactionDescription: string;
}

export interface Client {
    ipAddress: string;
    userAgent: string;
}

export interface Customer {
    accountNumber: string;
    customerName: string;
    customerLastName: string;
    customerEmail: string;
    subscriberNumber: string;
    address: Address;
    transactionDescription: string;
    transactionAmount: number;
}

export interface Address {
    address1: string;
    address2: string;
    city: string;
    state: string;
    zipCode: string;
}

export interface Payment {
    cardNumber: string;
    expirationDate: string;
    securityCode: string;
    paymentMethod: string;
    amount: number;
    tokenInstrumentId?: number;
}

export interface Subscription {
    expirationDate: string;
    cardNumber: string;
    securityCode: string;
    merchantId: number;
    brandCode: string;
    tokenInstrumentId: number;
    subscriptionActions: number;
}

export interface GetTransactionAuthorizationResponse {
    paymentId: number;
    transactionResponseType: number;
    webCheckOutAttribute: WebCheckOutAttribute;
    apiCallMethodAttributes: APICallMethodAttributes;
    hasError: boolean;
    errorDisplay: string;
    errorDescription: string;
    errorNum: number;
    errorSubject: string;
    message: string;
}

export interface APICallMethodAttributes {
    authorizationNumber: string;
    confirmationNumber: string;
}

export interface WebCheckOutAttribute {
    requestId: string;
    checkOutUrl: string;
    status: string;
    description: string;
    displayMessage: string;
    responseDate: Date;
}

export interface GetTransactionStatusRequest {
    paymentId: number;
    paymentToken: string;
    bankToken: string;
    authorizationNumber: string;
    confirmationNumber: string;
}

export interface GetTransactionStatusResponse {
    paymentId: number;
    paymentToken: string;
    bankToken: string;
    paymentStatusId: number;
    paymentStatusDesription: string;
    isFinishedTransaction: boolean;
    initialDate: Date;
    paymentDate: Date;
    productId: number;
    productDescription: string;
    authorizationNumber: string;
    paymentMethod: string;
    hasError: boolean;
    errorDisplay: string;
    errorDescription: string;
    errorNum: number;
    errorSubject: string;
    message: string;
}

export interface PaymentOptionsEnabled {
    id: number;
    description: string;
    productCode: string;
    paymentOptions: PaymentOption[];
}

export interface PaymentOption {
    id: number;
    description: string;
    merchantId: number;
    merchantCode: string;
    merchantDescription: string;
    merchantProviderId: number;
    merchantProviderName: string;
    merchantProviderLogoUrl: string;
    merchantTypeId: number;
    merchantTypeCode: string;
    paymentMethods: PaymentMethod[];
}

export interface PaymentMethod {
    id: number;
    description: string;
    imageUrl: string;
}

export interface TokenCard {
    brandCode: string;
    expirationDate: string;
    franchise: string;
    franchiseName: string;
    id: number;
    lastFourDigits: string;
    payerName: string;
    token: string;
    token2: string;
    tokenAlias: string;
}

export interface ExtraParamsPayment {
    productId?: string;
    merchantId?: string;
    locationId?: string;
    invoiceNumber?: string;
}

export default interface DeviceConnectionInformation {
    ip_address: string;
    country: string;
    country_code: string;
    continent: string;
    continent_code: string;
    city: string;
    county: string;
    region: string;
    region_code: string;
    postal_code: any;
    timezone: string;
    owner: any;
    longitude: number;
    latitude: number;
    currency: string;
    languages: any[];
}

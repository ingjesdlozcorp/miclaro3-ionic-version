import { formatDate } from '@angular/common';

export class FlashPaymentUtil {
    static AmexCardnumber(inputtxt) {
        const cardno = /^(?:3[47][0-9]{13})$/;
        return cardno.test(inputtxt);
        // ccv es de 3 numeros en este caso.
    }

    static VisaCardnumber(inputtxt) {
        const cardno = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
        return cardno.test(inputtxt);
    }

    static MasterCardnumber(inputtxt) {
        const cardno = /^(?:5[1-5][0-9]{14})$/;
        return cardno.test(inputtxt);
    }

    static DiscoverCardnumber(inputtxt) {
        const cardno = /^(:?6011|5[0-9][0-9])[0-9]{12}$/;
        return cardno.test(inputtxt);
    }

    static DinerClubCardnumber(inputtxt) {
        const cardno = /^(?:3(?:0[0-5]|[68][0-9])[0-9]{11})$/;
        return cardno.test(inputtxt);
    }

    static JCBCardnumber(inputtxt) {
        const cardno = /^(?:(?:2131|1800|35\d{3})\d{11})$/;
        return cardno.test(inputtxt);
    }

    static ATHcardnumber(inputtxt: string) {
        const format1 = '2215';
        const format2 = '0215';
        if (inputtxt.startsWith(format1) || inputtxt.startsWith(format2)) {
            if (inputtxt.length === 16) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    static isNumericValidCharacter(text: string): boolean {
        const numbers = '1234567890';
        let hasSpecial = false;

        for (let i = 0; i < text.length; i++) {
            if (numbers.indexOf(text.charAt(i), 0) === -1) {
                hasSpecial = true;
            }
        }
        return hasSpecial;
    }


    static isInvalidCharacter(text: string): boolean {
        const numbers = 'abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ';
        let hasSpecial = false;

        for (let i = 0; i < text.length; i++) {
            if (numbers.indexOf(text.charAt(i), 0) === -1) {
                hasSpecial = true;
            }
        }
        return hasSpecial;
    }

    static formatFromPickerToServer(date: Date) {
        return formatDate(date, 'yyyy-M-d', 'en-US');
    }

    static formatEncryptData(data: string) {
        return data.replace('\0', '').replace('\\u000f', '').replace('\\u0005', '').replace(/\u0000/g, '');
    }
}

export const STATES = [
    'AL', 'AK', 'AZ', 'AR', 'CA',
    'CO', 'CT', 'DE', 'DC', 'FL',
    'GA', 'HI', 'ID', 'IL', 'IN',
    'IA', 'KS', 'KY', 'LA', 'ME',
    'MD', 'MA', 'MI', 'MN', 'MS',
    'MO', 'MT', 'NE', 'NV', 'NH',
    'NJ', 'NM', 'NY', 'NC', 'ND',
    'OH', 'OK', 'OR', 'PA', 'RI',
    'SC', 'SD', 'TN', 'TX', 'UT',
    'VT', 'VI', 'VA', 'WA', 'WV',
    'WI', 'WY',
];

export const MONTHS = [
    '01', '02', '03', '04',
    '05', '06', '07', '08',
    '09', '10', '11', '12',
];

export const TRANSACTION_TYPES = [
    'INVOICE PAYMENTS',
    'PREPAID PRODUCT RECHARGES',
    'INSTALLMENT ACCELERATION',
];

export const VARS_NEEDED = {
    name: '',
    cardNumberString: '',
    cardNumber: '',
    expirationDate: '',
    cvv: '',
    direction: '',
    direction2: '',
    condition: null,
    town: '',
    PostalCode: '',
    accountNumber: '',
    accountNumber2: '',
    routeNumber: '',
    numberOfOrder: '',
    Bank: '',
    BankAccount: '',
    AccordingToTheOperation: false,
    paymentMethod: '',
    reference: '',
    operationAmount: '',
    step1: true,
    step2: false,
    step3: false,
    isValidCard: false,
    cardType: null,
    isTheCardNumberEmpty: false,
    cardIsExpired: false,
    isEmptyZipCode: false,
    isTheCcvError: false,
    isTheCcvEmpty: false,
    isValidZip: true,
    months: MONTHS,
    states: STATES,
    state: 'PR',
    useSavedInformation: false,
    isACH: false,
    isCard: false,
    isPlaceToPay: true,
    cardFormComplete: false,
    achFormComplete: false,
    authCode: '',
    paymentToken: null,
    openWeb: false,
    redirectURL: null,
    operationError: false,
    operationWarning: false,
    operationSuccess: false,
    newPaymentRequest: null,
    doPaymentRequest: null,
    authorizationNumber: '',
    confirmationNumber: '',
    paymentStatusDesription: null,
    userData: null,
    subscriberInfo: null,
    isValidAddress: false,
    isPrepaid: false,
    today: new Date(),
    jstoday: '',
    expirationYear: '',
    years: [],
    expirationMonth: '',
    applyPaymentResponse: null,
    serviceComplementData: null,
    transactionType: null,
    isACHVisible: false,
    isCardVisible: false,
    isPlaceToPayVisible: false,
    tokenCards: [],
    paymentOptions: [],
    paymentOptionSelected: null,
    canOpenAthPinPad: false,
    athURL: null,
    tokenInstrumentId: 0,
    canActivateTokenizationProcess: false,
    invoiceContent: null,
    warningDescription: '',
    showSubscriptionOption: false,
    locationId: '',
    invoiceNumber: '',
    quantityToAccelerate: 0,
    cardSelected: null,
    errorMessage: '',
    ipAddress: '',
    paymentInProcess: false,
    canRunPaymentMethod: true
};

export const JWT = {
    TOKEN: 'JWTServiceAccessToken',
    KEY: 'Yh2k7QSu4l8CZg5p6X3Pna9L0Miy4D3Bvt0JVr87UcOj69Kqw5R2Nmf4FWs03Hdx',
    ISSUER: 'JWTAuthenticationServer',
    AUDIENCE: 'JWTServicePostmanClient',
    EnabledSecurity: 'false'
};

export const KEYS = {
    Encriptkey: '%C*F-JaNdRgUkXp2s5v8y/A?D(G+KbPe',
    Encriptiv: 'kYp2s5v8y/B?E(H+',
};

export const PAYMENT_VERSION = {
    P2P: 'P2P',
    MMP: 'MMP'
};

export const PAYMENT_FORM_METHOD = {
    CARD: 0,
    WALLET: 1,
    ACH: 2
};

import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
    selector: '[appTwoDigitDecimalNumber]'
})
export class TwoDigitDecimalNumberDirective {

    /**
     * Value before the decimal point specifies the number of digits before decimal and value after the decimal specifies the number of digits after decimal.
     * Example: 4.2 (Before decimal 4 digits & 2 digits after decimal)
     */
    @Input('appTwoDigitDecimalNumber') decimals = '4.2';
    private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', '-', 'ArrowLeft', 'ArrowRight', 'Del', 'Delete'];


    private check(value: string): RegExpMatchArray {
        const [length, precision] = this.decimals.split('.');
        const regex = `^([\\d]{0,${+length}})((\\.{1})([\\d]{1,${+precision}})?)?$`;
        return String(value).match(new RegExp(regex));
    }

    private run(oldValue: string) {
        setTimeout(() => {
            const currentValue: string = this.el.nativeElement.value;
            if (currentValue && (!this.check(currentValue) || +currentValue > 2000)) {
                this.control.control.patchValue(oldValue);
            }
        });
    }

    constructor(private el: ElementRef, private control: NgControl) {
    }

    @HostListener('keydown', ['$event'])
    onKeyDown(event: KeyboardEvent) {
        if (this.specialKeys.indexOf(event.key) !== -1) {
            return;
        }
        this.run(this.el.nativeElement.value);
    }

    @HostListener('paste', ['$event'])
    onPaste(event: ClipboardEvent) {
        this.run(this.el.nativeElement.value);
    }

}

import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-popup-splash-claro-tv',
    templateUrl: './splash-claro-tv.component.html',
    styleUrls: ['./splash-claro-tv.component.scss'],
})
export class SplashClaroTvComponent {

    @Output() close = new EventEmitter<boolean>();

    closePopup() {
        this.close.emit();
    }

    goToClaroTVHome() {
        this.close.emit(true);
    }
}

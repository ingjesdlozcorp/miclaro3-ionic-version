import {Component} from '@angular/core';
import { ServicesProvider } from 'src/app/services/services.provider';
import { BodyPopupBase, PopupBase, UtilsService } from '../../../../../services/utils.service';
import { Popups } from '../../../../../utils/const/popups';

@Component({
    selector: 'app-popup-base',
    templateUrl: './popup-base.component.html',
    styleUrls: ['./popup-base.component.scss'],
})
export class PopupBaseComponent {

    open: boolean;
    title: string;
    body: BodyPopupBase;
    extraClasses: string;
    zIndex = 10000;
    popups = Popups;
    isClientMessage = false;
    updateMessagePayload: any = null;

    constructor(utilsService: UtilsService, 
                private servicesProvider: ServicesProvider) {
        utilsService.popupBase.subscribe((data: PopupBase) => {
            this.open = data.open;
            this.title = data.title;
            this.body = data.body;
            this.extraClasses = data.extraClasses || 'new-popup';
            this.zIndex = data.zIndex || 10000;
            if (data.body.name === this.popups.ACCOUNT_MESSAGE) {
                this.isClientMessage = true;
                this.updateMessagePayload = {
                    id_message: data.body.data.idNotic,
                    token: data.body.data.token,
                    account: data.body.data.ban,
                    method: data.body.data.method,
                }
            }
        });
    }

    closePopup() {
        this.open = false;
        if(this.isClientMessage) {
            this.checkMessageAsRead();
        }
    }

    checkMessageAsRead() {
        this.servicesProvider.updateMessageStatus(this.updateMessagePayload);
    }

    
}


import {Component, EventEmitter, Input, Output} from '@angular/core';
import {UtilsService} from '../../../../../services/utils.service';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {ServicesProvider} from '../../../../../services/services.provider';
import {AlertController} from '@ionic/angular';
import {IntentProvider} from '../../../../../services/intent.provider';
import {BasePage} from '../../../../base.page';

@Component({
    selector: 'app-popup-general-terms',
    templateUrl: './popup-general-terms.component.html',
    styleUrls: ['./popup-general-terms.component.scss'],
})
export class PopupGeneralTermsComponent extends BasePage {

    @Output() close = new EventEmitter();

    constructor(router: Router,  storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    closeTerms() {
        this.close.emit();
    }
}


import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-popup-gift-received',
    templateUrl: './gift-received.component.html',
    styleUrls: ['./gift-received.component.scss'],
})
export class GiftReceivedComponent {

    @Input() product: any = {};
    @Output() close = new EventEmitter();

    closePopup() {
        this.close.emit();
    }
}

import { Component, OnInit } from '@angular/core';
import { ServicesProvider } from 'src/app/services/services.provider';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import {AlertController} from '@ionic/angular';
import {UtilsService} from 'src/app/services/utils.service';
import {IntentProvider} from '../../../../services/intent.provider';
import {Account} from '../../../../models/authenticate';

@Component({
    selector: 'app-account-select',
    templateUrl: './account-select.component.html',
    styleUrls: ['./account-select.component.scss'],
})
export class AccountSelectComponent extends BasePage implements OnInit {

    accounts: Account[] = [];
    selectedBan: string;

    constructor(public router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        const data = this.cacheStorage();
        if (data.isPostpaidAccount) {
            this.accounts = data.postpaidAccounts;
        }
        if (data.isPrepaidAccount) {
            this.accounts = data.prepaidAccounts;
        }
        if (data.isTelephonyAccount) {
            this.accounts = data.telephonyAccounts;
        }
        this.selectedBan = String(data.accountInfo.bANField);
    }

    onChange() {
        const accountSelected: Account = this.cacheStorage().getAccountByBan(this.selectedBan);
        this.selectAccount(accountSelected);
    }
}

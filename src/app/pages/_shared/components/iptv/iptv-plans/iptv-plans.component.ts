import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { forkJoin } from 'rxjs';
import { ServicesProvider } from '../../../../../services/services.provider';
import { IntentProvider } from '../../../../../services/intent.provider';
import { Channel, Plan } from '../../../../../models/iptv';
import { BasePage } from '../../../../base.page';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController, IonSlides } from '@ionic/angular';
import { UtilsService } from '../../../../../services/utils.service';
import { Popups } from '../../../../../utils/const/popups';

@Component({
    selector: 'app-iptv-plans',
    templateUrl: './iptv-plans.component.html',
    styleUrls: ['./iptv-plans.component.scss'],
})
export class IptvPlansComponent extends BasePage implements OnInit {

    @ViewChild('slides', {static: true}) slides: IonSlides;
    @Input() hasToken: boolean;
    @Input() disableSelectTerms: boolean;
    @Output() selectPlan: EventEmitter<any> = new EventEmitter<any>();

    plans: Plan[] = [];
    planChannels: Channel[];
    planSelected: Plan;
    indexPlanShowing = 0;

    popupController = {
        popupChannels: false,
        popupTermsIptv: false,
    };
    typeBan: { telephony: boolean; byop: boolean; postpaid: boolean; prepaid: boolean };

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    slideChanged(e: any) {
        this.slides.getActiveIndex().then((index: number) => {
            this.indexPlanShowing = index;
        });
    }

    ngOnInit() {
        const data = this.cacheStorage();
        this.typeBan = {
            postpaid: data.isPostpaidAccount,
            prepaid: data.isPrepaidAccount,
            telephony: data.isTelephonyAccount,
            byop: data.isByop
        };
        this.getPlans();
    }

    private getPlans() {
        this.showProgress('Cargando planes...');
        const token = this.cacheStorage().tokenSession;
        this.services.getPlansIPTV(token).then(
            (success: any) => {
                this.plans = success.catalogs[2].catalog[0].products;
                this.getChannelsFeatured();
            }, error => {
                this.showError(error.message, () => {
                    this.goHomePage();
                });
                this.dismissProgress();
            });
    }

    private getChannelsFeatured() {
        const token = this.cacheStorage().tokenSession;
        const requestList = [];
        this.plans.map(plan => {
            requestList.push(this.services.getChannelsIPTV(token, plan.productId, 0, true));
        });
        forkJoin(requestList).subscribe(
            (results: any) => {
                this.plans.map((plan, index) => {
                    plan.channelsFeatured = results[index];
                });
                this.dismissProgress();
            }, e => {
                this.dismissProgress();
            }
        );
    }

    selectPlanEmit(plan: Plan) {
        if (!this.hasToken) {
            this.showProgress();
            return;
        }
        const account = this.cacheStorage().accountSSO.accountQualifications;
        if (!account.iptvCapable.iptvCapable || !account.iptvCapable.subscriberCapable) {
            this.showError('Estimado cliente, su tipo de cuenta o plan actual no es elegible para adquirir este servicio. Para mayor información consulte la sección de Términos y Condiciones o Soporte Técnico.');
            return;
        }
        this.planSelected = plan;
        this.validateIfAddPlanToCart();
    }

    private validateIfAddPlanToCart() {
        const token = this.cacheStorage().tokenSession;
        const tokenIptv = this.cacheStorage().tokenIptv;

        this.services.getCartIptv(token, tokenIptv).then((success: any) => {
            if (success.products.length) {
                this.cleanCart(token, tokenIptv, success.products);
            } else {
                this.addPlanToCart();
            }
            this.dismissProgress();
        }, e => {
            this.showError(UtilsService.errorGeneral);
            this.dismissProgress();
        });
    }

    private cleanCart(token: string, tokenIptv: string, products: any[]) {
        const promises = [];
        for (const prod of products) {
            promises.push(this.services.deleteItemFromCart(token, tokenIptv, 0));
        }

        if (promises.length) {
            // tslint:disable-next-line:no-shadowed-variable
            Promise.all(promises).then((success: any[]) => {
                this.addPlanToCart();
            }, e => {
                this.showError(e.message);
                this.dismissProgress();
            });
        }
    }

    addPlanToCart() {
        this.showProgress('Procesando...');
        const token = this.cacheStorage().tokenIptv;
        const {productId, salePrice, downPayment} = this.planSelected;

        const cartItems = [
            {
                token,
                productId,
                promoCode: '',
                installments: 0,
                decPrice: salePrice,
                decDeposit: 0,
                decDownPayment: downPayment,
                decTotalPrice: salePrice,
                Qty: 1,
                flowId: 1,
                ssoToken: '',
                userID: 0,
                parentProductId: 0,
                parentCartId: 0,
                creditClass: 'C',
                downgradeAllowed: false,
                pendingAccelerated: 0,
                acceletartedAmount: 0,
                pastDueAmount: 0,
                delicuency: false
            }
        ];

        // tslint:disable-next-line:no-shadowed-variable
        this.services.addItemsToCart(token, JSON.stringify(cartItems)).then(success => {
            this.selectPlan.emit(this.planSelected);
            this.dismissProgress();
        }, error => {
            this.showError(error.message);
            this.dismissProgress();
        });
    }

    viewChannels(plan: Plan) {
        this.planSelected = plan;
        this.showProgress('Espere mientras cargamos los canales...');
        this.getChannels();
    }

    viewTermsOffer(plan: Plan) {
        this.showPopup(plan.offer.chvOfferDetails, {name: Popups.TERMS_OFFER, terms: plan.offer.chvOfferTems});
    }

    viewTermsIptv() {
        this.popupController.popupTermsIptv = true;
    }

    closePopupIptv() {
        this.popupController.popupTermsIptv = false;
    }

    private async getChannels() {
        const token = this.cacheStorage().tokenSession;
        this.services.getChannelsIPTV(token, this.planSelected.productId).subscribe(success => {
            if (success) {
                this.planChannels = success;
                this.showPopup(`Guía de Canales: ${this.planSelected.productName}`, {name: Popups.IPTV_CHANNELS, channels: this.planChannels});
            }
            this.dismissProgress();
        }, e => {
            this.dismissProgress();
        });
    }

}

import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-popup-iptv-terms',
    templateUrl: './popup-terms-iptv.component.html',
    styleUrls: ['./popup-terms-iptv.component.scss'],
})
export class PopupTermsIptvComponent {

    @Output() close = new EventEmitter();
    channels: any[] = new Array(450);

    constructor() {
    }

    closeEmit() {
        this.close.emit();
    }
}


import { Component, Input } from '@angular/core';
import { Channel } from '../../../../../models/iptv';

@Component({
    selector: 'app-popup-iptv-channels',
    templateUrl: './popup-iptv-channels.component.html',
    styleUrls: ['./popup-iptv-channels.component.scss'],
})
export class PopupIptvChannelsComponent {

    @Input() channels: Channel[];

    constructor() {
    }
}


import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-popup-iptv-terms-offer',
    templateUrl: './popup-terms-offer.component.html',
    styleUrls: ['./popup-terms-offer.component.scss'],
})
export class PopupTermsOfferComponent {

    @Input() offerTerms: string;
    currentYear: number;

    constructor() {
        this.currentYear = new Date().getFullYear();
    }
}


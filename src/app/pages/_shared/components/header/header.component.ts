import { Component, OnInit } from '@angular/core';
import { ServicesProvider } from 'src/app/services/services.provider';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import {AlertController} from '@ionic/angular';
import {UtilsService} from 'src/app/services/utils.service';
import {IntentProvider} from '../../../../services/intent.provider';
import { Utils } from '../../../../utils/utils';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent extends BasePage implements OnInit {

    name = '';
    close = true;
    countNotifications = 0;

    typeBan = {
        postpaid: false,
        prepaid: false,
        telephony: false
    };

    constructor(public router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);

    }

    ngOnInit() {
        this.name = this.cacheStorage().accountInfo.firstNameField;
        const data = this.cacheStorage();
        if (this.router.isActive('/home/no-associated', true)) {
            this.typeBan = {
                postpaid: data.tab === 0,
                prepaid: data.tab === 1,
                telephony: data.tab === 2
            };
        } else {
            this.typeBan = {
                postpaid: data.isPostpaidAccount,
                prepaid: data.isPrepaidAccount,
                telephony: data.isTelephonyAccount
            };
        }

        this.utils.openMenu.subscribe(open => {
            this.close = !open;
        });
        this.countNotifications = data.countPendingNotifications;
    }

    openNav() {
        this.close = false;
        this.utils.openMenu.emit(true);
    }

    closeNav() {
        this.close = true;
        this.utils.openMenu.emit(false);
    }

    typeOfClient(param: number) {
        switch (param) {
            case 1:
                if (!this.typeBan.postpaid) {
                    if (this.cacheStorage().isGuest) {
                        this.showAlertAccessLimited();
                    } else {
                        this.utils.registerScreen('postpaid');
                        this.goPostpaid();
                    }
                }
                break;
            case 2:
                if (!this.typeBan.prepaid) {
                    if (this.cacheStorage().isGuest) {
                        this.showAlertAccessLimited();
                    } else {
                        this.goPrepaid();
                    }
                }
                break;
            case 3:
                if (!this.typeBan.telephony) {
                    if (this.cacheStorage().isGuest) {
                        this.showAlertAccessLimited();
                    } else {
                        this.goTelephony();
                    }
                }
                break;
        }
    }

    goPostpaid() {
        const accounts = this.cacheStorage().postpaidAccounts;
        if (accounts.length > 0) {
            this.selectAccount(this.cacheStorage().getAccountByBan(accounts[0].account));
        } else {
            this.cacheStorage().tab = 0;
            this.goPageNoAssociated();
        }
        this.typeBan.postpaid = true;
        this.typeBan.prepaid = false;
        this.typeBan.telephony = false;
    }

    goPrepaid() {
        const accounts = this.cacheStorage().prepaidAccounts;
        if (accounts.length > 0) {
            this.selectAccount(this.cacheStorage().getAccountByBan(accounts[0].account));
        } else {
            this.cacheStorage().tab = 1;
            this.goPageNoAssociated();
        }
        this.typeBan.postpaid = false;
        this.typeBan.prepaid = true;
        this.typeBan.telephony = false;
    }

    goTelephony() {
        const accounts = this.cacheStorage().telephonyAccounts;
        if (accounts.length > 0) {
            this.selectAccount(this.cacheStorage().getAccountByBan(accounts[0].account));
        } else {
            this.cacheStorage().tab = 2;
            this.goPageNoAssociated();
        }
        this.typeBan.postpaid = false;
        this.typeBan.prepaid = false;
        this.typeBan.telephony = true;
    }
    notifications() {
        this.goPage('/module/notifications');
    }

    // Add here all module where should be hide
    tabHidden(): boolean {
        return this.router.isActive('/module/purchases', false) ||
            this.router.isActive('/module/support', false) ||
            this.router.isActive('/module/netflix', false) ||
            this.router.isActive('/module/netflix/terms', false) ||
            this.router.isActive('/module/netflix/faq', false) ||
            this.router.isActive('/module/netflix/support', false) ||
            this.router.isActive('/module/netflix/subscription', false) ||
            this.router.isActive('/module/netflix/redirect', false) ||
            this.router.isActive('/module/data-plan/success', false) ||
            this.router.isActive('/module/notifications', false) ||
            this.router.isActive('/module/change-plan/confirm', false) ||
            this.router.isActive('/module/change-plan/recharge', false) ||
            this.router.isActive('/module/change-plan/success', false) ||
            this.router.isActive('/module/refer/home', false) ||
            this.router.isActive('/module/refer/invite', false) ||
            this.router.isActive('/module/refer/redeem', false) ||
            this.router.isActive('/module/refer-questions', false) ||
            this.router.isActive('/module/gift/sent', false) ||
            this.router.isActive('/module/add-account', false) ||
            this.router.isActive('/module/club/home', false) ||
            this.router.isActive('/module/club/terms', false) ||
            this.router.isActive('/module/club/faq', false) ||
            this.cacheStorage().isBusiness();
    }

    menuHidden(): boolean {
        return this.router.isActive('/module/change-plan/confirm', false) ||
            this.router.isActive('/module/data-plan/success', false) ||
            this.router.isActive('/module/change-plan/success', false) ||
            this.router.isActive('/module/change-plan/recharge', false) ||
            this.router.isActive('/module/gift/sent', false) ||
            this.router.isActive('/module/payment/step1', false) ||
            this.router.isActive('/module/payment/step2', false);
    }

    backButtonHidden(): boolean {
        return this.router.isActive('/home/dashboard', false) ||
            (this.router.isActive('/module/consumption', false) && this.cacheStorage().isBusiness());

    }
}

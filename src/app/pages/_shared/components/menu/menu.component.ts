import { Component, OnInit } from '@angular/core';
import { ServicesProvider } from 'src/app/services/services.provider';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import {AlertController} from '@ionic/angular';
import {UtilsService} from 'src/app/services/utils.service';
import {IntentProvider} from '../../../../services/intent.provider';
import {Section} from '../../../../models/account.access';
import {AccessFilter} from '../../../../models/access.filter';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
})
export class MenuComponent extends BasePage implements OnInit {

    access: Section[] = [];
    selectedSection: number;
    close = true;
    isGuest = false;

    constructor(public router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.access = this.cacheStorage().access;
        this.utils.openMenu.subscribe(open => {
            this.close = !open;
            if (!open) {
                this.selectedSection = undefined;
            }
        });
        this.isGuest = this.cacheStorage().isGuest;
    }

    going(page) {
        if (this.isGuest && !page.allowAsGuest) {
            this.utils.openMenu.emit(false);
            this.showAlertAccessLimited();
        } else {
            if (page.accessID === 20) {
                this.utils.openMenu.emit(false);
                this.openStore();
            } else if (page.accessID === 8) {
                this.utils.confirmCloseSession();
            } else if (page.accessID === 40) {
                this.cacheStorage().processIptvFrom = 'menu';
                this.utils.registerScreen('iptv', {from: 'menu'});
                this.filterAndGo(page);
            } else {
                if (!this.isGuest && !this.services.isAccountDetailsUpdated() && this.pageIsNeedingSubscribers(page)) {
                    const account = this.cacheStorage().accountInfo.bANField;
                    const subscriber = this.cacheStorage().accountInfo.defaultSubscriberField;
                    this.showProgress();
                    this.services.loadAccount(String(account), subscriber, this.cacheStorage().tokenSession, this.cacheStorage().isGuest)
                        .then(() => {
                            this.dismissProgress();
                            this.services.setAccountDetailsUpdate();
                            this.filterAndGo(page);
                        } );
                } else {
                    this.filterAndGo(page);
                }
            }
        }
    }

    pageIsNeedingSubscribers(page): boolean {
        for (const route of AccessFilter.routesToGo) {
            if (page.accessID === route.accessID && route.path !== '') {
                if (route.needSubscribers) {
                    return true;
                }
            }
        }
        return false;
    }

    filterAndGo(page) {
        AccessFilter.routesToGo.forEach(route => {
            if (page.accessID === route.accessID && route.path !== '') {
                this.goPage('module/' + route.path);
                return;
            }
        });
    }

    showingMenu(index: number) {
        if (this.selectedSection !== index) {
            this.selectedSection = index;
        } else {
            this.selectedSection = undefined;
        }
    }

    isActive(page): boolean {
        return AccessFilter.isRouteActive(page.accessID, this.router.url);
    }
}

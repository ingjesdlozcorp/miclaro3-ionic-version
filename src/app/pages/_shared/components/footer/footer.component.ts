import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {ServicesProvider} from '../../../../services/services.provider';
import {AlertController} from '@ionic/angular';
import {UtilsService} from '../../../../services/utils.service';
import {IntentProvider} from '../../../../services/intent.provider';
import {BasePage} from '../../../base.page';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss'],
})
export class FooterComponent extends BasePage implements OnInit {

    constructor(public router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() { }

    shouldShow(): boolean {
        return !this.router.isActive('/update-app', false);
    }
}

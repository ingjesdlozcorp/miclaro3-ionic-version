import { Component } from '@angular/core';

@Component({
    selector: 'app-header-static',
    templateUrl: './header-static.component.html',
    styleUrls: ['./header-static.component.scss'],
})
export class HeaderStaticComponent {

    constructor() {
    }
}

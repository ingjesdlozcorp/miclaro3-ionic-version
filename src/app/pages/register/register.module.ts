import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../_shared/shared.module';
import {Step1Component} from './step1/step1.component';
import {Step2Component} from './step2/step2.component';
import {Step3Component} from './step3/step3.component';
import {RegisterComponent} from './register.component';
import {FormsModule} from '@angular/forms';
import {NgxPopperModule} from 'ngx-popper';
import {RouterModule, Routes} from '@angular/router';
import {GuestComponent} from './guest/guest.component';

export const routes: Routes = [
    { path: 'guest', component: GuestComponent},
    { path: 'step1', component: Step1Component},
    { path: 'step2', component: Step2Component},
    { path: 'step3', component: Step3Component}
];


@NgModule({
    declarations: [
        RegisterComponent,
        GuestComponent,
        Step1Component,
        Step2Component,
        Step3Component
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        SharedModule,
        FormsModule,
        NgxPopperModule.forRoot({placement: 'top', styles: {'background-color': 'white'}})
    ]
})
export class RegisterModule { }

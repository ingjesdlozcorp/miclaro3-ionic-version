import { Component, OnInit } from '@angular/core';
import {BasePage} from '../../base.page';
import {Router} from '@angular/router';
import {ServicesProvider} from '../../../services/services.provider';
import { Storage } from '@ionic/storage';
import {keys} from '../../../utils/const/keys';
import {AlertController, NavController} from '@ionic/angular';
import {UtilsService} from '../../../services/utils.service';
import {Utils} from '../../../utils/utils';
import {IntentProvider} from '../../../services/intent.provider';

@Component({
    selector: 'register-step2',
    templateUrl: './step2.component.html',
    styleUrls: ['./step2.component.scss'],
})
export class Step2Component extends BasePage implements OnInit {

    code = '';
    ssn = '';
    email = '';
    subscriber = '';
    isPostpaid: false;
    isPrepaid: false;
    check: false;

    showTerms = false;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.fetch(keys.REGISTER.SUBSCRIBER).then(
            success => {
                this.subscriber = success;
            }
        );
        this.fetch(keys.REGISTER.IS_PREPAID).then(
            success => {
                this.isPrepaid = success;
            }
        );
        this.fetch(keys.REGISTER.IS_POSTPAID).then(
            success => {
                this.isPostpaid = success;
            }
        );
    }

    nextStep() {
        if (this.code.length !== 6) {
            this.showError('Debe ingresar los datos solicitados.');
        } else if (!this.isPrepaid && this.ssn.length !== 4) {
            this.showError('Debe ingresar los datos solicitados.');
        } else if (this.email.length === 0) {
            this.showError('Debe ingresar los datos solicitados.');
        } else if (!Utils.validateEmail(this.email)) {
            this.showError('Debe ingresar un correo electrónico válido.');
        } else if (!this.check) {
            this.showError('Debe seleccionar los términos y condiciones para poder continuar.');
        } else {
            if (this.isPrepaid) {
                this.ssn = this.code;
            }
            this.showProgress();
            this.services.validateSSNAndEmail(this.subscriber, this.code, this.ssn, this.email).then(
                (success: any) => {
                    this.dismissProgress();
                    this.processResponse(success);
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }

    processResponse(response) {
        this.store(keys.REGISTER.SUBSCRIBER, this.subscriber);
        this.store(keys.REGISTER.EMAIL, this.email);
        this.store(keys.REGISTER.SSN, this.ssn);
        this.store(keys.REGISTER.CODE, this.code);
        this.goPage('/register/step3');
    }

    resend() {
        this.showProgress();
        this.services.validateSubscriber(this.subscriber).then(
            (success: any) => {
                this.dismissProgress();
                this.showAlert(success.errorDisplay);
            }, error => {
                if (error.includes('href="http://miclaroreferals.claroinfo.com/forgotpassword')) {
                    error = error.replace(
                        'id="forgotpassword" target="_blank" href="http://miclaroreferals.claroinfo.com/forgotpassword"',
                        'href="/recover/step1"');
                }
                if (error.includes('href="https://miclaro.claropr.com/forgotpassword')) {
                    error = error.replace(
                        'id="forgotpassword" target="_blank" href="https://miclaro.claropr.com/forgotpassword"',
                        'href="/recover/step1"');
                }

                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }
}

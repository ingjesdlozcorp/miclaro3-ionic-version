import { Component, OnInit } from '@angular/core';
import {BasePage} from '../../base.page';
import {Router} from '@angular/router';
import {ServicesProvider} from '../../../services/services.provider';
import { Storage } from '@ionic/storage';
import {keys} from '../../../utils/const/keys';
import {AlertController, NavController} from '@ionic/angular';
import {UtilsService} from '../../../services/utils.service';
import {IntentProvider} from '../../../services/intent.provider';

@Component({
    selector: 'register-step3',
    templateUrl: './step3.component.html',
    styleUrls: ['./step3.component.scss'],
})
export class Step3Component extends BasePage implements OnInit {

    password = '';
    passwordRepeat = '';
    subscriber = '';
    email = '';
    code = '';
    ssn = '';

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.fetch(keys.REGISTER.SUBSCRIBER).then(
            success => {
                this.subscriber = success;
            }
        );
        this.fetch(keys.REGISTER.EMAIL).then(
            success => {
                this.email = success;
            }
        );
        this.fetch(keys.REGISTER.CODE).then(
            success => {
                this.code = success;
            }
        );
        this.fetch(keys.REGISTER.SSN).then(
            success => {
                this.ssn = success;
            }
        );
    }

    nextStep() {
        if (this.password.length < 8 || this.password.length > 15
            || this.password !== this.passwordRepeat
            || !this.lowercaseValidator(this.password)
            || !this.uppercaseValidator(this.password)
            || !this.twoNumberValidator(this.password)
            || !this.specialCharacterValidator(this.password))  {
            return;
        } else {
            this.showProgress();
            this.services.validatePassword(this.subscriber, this.code, this.ssn, this.email, this.password).then(
                (success: any) => {
                    this.dismissProgress();
                    this.clearStore();
                    this.showAlert('Has sido registrado con Exito!', () => {
                        this.goLoginPage();
                    });
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }

    lowercaseValidator(c: string) {
        const regex = /[a-z]/g;
        return regex.test(c);
    }

    uppercaseValidator(c: string) {
        const regex = /[A-Z]/g;
        return regex.test(c);
    }

    twoNumberValidator(c: string) {
        const numbers = '0123456789';
        let count = 0;
        for (let i = 0; i < c.length; i++) {
            if (numbers.indexOf(c.charAt(i), 0) !== -1) {
                count++;
            }
        }
        return count >= 2;
    }

    specialCharacterValidator(c: string) {
        return c.match('^[A-z0-9]+$');
    }
}

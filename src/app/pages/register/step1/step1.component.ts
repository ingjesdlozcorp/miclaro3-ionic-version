import { Component, OnInit } from '@angular/core';
import {BasePage} from '../../base.page';
import {Router} from '@angular/router';
import {ServicesProvider} from '../../../services/services.provider';
import { Storage } from '@ionic/storage';
import {keys} from '../../../utils/const/keys';
import {AlertController} from '@ionic/angular';
import {UtilsService} from '../../../services/utils.service';
import {IntentProvider} from '../../../services/intent.provider';

@Component({
    selector: 'register-step1',
    templateUrl: './step1.component.html',
    styleUrls: ['./step1.component.scss'],
})
export class Step1Component extends BasePage implements OnInit {

    number: number;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('register');
    }

    ngOnInit() {}

    nextStep() {
        if (String(this.number).length !== 10) {
            this.showError('Debe ingresar un número de suscriptor válido.');
        } else {
            this.showProgress();
            this.services.validateSubscriber(String(this.number)).then(
                (success: any) => {
                    this.dismissProgress();
                    this.processResponse(success);
                }, error => {
                    this.dismissProgress();
                    if (error.message.includes('cuenta ya existente')) {
                        error.message = 'Hemos detectado que está intentando registrar una cuenta ya existente en nuestro sistema. Por favor presione la opción ' +
                            '<b>Olvido su Contraseña</b> para recuperar su acceso al sistema';
                        this.showConfirmCustom('Aviso', error.message, 'Olvido su Contraseña', 'Cerrar', () => {
                            this.goPage('recover/step1');
                        });
                    } else {
                        this.showError(error.message);
                    }
                }
            );
        }
    }

    processResponse(response) {

        this.store(keys.REGISTER.SUBSCRIBER, String(this.number));

        if ((response.accountType === 'I' && response.accountSubType === 'P') ||
            (response.accountType === 'I3' && response.accountSubType === 'P')) {
            this.store(keys.REGISTER.IS_PREPAID, true);
        } else {
            this.store(keys.REGISTER.IS_PREPAID, false);
        }

        if ((response.accountType === 'I2' && response.accountSubType === '4') ||
            (response.accountType === 'I' && response.accountSubType === 'R') ||
            (response.accountType === 'I' && response.accountSubType === '4') ||
            (response.accountType === 'I' && response.accountSubType === 'E')) {
            this.store(keys.REGISTER.IS_POSTPAID, true);
        } else {
            this.store(keys.REGISTER.IS_POSTPAID, false);
        }

        this.goPage('/register/step2');
    }

}

import { Component, OnInit } from '@angular/core';
import {BasePage} from '../../base.page';
import {Router} from '@angular/router';
import {ServicesProvider} from '../../../services/services.provider';
import { Storage } from '@ionic/storage';
import {keys} from '../../../utils/const/keys';
import {AlertController, NavController} from '@ionic/angular';
import {UtilsService} from '../../../services/utils.service';
import {IntentProvider} from '../../../services/intent.provider';
import { RedirectProvider } from '../../../services/redirect.provider';
import { StorageProvider } from '../../../services/storage.provider';

@Component({
    selector: 'register-guest',
    templateUrl: './guest.component.html',
    styleUrls: ['./guest.component.scss'],
})
export class GuestComponent extends BasePage implements OnInit {

    code: string;

    number: number;
    isUpdate: boolean;
    token: string;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private storageProvider: StorageProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.fetch(keys.REGISTER.SUBSCRIBER).then(
            success => {
                this.number = success;
            }
        );
        this.fetch(keys.REGISTER.IS_GUEST_UPDATE).then(
            success => {
                this.isUpdate = success;
            }
        );
        this.fetch(keys.REGISTER.TOKEN).then(
            success => {
                this.token = success;
            }
        );
    }

    nextStep() {
        if (this.code.length !== 6) {
            this.showError('Debe ingresar los datos solicitados.');
        } else {
            if (this.isUpdate) {
                this.updateGuest();
            } else {
                this.validateGuest();
            }
        }
    }

    async validateGuest() {
        this.showProgress();
        const pushToken = await this.storageProvider.getNotificationPushToken();
        this.services.validateGuest(this.number, this.code, this.token, pushToken).then(
            (success: any) => {
                this.store(keys.LOGIN.SUBSCRIBER, String(this.number));
                this.store(keys.LOGIN.IS_LOGGED, true);
                this.store(keys.LOGIN.IS_GUEST, true);
                this.store(keys.LOGIN.BIOMETRIC, false);
                this.dismissProgress();
                this.showAlert('Su registro como usuario invitado fue realizado exitosamente.', () => {
                    this.goPage('/guest');
                });
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    async updateGuest() {
        this.showProgress();
        const pushToken = await this.storageProvider.getNotificationPushToken();
        this.services.updateGuest(this.number, this.code, pushToken).then(
            (success: any) => {
                this.store(keys.LOGIN.SUBSCRIBER, String(this.number));
                this.store(keys.LOGIN.IS_LOGGED, true);
                this.store(keys.LOGIN.IS_GUEST, true);
                this.store(keys.LOGIN.BIOMETRIC, false);
                this.dismissProgress();
                this.goPage('/guest');
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

    resend() {
        this.showProgress();
        this.services.resendGuestCode(this.number).then(
            (success: any) => {
                this.dismissProgress();
                this.store(keys.REGISTER.SUBSCRIBER, String(this.number));
                this.store(keys.REGISTER.TOKEN, success.token);
                this.store(keys.REGISTER.IS_GUEST_UPDATE, true);
                this.showAlert('Su código de verificación ha sido enviado nuevamente.');
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            }
        );
    }

}

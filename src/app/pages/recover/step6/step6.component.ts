import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../services/services.provider';
import { BasePage } from '../../base.page';
import { keys } from '../../../utils/const/keys';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../services/utils.service';
import { IntentProvider } from '../../../services/intent.provider';

@Component({
    selector: 'recover-step6',
    templateUrl: './step6.component.html',
    styleUrls: ['./step6.component.scss'],
})
export class Step6Component extends BasePage implements OnInit {

    oldPassword = '';
    password = '';
    passwordRepeat = '';
    token = '';

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.fetch(keys.RECOVER_PASSWORD.TEMPORARY_PASSWORD).then(
            success => {
                this.oldPassword = success;
            }
        );
        this.fetch(keys.RECOVER_PASSWORD.TOKEN).then(
            success => {
                this.token = success;
            }
        );
    }

    nextStep() {
        if (this.password.length < 8 || this.password.length > 15
            || this.password !== this.passwordRepeat
            || !this.lowercaseValidator(this.password)
            || !this.uppercaseValidator(this.password)
            || !this.twoNumberValidator(this.password)
            || !this.specialCharacterValidator(this.password)) {
            return;
        } else {
            this.showProgress();
            this.services.passwordUpdate(this.oldPassword, btoa(this.password), this.token).then(
                (success: any) => {
                    this.dismissProgress();
                    this.showAlert(success.response, () => {
                        this.goLoginPage();
                    });
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }

    lowercaseValidator(c: string) {
        const regex = /[a-z]/g;
        return regex.test(c);
    }

    uppercaseValidator(c: string) {
        const regex = /[A-Z]/g;
        return regex.test(c);
    }

    twoNumberValidator(c: string) {
        const numbers = '0123456789';
        let count = 0;
        for (let i = 0; i < c.length; i++) {
            if (numbers.indexOf(c.charAt(i), 0) !== -1) {
                count++;
            }
        }
        return count >= 2;
    }

    specialCharacterValidator(c: string) {
        return c.match('^[A-z0-9]+$');
    }
}

import { Component } from '@angular/core';
import { BasePage } from '../../base.page';
import { Router } from '@angular/router';
import { ServicesProvider } from '../../../services/services.provider';
import { Storage } from '@ionic/storage';
import { keys } from '../../../utils/const/keys';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../services/utils.service';
import { IntentProvider } from '../../../services/intent.provider';
import { environment } from '../../../../environments/environment';
import { FormBuilder } from '@angular/forms';

export interface FormModel {
    captcha?: string;
}

@Component({
    selector: 'recover-step1',
    templateUrl: './step1.component.html',
    styleUrls: ['./step1.component.scss'],
})
export class Step1Component extends BasePage {
    username;
    public formModel: FormModel = {};

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private fb: FormBuilder) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('forgot_password');
    }

    nextStep() {
        if (this.formModel.captcha) {
            this.goNext();
        } else {
            this.showAlert('Debe completar el captcha para continuar');
        }
    }

    goNext() {
        if (!this.username) {
            this.showError('El número de teléfono o correo ingresado no se encuentra registrado en nuestros sistemas, ' +
                'su formato es incorrecto o no pertenece a nuestra red. Por favor intente nuevamente.');
            this.formModel.captcha = '';
        } else {
            this.showProgress();
            this.services.getChallengeQuestions(this.username).then(
                (success: any) => {
                    this.dismissProgress();
                    this.processResponse(success);
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                    this.formModel.captcha = '';
                }
            );
        }
    }

    processResponse(response) {
        this.store(keys.RECOVER_PASSWORD.SUBSCRIBER, response.subscriber);
        this.store(keys.RECOVER_PASSWORD.EMAIL, response.email);

        if (response.accountType === 'I' &&
            response.accountSubType === 'W' &&
            response.productType === 'O') {
            this.store(keys.RECOVER_PASSWORD.IS_TELEPHONY, true);
        } else {
            this.store(keys.RECOVER_PASSWORD.IS_TELEPHONY, false);
        }

        if (response.ResponseList == null) {
            this.goPage('/recover/step4');
        } else {
            const questions = [];
            for (const object of response.ResponseList) {
                questions.push({
                    question: object.question,
                    questionID: object.questionID,
                    response: object.response
                });
            }
            this.store(keys.RECOVER_PASSWORD.QUESTION_LIST, questions);
            this.goPage('/recover/step2');
        }
        this.formModel.captcha = '';
    }

    get siteKey() {
        return environment.siteKey;
    }
}

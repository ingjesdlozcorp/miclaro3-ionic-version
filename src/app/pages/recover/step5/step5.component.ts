import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../../services/services.provider';
import { BasePage } from '../../base.page';
import { keys } from '../../../utils/const/keys';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../../services/utils.service';
import { IntentProvider } from '../../../services/intent.provider';

@Component({
    selector: 'recover-step5',
    templateUrl: './step5.component.html',
    styleUrls: ['./step5.component.scss'],
})
export class Step5Component extends BasePage implements OnInit {

    oldPassword = '';
    password = '';
    passwordRepeat = '';
    token = '';

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.fetch(keys.RECOVER_PASSWORD.TOKEN).then(
            success => {
                this.token = success;
            }
        );
    }

    nextStep() {
        if (this.password.length < 8 || this.password.length > 15
            || this.password !== this.passwordRepeat
            || !this.lowercaseValidator(this.password)
            || !this.uppercaseValidator(this.password)
            || !this.twoNumberValidator(this.password)
            || !this.specialCharacterValidator(this.password)) {
            return;
        } else if (this.oldPassword.length < 8) {
            this.showError('Hemos detectado que la clave temporal ingresada no es válida. Por favor intente nuevamente.');
        } else {
            this.showProgress();
            this.services.passwordUpdate(btoa(this.oldPassword), btoa(this.password), this.token).then(
                (success: any) => {
                    this.dismissProgress();
                    this.showAlert(success.response, () => {
                        this.goLoginPage();
                    });
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }

    lowercaseValidator(c: string) {
        const regex = /[a-z]/g;
        return regex.test(c);
    }

    uppercaseValidator(c: string) {
        const regex = /[A-Z]/g;
        return regex.test(c);
    }

    twoNumberValidator(c: string) {
        const numbers = '0123456789';
        let count = 0;
        for (let i = 0; i < c.length; i++) {
            if (numbers.indexOf(c.charAt(i), 0) !== -1) {
                count++;
            }
        }
        return count >= 2;
    }

    specialCharacterValidator(c: string) {
        return c.match('^[A-z0-9]+$');
    }

}

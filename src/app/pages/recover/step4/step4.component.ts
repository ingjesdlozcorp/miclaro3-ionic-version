import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {ServicesProvider} from '../../../services/services.provider';
import {BasePage} from '../../base.page';
import {keys} from '../../../utils/const/keys';
import {AlertController, NavController} from '@ionic/angular';
import {UtilsService} from '../../../services/utils.service';
import {IntentProvider} from '../../../services/intent.provider';

@Component({
    selector: 'recover-step4',
    templateUrl: './step4.component.html',
    styleUrls: ['./step4.component.scss'],
})
export class Step4Component extends BasePage implements OnInit {

    isTelephony = true;
    subscriberHidden = '';
    subscriber = '';
    emailHidden = '';
    email = '';
    bySubscriber = false;
    byEmail = false;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.fetch(keys.RECOVER_PASSWORD.IS_TELEPHONY).then(
            success => {
                this.isTelephony = success;
            }
        );
        this.fetch(keys.RECOVER_PASSWORD.SUBSCRIBER).then(
            success => {
                this.subscriber = success;
                this.subscriberHidden = success.substr(success.length - 4);
                this.subscriberHidden = '********' + this.subscriberHidden;
            }
        );
        this.fetch(keys.RECOVER_PASSWORD.EMAIL).then(
            success => {
                this.email = success;
                const first2 = success.substring(0, 2);
                const emailCut = success.split('@');
                this.emailHidden = first2 + '******@' + emailCut[1];
            }
        );
    }

    nextStep() {
        if (!this.bySubscriber && !this.byEmail) {
            return;
        } else {
            this.showProgress();
            this.services.recoveryPasswordBySubscriber(
                this.subscriber,
                this.bySubscriber ? this.subscriber : '',
                this.byEmail ? this.email : ''
            ).then(
                (success: any) => {
                    this.showAlert(success.response, () => {
                        this.goLoginPage();
                    });
                    this.dismissProgress();
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }
}

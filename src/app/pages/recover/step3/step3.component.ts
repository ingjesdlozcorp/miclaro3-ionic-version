import { Component, OnInit } from '@angular/core';
import {BasePage} from '../../base.page';
import {Router} from '@angular/router';
import {ServicesProvider} from '../../../services/services.provider';
import { Storage } from '@ionic/storage';
import {AlertController, NavController} from '@ionic/angular';
import {UtilsService} from '../../../services/utils.service';
import {keys} from '../../../utils/const/keys';
import {IntentProvider} from '../../../services/intent.provider';

@Component({
    selector: 'recover-step3',
    templateUrl: './step3.component.html',
    styleUrls: ['./step3.component.scss'],
})
export class Step3Component extends BasePage implements OnInit {

    firstQuestion = '¿Primera Pregunta?';
    secondQuestion = '¿Segunda Pregunta?';

    firstAnswer = '';
    secondAnswer = '';

    questions: any[] = [];
    subscriber: any;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.fetch(keys.RECOVER_PASSWORD.QUESTION_LIST).then(
            questions => {
                this.questions = questions;
                if (this.questions.length > 0) {
                    this.setQuestions();
                }
            }
        );
        this.fetch(keys.RECOVER_PASSWORD.SUBSCRIBER).then(
            success => {
                this.subscriber = success;
            }
        );
    }

    setQuestions() {
        let question1 = this.questions[0].question;
        let question2 = this.questions[1].question;

        // init validate if you have question marks
        if (!question1.includes('¿')) {
            question1 = '¿' + question1;
        }
        if (!question1.includes('?')) {
            question1 = question1 + '?';
        }
        if (!question2.includes('¿')) {
            question2 = '¿' + question2;
        }
        if (!question2.includes('?')) {
            question2 = question2 + '?';
        }
        this.firstQuestion = question1;
        this.secondQuestion = question2;
    }

    nextStep() {
        if (this.firstAnswer.length <= 0 || this.secondAnswer.length <= 0) {
            this.showError('Por favor, complete los campos para continuar.');
        } else {
            this.questions[0].response = this.firstAnswer;
            this.questions[1].response = this.secondAnswer;
            this.showProgress();
            this.services.answerSecurityQuestions(this.subscriber, this.questions).then(
                (success: any) => {
                    this.dismissProgress();
                    this.store(keys.RECOVER_PASSWORD.TEMPORARY_PASSWORD, success.newpassword);
                    this.store(keys.RECOVER_PASSWORD.TOKEN, success.token);
                    this.goPage('/recover/step6');
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                }
            );
        }
    }
}


import { Component, OnInit } from '@angular/core';
import {BasePage} from '../../base.page';
import {Router} from '@angular/router';
import {ServicesProvider} from '../../../services/services.provider';
import { Storage } from '@ionic/storage';
import {keys} from '../../../utils/const/keys';
import {AlertController, NavController} from '@ionic/angular';
import {UtilsService} from '../../../services/utils.service';
import {IntentProvider} from '../../../services/intent.provider';

@Component({
    selector: 'recover-step2',
    templateUrl: './step2.component.html',
    styleUrls: ['./step2.component.scss'],
})
export class Step2Component extends BasePage implements OnInit {

    username = '';

    isTelephony = false;

    selected = 1;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {}

    nextStep() {
        console.log(this.selected);
        if (this.selected === 1) {
            this.goPage('/recover/step4');
        }
        if (this.selected === 2) {
            this.goPage('/recover/step3');
        }
    }


}

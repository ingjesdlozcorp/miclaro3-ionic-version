import { Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { Storage } from '@ionic/storage';
import {AlertController} from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import {IntentProvider} from '../../../services/intent.provider';

@Component({
    selector: 'app-help-success-report',
    templateUrl: './success-report.component.html',
    styleUrls: ['./success-report.component.scss'],
})
export class SuccessReportComponent extends BasePage implements OnInit {

    public title: string;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        console.log('reporte exitoso');
        if (this.services.temporaryStorage.success_report_title !== null) {
            this.title = this.services.temporaryStorage.success_report_title;
        }
    }

    goHelp() {
        this.goPage('help/home');
    }

}

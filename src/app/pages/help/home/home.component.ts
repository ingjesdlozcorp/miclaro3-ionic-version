import { Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { ServicesProvider } from 'src/app/services/services.provider';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { IntentProvider } from '../../../services/intent.provider';
import { BrowserProvider } from '../../../services/browser.provider';
import { RedirectProvider } from '../../../services/redirect.provider';

@Component({
    selector: 'app-help-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent extends BasePage implements OnInit {

    public open = false;

    tokenSession = '';
    tokenSSO = '';
    isLogged = false;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider, private redirectProvider: RedirectProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.services.temporaryStorage.storeList = [];
        if (this.cacheStorage().logged) {
            this.tokenSession = this.cacheStorage().loginData.token;
            this.tokenSSO = this.cacheStorage().loginData.SSOAccessToken;
            this.isLogged = true;
        }
    }

    openTerms() {
        this.open = true;
    }

    closeTerms() {
        this.open = false;
    }

    openChat() {
        this.redirectProvider.openChat();
    }
}

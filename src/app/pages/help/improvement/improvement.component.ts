import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { BasePage } from 'src/app/pages/base.page';
import { Storage } from '@ionic/storage';
import { IntentProvider } from '../../../services/intent.provider';
import { Utils } from 'src/app/utils/utils';


@Component({
    selector: 'app-help-improvement',
    templateUrl: './improvement.component.html',
    styleUrls: ['./improvement.component.scss'],
})
export class ImprovementComponent extends BasePage implements OnInit {

    public logged = false;
    public accountSelected;
    public email: string;
    public comment;
    public enabled = true;
    public accountInfo;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        if (this.services.data !== undefined) {
            console.log(this.services.data);
            this.logged = true;
            this.accountSelected = this.cacheStorage().accountInfo.bANField;
            this.email = this.cacheStorage().accountInfo.emailField;
        } else {
            this.logged = false;
            this.accountSelected = null;
            this.email = '';
        }
    }

    send() {
        if (this.comment === '') {
            this.showError('Campo de "Detalle tu comentario" es requerido');
            return;
        } else if (this.email === '') {
            this.showError('Campo "Correo electrónico" es requerido');
            return;
        }
        this.sendImprovement();
    }

    sendImprovement() {
        if (Utils.validateEmail(this.email)) {
            this.showProgress();
            this.enabled = false;
            this.services.sendHelpMessage(this.email, this.comment).then(
                (success: any) => {
                    this.dismissProgress();
                    this.services.temporaryStorage.success_report_title = 'Sugerencias';
                    this.goPage('help/success-report');
                    this.enabled = true;
                }, error => {
                    this.dismissProgress();
                    this.showError('No se puede enviar su mensaje en este momento, intente más tarde');
                }
            );
        } else {
            this.showError('Por favor introduzca un correo electrónico válido');
        }
    }
}

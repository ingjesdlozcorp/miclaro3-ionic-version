import { Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import {AlertController, NavController} from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { Storage } from '@ionic/storage';
import { IntentProvider } from 'src/app/services/intent.provider';
import {keys} from '../../../utils/const/keys';
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    CameraPosition,
    MarkerOptions,
    Marker,
    GoogleMapOptions
} from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { LoadingController } from '@ionic/angular';
declare var google;



@Component({
    selector: 'app-help-locations-details',
    templateUrl: './locations-details.component.html',
    styleUrls: ['./locations-details.component.scss'],
})
export class LocationsDetailsComponent extends BasePage implements OnInit {

    public store;
    public mapElement: any;
    public map: GoogleMap;
    public mapRef = null;


    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider,
                private geolocation: Geolocation, private launchNavigator: LaunchNavigator, private loadingCtrl: LoadingController) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        this.checkData();
    }

    checkData() {
        this.store = this.services.temporaryStorage.store;
        this.services.temporaryStorage.storeList = [];
        console.log(this.store);
        const acum = this.store.distance;
        this.store.distance = parseFloat(acum).toFixed(2);
        this.showMap();
        // this.loadMap();
    }

    showMap() {

        this.mapElement = document.getElementById('store-map');
        const mapOption: GoogleMapOptions = {
            camera: {
                target: {
                    lat: this.store.latitude,
                    lng: this.store.longitude
                },
                zoom: 18,
                tilt: 30
            }
        };
        this.map = GoogleMaps.create(this.mapElement, mapOption);

        this.map.one(GoogleMapsEvent.MAP_READY).then(
            () => {
                this.map.addMarker({
                       title : this.store.name,
                       icon: 'red',
                       animation: 'DROP',
                       position: {
                        lat: this.store.latitude,
                        lng: this.store.longitude
                       }
                    }
                );
            }
        );
    }

    showStoreRoute() {
        const launchnavigator =  this.launchNavigator;
        launchnavigator.navigate([this.store.latitude, this.store.longitude, ], {
           start: this.store.latitude + ',' + this.store.longitude
        });
    }

}

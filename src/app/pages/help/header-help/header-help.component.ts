import { Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import {AlertController, NavController} from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { Storage } from '@ionic/storage';
import {IntentProvider} from '../../../services/intent.provider';
import { Location } from '@angular/common';

@Component({
    selector: 'app-header-help',
    templateUrl: './header-help.component.html',
    styleUrls: ['./header-help.component.scss'],
})
export class HeaderHelpComponent extends BasePage implements OnInit {

    constructor(public router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {}

    goBack() {
        super.goBack();
    }

    goHome() {
        if (this.cacheStorage().logged) {
            super.goHomePage();
        } else {
            super.goLoginPage();
        }
    }
}

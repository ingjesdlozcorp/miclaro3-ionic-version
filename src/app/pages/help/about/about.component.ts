import { Component, OnInit } from '@angular/core';
import { Device } from '@ionic-native/device/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
    selector: 'app-help-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {

    public version: string;

    constructor(private device: Device, private appVersion: AppVersion) {
    }

    ngOnInit() {
        this.getVersion();
    }
    getVersion() {
        this.appVersion.getVersionNumber().then(
            (result) => {
                console.log(result);
                this.version =  result;
            }
        ).catch(
            error => {
                console.log(error);
            }
        );
    }

}

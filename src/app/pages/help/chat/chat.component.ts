import {Component, OnDestroy, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { BasePage } from 'src/app/pages/base.page';
import { Storage } from '@ionic/storage';
import {IntentProvider} from '../../../services/intent.provider';
import {Utils} from '../../../utils/utils';

@Component({
    selector: 'app-help-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss'],
})
export class ChatComponent extends BasePage implements OnInit {

    name = '';
    email = '';
    question = '';
    enabled = true;
    isLogged = false;

    selectedChatCategory;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    ngOnInit() {
        if (this.cacheStorage().logged) {
            const data = this.cacheStorage().accountInfo;
            this.name = data.firstNameField + ' ' + data.lastNameField;
            this.email = data.emailField;
            this.isLogged = true;
        }
    }

    send() {
        if (this.name === '') {
            alert('Debe ingresar su nombre y apellido.');
        } else if (!Utils.validateEmail(this.email)) {
            alert('Debe ingresar un correo electrónico válido.');
        } else if (this.selectedChatCategory === undefined) {
            alert('Debe seleccionar un departamento.');
        } else {
            this.openChat();
        }
    }

    openChat() {
        const question = this.question;
        let name = this.name;
        const email = this.email;
        const dept = this.selectedChatCategory;

        if (this.isLogged) {
            const data = this.cacheStorage().accountInfo;
            name = `${data.firstNameField} ${data.lastNameField}`;
        }

      /*  let url = this.utils.chat;
        url += '?Email=' + email;
        url += '&Department=' + dept;
        url += '&firstname=' + name;
        url += '&lastname=' + lastname;
        url += '&Question=' + question;
        url = url.replace(/\s+/g, '%20');*/
 /*       this.utils.registerScreen('chat');
        this.showProgress();
        this.services.chat(
            this.isLogged ? this.cacheStorage().tokenSession : undefined,
            name,
            email,
            dept)
            .then(response => {
                console.log(response);
                this.openIABrowser(response.url).then(() => {
                    this.dismissProgress();
                });
            }, error => {
                this.dismissProgress();
                this.showAlert(error.message);
            });*/
    }

}


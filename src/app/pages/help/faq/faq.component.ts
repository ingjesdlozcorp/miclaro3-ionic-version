import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-help-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.scss'],
})
export class FaqComponent implements OnInit {

    public open = true;
    public touched = false;
    public contentShow = [
        false,  false,  false, false,
        false, false, false, false,
        false, false, false, false,
        false,
    ];
    public theOpenGroup = [];

    constructor() { }

    ngOnInit() {}

    searchDelete( param: number ) {
        for ( let i = 0; i < this.theOpenGroup.length; i++ ) {
            if ( param === this.theOpenGroup[i]) {
                this.theOpenGroup.splice(i, 1);
            }
        }
    }
    showingMenu(param: number) {
        let close = false;

        if (this.theOpenGroup.length > 0) {
            this.theOpenGroup.forEach(group => {
                if (group === param) {
                    close = true;
                }
            });
        }
        // to show content
        if (!close) {
            for (let i = 0; i < this.contentShow.length; i++ ) {
                if (param === i) {
                    this.contentShow[i] = true;
                    this.theOpenGroup.push(param);
                    console.log(this.contentShow[i]);
                }
            }

        } else {
            // para cerrar el contenido;
            console.log('quieres ocultar contenido');
            this.searchDelete(param);
            for ( let i = 0; i < this.contentShow.length; i++ ) {
                if ( param === i) {
                    this.contentShow[i] = false;
                    console.log(this.contentShow[i]);
                }
            }
        }
        console.log(this.theOpenGroup);
    }

}

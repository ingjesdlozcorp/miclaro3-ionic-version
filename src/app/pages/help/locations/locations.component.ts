import { Component, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base.page';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import {AlertController} from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { Storage } from '@ionic/storage';
import { IntentProvider } from 'src/app/services/intent.provider';
import {keys} from '../../../utils/const/keys';
import {environment} from '../../../../environments/environment';


@Component({
    selector: 'app-help-locations',
    templateUrl: './locations.component.html',
    styleUrls: ['./locations.component.scss'],
})
export class LocationsComponent extends BasePage implements OnInit {

    public coords;

    public storeType = [
        {name: 'CACs', value: '1', type: 'postpago', selected: true},
        {name: 'Kioscos', value: '2', type: 'prepago', selected: false },
        {name: 'Tiendas', value: '3', type: 'telefonia', selected: false}
    ];

    public storeList: any;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    async ngOnInit() {
            this.showProgress();
            this.coords = await this.services.myPosition();
            this.services.temporaryStorage.coords = this.coords;
            this.changeStoreList(1);
    }

    tabSelected(param) {
        this.showProgress();
        this.storeType[param - 1].selected = true;
        for (let i = 0; i < this.storeType.length ; i ++) {
            if (i !== (param - 1)) {
                this.storeType[i].selected = false;
            }
        }
        this.changeStoreList(param);
    }

    changeStoreList(param: number) {

        const storeType = this.storeType[param - 1].value;
        const userLocation = {
            latitude: this.services.location.latitude,
            longitude: this.services.location.longitude
        };
        const distanceMeasure =   environment.DISTANCE_MEASURE;
        const url = environment.API_URL + '/store/list';

        this.services.getStores(storeType, userLocation, distanceMeasure, url)
            .then(
                (success: any) => {
                    this.dismissProgress();
                    this.storeList = success.object.stores;
                    for (const storeItem of this.storeList) {
                        const acum = storeItem.distance;
                        storeItem.distance = parseFloat(acum).toFixed(2);
                    }
                    this.services.temporaryStorage.storeList = this.storeList;
                    this.services.temporaryStorage.storeType = param;
                }
            ).catch(error => {
            this.dismissProgress();
            console.log('Error getting location', error);
            alert( 'Disculpe, no fue posible establecer la comunicación');
            this.goBack();
        });

    }

    storeDetail(store) {
        this.services.temporaryStorage.store = store;
        this.goPage('help/locations-detail');
    }
}

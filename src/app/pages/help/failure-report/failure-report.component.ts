import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesProvider } from 'src/app/services/services.provider';
import { AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { BasePage } from 'src/app/pages/base.page';
import { Storage } from '@ionic/storage';
import { IntentProvider } from '../../../services/intent.provider';
import { Utils } from 'src/app/utils/utils';


@Component({
    selector: 'app-help-failure-report',
    templateUrl: './failure-report.component.html',
    styleUrls: ['./failure-report.component.scss'],
})
export class FailureReportComponent extends BasePage implements OnInit {

    public email: string;
    public failureComment;
    public enabled = true;
    public failureCategory = [
        'La aplicación no permite ingresar',
        'La aplicación no actualiza correctamente',
        'La aplicación no funciona en el teléfono',
        'La aplicación se traba',
        'La aplicación cierra inesperadamente'
    ];
    public selectedFailureCategory = '';
    public loged;
    public accountSelected;
    public accountInfo;

    constructor(router: Router, storage: Storage, modelsServices: ServicesProvider,
                alertController: AlertController, utilsService: UtilsService,
                userStorage: IntentProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }

    radioChangeHandler(event: any) {
        this.selectedFailureCategory = event.target.value;
    }

    ngOnInit() {
        if (this.services.data !== undefined) {
            this.loged = true;
            this.accountSelected = this.cacheStorage().accountInfo.bANField;
            this.email = this.cacheStorage().accountInfo.emailField;
        } else {
            this.loged = false;
            this.accountSelected = null;
            this.email = '';
        }
    }

    send() {
        for (let i = 0; i < this.failureCategory.length; i++) {
            if (this.selectedFailureCategory === this.failureCategory[i]) {
                this.selectedFailureCategory = 'C0' + (i + 1);
            }
        }
        if (this.failureComment === '') {
            this.showError('Campo de "Detalle tu comentario" es requerido');
            return;
        } else if (this.email === '') {
            this.showError('Campo "Correo electrónico" es requerido');
            return;
        } else if (this.selectedFailureCategory === '') {
            this.showError('Debe seleccionar una categoría para su reporte');
            return;
        }
        this.sendFailure();
    }

    sendFailure() {
        let failureCategory, failureComment, message;
        failureCategory = this.selectedFailureCategory;
        failureComment = this.failureComment;
        message = failureCategory + '*' + failureComment;
        if (Utils.validateEmail(this.email)) {
            this.showProgress();
            this.enabled = false;
            this.services.sendHelpMessage(this.email, message).then(
                (success: any) => {
                    this.dismissProgress();
                    this.services.temporaryStorage.success_report_title = 'Reportar Falla';
                    this.goPage('help/success-report');
                    this.enabled = true;
                }, error => {
                    this.dismissProgress();
                    alert('No se puede enviar su mensaje en este momento, intente más tarde');
                });
        } else {
            alert('Por favor introduzca un correo electrónico válido');
        }
    }

}

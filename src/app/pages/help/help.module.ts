import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HelpComponent } from './help.component';
import { AboutComponent } from './about/about.component';
import { ChatComponent } from './chat/chat.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FailureReportComponent } from './failure-report/failure-report.component';
import { FaqComponent } from './faq/faq.component';
import { HeaderHelpComponent } from './header-help/header-help.component';
import { ImprovementComponent } from './improvement/improvement.component';
import { LocationsComponent } from './locations/locations.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SuccessReportComponent } from './success-report/success-report.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {SharedModule} from '../_shared/shared.module';
import {HomeComponent} from './home/home.component';
import { LocationsDetailsComponent } from './locations-details/locations-details.component';

export const routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'chat', component: ChatComponent },
    { path: 'contact-us', component: ContactUsComponent },
    { path: 'faq', component: FaqComponent },
    { path: 'failure-report', component: FailureReportComponent },
    { path: 'improvement', component: ImprovementComponent },
    { path: 'locations', component: LocationsComponent },
    { path: 'locations-detail', component: LocationsDetailsComponent },
    { path: 'success-report', component: SuccessReportComponent },
    { path: '', redirectTo: '/help/home', pathMatch: 'full' },
];
@NgModule({
    declarations: [
        HelpComponent,
        HomeComponent,
        AboutComponent,
        ChatComponent,
        ContactUsComponent,
        FailureReportComponent,
        FaqComponent,
        ImprovementComponent,
        HeaderHelpComponent,
        LocationsComponent,
        LocationsDetailsComponent,
        SuccessReportComponent
    ],
    exports: [RouterModule],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        HttpClientModule,
        SharedModule,
        ReactiveFormsModule
    ]
})
export class HelpModule { }

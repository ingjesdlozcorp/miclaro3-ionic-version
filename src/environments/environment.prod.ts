export const environment = {
    production: true,
    PROCESS_URL: 'https://pciapp3.claropr.com/proccess/procesos-mobile.aspx', // PROD 2021
    HELP_URL: 'http://soporteapps.speedymovil.com:8090/appFeedback/service/feedback/application',
    CHAT_URL: 'https://chat3.claropr.com/webapiserver/ECSApp/ChatWidget3/ChatPanel.aspx',
    API_URL: 'https://wsclarorprodnew.claropr.com/api-miclaro-services-prod-new/miclaro/', // OLD SERVICES,
    STORE_URL: 'https://tienda.claropr.com/',
    WEB_URL: 'https://miclaro.claropr.com/',
    DISTANCE_MEASURE: 'mi',
    GATEWAY_APP_ID: 'At01bMi0aXhr6ktmTaow',
    GROUP_ID: 'group.com.claro.pr.MiClaro', // IOS PROD
    // GROUP_ID: 'group.com.todoclaro.miclaroapp.test', // IOS DEV
    PACKET_NAME: 'com.todoclaro.miclaroapp',
    BIOMETRIC_SECRET_KEY: 'todoclaro-biometric',
    WEB_PAGE_BUSINESS: 'https://empresas.claropr.com', // PROD
    // WEB_PAGE_BUSINESS: 'http://empresas.claroinfo.com/', // DEV
    SHOW_ONLY_BANNERS_FOR_PRODUCTION: true,

    ID: '775322054',
    PAYMENT_ID_ANDROID: 'MICLAROANDROID',
    PAYMENT_TOKEN_ANDROID: '8JNJr579UI363751',
    PAYMENT_ID_IOS: 'MICLAROIOS',
    PAYMENT_TOKEN_IOS: 'd6pT5bnztZGrsv49',
    // TIME OUT for https requests
    TIMEOUT: 60 * 1000,
    siteKey: '6Ldo2yQaAAAAAGLxadch63xDKfiMwz0w4off74ry'
};
